texture tex0 < string name = "Beach.tga"; >;
texture tex1 < string name = "Beach.tga"; >;
texture tex2 < string name = "Beach.tga"; >;
texture tex3 < string name = "Beach.tga"; >;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler AlphaTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler SandTexture  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


sampler TerraIncognitaFiltered  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};



struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float2 vTexCoord0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4  vPosition  : POSITION;
    float2  vTexCoord0 : TEXCOORD0;
    float2  vTexCoord1 : TEXCOORD1;
    float2  vTexCoord2 : TEXCOORD2;
    float2  vTexCoord3 : TEXCOORD3;
};


VS_OUTPUT VertexShader_Beach(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	return Out;
}

float4 PixelShader_Beach( VS_OUTPUT v ) : COLOR
{	
	float4 OutColor = float4(0,0,0,0);	
	return OutColor;
}

technique BeachShader
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_Beach();
		PixelShader = compile ps_2_0 PixelShader_Beach();
	}
}
