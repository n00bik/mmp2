//*******************************************************
//THIS DEFFINITION CONTROLS THE BRIGHTNES OF THE LAND.   
//THE HIGHER THE VALUE THE BRIGHTER THE LAND IS.        
//*******************************************************
                                                      
#define LIGHTFACTOR 0.95                             

//*******************************************************

#define BRIGHTNESS 0.1 //-0.03
#define CONTRAST 3.0
#define DESATURATION 0.4
#define BLACK float4(0.0,0.0,0.0,1.0)
#define TAN float3(1.0,1.0,1.0)
#define LIGHTSHIFT 0.0
#define FOWFACTOR 1.5
#define BARE   float3( 0.88, 0.83, 0.75 )
#define TI_CLR float3(1.0,0.9625,0.925) 
#define BS_AV  float3(0.87,0.85,0.82)

#define X_OFFSET 0.5
#define Z_OFFSET 0.5
#define TEX_SZ   60
#define ZM_BS    3.0

texture tex0 < string ResourceName = "Base.tga"; >;		// Base texture
texture tex1 < string ResourceName = "Terrain.tga"; >;	// Terrain texture
texture tex2 < string ResourceName = "Color.dds"; >;		// Color texture
texture tex3 < string ResourceName = "Alpha.dds"; >;		// Terrain Alpha Mask
texture tex4 < string ResourceName = "BorderDirection.dds"; >;	// Borders texture
texture tex5 < string ResourceName = "ProvinceBorders.dds"; >;
texture tex6 < string ResourceName = "CountryBorders.dds"; >;
texture tex7 < string ResourceName = "TerraIncog.dds"; >;


float4x4 WorldMatrix		: World; 
float4x4 ViewMatrix		: View; 
float4x4 ProjectionMatrix	: Projection; 
float4x4 AbsoluteWorldMatrix;
float3	 LightDirection;
float	 vAlpha;

float	ColorMapHeight;
float	ColorMapWidth;

float	ColorMapTextureHeight;
float	ColorMapTextureWidth;

float	MapWidth;
float	MapHeight;

float	BorderWidth;
float	BorderHeight;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point;
    MipFilter = None; //Linear; //None; /n
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler TreeTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler MapTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point;
    MipFilter = Linear; //None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler StripesTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear; //Point;
    MagFilter = Point; //Point;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler ColorTexture  =
sampler_state
{
    Texture = <tex4>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

// used for political, religious etc etc..
sampler GeneralTexture  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler GeneralTexture2  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler TerrainAlphaTexture  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler BorderDirectionTexture  =
sampler_state
{
    Texture = <tex4>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler WinterTexture  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


sampler ProvinceBorderTexture  =
sampler_state
{
    Texture = <tex5>;
    MinFilter = Point; //Linear;
    MagFilter = Point; //Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler CountryBorderTexture  =
sampler_state
{
    Texture = <tex6>;
    MinFilter = Point; //Linear;
    MagFilter = Point; //Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler BorderDiagTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Point; //Linear;
    MagFilter = Point; //Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


sampler TerraIncognitaTextureTerrain =
sampler_state
{
    Texture = <tex7>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler TerraIncognitaTextureTree =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float3 vNormal    : NORMAL;
    float2 vTexCoord  : TEXCOORD0;
    float2 vProvinceIndexCoord  : TEXCOORD1;
};

struct VS_INPUT_BEACH
{
    float4 vPosition  : POSITION;
    float3 vNormal    : NORMAL;

    float2 vProvinceIndexCoord  : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4  vPosition           : POSITION;
    float4  vTexCoord0          : TEXCOORD0;	// third beein' lightIntensity, 4th height
    float2  localTexCoord       : TEXCOORD1;
    float2  vColorTexCoord      : TEXCOORD2;
    float2  worldTexCoord       : TEXCOORD3;
    float2  vBorderTexCoord1    : TEXCOORD4;
    float2  vTerrainTexCoord    : TEXCOORD5;
    float2  vProvinceIndexCoord : TEXCOORD6;
    float4  General		: TEXCOORD7; //n
};

struct VS_OUTPUT_BEACH
{
    float4  vPosition           : POSITION;
    float2  vTerrainTexCoord    : TEXCOORD0;
    float2  vColorTexCoord      : TEXCOORD1;
    // Later put this into ONE texcoord, this is easier for debugging etc..
    float3  vLightIntensity     : TEXCOORD2;
    float2 vProvinceIndexCoord  : TEXCOORD3;
    float2 worldTexCoord	: TEXCOORD4;
    float2  General		: TEXCOORD5; //n
};

struct VS_INPUT_PTI
{
    float4 vPosition  : POSITION;
};

struct VS_OUTPUT_PTI
{
    float2  worldTexCoord     : TEXCOORD3;
    float4  vPosition         : POSITION;
    float2  General	      : TEXCOORD1; //n
};


struct VS_INPUT_TREE
{
    float4 vPosition : POSITION;
    float2 vTexCoord : TEXCOORD0;
};

struct VS_OUTPUT_TREE
{
    float4 vPosition   : POSITION;
    float2 vTexCoord   : TEXCOORD0;
    float2 vTexCoordTI : TEXCOORD1;
};

const float2 off = float2(0.5, 0.5);

const float2 offx = float2(1.0, 0.0); 
const float2 offy = float2(-0.0, 1.0);

/////////////////////////////////////////////////////////////////////////////////



VS_OUTPUT VertexShader_Map_General(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );
	float3 direction = normalize( LightDirection );
	Out.vTexCoord0.xy = v.vTexCoord;
	Out.vTexCoord0.z = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
	Out.vTexCoord0.w = v.vPosition.y;


	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );
	

	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.3); //zoom
	Out.localTexCoord  = Out.worldTexCoord*float2(TEX_SZ*BorderWidth/BorderHeight,TEX_SZ);

	
	return Out;
}



float4 PixelShader_Map2_0_General( VS_OUTPUT v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);  // World terrain
	float4 BaseColor = tex2D( BaseTexture, v.localTexCoord); // local texture
	float4 Color1 = tex2D( GeneralTexture, v.vProvinceIndexCoord );
	float4 Color2 = tex2D( GeneralTexture2, v.vProvinceIndexCoord );
	float4 stripes = tex2D( StripesTexture, v.vTerrainTexCoord );

        float jngls = 0.0;
        float frst1 = 0.0;
        float dsrt = 0.0;

        if ( stripes.r > 0.42 ){
            jngls = stripes.r - 0.5;
            stripes.r =  1.0f;
        }else{
            jngls = stripes.r;
            stripes.r = 0.0f;
        }
        jngls = jngls*2.4;

        if ( stripes.g > 0.42 ){
            frst1 = stripes.g - 0.5;
            stripes.g =  1.0f;
        }else{
            frst1 = stripes.g;
            stripes.g = 0.0f;
        }
        frst1 = frst1*2.9;


        if ( stripes.b > 0.42 ){
            dsrt = stripes.b - 0.5;
            stripes.b =  1.0f;
        }else{
            dsrt = stripes.b;
            stripes.b = 0.0f;
        }
        dsrt = dsrt*2.9;


	float4 Color = lerp(Color2, Color1, stripes.g); //data color
	float3 tmp = Color.rgb-0.5;
	if (dot(tmp,tmp)<0.001) Color.rgb = BARE;	//fix diplomacy map colour

        BaseColor.rgb = lerp(BS_AV, BaseColor.rgb, saturate(ZM_BS*v.General.x*0.75f));	
        float3 LksTex = BaseColor.rgb;       
	float4 OutColor = float4( BaseColor.rgb * v.vTexCoord0.z,1.0);  //paper

     // ********* Terrain

      float4 Clr_am = OutColor;

      if (WorldColor.g < 0.92){ 
            Clr_am.rgb = OutColor.rgb*float3( 0.925, 0.925, 0.925);
            Clr_am.rgb = lerp(Clr_am.rgb, float3(0.0,0.0,0.0),
                              v.General.x*stripes.r);    //Hills
      }
      if (WorldColor.g < 0.63){ 
            Clr_am.rgb = OutColor.rgb; 
            Clr_am.rgb = lerp(Clr_am.rgb, float3(0.0,0.0,0.0),
                              v.General.x*stripes.b);   //Mountains
      }
      if (WorldColor.r < 0.85){
            Clr_am.rgb = OutColor.rgb*float3( 0.975, 0.975, 0.975);
            Clr_am.rgb = lerp(Clr_am.rgb, float3(0.0,0.0,0.0), 
                              v.General.x*frst1);    //Forests1
      }

      if (WorldColor.r < 0.60){
            Clr_am.rgb = OutColor.rgb*float3( 0.975, 0.975, 0.975);
            Clr_am.rgb = lerp(Clr_am.rgb, float3(0.0,0.0,0.0), 
                              v.General.x*jngls);    //Jungles
      }

      if (WorldColor.r < 0.49){
            Clr_am.rgb = OutColor.rgb*float3( 0.975, 0.975, 0.975);
            Clr_am.rgb = lerp(Clr_am.rgb, float3(0.0,0.0,0.0), 
                              v.General.x*stripes.a);    //Forests2
      }
      if (WorldColor.b < 0.1){
            Clr_am.rgb = OutColor.rgb; 
            Clr_am.rgb = lerp(Clr_am.rgb, float3(0.0,0.0,0.0),
                              v.General.x*dsrt);   //Desert
      }

      OutColor.rgb = Clr_am.rgb;

      OutColor.rgb *= lerp(1.0, Color.rgb, 0.7 
  		-0.1*(saturate(2*BaseColor.a) -  saturate(35*WorldColor.b))); //colours the map

      if (WorldColor.a > 0.85){ 
            OutColor.rgb  = float3(0.0,0.0,0.0);  //Lake borders
      }
      if (WorldColor.a > 0.88){
            OutColor.rgb = float3(0.24,0.61,1.0)*LksTex;  //Lakes
      }

 
      //OutColor.rgb *= BaseColor*v.vTexCoord0.z;

	// 0.35 -- 0.4 is clear -- black Outline at coast
	//if (v.vTexCoord0.w < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) ); 

  // Terra incognita and FOW

  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy/BorderHeight).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy/BorderHeight).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb*TI_CLR,1.0), local);
  //OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BS_AV*TI_CLR,1.0), local);


  return OutColor;
}




VS_OUTPUT_BEACH VertexShader_Beach_General(const VS_INPUT_BEACH v )
{
	VS_OUTPUT_BEACH Out = (VS_OUTPUT_BEACH)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );

	float3 direction = normalize( LightDirection );
	Out.vLightIntensity.xy = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
	Out.vLightIntensity.z = v.vPosition.y;

	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	
	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );
	
	//Out.vColorTexCoord.xy = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );
	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.3); //zoom


	return Out;
}


float4 PixelShader_Beach_General( VS_OUTPUT_BEACH v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(TEX_SZ*BorderWidth/BorderHeight,TEX_SZ)); //v.vTerrainTexCoord );
	float4 Color1 = tex2D( GeneralTexture, v.vProvinceIndexCoord ) ;
	float4 Color2 = tex2D( GeneralTexture2, v.vProvinceIndexCoord );
        float4 stripes = tex2D( StripesTexture, v.vTerrainTexCoord );

        BaseColor.rgb = lerp(BS_AV, BaseColor.rgb, saturate(ZM_BS*v.General.x*0.75));	

        if ( stripes.g > 0.42 ){
            stripes.g =  1.0f;
        }else{
            stripes.g = 0.0f;
        }

	float4 Color = lerp(Color2, Color1, stripes.g); //data color
	float3 tmp = Color.rgb-0.5;
	if (dot(tmp,tmp)<0.001) Color.rgb = BARE;	//fix diplomacy map colour
	if (any(Color1.rgb-Color2.rgb)) 		//double printed bits
		Color.rgb *= lerp(1.0, Color2.rgb, stripes.b);

	float4 OutColor = 1.0;
        OutColor.rgb = BaseColor.rgb * v.vLightIntensity.x;


      if (WorldColor.g < 0.92) OutColor.rgb = OutColor.rgb*float3( 0.925, 0.925, 0.925);  //Hills and Mountains
      if (WorldColor.r < 0.85) OutColor.rgb = OutColor.rgb*float3( 0.975, 0.975, 0.975);   //Forests1 and Jungles
 
	//misprint from sea
 	//if (WorldColor.b *BaseColor.a > 0.17 && v.vLightIntensity.z < 0.45) OutColor.rgb *= float3(0.24,0.61,1.0);
 	//if (WorldColor.b *BaseColor.a > 0.17 && v.vLightIntensity.z < 0.45) OutColor.rgb *= float3(0.45,0.64,0.86);

	 OutColor.rgb *= lerp(1.0, Color.rgb, 0.7 
  		-0.1*(saturate(2*BaseColor.a) -  saturate(35*WorldColor.b))); //colours the map

      //OutColor.rgb *= BaseColor*v.vLightIntensity.x;


  	// 0.35 -- 0.4 is clear -- black
	//if (v.vLightIntensity.z < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) ); 
  
  // Terra incognita and FOW

  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy/BorderHeight).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy/BorderHeight).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb*TI_CLR,1.0), local);
  //OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BS_AV*TI_CLR,1.0), local);

  return OutColor;

}

	
/////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT VertexShader_Map_Border(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );
		
	Out.vBorderTexCoord1 = float2( WorldPosition.x, WorldPosition.z );

	return Out;
}


float4 PixelShader_Map2_0_Border( VS_OUTPUT v ) : COLOR
{
	float4 BorderDirectionColor = tex2D( BorderDirectionTexture, v.worldTexCoord );
	float2 TexCoord = v.vBorderTexCoord1;

	TexCoord += 0.5;

	TexCoord %= 1;				// 0 => 1 range.. only thing we need is the decimal part.
	TexCoord.y = 1.0 - TexCoord.y;
	TexCoord.x /= 16;
	TexCoord.x *= 0.8; //75;
	float2 TexCoord2 = TexCoord;
	float2 TexCoord3 = TexCoord;

	TexCoord.x += BorderDirectionColor.b;
	float4 ProvinceBorder = tex2D( ProvinceBorderTexture, TexCoord );

	TexCoord2.x += BorderDirectionColor.r;
	float4 CountryBorder = tex2D( CountryBorderTexture, TexCoord2 );

	TexCoord3.x += BorderDirectionColor.a;
	float4 DiagBorder = tex2D( BorderDiagTexture, TexCoord3 );

	float4 OutColor;
	OutColor.a = max( ProvinceBorder.a, CountryBorder.a );
	OutColor.a = max( OutColor.a, DiagBorder.a );
	OutColor.rgb = float3(0.19, 0.14, 0.045);
        float3 Cntr_Brdr_clr = OutColor.rgb;
        float mask = CountryBorder.a;
        if (DiagBorder.a > 0.7)  mask = DiagBorder.a; 

        //*********************************************************************************************
        // COMMENT (UNCOMMENT) THE FOLLOWING LINE BELOW THIS INSTRUCTIONS, BY PLACING (REMOVING) THE TWO 
        // SLASHES '//' AT THE BEGINNING OF THE LINE FOR BLACK (RED) COUNTRY BORDERS
        // BY MANIPULATING THE VALUES BETWEEN 0 AND 1 THE COUNTRY BORDERS CAN BE SET TO ANY COLOR
        //*********************************************************************************************

        //Cntr_Brdr_clr = float3(0.7, 0.0, 0.0);      //RED
        //Cntr_Brdr_clr = float3(0.0, 0.0, 0.7);      //BLUE
        //OutColor.a = mask;                            //ONLY COUNTRY BORDERS (NO PROVINCES)
        //OutColor.a = 0.0;                           //NO BORDERS

        //*********************************************************************************************


        OutColor.rgb = Cntr_Brdr_clr*mask + OutColor.rgb*( 1.0f - mask );

       

	// Terra incognita
	float4 TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord )-0.25;
	if ( TerraIncognita.g > 0 )
		OutColor.a -= 10*(TerraIncognita.ggg);
	else OutColor.rgb *= ( 1+1.25*TerraIncognita.g);

	return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT VertexShader_Map(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );

	float3 direction = normalize( LightDirection );
	Out.vTexCoord0.xy = v.vTexCoord;
	Out.vTexCoord0.z = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
//	Out.vTexCoord1  = v.vTexCoord;

	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	
	float WorldX = (ColorMapWidth * WorldPosition.x) / MapWidth;
	float WorldY = (ColorMapHeight * WorldPosition.z) / MapHeight;
	
	Out.vColorTexCoord.xy = float2( ( WorldX + X_OFFSET)/ColorMapTextureWidth, (WorldY + Z_OFFSET)/ColorMapTextureHeight );


	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );


	Out.vTexCoord0.w = v.vPosition.y; //height
	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.55); //zoom


	Out.localTexCoord  = Out.worldTexCoord*float2(TEX_SZ*BorderWidth/BorderHeight,TEX_SZ);


	return Out;
}


float4 PixelShader_Map2_0( VS_OUTPUT v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);  // World terrain
	float4 BaseColor = tex2D( BaseTexture, v.localTexCoord); // local texture

	float4 TerrainColor = tex2D( MapTexture, v.vTerrainTexCoord );
	//float4 Color = tex2D( ColorTexture, v.vColorTexCoord );
	float4 Color   = 1.0;

     //*********************************************************************************************
     // COMMENT (UNCOMMENT) ALL LINES IN A PALETTE BELOW THIS INSTRUCTIONS, BY PLACING (REMOVING) THE TWO 
     // SLASHES '//' AT THE BEGINNING OF THE LINE FOR SELECTING (UNSELECTING) A PALETTE
     // BY MANIPULATING THE VALUES BETWEEN 0 AND 1 THE DIFFERENT TERRAINS CAN BE SET TO ANY COLOR
     // LEAVE THE LAST(FOURTH) VALUE AS 1.0
     //*********************************************************************************************

     // ********* DEFAULT PALETTE

     float4 plains    = float4(0.76,0.85,0.38,1.0);
     float4 farmland  = float4(0.78,0.85,0.17,1.0);
     float4 steppes   = float4(0.85,0.93,0.35,1.0);
     float4 hills     = float4(0.79,0.67,0.34,1.0);
     float4 mountains = float4(0.92,0.85,0.47,1.0);
     float4 desert    = float4(1.00,0.88,0.28,1.0);
     

        
     // ********* OLD PALETTE (duller colors)

     //float4 plains    = float4(0.70,0.82,0.51,1.0);
     //float4 farmland  = float4(0.70,0.82,0.51,1.0);
     //float4 steppes   = float4(0.76,0.87,0.56,1.0);
     //float4 hills     = float4(0.62,0.59,0.41,1.0);
     //float4 mountains = float4(0.84,0.83,0.55,1.0);
     //float4 desert    = float4(0.91,0.85,0.38,1.0);
          

     // ********* TOT PALETTE

     //float4 plains    = float4(0.80,0.82,0.60,1.0);
     //float4 farmland  = float4(0.80,0.82,0.60,1.0);
     //float4 steppes   = float4(0.91,0.92,0.70,1.0);
     //float4 hills     = float4(0.70,0.69,0.49,1.0);
     //float4 mountains = float4(0.76,0.78,0.60,1.0);
     //float4 desert    = float4(0.70,0.69,0.49,1.0);
     
     //*********************************************************************************************
     BaseColor.rgb = lerp( BS_AV, BaseColor.rgb, saturate(ZM_BS*v.General.x));	

      Color = plains;

      if (WorldColor.b < 0.28) Color = steppes;
      if (WorldColor.b < 0.12) Color = farmland;
      if (WorldColor.b < 0.1) Color = desert;

      if (WorldColor.g < 0.92) Color = hills;
      if (WorldColor.g < 0.63) Color = mountains;

      if (WorldColor.a > 0.85){ 
          Color.rgb = float3(0.0,0.0,0.0);  //Lake borders
          TerrainColor = float4(1.0,1.0,1.0,0.0);
      }
      if (WorldColor.a > 0.88) Color.rgb = float3(0.24,0.61,1.0);  //Lakes

      Color.rgb = lerp(Color.rgb, 1.0, 0.2*saturate(1-2*BaseColor.a));

        float TerrainAlpha = tex2D( TerrainAlphaTexture, v.vTexCoord0 ).a;
	TerrainColor = lerp(1,TerrainColor,TerrainAlpha);
	Color = lerp(Color, Color*TerrainColor, 0.8);
	float4 OutColor = Color;

	float Winter = tex2D( WinterTexture, v.vProvinceIndexCoord ).x;
	if (Winter>0.1) Winter = 0.8*Winter+0.3;
        if (WorldColor.a < 0.25) Winter = 0.8;
        float4 WinterTarget = lerp(OutColor, float4(1.0,1.0,1.0,1.0), TerrainColor.a);
	OutColor = lerp(OutColor, WinterTarget, Winter) * BaseColor*v.vTexCoord0.z;
	OutColor.a = 1;

	// 0.35 -- 0.4 is clear -- black
	//if (v.vTexCoord0.w < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) );


  // Terra incognita and FOW

  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy/BorderHeight).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy/BorderHeight).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb*TI_CLR,1.0), local);
  //OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BS_AV*TI_CLR,1.0), local);
 			
  return OutColor;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////


VS_OUTPUT_BEACH VertexShader_Beach(const VS_INPUT_BEACH v )
{
	VS_OUTPUT_BEACH Out = (VS_OUTPUT_BEACH)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );

	float3 direction = normalize( LightDirection );
	Out.vLightIntensity.xy = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
	Out.vLightIntensity.z = v.vPosition.y;
	//Out.vLightIntensity.xy = v.vPosition.y;
	//Out.vLightIntensity.z = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;

	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	
	float WorldX = (ColorMapWidth * WorldPosition.x) / MapWidth;
	float WorldY = (ColorMapHeight * WorldPosition.z) / MapHeight;
	
	Out.vColorTexCoord.xy = float2( ( WorldX + X_OFFSET)/ColorMapTextureWidth, (WorldY + Z_OFFSET)/ColorMapTextureHeight );
	

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );

	
	Out.vLightIntensity.y = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.55); //zoom
	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.55); //zoom


	return Out;

}

float4 PixelShader_Beach( VS_OUTPUT_BEACH v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);  // World terrain
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(TEX_SZ*BorderWidth/BorderHeight,TEX_SZ)); // local texture

	float4 OutColor = 1.0;

     // ********* Terrain

     float4 plains    = float4(0.76,0.85,0.38,1.0);
     float4 farmland  = float4(0.78,0.85,0.17,1.0);
     float4 steppes   = float4(0.85,0.93,0.35,1.0);
     float4 hills     = float4(0.79,0.67,0.34,1.0);
     float4 mountains = float4(0.92,0.85,0.47,1.0);
     float4 desert    = float4(1.00,0.88,0.28,1.0);     

      OutColor = plains;
     BaseColor.rgb = lerp( BS_AV, BaseColor.rgb, saturate(ZM_BS*v.General.x));	

      if (WorldColor.b < 0.28) OutColor = steppes;
      if (WorldColor.b < 0.12) OutColor = farmland;
      if (WorldColor.b < 0.1) OutColor = desert;

      if (WorldColor.g < 0.92) OutColor = hills;
      if (WorldColor.g < 0.63) OutColor = mountains;

	OutColor.rgb = lerp(OutColor.rgb, 1.0, 0.2*saturate(1-2*BaseColor.a));


	float Winter = tex2D( WinterTexture, v.vProvinceIndexCoord ).x;



	//OutColor.rgb = lerp(OutColor.rgb, 1.0, Winter) *BaseColor.rgb* v.vLightIntensity.x;
	if (Winter>0.1) Winter = 0.8*Winter+0.3;
        float4 WinterTarget = lerp(OutColor, float4(1.0,1.0,1.0,1.0), 0.84);
	OutColor = lerp(OutColor, WinterTarget, Winter) * BaseColor * v.vLightIntensity.x;
	OutColor.a = 1;
	
	// 0.35 -- 0.4 is clear -- black
	//if (v.vLightIntensity.z < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) );
        //if (WorldColor.b *BaseColor.a > 0.17 && v.vLightIntensity.z < 0.45) OutColor.rgb *= float3(0.6,0.6,1.0);

  // Terra incognita and FOW

  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx/BorderWidth).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy/BorderHeight).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy/BorderHeight).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb*TI_CLR,1.0), local);
  //OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BS_AV*TI_CLR,1.0), local);

return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////


VS_OUTPUT_PTI VertexShader_PTI(const VS_INPUT_PTI v )
{
	VS_OUTPUT_PTI Out = (VS_OUTPUT_PTI)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);
	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	
	Out.worldTexCoord = float2( (WorldPosition.x + X_OFFSET)/BorderWidth, (WorldPosition.z + Z_OFFSET)/BorderHeight );
	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.55); //zoom
	
	return Out;
}

float4 PixelShader_PTI( VS_OUTPUT_PTI v ) : COLOR
{
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(TEX_SZ*BorderWidth/BorderHeight,TEX_SZ)); // local texture
        BaseColor.rgb = lerp( BS_AV, BaseColor.rgb, saturate(ZM_BS*v.General.x));	

        return float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*TI_CLR*BaseColor.rgb, 1.0 ); //0.6875
        //return float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*TI_CLR*BS_AV, 1.0 ); //0.6875
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT_TREE VertexShader_TREE(const VS_INPUT_TREE v )
{
	VS_OUTPUT_TREE Out = (VS_OUTPUT_TREE)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	
	Out.vTexCoordTI = float2( WorldPosition.x/BorderWidth, WorldPosition.z/BorderHeight );
	

	Out.vTexCoord = v.vTexCoord;

	return Out;
}

float4 PixelShader_TREE( VS_OUTPUT_TREE v ) : COLOR
{
	float4 OutColor = tex2D( TreeTexture, v.vTexCoord );
	float vFOW = ( tex2D( TerraIncognitaTextureTree, v.vTexCoordTI ).g - 0.25 );
	if ( vFOW < 0 )
		OutColor.rgb *= 1+FOWFACTOR*vFOW;
	//OutColor.rgb *= 0.8;
	OutColor.a *= vAlpha;
	//OutColor.a *= 1.1; //n

	return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

technique TerrainShader_Graphical
{
	pass p0
	{
				ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_Map();
		PixelShader = compile ps_2_0 PixelShader_Map2_0();
	}
}

technique TerrainShader_General
{
	pass p0
	{
			ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_Map_General();
		PixelShader = compile ps_2_0 PixelShader_Map2_0_General();
	}
}

technique TerrainShader_Border
{
	pass p0
	{
		VertexShader = compile vs_1_1 VertexShader_Map_Border();
		PixelShader = compile ps_2_0 PixelShader_Map2_0_Border();
	}
}


technique BeachShader_Graphical
{
	pass p0
	{
		VertexShader = compile vs_1_1 VertexShader_Beach();
		PixelShader = compile ps_2_0 PixelShader_Beach();
	}
}


technique BeachShader_General
{
	pass p0
	{
		VertexShader = compile vs_1_1 VertexShader_Beach_General();
		PixelShader = compile ps_2_0 PixelShader_Beach_General();
	}
}


technique PTIShader
{
	pass p0
	{
		fvf = XYZ;

		LightEnable[0] = false;
		Lighting = False;

		ALPHABLENDENABLE = True;

		ColorOp[0] = Modulate;
		ColorArg1[0] = Texture;
		ColorArg2[0] = current;
  
		ColorOp[1] = Disable;
		AlphaOp[1] = Disable;

		VertexShader = compile vs_1_1 VertexShader_PTI();
		PixelShader = compile ps_2_0 PixelShader_PTI();
	}
}

technique TreeShader
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_TREE();
		PixelShader = compile ps_2_0 PixelShader_TREE();
	}
}
