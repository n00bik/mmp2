//*******************************************************
//THIS DEFFINITION CONTROLS THE BRIGHTNES OF THE WATER.   
//THE HIGHER THE VALUE THE BRIGHTER THE WATER IS.        
//*******************************************************
                                                      
#define LIGHTFACTOR 0.85                             

//*******************************************************

#define BLUE float4(0.24,0.61,1.0,1.0)
#define CLEAR float4(0.0, 0.0, 0.0, 0.0)
#define BLACK float4(0.0,0.0,0.0,1.0)
#define HALFBLUE float4(0.63,0.82,1.0,1.0)

#define TAN float4(1.0,1.0, 1.0, 1.0)
#define DUST float4(1.0,1.0, 1.0, 1.0)
#define LIGHTSHIFT 0.0
#define FOWFACTOR 1.5
#define TI_CLR float3(1.0,0.9625,0.925) 
#define BS_AV  float3(0.87,0.85,0.82)

texture tex0 < string name = "Base.tga"; >;	// Base texture
texture tex1 < string name = "Base.tga"; >;	// Base texture
texture tex2 < string name = "Base.tga"; >;	// Base texture
texture tex3 < string name = "Base.tga"; >;	// Base texture
texture tex4 < string name = "Base.tga"; >;	// Base texture

float4x4 WorldViewProjectionMatrix; 
float4x4 WorldMatrix		: World; 
float4x4 ViewMatrix		: View; 

float3	LightPosition;
float3	CameraPosition;
float	Time;
float	vAlpha;

#define X_OFFSET 0.5
#define Z_OFFSET 0.5
#define TEX_SZ   60

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear; //Point;
    MagFilter = Point;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
//    AddressW = Wrap;
};



sampler TheBackgroundTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear; //Point;
    MagFilter = Point;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler TerraIncognitaFiltered  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler WorldColorTex  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler NormalMap  =   //water.dds
sampler_state
{
    Texture = <tex4>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point; //n
    MipFilter = None; //Linear;  //n
    AddressU = Wrap;
    AddressV = Wrap;
};


struct VS_INPUT_WATER
{
    float3 position			: POSITION;
    float4 texCoord0			: TEXCOORD0;
    float4 texCoord1			: TEXCOORD1;
};

struct VS_OUTPUT_WATER
{
    float4 position		: POSITION;
    float4 texCoord0		: TEXCOORD0;

    float3 eyeDirection		: TEXCOORD1;
    float3 lightDirection	: TEXCOORD2;
    float2 localTexCoord	: TEXCOORD4;
    float4 WorldTexture		: TEXCOORD3;
    float2 WorldTextureTI       : TEXCOORD5;
    float2  General		: TEXCOORD6; //n
};

const float3 offY = float3(0.11, 0.74, 0.43);
const float3 offZ = float3(0.47, 0.19, 0.78);

const float2 off = float2(0.5, 0.5);
const float2 offx = float2(1.0, 0.0);
const float2 offy = float2(-0.0, 1.5);

////////////////////////////////////////////////////////////////////////////////////////////////////////////


float	ColorMapHeight;
float	ColorMapWidth;

float	ColorMapTextureHeight;
float	ColorMapTextureWidth;

float	MapWidth;
float	MapHeight;

float	BorderWidth;
float	BorderHeight;


VS_OUTPUT_WATER VertexShader_Water_2_0(const VS_INPUT_WATER IN )
{
    VS_OUTPUT_WATER OUT = (VS_OUTPUT_WATER)0;
    float4x4 WorldView = mul(WorldMatrix, ViewMatrix);

    float4 position = mul( float4(IN.position.xyz , 1.0) , WorldViewProjectionMatrix );
    OUT.position = position;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord0.z = OUT.position.w*LIGHTFACTOR-LIGHTSHIFT;

    float WorldPositionX = ( ColorMapWidth * IN.texCoord1.x ) / MapWidth;
    float WorldPositionY = ( ColorMapHeight * IN.texCoord1.y ) / MapHeight;

    OUT.WorldTexture.xy = float2( ( WorldPositionX + X_OFFSET)/ColorMapTextureWidth, (WorldPositionY + Z_OFFSET)/ColorMapTextureHeight );
    //OUT.WorldTextureTI	= float2( ( IN.texCoord1.x + X_OFFSET)/BorderWidth, (IN.texCoord1.y + Z_OFFSET)/BorderHeight );
    OUT.WorldTextureTI	= float2( ( IN.texCoord1.x+0.25)/BorderWidth, (IN.texCoord1.y+0.25)/BorderHeight );
    OUT.localTexCoord.xy = OUT.WorldTexture.xy*float2(TEX_SZ*BorderWidth/BorderHeight,TEX_SZ);
    
    //OUT.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.3); //zoom

    
   return OUT;
}


float4 PixelShader_Water_2_0( VS_OUTPUT_WATER IN ) : COLOR
{
  float4 WorldColor = tex2D( WorldColorTex, IN.WorldTexture );

  float4 paper = tex2D(BaseTexture, IN.localTexCoord.xy); //noise-2d (paper, .a is noise)
  float4 noise = tex2D(NormalMap, IN.texCoord0.xy);
  float4 seaTexture = tex2D(TheBackgroundTexture, IN.texCoord0.xy);

  //paper.rgb = lerp( paper.rgb, BS_AV, 0.05*(1-IN.General.x) );	


  // OUTLINE AT COAST
  // 0.3 -- 0.35 is clear -- black
  //float4 OutColor = lerp(CLEAR, BLACK, saturate(20*(WorldColor.b - 0.40)) );
  float4 OutColor = CLEAR;
  if ( WorldColor.b > 0.33) OutColor = BLACK; 

  // 0.4 -- 0.45? is black -- paper texture
  //OutColor = lerp(OutColor, float4(paper.rgb, 1.0), saturate(20*(WorldColor.b - 0.36 + 0.05-0.1*noise.g)) );
  if ( WorldColor.b > 0.43) OutColor = float4(paper.rgb, 1.0); 
	  

  // HATCHING  
  //float row = frac(8*IN.texCoord0.z);
  //float coast = WorldColor.b - 0.1*noise.g +0.05;
  //float ink = 0.5*saturate (abs(4*row-2) - 5*(coast-0.4))*saturate(10*(0.61-coast));
  //OutColor.rgb *= (1-ink)*(1.0-LIGHTSHIFT); //correction

 
  // Water colours
  if (WorldColor.b < 0.95) OutColor.rgb *= float3(0.24,0.61,1.0).rgb;
    else OutColor.rgb *=lerp (HALFBLUE.rgb, BLUE.rgb, seaTexture.r*saturate (40*WorldColor.b - 16));

  float4 Color = OutColor;
  //Objects
  //if (WorldColor.r > 0.48) Color = BLACK;
  if (WorldColor.r > 0.25)  Color = float4(0.0,0.31,0.65,1.0);
  if (WorldColor.r > 0.75) Color = float4(0.0,0.03,0.33,1.0);

  OutColor = lerp (OutColor, Color, 0.75);

  // Terra incognita and FOW

  float TerraIncognita = tex2D( TerraIncognitaFiltered, IN.WorldTextureTI ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaFiltered, IN.WorldTextureTI + offx/BorderWidth).g,
  	tex2D( TerraIncognitaFiltered, IN.WorldTextureTI - offx/BorderWidth).g,
  	tex2D( TerraIncognitaFiltered, IN.WorldTextureTI + offy/BorderHeight).g,
  	tex2D( TerraIncognitaFiltered, IN.WorldTextureTI - offy/BorderHeight).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*paper.rgb*TI_CLR,1.0), local);

  return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT_WATER VertexShader_Water_1_1(const VS_INPUT_WATER IN )
{
    VS_OUTPUT_WATER OUT = (VS_OUTPUT_WATER)0;
    float4 position = mul( float4(IN.position.xyz , 1.0) , WorldViewProjectionMatrix );
    OUT.position = position;

    OUT.texCoord0 = float4 (IN.texCoord1 + off,0,0);


   // OUT.texCoord0 = float4 (IN.texCoord1 + (off/float2(BorderWidth,BorderHeight)),0,0);

  // OUT.WorldTexture = float4 (IN.texCoord1 + (off/float2(BorderWidth,BorderHeight)),0,0);


    return OUT;
}

float4 PixelShader_Water_1_1( VS_OUTPUT_WATER IN ) : COLOR
{
 float4 OutColor = float4(0.7, 0.7, 0.95, 1);

    //float4 WorldColor = tex2D( WorldColorTex, IN.WorldTexture.xy  );

  	//OutColor.rgb = lerp(OutColor, float3(0.22, 0.22, 0.4), WorldColor.r);
		
	OutColor.rgb += tex2D( TerraIncognitaFiltered, IN.texCoord0.xy ).g - 0.25;


	//OutColor += WorldColor.b;
	return OutColor;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Hambut_Bulge, 30/05/2008:
// Paradox use two seperate vertex shaders, WaterShader_2_0 for the terrain map mode, and WaterShader_1_1 for all others.
// Since I couldn't get WaterShader_1_1 to work with TOT, I've rerouted other modes down the terrain route. Seems to work!

technique WaterShader_2_0
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_2_0 VertexShader_Water_2_0();
		PixelShader = compile ps_2_0 PixelShader_Water_2_0();
	}
}

technique WaterShader_1_1
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_2_0 VertexShader_Water_2_0();
		PixelShader = compile ps_2_0 PixelShader_Water_2_0();
	}
}
