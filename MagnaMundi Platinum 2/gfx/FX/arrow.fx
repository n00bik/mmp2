texture tex0 < string name = "C:\\Active Projects\\eu3\\game\\gfx\\test\\testred.dds"; >;	// Base texture

float4x4 WorldViewProjectionMatrix; 
float CurrentState;
float4 vFirstColor;
float4 vSecondColor;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float3 vNormal    : NORMAL;
    float2 vTexCoord  : TEXCOORD0;
    float4 vDiffuse   : COLOR;
};

struct VS_OUTPUT
{
    float4  vPosition : POSITION;
    float4  vDiffuse  : COLOR;
    float2  vTexCoord : TEXCOORD0;
};


VS_OUTPUT VertexShader_Arrow(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
   	Out.vPosition  = mul(v.vPosition, WorldViewProjectionMatrix );
	//Out.vPosition  = v.vPosition;
	
	Out.vDiffuse   = v.vDiffuse;
	Out.vTexCoord  = v.vTexCoord;

	return Out;
}

/*
float4 PixelShader_Arrow_2_0( VS_OUTPUT v ) : COLOR
{
	float4 OutColor = tex2D( BaseTexture, v.vTexCoord );
	float vBlend = saturate( 20.0*( CurrentState - v.vDiffuse.b ) );
	vBlend = max( vBlend, 0.3 );
	OutColor *= vBlend;
	OutColor *= v.vDiffuse.a;
  	return OutColor;
}*/

float4 PixelShader_Arrow_2_0( VS_OUTPUT v ) : COLOR
{
	float4 OutColor = tex2D( BaseTexture, v.vTexCoord );
	if ( CurrentState < v.vDiffuse.b )
		OutColor *= 0.4; //.3
	OutColor *= v.vDiffuse.a;
  	return OutColor;
}

float4 PixelShader_Arrow_2_0_Pass0( VS_OUTPUT v ) : COLOR
{
	float4 OutColor = float4( 0.2, 0.2, 0.2, 0.7 );
	if ( CurrentState < v.vDiffuse.b )
		OutColor *= 0.65; //.5
	OutColor.a *= v.vDiffuse.a;
  	return OutColor;
}


technique tec0
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA;

		VertexShader = compile vs_1_1 VertexShader_Arrow();
		PixelShader = compile ps_2_0 PixelShader_Arrow_2_0_Pass0();
	}

	pass p1
	{
		ALPHABLENDENABLE = True;
		DestBlend = ONE;
		SrcBlend = ONE;

		VertexShader = compile vs_1_1 VertexShader_Arrow();
		PixelShader = compile ps_2_0 PixelShader_Arrow_2_0();
	}
}
