xof 0303txt 0032
//
// DirectX file: D:\dick\eu3commongoods\New_Project\data\ebony.x
//
// Converted by the PolyTrans geometry converter from Okino Computer Graphics, Inc.
// Date/time of export: 04/26/2007 13:22:38
//
// Bounding box of geometry = (-1.88805,-0.0629482,-3.20911) to (2.76453,1.27406,1.49503).


template Header {
 <3D82AB43-62DA-11cf-AB39-0020AF71E433>
 WORD major;
 WORD minor;
 DWORD flags;
}

template Vector {
  <3D82AB5E-62DA-11cf-AB39-0020AF71E433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template Coords2d {
  <F6F23F44-7686-11cf-8F52-0040333594A3>
 FLOAT u;
 FLOAT v;
}

template Matrix4x4 {
  <F6F23F45-7686-11cf-8F52-0040333594A3>
 array FLOAT matrix[16];
}

template ColorRGBA {
  <35FF44E0-6C7C-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <D3E16E81-7835-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template IndexedColor {
 <1630B820-7842-11cf-8F52-0040333594A3>
DWORD index;
 ColorRGBA indexColor;
}

template Boolean {
 <4885AE61-78E8-11cf-8F52-0040333594A3>
WORD truefalse;
}

template Boolean2d {
 <4885AE63-78E8-11cf-8F52-0040333594A3>
Boolean u;
 Boolean v;
}

template MaterialWrap {
 <4885AE60-78E8-11cf-8F52-0040333594A3>
Boolean u;
 Boolean v;
}

template TextureFilename {
 <A42790E1-7810-11cf-8F52-0040333594A3>
 STRING filename;
}

template Material {
 <3D82AB4D-62DA-11cf-AB39-0020AF71E433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshFace {
 <3D82AB5F-62DA-11cf-AB39-0020AF71E433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template MeshFaceWraps {
 <4885AE62-78E8-11cf-8F52-0040333594A3>
 DWORD nFaceWrapValues;
 Boolean2d faceWrapValues;
}

template MeshTextureCoords {
 <F6F23F40-7686-11cf-8F52-0040333594A3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshMaterialList {
 <F6F23F42-7686-11cf-8F52-0040333594A3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material]
}

template MeshNormals {
 <F6F23F43-7686-11cf-8F52-0040333594A3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template MeshVertexColors {
 <1630B821-7842-11cf-8F52-0040333594A3>
 DWORD nVertexColors;
 array IndexedColor vertexColors[nVertexColors];
}

template Mesh {
 <3D82AB44-62DA-11cf-AB39-0020AF71E433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

template FrameTransformMatrix {
 <F6F23F41-7686-11cf-8F52-0040333594A3>
 Matrix4x4 frameMatrix;
}

template Frame {
 <3D82AB46-62DA-11cf-AB39-0020AF71E433>
 [...]
}

Header {
	1; // Major version
	0; // Minor version
	1; // Flags
}

Material xof_default {
	0.400000;0.400000;0.400000;1.000000;;
	32.000000;
	0.700000;0.700000;0.700000;;
	0.000000;0.000000;0.000000;;
}

Material lambert9 {
	1.0;1.0;1.0;1.000000;;
	0.000000;
	0.000000;0.000000;0.000000;;
	0.000000;0.000000;0.000000;;
	TextureFilename {
		"ebony.tga";
	}
}

// Top-most frame encompassing the 'World'
Frame Frame_World {
	FrameTransformMatrix {
		1.000000, 0.0, 0.0, 0.0, 
		0.0, 1.000000, 0.0, 0.0, 
		0.0, 0.0, -1.000000, 0.0, 
		0.0, 0.0, 0.0, 1.000000;;
	}

Frame Frame_ebony {
	FrameTransformMatrix {
		1.000000, 0.0, 0.0, 0.0, 
		0.0, 1.000000, 0.0, 0.0, 
		0.0, 0.0, 1.000000, 0.0, 
		-56.092888, 0.0, 0.0, 1.000000;;
	}

// Original object name = "ebony"
Mesh ebony {
	52;		// 52 vertices
	57.661865;0.276558;0.249105;,
	57.536133;0.719345;0.578888;,
	57.319824;0.521057;1.063087;,
	57.311768;-0.044281;1.032555;,
	58.693604;0.291967;0.485596;,
	58.693604;0.291967;0.485596;,
	58.625244;0.803696;0.882118;,
	58.625244;0.803696;0.882118;,
	58.622803;0.584347;1.495026;,
	58.622803;0.584347;1.495026;,
	58.689453;-0.062948;1.477295;,
	58.689453;-0.062948;1.477295;,
	58.694580;0.044945;0.967255;,
	56.802734;0.358589;-0.240025;,
	56.662354;0.751205;0.033870;,
	56.375732;0.584579;0.406548;,
	56.338867;0.088978;0.362980;,
	55.996582;0.556911;-1.052734;,
	55.869141;0.841980;-0.887344;,
	55.575195;0.746468;-0.715973;,
	55.520996;0.402367;-0.775459;,
	55.620605;0.798264;-1.889435;,
	55.527344;0.991081;-1.811859;,
	55.302490;0.958939;-1.795273;,
	55.256592;0.746262;-1.862625;,
	55.604980;1.274063;-3.209106;,
	57.480225;0.056033;-0.709167;,
	57.524170;0.523304;-0.541172;,
	57.622070;0.502861;-0.052810;,
	57.638672;0.022953;0.081023;,
	58.343018;0.026958;-1.060768;,
	58.343018;0.026958;-1.060768;,
	58.457031;0.563286;-0.894020;,
	58.457031;0.563286;-0.894020;,
	58.774902;0.536915;-0.417980;,
	58.774902;0.536915;-0.417980;,
	58.857422;-0.015713;-0.290514;,
	58.857422;-0.015713;-0.290514;,
	58.599854;-0.056490;-0.668198;,
	56.610107;0.084804;-0.640194;,
	56.618896;0.497269;-0.489552;,
	56.608398;0.481750;-0.050752;,
	56.593018;0.059695;0.069797;,
	55.609619;0.122771;-0.848122;,
	55.580811;0.416225;-0.748886;,
	55.459961;0.409386;-0.462261;,
	55.414063;0.111703;-0.384350;,
	54.904785;0.147881;-1.295578;,
	54.863281;0.338276;-1.248367;,
	54.711182;0.337975;-1.117798;,
	54.658691;0.147387;-1.084320;,
	54.204834;0.209677;-2.311218;;

	60;		// 60 faces
	3;1,5,7;,
	3;5,1,0;,
	3;2,7,9;,
	3;7,2,1;,
	3;3,9,11;,
	3;9,3,2;,
	3;12,6,4;,
	3;12,8,6;,
	3;12,10,8;,
	3;14,0,1;,
	3;14,13,0;,
	3;15,1,2;,
	3;15,14,1;,
	3;16,2,3;,
	3;16,15,2;,
	3;18,13,14;,
	3;18,17,13;,
	3;19,14,15;,
	3;19,18,14;,
	3;20,15,16;,
	3;20,19,15;,
	3;22,17,18;,
	3;22,21,17;,
	3;23,18,19;,
	3;23,22,18;,
	3;24,19,20;,
	3;24,23,19;,
	3;25,21,22;,
	3;25,22,23;,
	3;25,23,24;,
	3;27,31,33;,
	3;31,27,26;,
	3;28,33,35;,
	3;33,28,27;,
	3;29,35,37;,
	3;35,29,28;,
	3;38,32,30;,
	3;38,34,32;,
	3;38,36,34;,
	3;40,26,27;,
	3;40,39,26;,
	3;41,27,28;,
	3;41,40,27;,
	3;42,28,29;,
	3;42,41,28;,
	3;44,39,40;,
	3;44,43,39;,
	3;45,40,41;,
	3;45,44,40;,
	3;46,41,42;,
	3;46,45,41;,
	3;48,43,44;,
	3;48,47,43;,
	3;49,44,45;,
	3;49,48,44;,
	3;50,45,46;,
	3;50,49,45;,
	3;51,47,48;,
	3;51,48,49;,
	3;51,49,50;;

	MeshMaterialList {
		1;1;0;;
		{lambert9}
	}

	MeshNormals {
		52; // 52 normals
		-0.977817;0.163647;0.130742;,
		-0.873120;0.047673;0.485168;,
		-0.741426;0.303530;0.598463;,
		-0.669326;0.637445;0.381663;,
		-0.659426;-0.019190;0.751525;,
		-0.570885;0.800715;0.181513;,
		-0.555891;0.582516;0.593010;,
		-0.446190;0.284774;0.848421;,
		-0.426711;0.877903;0.217264;,
		-0.400308;-0.047340;0.915157;,
		-0.396703;0.554685;0.731404;,
		-0.381957;0.792073;0.476161;,
		-0.344503;0.345457;-0.872913;,
		-0.309762;0.330067;0.891686;,
		-0.307403;-0.057672;0.949830;,
		-0.170323;0.832183;-0.527695;,
		-0.165066;0.788575;0.592371;,
		-0.158215;0.347432;-0.924261;,
		-0.152460;0.939522;-0.306683;,
		-0.114940;0.274642;0.954652;,
		-0.103490;0.989068;0.105044;,
		-0.100292;0.981954;-0.160334;,
		-0.059658;0.996978;-0.049765;,
		-0.020473;0.826029;-0.563255;,
		0.046077;0.784428;0.618506;,
		0.061423;0.963215;-0.261618;,
		0.116009;0.338400;-0.933824;,
		0.144822;0.815989;-0.559633;,
		0.166417;0.618369;-0.768066;,
		0.192576;0.266251;0.944471;,
		0.205084;0.956801;-0.206086;,
		0.225142;0.607782;0.761520;,
		0.289310;0.255950;0.922382;,
		0.339969;0.933653;-0.112756;,
		0.347926;0.621716;-0.701724;,
		0.417370;0.718373;-0.556545;,
		0.418408;0.324573;-0.848285;,
		0.572889;0.819485;0.015574;,
		0.579492;0.595247;-0.556660;,
		0.667693;0.313035;-0.675422;,
		0.781528;0.533464;-0.323464;,
		0.826476;-0.006431;-0.562936;,
		0.828902;-0.008668;-0.559326;,
		0.831316;-0.008176;-0.555740;,
		0.833711;-0.009048;-0.552126;,
		0.836095;-0.007192;-0.548538;,
		0.890593;0.451790;-0.052255;,
		0.994241;0.096161;0.047302;,
		0.994336;0.101521;0.031471;,
		0.994462;0.097617;0.038932;,
		0.994499;0.095663;0.042662;,
		0.994546;0.098343;0.034746;;

		60;		// 60 faces
		3;25,28,22;,
		3;28,25,34;,
		3;10,22,13;,
		3;22,10,25;,
		3;9,13,14;,
		3;13,9,10;,
		3;49,50,47;,
		3;49,51,50;,
		3;49,48,51;,
		3;30,34,25;,
		3;30,38,34;,
		3;6,25,10;,
		3;6,30,25;,
		3;4,10,9;,
		3;4,6,10;,
		3;33,38,30;,
		3;33,40,38;,
		3;3,30,6;,
		3;3,33,30;,
		3;1,6,4;,
		3;1,3,6;,
		3;37,40,33;,
		3;37,46,40;,
		3;5,33,3;,
		3;5,37,33;,
		3;0,3,1;,
		3;0,5,3;,
		3;20,46,37;,
		3;20,37,5;,
		3;20,5,0;,
		3;15,12,18;,
		3;12,15,17;,
		3;24,18,31;,
		3;18,24,15;,
		3;29,31,32;,
		3;31,29,24;,
		3;43,44,45;,
		3;43,42,44;,
		3;43,41,42;,
		3;23,17,15;,
		3;23,26,17;,
		3;16,15,24;,
		3;16,23,15;,
		3;19,24,29;,
		3;19,16,24;,
		3;27,26,23;,
		3;27,36,26;,
		3;11,23,16;,
		3;11,27,23;,
		3;7,16,19;,
		3;7,11,16;,
		3;35,36,27;,
		3;35,39,36;,
		3;8,27,11;,
		3;8,35,27;,
		3;2,11,7;,
		3;2,8,11;,
		3;21,39,35;,
		3;21,35,8;,
		3;21,8,2;;
	}  // End of Normals

	MeshTextureCoords {
		52; // 52 texture coords
		0.668275;0.675051;,
		0.661391;0.714537;,
		0.642529;0.826573;,
		0.637755;0.856330;,
		0.827823;0.424396;,
		0.878547;0.670886;,
		0.783167;0.331937;,
		0.886146;0.715867;,
		0.671536;0.337313;,
		0.914667;0.843329;,
		0.647200;0.433094;,
		0.924694;0.877125;,
		0.736157;0.442737;,
		0.479336;0.618405;,
		0.466752;0.651302;,
		0.429041;0.743888;,
		0.418317;0.768213;,
		0.285292;0.492077;,
		0.269608;0.510917;,
		0.220986;0.562061;,
		0.206620;0.574829;,
		0.173056;0.339000;,
		0.159462;0.345695;,
		0.116844;0.361028;,
		0.104099;0.363810;,
		0.107631;0.065290;,
		0.668275;0.675051;,
		0.661391;0.714537;,
		0.642529;0.826573;,
		0.637755;0.856330;,
		0.827823;0.424396;,
		0.878547;0.670886;,
		0.783167;0.331937;,
		0.886146;0.715867;,
		0.671536;0.337313;,
		0.914667;0.843329;,
		0.647200;0.433094;,
		0.924694;0.877125;,
		0.736157;0.442737;,
		0.479336;0.618405;,
		0.466752;0.651302;,
		0.429041;0.743888;,
		0.418317;0.768213;,
		0.285292;0.492077;,
		0.269608;0.510917;,
		0.220986;0.562061;,
		0.206620;0.574829;,
		0.173056;0.339000;,
		0.159462;0.345695;,
		0.116844;0.361028;,
		0.104099;0.363810;,
		0.107631;0.065290;;
	}  // End of texture coords
} // End of Mesh
} // End of frame for 'ebony'
} // End of "World" frame
