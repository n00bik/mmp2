province_decisions = {



	cw_provincial_loyalty = {

		potential = {

			citysize = 3000

			owner = {
				has_country_flag = civil_war
				ADM = 5
			}
			is_core = yes
				
			NOT = { has_province_flag = possess�o_colonial }
			NOT = { has_province_modifier = cw_prov_loyalty }

		}

		allow = {
		}

		effect = {

			add_province_modifier = {
				name = "cw_prov_loyalty"
				duration = 1095
			}

			owner = {
				years_of_income = -0.10
			}


		}
		ai_will_do = {
			factor = 0
		}
	}




	cw_provincial_retake = {

		potential = {

			citysize = 3000

			owner = {
				has_country_flag = civil_war
			}

			controller = {
				tag = REB
			}

			has_province_modifier = cw_prov_loyalty
			NOT = { has_province_flag = cw_provincial_retake_evt }

		}

		allow = {
		}

		effect = {

			set_province_flag = cw_provincial_retake_evt

		}

		ai_will_do = {
			factor = 0
		}
	}


}