country_decisions = {

	enact_prussian_military_reforms = {
		potential = {
			tag = PRU
			land_tech = 30
			NOT = { has_country_modifier = prussian_military_reforms }
		}
		allow = {
			MIL = 7
			capital = 50
			army_tradition = 0.5
		}
		effect = {
			add_country_modifier = {
				name = "prussian_military_reforms"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
