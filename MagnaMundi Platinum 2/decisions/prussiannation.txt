country_decisions = {

	prussian_nation = {
		potential = {
			tag = TEU
			NOT = { exists = PRU }
			NOT = { tag = GER }
			is_religion_enabled = protestant
			any_owned_province = { culture = prussian }
			}
		allow = {
			NOT = { is_subject = yes }
			NOT = { regency = yes }
			owns = 41			# Ostpreussen
			OR = { 
				ADM = 6
				DIP = 6
				MIL = 6
				}
			war = no
			}
		effect = {
			capital_scope = { base_tax = 1 }
			centralization_decentralization = -1
			prestige = 0.05
			change_tag = PRU
			PRU = {
				capital = 41 # Ostpreussen
				any_owned = {
					limit = { is_core = TEU }
					add_core = PRU
					}
				any_owned = {
					limit = { has_province_flag = beneficiary_TEU }
					clr_province_flag = beneficiary_TEU
					set_province_flag = beneficiary_PRU
					}
				random_owned = {
					limit = { owner = { NOT = { government = monarchy } } }
					owner = {
						clr_country_flag = state
						clr_country_flag = principality
						set_country_flag = kingdom
						clr_country_flag = empire
						government = feudal_monarchy
					    }
				    }
				}
			any_country = {
				limit = {
					OR = {
						any_owned_province = { is_core = TEU }
						any_owned_province = { has_province_flag = beneficiary_TEU }
						}
					}
				any_owned = {
					limit = { is_core = TEU }
					add_core = PRU
					}
				any_owned = {
					limit = { has_province_flag = beneficiary_TEU }
					clr_province_flag = beneficiary_TEU
					set_province_flag = beneficiary_PRU
					}
				}
			PRU = { country_event = 11110 }
			TEU = { country_event = 112982 }
			}
		ai_will_do = {
			factor = 1
			modifier = { 
				factor = 0
				NOT = { 
					OR = { 
						NOT = { religion = catholic } 
						NOT = { government = theocracy }
						has_country_flag = excommunicated
						has_country_flag = royal_scandal
						has_country_flag = hated_enemy_of_pope
						}
					}
				}
			}
		}

	form_kingdom_of_prussia = {
		potential = {
			NOT = { exists = TEU }
			OR = { 
				primary_culture = pommeranian
				primary_culture = saxon
				}
			NOT = { is_emperor = yes }
			government = monarchy
			OR = { 
				NOT = { exists = PRU }
				PRU = { vassal_of = THIS ai = yes NOT = { num_of_cities = 5 } }
				}
			NOT = { tag = GER }
			NOT = { tag = DDR }
			NOT = { has_country_flag = prussian_kingdom }
			NOT = { has_country_flag = prussian_kingdom_declined }
			}
		allow = {
			NOT = { is_subject = yes }
			owns = 41			# Ostpreussen
			is_core = 41
			OR = {
				owns = 42		# Warmia
				owns = 43		# Danzig
				owns = 48		# Hinterpommern
				}
			war = no
			}
		effect = {
			capital_scope = { base_tax = 1 }
			centralization_decentralization = -1
			prestige = 0.05
			random_owned = { 
				limit = { exists = PRU }
				owner = { inherit = PRU }
				}
			any_country = {
				limit = { 
					capital_scope = { hre = yes }
					elector = yes
					NOT = { is_emperor = yes }
					NOT = { capital_scope = { owned_by = THIS } }
					NOT = { primary_culture = pommeranian }
					num_of_cities = 1 
					}
				relation = { who = THIS value = -50 }
				}
			any_country = {
				limit = { 
					OR = {
						primary_culture = pommeranian
						is_emperor = yes
						}
					num_of_cities = 1 
					}
				relation = { who = THIS value = -100 }
				casus_belli = THIS
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = feudal_monarchy-lower } }
				owner = { government = feudal_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = merchant_republic-lower } }
				owner = { government = merchant_republic set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = despotic_monarchy-lower } }
				owner = { government = despotic_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = noble_republic-lower } }
				owner = { government = noble_republic set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = administrative_monarchy-lower } }
				owner = { government = administrative_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = administrative_republic-lower } }
				owner = { government = administrative_republic set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = absolute_monarchy-lower } }
				owner = { government = absolute_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = republican_dictatorship-lower } }
				owner = { government = republican_dictatorship set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = constitutional_monarchy-lower } }
				owner = { government = constitutional_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = enlightened_despotism-lower } }
				owner = { government = enlightened_despotism set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = constitutional_republic-lower } }
				owner = { government = constitutional_republic set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = bureaucratic_despotism-lower } }
				owner = { government = bureaucratic_despotism set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = theocratic_government-lower } }
				owner = { government = theocratic_government set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = religious_order-lower } }
				owner = { government = religious_order set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = tribal_despotism-lower } }
				owner = { government = tribal_despotism set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = tribal_democracy-lower } }
				owner = { government = tribal_democracy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = tribal_federation-lower } }
				owner = { government = tribal_federation set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = early_feudal_monarchy-lower } }
				owner = { government = early_feudal_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = transitional_monarchy-lower } }
				owner = { government = transitional_monarchy set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = revolutionary_republic-lower } }
				owner = { government = revolutionary_empire set_country_flag = kingdom clr_country_flag = principality clr_country_flag = default_principality set_country_flag = default_kingdom  }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = noble_republic-lowest } }
				owner = { government = noble_republic-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = religious_order-lowest } }
				owner = { government = religious_order-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = { 
				limit = { owner = { has_global_flag = magna_mundi government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower clr_country_flag = state set_country_flag = principality clr_country_flag = default_state set_country_flag = default_principality }
				}
			random_owned = {
				limit = { has_global_flag = magna_mundi }
				owner = {
					primary_culture = pommeranian
					any_owned = { 
						limit = { culture = prussian }
						culture = pommeranian
						}
					set_country_flag = prussian_kingdom
					}	
				}
			PRU = {
				add_core = 41			# Ostpreussen
				add_core = 42			# Warmia
				add_core = 43			# Danzig
				any_owned = {
					limit = { is_core = BRA }
					add_core = PRU
					}
				any_owned = {
					limit = { has_province_flag = beneficiary_BRA }
					clr_province_flag = beneficiary_BRA
					set_province_flag = beneficiary_PRU
					}
				}
			any_country = {
				limit = {
					OR = {
						any_owned_province = { is_core = TEU }
						any_owned_province = { is_core = BRA }
						any_owned_province = { has_province_flag = beneficiary_TEU }
						any_owned_province = { has_province_flag = beneficiary_BRA }
						}
					}
				any_owned = {
					limit = { is_core = TEU }
					add_core = PRU
					}
				any_owned = {
					limit = { is_core = BRA }
					add_core = PRU
					}
				any_owned = {
					limit = { has_province_flag = beneficiary_TEU }
					clr_province_flag = beneficiary_TEU
					set_province_flag = beneficiary_PRU
					}
				any_owned = {
					limit = { has_province_flag = beneficiary_BRA }
					clr_province_flag = beneficiary_BRA
					set_province_flag = beneficiary_PRU
					}
				}
			TEU = { country_event = 112982 }
			}
		ai_will_do = {
			factor = 1
		}
	}
	
}