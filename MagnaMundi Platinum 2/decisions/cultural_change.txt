province_decisions = {



	peaceful_cultural_change = {

		potential = {
			is_capital = yes
			NOT = { citysize = 5000 }
			has_owner_culture = no
		}

		allow = {
			owner = {
				stability = 3
				war = no
				check_variable = { which = AE_level value = 7 }
				ADM = 7
				prestige = 0.75

				OR = {
					has_country_flag = major_power
					has_country_flag = great_power
				}

			}

			any_neighbor_province = {
				owned_by = THIS
				is_core = THIS
				citysize = 20000
				has_owner_culture = yes
			}



		}

		effect = {

			culture = THIS
			revolt_risk = +20

			add_province_modifier = {
				name = "cultural_tensions"
				duration = 18250
			}

		}
		
		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0

				owner = { 
					NOT = { treasury = 400 }
				}			
			}

		}
	}


}