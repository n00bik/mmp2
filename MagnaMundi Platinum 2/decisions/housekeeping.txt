country_decisions = {

	ai_envia_mercadores = {
		potential = {
			merchants = 3
			treasury = 50
			
			OR = {
				ADM = 5
				advisor = trader
				advisor = jewish_advisor_5
			}
			
			war = no
			ai = yes
			NOT = { has_country_flag = mercadores_enviados }
			
            any_center_of_trade = {
                has_discovered = THIS
                range = yes
                NOT = { placed_merchants = 6 }
                owner = {
                    open_market = THIS
                    NOT = { trade_embargo_by = THIS }
                }
            }
			OR = {
				AND = {
					num_of_ports = 1
					any_owned_province = { port = yes is_core = THIS }
				}
				AND = {
                    capital_scope = { continent = europe }
                    any_center_of_trade = {
                        continent = europe
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
				AND = {
                    capital_scope = { continent = asia }
                    any_center_of_trade = {
                        continent = asia
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
				AND = {
                    capital_scope = { continent = africa }
                    any_center_of_trade = {
                        continent = africa
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
                AND = {
                    capital_scope = { continent = siberia }
                    any_center_of_trade = {
                        continent = siberia
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
                AND = {
                    capital_scope = { continent = south_east }
                    any_center_of_trade = {
                        continent = south_east
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
                AND = {
                    capital_scope = { region = north_american_region }
                    any_center_of_trade = {
                        region = north_american_region
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
                AND = {
                    capital_scope = { region = south_american_region }
                    any_center_of_trade = {
                        region = south_american_region
                        has_discovered = THIS
                        range = yes
                        NOT = { placed_merchants = 6 }
                        owner = {
                            open_market = THIS
                            NOT = { trade_embargo_by = THIS }
                        }
                    }
                }
			}
		}
		allow = {
		}
		effect = {
			set_country_flag = mercadores_enviados
			merchants = -3
			treasury = -10
			
			random_center_of_trade = {
				limit = {
					has_discovered = THIS
					range = yes
					NOT = { has_province_flag = marcador }
					NOT = { placed_merchants = 6 }
					owner = {
						open_market = THIS
						NOT = { trade_embargo_by = THIS }
					}
					OR = {
						THIS = { any_owned_province = { port = yes is_core = THIS } }
                        AND = {
                            continent = europe
                            THIS = { capital_scope = { continent = europe } }
                        }
                        AND = {
                            continent = asia
                            THIS = { capital_scope = { continent = asia } }
                        }
                        AND = {
                            continent = africa
                            THIS = { capital_scope = { continent = africa } }
                        }
                        AND = {
                            continent = siberia
                            THIS = { capital_scope = { continent = siberia } }
                        }
                        AND = {
                            continent = south_east
                            THIS = { capital_scope = { continent = south_east } }
                        }
                        AND = {
                            region = north_american_region
                            THIS = { capital_scope = { region = north_american_region } }
                        }
                        AND = {
                            region = south_american_region
                            THIS = { capital_scope = { region = south_american_region } }
                        }
					}
				}
				send_merchant = THIS
				set_province_flag = marcador
			}
			
			random_center_of_trade = {
				limit = {
					has_discovered = THIS
					range = yes
					NOT = { has_province_flag = marcador }
					NOT = { placed_merchants = 6 }
					owner = {
						open_market = THIS
						NOT = { trade_embargo_by = THIS }
					}
					OR = {
						THIS = { any_owned_province = { port = yes is_core = THIS } }
                        AND = {
                            continent = europe
                            THIS = { capital_scope = { continent = europe } }
                        }
                        AND = {
                            continent = asia
                            THIS = { capital_scope = { continent = asia } }
                        }
                        AND = {
                            continent = africa
                            THIS = { capital_scope = { continent = africa } }
                        }
                        AND = {
                            continent = siberia
                            THIS = { capital_scope = { continent = siberia } }
                        }
                        AND = {
                            continent = south_east
                            THIS = { capital_scope = { continent = south_east } }
                        }
                        AND = {
                            region = north_american_region
                            THIS = { capital_scope = { region = north_american_region } }
                        }
                        AND = {
                            region = south_american_region
                            THIS = { capital_scope = { region = south_american_region } }
                        }
					}
				}
				send_merchant = THIS
				set_province_flag = marcador
			}

			random_center_of_trade = {
				limit = {
					has_discovered = THIS
					range = yes
					NOT = { has_province_flag = marcador }
					NOT = { placed_merchants = 6 }
					owner = {
						open_market = THIS
						NOT = { trade_embargo_by = THIS }
					}
					OR = {
						THIS = { any_owned_province = { port = yes is_core = THIS } }
                        AND = {
                            continent = europe
                            THIS = { capital_scope = { continent = europe } }
                        }
                        AND = {
                            continent = asia
                            THIS = { capital_scope = { continent = asia } }
                        }
                        AND = {
                            continent = africa
                            THIS = { capital_scope = { continent = africa } }
                        }
                        AND = {
                            continent = siberia
                            THIS = { capital_scope = { continent = siberia } }
                        }
                        AND = {
                            continent = south_east
                            THIS = { capital_scope = { continent = south_east } }
                        }
                        AND = {
                            region = north_american_region
                            THIS = { capital_scope = { region = north_american_region } }
                        }
                        AND = {
                            region = south_american_region
                            THIS = { capital_scope = { region = south_american_region } }
                        }
					}
				}
				send_merchant = THIS
				set_province_flag = marcador
			}
			
			random_center_of_trade = {
				limit = {
					has_province_flag = marcador
				}
				clr_province_flag = marcador
			}

			random_center_of_trade = {
				limit = {
					has_province_flag = marcador
				}
				clr_province_flag = marcador
			}

			random_center_of_trade = {
				limit = {
					has_province_flag = marcador
				}
				clr_province_flag = marcador
			}			

		}
		ai_will_do = {
			factor = 1
		}
	}
	
	drastic_measures = {
		potential = {
			NOT = { stability = -1 }
			NOT = { has_country_flag = stability_parachute }
		}
		allow = {
			
			OR = {
				check_variable = { which = AE_level value = 5 }
				ADM = 6
				statesman = 3
				treasurer = 3
				banker = 3
				jewish_advisor_1 = 3
			}
		}
		effect = {
			set_country_flag = stability_parachute
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				
				OR = {
					inflation = 4
					NOT = { num_of_cities = 10 }
				}
			}			
			
			
		}
	}	

	
}