country_decisions = {


	pbc_ndp = {
		potential = {
			NOT = { has_country_modifier = national_defence_plan }
			num_of_ports = 1
			NOT = { has_country_flag = no_provantipiracy_display }
			NOT = { technology_group = new_world }
			NOT = { technology_group = america }
		}

		allow = {
			OR = {
				MIL = 6
				ADM = 7
				naval_reformer = 5
				statesman = 6
				grand_admiral = 4
				naval_organiser = 3
				rear_admiral = 5
				KoSJ_advisor_2 = 4
			}
		}

		effect = {

			remove_country_modifier = national_defence_plan2

			add_country_modifier = {
				name = "national_defence_plan"
				duration = -1
			}
			
			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = +10 } 
					change_variable = { which = aristocrat_mood value = 0 } 
					change_variable = { which = bureaucrat_mood value = +2 } 
					change_variable = { which = clergy_mood value = 0 } 
					change_variable = { which = colonialist_mood value = +5 } 
					change_variable = { which = commercial_mood value = +5 } 
					change_variable = { which = generals_mood value = -2 } 
					change_variable = { which = intellectual_mood value = 0 } 
					change_variable = { which = monarchist_mood value = 0 } 
				} 
			}

			years_of_income = -0.20

		}

		ai_will_do = {
			factor = 1


			modifier = {
				factor = 0
				NOT = { check_variable = { which = piracy_level value = 10 } }
			}


			modifier = {
				factor = 0
				
				check_variable = { which = piracy_level value = 10 }
				NOT = { check_variable = { which = piracy_level value = 20 } }
				NOT = { treasury = 250 }
			}

			modifier = {
				factor = 0
				check_variable = { which = piracy_level value = 20 }
				NOT = { check_variable = { which = piracy_level value = 30 } }
				NOT = { treasury = 200 }
			}

			modifier = {
				factor = 0
				check_variable = { which = piracy_level value = 30 }
				NOT = { check_variable = { which = piracy_level value = 40 } }
				NOT = { treasury = 150 }
			}

			modifier = {
				factor = 0
				check_variable = { which = piracy_level value = 40 }
				NOT = { check_variable = { which = piracy_level value = 50 } }
				NOT = { treasury = 100 }
			}

			modifier = {
				factor = 0
				check_variable = { which = piracy_level value = 50 }
				NOT = { check_variable = { which = piracy_level value = 60 } }
			}
			
			modifier = {
				factor = 0
				NOT = { num_of_ports = 4}
				NOT = { treasury = 300 }
			}

			modifier = {
				factor = 0
				NOT = { num_of_ports = 1}
			}

			modifier = {
				factor = 0
				navy_size_percentage = 0.5
				NOT = { check_variable = { which = piracy_level value = 20 } }
			}			
			
			modifier = {
				factor = 0
				navy_size_percentage = 0.6
				check_variable = { which = piracy_level value = 20 }
				NOT = { check_variable = { which = piracy_level value = 30 } }
			}	
			
			modifier = {
				factor = 0
				navy_size_percentage = 0.7
				check_variable = { which = piracy_level value = 30 }
				NOT = { check_variable = { which = piracy_level value = 40 } }
			}
			
			modifier = {
				factor = 0
				has_country_modifier = national_defence_plan2
				NOT = { navy_size_percentage = 1.3 }
			}

		}
	}
	
	
	pbc_ndp2 = {
		potential = {
			NOT = { has_country_modifier = national_defence_plan2 }
			num_of_ports = 1
			NOT = { has_country_flag = no_provantipiracy_display }
			NOT = { technology_group = new_world }
			NOT = { technology_group = america }

		}

		allow = {
			OR = {
				MIL = 7
				ADM = 8
				naval_reformer = 6
				grand_admiral = 5
				naval_organiser = 4
				rear_admiral = 6
				KoSJ_advisor_2 = 5
			}
		}

		effect = {

			remove_country_modifier = national_defence_plan

			add_country_modifier = {
				name = "national_defence_plan2"
				duration = -1
			}
			
			
			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = +20 } 
					change_variable = { which = aristocrat_mood value = 0 } 
					change_variable = { which = bureaucrat_mood value = +4 } 
					change_variable = { which = clergy_mood value = 0 } 
					change_variable = { which = colonialist_mood value = +10 } 
					change_variable = { which = commercial_mood value = +10 } 
					change_variable = { which = generals_mood value = -5 } 
					change_variable = { which = intellectual_mood value = 0 } 
					change_variable = { which = monarchist_mood value = 0 } 
				} 
			}

			years_of_income = -0.35

		}

		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0
				NOT = { check_variable = { which = piracy_level value = 20 } }
			}

			modifier = {
				factor = 0
				NOT = { check_variable = { which = piracy_level value = 30 } }
				NOT = { treasury = 750 }
			}

			modifier = {
				factor = 0
				NOT = { check_variable = { which = piracy_level value = 40 } }
				NOT = { treasury = 500 }
			}

			modifier = {
				factor = 0
				NOT = { check_variable = { which = piracy_level value = 50 } }
				NOT = { treasury = 250 }
			}

			modifier = {
				factor = 0
				NOT = { check_variable = { which = piracy_level value = 60 } }
				NOT = { treasury = 200 }
			}

			modifier = {
				factor = 0
				NOT = { num_of_ports = 4}
				NOT = { treasury = 1500 }
			}

			modifier = {
				factor = 0
				NOT = { num_of_ports = 1}
			}
			
			modifier = {
				factor = 0
				has_country_modifier = national_defence_plan
				NOT = { check_variable = { which = piracy_level value = 50 } }
			}			
			
			modifier = {
				factor = 0
				has_country_modifier = national_defence_plan
				navy_size_percentage = 0.75
				check_variable = { which = piracy_level value = 50 }
				NOT = { check_variable = { which = piracy_level value = 60 } }
			}	
			
			modifier = {
				factor = 0
				navy_size_percentage = 0.85
				check_variable = { which = piracy_level value = 60 }
			}
			
		}
	}

	
	remove_ndp = {
		potential = {
				ai = no
				OR = {
					has_country_modifier = national_defence_plan
					has_country_modifier = national_defence_plan2
				}
			NOT = { has_country_flag = no_provantipiracy_display }
		}

		allow = {

		}

		effect = {
		
			remove_country_modifier = national_defence_plan
			remove_country_modifier = national_defence_plan2		
			
			stability = -1

				
			
			any_owned = {
				limit = {
					has_province_modifier = uncontrolled_piracy
					NOT = { revolt_risk = 6 } 
				}
				
				revolt_risk = +3
				
			}
			
			any_owned = {
				limit = {
					has_province_modifier = rampant_piracy
					NOT = { revolt_risk = 12 }
				}
				
				revolt_risk = +6
				
			}
			
		}

		ai_will_do = {
			factor = 0
		}
	}
	
	remove_ndp2_AI = {
		potential = {
				ai = yes
				has_country_modifier = national_defence_plan2
		}

		allow = {

		}

		effect = {

			remove_country_modifier = national_defence_plan2		

			add_country_modifier = {
				name = "national_defence_plan"
				duration = -1
			}
			
			any_owned = {
				limit = {
					has_province_modifier = uncontrolled_piracy
					NOT = { revolt_risk = 6 } 
				}
				
				revolt_risk = +3
				
			}
			
			any_owned = {
				limit = {
					has_province_modifier = rampant_piracy
					NOT = { revolt_risk = 12 }
				}
				
				revolt_risk = +6
				
			}
			
		}

		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0
				NOT = { navy_size_percentage = 1.2 }
			}
			
			modifier = {
				factor = 0
				navy_size_percentage = 1.8
				NOT = { navy_size_percentage = 2 }
				check_variable = { which = piracy_level value = 40 }
			}

			modifier = {
				factor = 0
				navy_size_percentage = 1.6
				check_variable = { which = piracy_level value = 30 }
				NOT = { check_variable = { which = piracy_level value = 40 } }
			}				
			
			modifier = {
				factor = 0
				navy_size_percentage = 1.4
				check_variable = { which = piracy_level value = 20 }
				NOT = { check_variable = { which = piracy_level value = 30 } }
			}
			
			modifier = {
				factor = 0
				navy_size_percentage = 1.2
				check_variable = { which = piracy_level value = 8 }
				NOT = { check_variable = { which = piracy_level value = 20 } }
			}

		}
	}
	

	pbc_pact_devil = {
		potential = {

			NOT = { has_country_modifier = berber_support }

			NOT = { has_country_flag = pirate_pact }
			
			NOT = { has_country_flag = crush_pirates }

			NOT = {
				OR = {
					idea = deus_vult
					government = religious_order-upper
					government = religious_order
					government = religious_order-lower
					government = religious_order-lowest
					government = theocratic_government-upper
					government = theocratic_government
					government = theocratic_government-lower
					government = theocratic_government-lowest
				}
			}
			

			REB = {
				NOT = { has_country_flag = pirates_sunset }
			}

			NOT = { religion_group = muslim }

			christian_med = { owned_by = THIS }

			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}				

		}

		allow = {
			DIP = 6
		}

		effect = {


			set_country_flag = pirate_pact


			years_of_income = -0.25


			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = -5 } 
					change_variable = { which = aristocrat_mood value = -10 } 
					change_variable = { which = bureaucrat_mood value = +10 } 
					change_variable = { which = clergy_mood value = -10 } 
					change_variable = { which = colonialist_mood value = +5 } 
					change_variable = { which = commercial_mood value = +15 } 
					change_variable = { which = generals_mood value = -5 } 
					change_variable = { which = intellectual_mood value = -5 } 
					change_variable = { which = monarchist_mood value = -5 } 
				} 
			}
			
			

		}

		ai_will_do = {
			factor = 0

		}
	}



	pbc_support = {
		potential = {

			REB = {
				NOT = { has_country_flag = pirates_sunset }
			}

			NOT = {
				OR = {
					government = religious_order-upper
					government = religious_order
					government = religious_order-lower
					government = religious_order-lowest
					government = theocratic_government-upper
					government = theocratic_government
					government = theocratic_government-lower
					government = theocratic_government-lowest
				}
			}

			NOT = { has_country_modifier = berber_support }
			NOT = { has_country_flag = pirate_pact }
			NOT = { has_country_flag = barbary_p_reset }
			NOT = { religion_group = muslim }
			NOT = { has_country_flag = crush_pirates }

			christian_med = { owned_by = THIS }

		}

		allow = {
			DIP = 7			
		}

		effect = {

			add_country_modifier = {
				name = "berber_support"
				duration = 3650
			}

			treasury = +25
			
			any_country = {
				limit = {
					OR = {
						has_country_flag = barbary_pirates1
						has_country_flag = barbary_pirates2
						has_country_flag = barbary_pirates3
					}

				}

				relation = { who = THIS value = +200 }

			}
			
			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = -10 } 
					change_variable = { which = aristocrat_mood value = -20 } 
					change_variable = { which = bureaucrat_mood value = +10 } 
					change_variable = { which = clergy_mood value = -30 } 
					change_variable = { which = colonialist_mood value = +5 } 
					change_variable = { which = commercial_mood value = +5 } 
					change_variable = { which = generals_mood value = -10 } 
					change_variable = { which = intellectual_mood value = -5 } 
					change_variable = { which = monarchist_mood value = -5 } 
				} 
			}

		}

		ai_will_do = {
			factor = 0.05

			modifier = {
				factor = 0
				
				NOT = { 
					relation = { who = MOR value = 0 } 
				}

				NOT = { 
					relation = { who = ALG value = 0 } 
				}

				NOT = { 
					relation = { who = TUN value = 0 } 
				}

				NOT = { 
					relation = { who = TRP value = 0 } 
				}
				
			}


			modifier = {
				factor = 0
				
				relation = { who = PAP value = 150 } 
			}

			modifier = {
				factor = 0
				
				NOT = {
					war_with = PAP
				}
			}


			modifier = {
				factor = 0
				
				NOT = { 
					relation = { who = TUR value = -50 } 
				}
			}


			modifier = {
				factor = 0
				
				war_with = TUR
			}


			modifier = {
				factor = 0
				
				NOT = { 
					relation = { who = MAM value = -50 } 
				}
			}


			modifier = {
				factor = 0
				
				war_with = MAM
			}


			modifier = {
				factor = 0
				
				is_emperor = yes
				relation = { who = PAP value = 50 } 
			}

			modifier = {
				factor = 0
				
				elector = yes
				NOT = { is_emperor = yes }

				emperor = {
					relation = { who = PAP value = 100 } 
				}
			}

			modifier = {
				factor = 0
				
				capital_scope = { hre = yes }
				NOT = { elector = yes }
				NOT = { is_emperor = yes }

				emperor = {
					relation = { who = PAP value = 50 } 

					any_owned_province = {

						OR = {

							has_province_modifier = power_06
							has_province_modifier = power_07
							has_province_modifier = power_08
							has_province_modifier = power_09
						}

					}
				}
			}


		}
	}


	pbc_protest = {
		potential = {

			NOT = { has_country_flag = protest_to_berbers }
       			NOT = { religion_group = muslim }

			NOT = {
				OR = {
					idea = deus_vult
					government = religious_order-upper
					government = religious_order
					government = religious_order-lower
					government = religious_order-lowest
					government = theocratic_government-upper
					government = theocratic_government
					government = theocratic_government-lower
					government = theocratic_government-lowest
				}
			}


            christian_med = { owned_by = THIS }
				

		}

		allow = {


			OR = {
				has_country_flag = country_raided1
				has_country_flag = country_raided2
				has_country_flag = country_raided3
			}
			
		}

		effect = {

			set_country_flag = protest_to_berbers
			prestige = -0.05

			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = 0 } 
					change_variable = { which = aristocrat_mood value = -10 } 
					change_variable = { which = bureaucrat_mood value = +5 } 
					change_variable = { which = clergy_mood value = 0 } 
					change_variable = { which = colonialist_mood value = +2 } 
					change_variable = { which = commercial_mood value = +2 } 
					change_variable = { which = generals_mood value = 0 } 
					change_variable = { which = intellectual_mood value = +5 } 
					change_variable = { which = monarchist_mood value = -5 } 
				} 
			}

			
			random_country = {
				limit = {
					OR = {
						has_country_flag = barbary_pirates1
						has_country_flag = barbary_pirates2
						has_country_flag = barbary_pirates3
					}

				}



				relation = { who = THIS value = -25 }

			}

		}

		ai_will_do = {
			factor = 1


			modifier = {
				factor = 0
				NOT = { DIP = 7 }
			}

			modifier = {
				factor = 0
				NOT = { prestige = 0 }
			}

			modifier = {
				factor = 0
				NOT = { navy_tradition = 0.1 }
			}

			modifier = {
				factor = 0
				NOT = { army_tradition = 0.1 }
			}

		}

	}


	pbc_forfeit_mil_pact = {
		potential = {
			has_country_modifier = pbc_strategic_pact
		}

		allow = {
		
		}

		effect = {

			remove_country_modifier = pbc_strategic_pact
			years_of_income = -0.15

			relation = { who = MOR value = -20 }
			relation = { who = TUN value = -20 }
			relation = { who = ALG value = -20 }
			relation = { who = TRP value = -20 }

		}

		ai_will_do = {
			factor = 0

		}

	}


	pbc_finish_pirates = {
		potential = {
			christian_med = { owned_by = THIS }

			OR = {
				has_country_flag = major_power
				has_country_flag = great_power
			}
	

			NOT = { has_country_flag = crush_pirates }
			NOT = { has_country_flag = no_crush_pirates }

			REB = {
				NOT = {
					has_country_flag = pirates_sunset
				}
			}


		}

		allow = {
			naval_tech = 61
			production_tech = 61
		}

		effect = {

			set_country_flag = crush_pirates
			prestige = 0.05

			remove_country_modifier = pbc_strategic_pact
			remove_country_modifier = berber_support

			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = +5 } 
					change_variable = { which = aristocrat_mood value = +10 } 
					change_variable = { which = bureaucrat_mood value = 0 } 
					change_variable = { which = clergy_mood value = +15 } 
					change_variable = { which = colonialist_mood value = 0 } 
					change_variable = { which = commercial_mood value = 0 } 
					change_variable = { which = generals_mood value = 0 } 
					change_variable = { which = intellectual_mood value = 0 } 
					change_variable = { which = monarchist_mood value = 0 } 
				} 
			}
			
			
			any_country = {
				limit = {
					tag = MOR
					num_of_cities = 1
					NOT = { war_with = THIS }
					OR = {
						has_country_flag = barbary_pirates1
						has_country_flag = barbary_pirates2
						has_country_flag = barbary_pirates3
					}
				}
				relation = { who = THIS value = -300 }
				THIS = { war = MOR }
			}

			any_country = {
				limit = {
					tag = TRP
					num_of_cities = 1
					NOT = { war_with = THIS }
					OR = {
						has_country_flag = barbary_pirates1
						has_country_flag = barbary_pirates2
						has_country_flag = barbary_pirates3
					}
				}
				relation = { who = THIS value = -300 }
				THIS = { war = TRP }
			}

			any_country = {
				limit = {
					tag = TUN
					num_of_cities = 1
					NOT = { war_with = THIS }
					OR = {
						has_country_flag = barbary_pirates1
						has_country_flag = barbary_pirates2
						has_country_flag = barbary_pirates3
					}
				}
				relation = { who = THIS value = -300 }
				THIS = { war = TUN }
			}

			any_country = {
				limit = {
					tag = ALG
					num_of_cities = 1
					NOT = { war_with = THIS }
					OR = {
						has_country_flag = barbary_pirates1
						has_country_flag = barbary_pirates2
						has_country_flag = barbary_pirates3
					}
				}
				relation = { who = THIS value = -300 }
				THIS = { war = ALG }
			}

		}
		
		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0
				OR = {
					THIS = { religion_group = muslim }
					THIS = { has_country_modifier = pbc_strategic_pact }
					THIS = { has_country_modifier = berber_support }
				}
			}

			modifier = {
				factor = 0
				OR = {
					AND = {
						TUN = { has_country_flag = great_power }
						OR = {
							TUN = { has_country_flag = barbary_pirates1 }
							TUN = { has_country_flag = barbary_pirates2 }
							TUN = { has_country_flag = barbary_pirates3 }
						}
					}
					AND = {
						MOR = { has_country_flag = great_power }
						OR = {
							MOR = { has_country_flag = barbary_pirates1 }
							MOR = { has_country_flag = barbary_pirates2 }
							MOR = { has_country_flag = barbary_pirates3 }
						}
					}
					AND = {
						ALG = { has_country_flag = great_power }
						OR = {
							ALG = { has_country_flag = barbary_pirates1 }
							ALG = { has_country_flag = barbary_pirates2 }
							ALG = { has_country_flag = barbary_pirates3 }
						}
					}
					AND = {
						TRP = { has_country_flag = great_power }
						OR = {
							TRP = { has_country_flag = barbary_pirates1 }
							TRP = { has_country_flag = barbary_pirates2 }
							TRP = { has_country_flag = barbary_pirates3 }
						}
					}
				}
			}
		}

	}


	pbc_smugglers = {
		potential = {

			has_country_modifier = berber_support
			NOT = { has_country_flag = pbcsmugglersflag }

			OR = {
				has_country_flag = has_smugglers_guild1
				has_country_flag = has_smugglers_guild2
			}
		}

		allow = {

			OR = {
				advisor = spymaster
				advisor = diplomat
				advisor = trader
				advisor = privateer
				advisor = ambassador
				DIP = 5
			}

		
		}

		effect = {

			set_country_flag = pbcsmugglersflag

			relation = { who = MOR value = +20 }
			relation = { who = TUN value = +20 }
			relation = { who = ALG value = +20 }
			relation = { who = TRP value = +20 }

		}

		ai_will_do = {
			factor = 0.05

		}

	}

	pbc_quit_smugglers = {
		potential = {
			has_country_flag = pbcsmugglersflag
		}

		allow = {

		}

		effect = {

			clr_country_flag = pbcsmugglersflag

			relation = { who = MOR value = -35 }
			relation = { who = TUN value = -35 }
			relation = { who = ALG value = -35 }
			relation = { who = TRP value = -35 }

		}

		ai_will_do = {
			factor = 0
		}

	}

	pbc_slavelords = {
		potential = {

			has_country_modifier = berber_support
			NOT = { has_country_flag = pbcslavelords }

			OR = {
				has_country_flag = has_smugglers_guild1
				has_country_flag = has_smugglers_guild2
			}
		}

		allow = {

			OR = {
				advisor = spymaster
				advisor = trader
				advisor = privateer
				DIP = 6
			}

		
		}

		effect = {

			set_country_flag = pbcslavelords
			
			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = -10 } 
					change_variable = { which = aristocrat_mood value = -10 } 
					change_variable = { which = bureaucrat_mood value = 0 } 
					change_variable = { which = clergy_mood value = -10 } 
					change_variable = { which = colonialist_mood value = +5 } 
					change_variable = { which = commercial_mood value = +5 } 
					change_variable = { which = generals_mood value = -10 } 
					change_variable = { which = intellectual_mood value = -20 } 
					change_variable = { which = monarchist_mood value = +5 } 
				} 
			}

			relation = { who = MOR value = +20 }
			relation = { who = TUN value = +20 }
			relation = { who = ALG value = +20 }
			relation = { who = TRP value = +20 }

		}

		ai_will_do = {
			factor = 0.05

		}

	}

	pbc_quit_slavelords = {
		potential = {
			has_country_flag = pbcslavelords
		}

		allow = {

		}

		effect = {

			clr_country_flag = pbcslavelords
			
			random_owned = { 
				limit = { 
					owner = { ai = no } 
					has_global_flag = faction_setup_complete 
				} 
				
				owner = { 
					change_variable = { which = admiralty_mood value = 0 } 
					change_variable = { which = aristocrat_mood value = 0 } 
					change_variable = { which = bureaucrat_mood value = 0 } 
					change_variable = { which = clergy_mood value = 0 } 
					change_variable = { which = colonialist_mood value = -5 } 
					change_variable = { which = commercial_mood value = -5 } 
					change_variable = { which = generals_mood value = 0 } 
					change_variable = { which = intellectual_mood value = +5 } 
					change_variable = { which = monarchist_mood value = 0 } 
				} 
			}

			relation = { who = MOR value = -35 }
			relation = { who = TUN value = -35 }
			relation = { who = ALG value = -35 }
			relation = { who = TRP value = -35 }

		}

		ai_will_do = {
			factor = 0
		}

	}


}



