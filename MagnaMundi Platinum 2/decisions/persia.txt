#To become Persia, surrender to the Safavids when the Safawiyyah attack your provinces

country_decisions = { 

	shah_of_azerbaijan = { 
		potential = { 
			OR = {
				primary_culture = azerbadjani
				primary_culture = persian
				}
			NOT = { government = republic }
			NOT = { has_country_flag = azerbadjani_shah }
			num_of_cities = 2
			}
		allow = { 
			NOT = { regency = yes }
			OR = {
				NOT = { is_core = 416 }
				NOT = { is_core = 420 }
				NOT = { is_core = 421 }
				NOT = { is_core = 424 }
				NOT = { is_core = 425 }
				}
			OR = { 
				owns = 416
				owns = 420
				owns = 421
				owns = 424
				owns = 425
				}
			}
		effect = { 
			set_country_flag = azerbadjani_shah

			random_owned = { 
				limit = { owner = { ai = yes NOT = { religion = shiite } religion_group = muslim culture_group = persian } }
				owner = { religion = shiite }
				}
			any_country = { 
				limit = { 
					NOT = { capital_scope = { owned_by = THIS } }
					OR = { 
						owns = 416
						owns = 420
						owns = 421
						owns = 424
						owns = 425
						}
					}
				relation = { who = THIS value = -50 }
				casus_belli = THIS
				}
			random_owned = { 
				limit = { 
					owner = { religion = shiite }
					capital_scope = { NOT = { religion = shiite } }
					}
				owner = { capital_scope = { religion = shiite } }
				}
			stability = 1
			prestige = 0.05
			add_core = 416
			add_core = 420
			add_core = 421
			add_core = 424
			add_core = 425
			any_owned = { 
				limit = { SHI = { primary_culture = THIS } has_province_modifier = local_dissatisfaction }
				remove_province_modifier = local_dissatisfaction
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lowest } }
				owner = { government = noble_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lowest } }
				owner = { government = religious_order-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower }
				}
			}
		ai_will_do = { 
			factor = 1
			}
		}
	shah_of_persia = { 
		potential = { 
			OR = {
				primary_culture = persian
				tag = PER
				}
			NOT = { government = republic }
			NOT = { has_country_flag = persian_shah }
			any_owned_province = { culture = persian }
			num_of_cities = 5
			}
		allow = {
			NOT = { regency = yes }
			OR = {
				AND = {
					owns = 428 #Azam = Tehran
					NOT = { is_core = 428 }
					}
				AND = {
					owns = 429 #Fars = Isfahan
					NOT = { is_core = 429 }
					}
				AND = {
					owns = 430 #Laristan = Shiraz
					NOT = { is_core = 430 }
					}
				AND = {
					owns = 431 #Hormuz
					NOT = { is_core = 431 }
					}
				AND = {
					owns = 432 #Kerman
					NOT = { is_core = 432 }
					}
				AND = {
					owns = 432 #Kerman
					NOT = { is_core = 432 }
					}
				AND = {
					owns = 409 #Karbala #holy city for the Shiites
					NOT = { is_core = 409 }
					}
				MIL = 7
				}
			}
		effect = { 
			set_country_flag = persian_shah

			any_country = { 
				limit = { 
					NOT = { capital_scope = { owned_by = THIS } }
					OR = { 
						owns = 412
						owns = 413
						owns = 414
						owns = 417
						owns = 426
						owns = 427
						owns = 428
						owns = 429
						owns = 430
						owns = 431
						owns = 432
						owns = 433
						owns = 436
						}
					}
				relation = { who = THIS value = -50 }
				casus_belli = THIS
				}
			random_owned = { 
				limit = { owner = { ai = yes NOT = { religion = shiite } religion_group = muslim } }
				owner = { religion = shiite }
				}
			any_owned = { 
				limit = { PER = { primary_culture = THIS } has_province_modifier = local_dissatisfaction }
				remove_province_modifier = local_dissatisfaction
				}
			random_owned = { 
				limit = { 
					owner = { religion = shiite }
					capital_scope = { NOT = { religion = shiite } }
					}
				owner = { capital_scope = { religion = shiite } }
				}
			stability = 1
			prestige = 0.05
			add_core = 409
			add_core = 412
			add_core = 413
			add_core = 414
			add_core = 417
			add_core = 426
			add_core = 427
			add_core = 428
			add_core = 429
			add_core = 430
			add_core = 431
			add_core = 432
			add_core = 433
			add_core = 436
			random_owned = {
				limit = { owner = { owns = 416 } } #Tabriz
				owner = { capital = 416 }
				}
			random_owned = {
				limit = { 
					owner = { NOT = { owns = 416 } } #Tabriz
					owner = { owns = 428 } #Tehran/Qasvin
					}
				owner = { capital = 428 }
				}
			random_owned = {
				limit = { 
					owner = { NOT = { owns = 416 } } #Tabriz
					owner = { NOT = { owns = 428 } } #Tehran/Qasvin
					owner = { owns = 430 }
					}
				owner = { capital = 430 }
				}
			random_owned = { 
				limit = { 
					owner = { NOT = { num_of_cots = 1 } }
					NOT = { 429 = { cot = yes } } #Fars
					NOT = { 431 = { cot = yes } } #Hormouz
					NOT = { any_neighbor_province = { cot = yes } }
					OR = { 
						culture = persian
						culture = azerbadjani
						}
					OR = { 
						province_id = 429
						province_id = 431
						citysize = 25000
						citysize = 10000
						port = yes
						base_tax = 8
						}
					}
				cot = yes
				}
			random_owned = {
				limit = { owner = { NOT = { primary_culture = persian } } }
				owner = { primary_culture = persian }
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lower } }
				owner = { government = feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lower } }
				owner = { government = merchant_republic }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lower } }
				owner = { government = despotic_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lower } }
				owner = { government = noble_republic }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lower } }
				owner = { government = administrative_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lower } }
				owner = { government = administrative_republic }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lower } }
				owner = { government = absolute_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lower } }
				owner = { government = republican_dictatorship }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lower } }
				owner = { government = constitutional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lower } }
				owner = { government = enlightened_despotism }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lower } }
				owner = { government = constitutional_republic }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lower } }
				owner = { government = bureaucratic_despotism }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lower } }
				owner = { government = theocratic_government }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lower } }
				owner = { government = religious_order }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lower } }
				owner = { government = tribal_despotism }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lower } }
				owner = { government = tribal_democracy }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lower } }
				owner = { government = tribal_federation }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lower } }
				owner = { government = early_feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lower } }
				owner = { government = transitional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lower } }
				owner = { government = revolutionary_empire }
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lowest } }
				owner = { government = noble_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lowest } }
				owner = { government = religious_order-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower }
				}
			}
		ai_will_do = { 
			factor = 1
			}
		}

}