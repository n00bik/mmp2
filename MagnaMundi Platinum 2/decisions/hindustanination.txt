country_decisions = {

	hindustan_nation = {
		potential = {
			NOT = { exists = HIN }
			NOT = { tag = MUG }
			religion = hinduism
			OR = { 
				culture_group = western_aryan
				culture_group = hindusthani
				primary_culture = bihari
				}
			OR = { 
				any_owned_province = { culture_group = western_aryan }
				any_owned_province = { culture_group = hindusthani }
				any_owned_province = { culture = bihari }
				}
			}
		allow = {
			NOT = { is_subject = yes }
			owns = 522 # Delhi
			owns = 523 # Lucknow, most populous region
			owns = 524 # Agra, capital of several empires
			owns = 555 # Oudh
			owns = 556 # Allahabad, including the Ganges AND holy city
			is_core = 522
			is_core = 523
			is_core = 524
			is_core = 555
			is_core = 556
			war = no
		}
		effect = {
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = bihari } } }
				owner = { add_accepted_culture = bihari }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = gujarati } } }
				owner = { add_accepted_culture = gujarati }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = marathi } } }
				owner = { add_accepted_culture = marathi }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = sindhi } } }
				owner = { add_accepted_culture = sindhi }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = rajput } } }
				owner = { add_accepted_culture = rajput }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = kanauji } } }
				owner = { add_accepted_culture = kanauji }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = avadhi } } }
				owner = { add_accepted_culture = avadhi }
				}
			HIN = {
				add_core = 510
				add_core = 512
				add_core = 513
				add_core = 514
				add_core = 518
				add_core = 519
				add_core = 520
				add_core = 522
				add_core = 523
				add_core = 524
				add_core = 525
				add_core = 526
				add_core = 527
				add_core = 546
				add_core = 550
				add_core = 551
				add_core = 555
				add_core = 556
				add_core = 558
				add_core = 559
				add_core = 560
			}
			random_owned = {
				base_tax = 1
				change_manpower = 1
			}
			centralization_decentralization = -1
			prestige = 0.05
			any_country = { 
				limit = { 
					OR = { 
						culture_group = western_aryan
						culture_group = hindusthani
						AND = { 
							religion_group = muslim
							neighbour = THIS
							}
						}
					NOT = { capital_scope = { owned_by = THIS } }
					num_of_cities = 1
					}
				relation = { who = THIS value = -100 }
				casus_belli = THIS
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lower } }
				owner = { government = feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lower } }
				owner = { government = merchant_republic }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lower } }
				owner = { government = despotic_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lower } }
				owner = { government = noble_republic }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lower } }
				owner = { government = administrative_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lower } }
				owner = { government = administrative_republic }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lower } }
				owner = { government = absolute_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lower } }
				owner = { government = republican_dictatorship }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lower } }
				owner = { government = constitutional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lower } }
				owner = { government = enlightened_despotism }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lower } }
				owner = { government = constitutional_republic }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lower } }
				owner = { government = bureaucratic_despotism }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lower } }
				owner = { government = theocratic_government }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lower } }
				owner = { government = religious_order }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lower } }
				owner = { government = tribal_despotism }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lower } }
				owner = { government = tribal_democracy }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lower } }
				owner = { government = tribal_federation }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lower } }
				owner = { government = early_feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lower } }
				owner = { government = transitional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lower } }
				owner = { government = revolutionary_empire }
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lowest } }
				owner = { government = noble_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lowest } }
				owner = { government = religious_order-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower }
				}
			change_tag = HIN
			}
		ai_will_do = {
			factor = 1
			}
		}
	
}
