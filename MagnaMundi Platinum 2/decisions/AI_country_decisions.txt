country_decisions = {
decision_indian_sea_route_part_one = {
	potential = {
		ai = yes
		OR = {
			idea = quest_for_the_new_world
			explorer = 1
		}
		capital_scope = { continent = europe }
		num_of_ports = 1
		NOT = { has_country_flag = completed_indian_sea_route_part_one }
	}
	allow = { west_african_coast = { has_discovered = THIS } }
	effect = {
		set_country_flag = completed_indian_sea_route_part_one
		navy_tradition = 0.05
		random_owned = {
			limit = { owner = { active_mission = indian_sea_route_part_one } }
			1465 = { discover = yes }
			1466 = { discover = yes }
			1467 = { discover = yes }
			1468 = { discover = yes }
			1469 = { discover = yes }
			1470 = { discover = yes }
		}
	}
	ai_will_do = { factor = 1 }
}
decision_indian_sea_route_part_two = {
	potential = {
		ai = yes
		OR = {
			idea = quest_for_the_new_world
			explorer = 1
		}
		capital_scope = { continent = europe }
		num_of_ports = 1
		NOT = { has_country_flag = completed_indian_sea_route_part_two }
	}
	allow = { cape = { has_discovered = THIS } }
	effect = {
		set_country_flag = completed_indian_sea_route_part_two
		navy_tradition = 0.05
		random_owned = {
			limit = { owner = { active_mission = indian_sea_route_part_two } }
			1460 = { discover = yes }
			1461 = { discover = yes }
			1462 = { discover = yes }
			1463 = { discover = yes }
			1464 = { discover = yes }
		}
	}
	ai_will_do = { factor = 1 }
}
decision_indian_sea_route_part_three = {
	potential = {
		ai = yes
		OR = {
			idea = quest_for_the_new_world
			explorer = 1
		}
		capital_scope = { continent = europe }
		num_of_ports = 1
		NOT = { has_country_flag = completed_indian_sea_route_part_three }
	}
	allow = {
		indian_ocean_islands = {
			owned_by = THIS
			controlled_by = THIS
			has_siege = no
			units_in_province = THIS
			infantry_in_province = 1
			sea_zone = { units_in_province = THIS }
		}
	}
	effect = {
		set_country_flag = completed_indian_sea_route_part_three
		navy_tradition = 0.05
		1201 = { discover = yes }
		1338 = { discover = yes }
		1339 = { discover = yes }
		1446 = { discover = yes }
		1447 = { discover = yes }
		1448 = { discover = yes }
		1449 = { discover = yes }
		1451 = { discover = yes }
		1452 = { discover = yes }
		1456 = { discover = yes }
		1457 = { discover = yes }
		1458 = { discover = yes }
		1459 = { discover = yes }
		1460 = { discover = yes }
	}
	ai_will_do = { factor = 1 }
}
} # end country_decisions