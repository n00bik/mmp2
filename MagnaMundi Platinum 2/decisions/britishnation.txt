country_decisions = {

	british_nation = {
		potential = {
			NOT = { exists = GBR }
			NOT = { tag = PAP }
			OR = {
				culture_group = british
				primary_culture = welsh
			}
			any_owned_province = { culture_group = british }
			NOT = { has_country_flag = war_roses }
			NOT = { has_country_flag = recovered_henry }
		}
		allow = {
			NOT = { is_subject = yes }
			owns = 236		# London
			owns = 237		# Oxfordshire
			owns = 234		# Wessex
			owns = 248		# Lothian
			owns = 251		# Aberdeen
			is_core = 236		# London
			is_core = 237		# Oxfordshire
			is_core = 234		# Wessex
			is_core = 248		# Lothian
			is_core = 251		# Aberdeen
			war = no
		}
		effect = {
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = welsh } } }
				owner = { add_accepted_culture = welsh }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = english } } }
				owner = { add_accepted_culture = english }
				}
			random_owned = { 
				limit = { owner = { NOT = { accepted_culture = scottish } } }
				owner = { add_accepted_culture = scottish }
				}
			GBR = {
				add_core = 236		# London
				add_core = 233		# Cornwall
				add_core = 234		# Wessex
				add_core = 235		# Kent
				add_core = 237		# Oxfordshire
				add_core = 238		# East Anglia
				add_core = 239		# Gloucestershire
				add_core = 240		# Marches
				add_core = 241		# Glamorgan
				add_core = 242		# Gwynedd
				add_core = 243		# Lincoln
				add_core = 244		# Lancashire
				add_core = 245		# Yorkshire
				add_core = 246		# Northumberland
				add_core = 247		# Cumbria
				add_core = 248		# Lothian
				add_core = 249		# Ayrshire
				add_core = 250		# Fife
				add_core = 251		# Aberdeen
				add_core = 252		# Highlands
				add_core = 253		# Western Isles
				add_core = 369		# Orkney
				add_core = 372		# Ulster
				add_core = 373		# Meath
				add_core = 374		# Leinster
				add_core = 375		# Munster
				add_core = 376		# Connaught
				}	
			centralization_decentralization = -1
			colonists = 1
			random_owned = {
				limit = { culture_group = british }
				base_tax = 1
			}
			prestige = 0.05
			any_country = { #This threatens other British nations
				limit = { 
					OR = { 
						culture_group = gaelic
						culture_group = british
						}
					NOT = { capital_scope = { owned_by = THIS } }
					num_of_cities = 1
					}
				relation = { who = THIS value = -100 }
				casus_belli = THIS 
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lower } }
				owner = { government = feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lower } }
				owner = { government = merchant_republic }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lower } }
				owner = { government = despotic_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lower } }
				owner = { government = noble_republic }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lower } }
				owner = { government = administrative_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lower } }
				owner = { government = administrative_republic }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lower } }
				owner = { government = absolute_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lower } }
				owner = { government = republican_dictatorship }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lower } }
				owner = { government = constitutional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lower } }
				owner = { government = enlightened_despotism }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lower } }
				owner = { government = constitutional_republic }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lower } }
				owner = { government = bureaucratic_despotism }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lower } }
				owner = { government = theocratic_government }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lower } }
				owner = { government = religious_order }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lower } }
				owner = { government = tribal_despotism }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lower } }
				owner = { government = tribal_democracy }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lower } }
				owner = { government = tribal_federation }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lower } }
				owner = { government = early_feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lower } }
				owner = { government = transitional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lower } }
				owner = { government = revolutionary_empire }
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lowest } }
				owner = { government = noble_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lowest } }
				owner = { government = religious_order-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower }
				}
			change_tag = GBR
			GBR = { capital = 236 }
			any_country = {
				limit = {
					exists = no
					NOT = { tag = GBR }
					culture_group = british
					capital_scope = { owner = { culture_group = british } }
					NOT = { has_country_flag = union_inherited }
					NOT = { has_country_flag = integrated_vassal }
				}
				country_event = 112982	# general core removal
			}
			GBR = { country_event = 295020 }
			}
		ai_will_do = {
			factor = 1
		}
	}
	
}
