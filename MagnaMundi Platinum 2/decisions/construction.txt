province_decisions = {
	build_royal_palace = {
		potential = {
			owner = {
				government = monarchy
				government_tech = 15
			}
			not = { has_province_modifier = royal_palace }
			is_capital = yes
			owner = { 
				  advisor = artist 
				  idea = patron_of_art
				  treasury = 200
			}

		}
		allow = {
		}
		effect = {
			owner = {
				treasury = -100
				prestige = 0.05
			}
			add_province_modifier = {
				name = "royal_palace"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { owner = { treasury = 500 } }
			}

			modifier = {
				factor = 0
				owner = { 
					NOT = { stability = -1 }					
				}
			}

			modifier = {
				factor = 0
				owner = { 
					war = yes
				}
			}

			modifier = {
				factor = 0
				owner = { 
					war_exhaustion = 6
				}
			}

		}
	}
}
