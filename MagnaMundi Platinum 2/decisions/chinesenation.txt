country_decisions = {

	unite_hordes = { 
		potential = {
			NOT = { tag = QNG }
			NOT = { tag = MUG }
			culture_group = altaic
			any_neighbor_country = { culture_group = chinese }
			NOT = { has_country_flag = manchu_ambition }
			}
		allow = { 
			NOT = { is_subject = yes }
			num_of_cities = 5
			MIL = 7
			OR = { 
				owns = 730
				730 = { owner = { vassal_of = THIS } }
				}
			OR = { 
				owns = 702
				702 = { owner = { vassal_of = THIS } }
				}
			}
		effect = { 
			set_country_flag = greater_ambition #Allows Chinese reformation event chain
			add_core = 725	# Hinggan	# MCH cores re-added just in case
			add_core = 730	# Haixi
			add_core = 724	# Yakesa
			add_core = 727	# Heilongjiang
			add_core = 731	# Ninguta
			add_core = 728	# Sanxing
			add_core = 729	# Wusuli
			add_core = 703	# Chengde
			add_core = 726	# Liaodong	# new cores on ne china
			add_core = 704	# Liaoxi
			add_core = 723	# Xilin Gol
			add_core = 721	# Dornod	# and on inner mongolia
			add_core = 722	# Chahar
			add_core = 702	# Ulaanchab
			add_core = 701	# Ordos
			capital_scope = { base_tax = 1 }
			centralization_decentralization = -1
			prestige = 0.05
			any_country = {
				limit = {
					OR = {
						culture_group = altaic
						culture_group = chinese
						}
					num_of_cities = 1
					NOT = { capital_scope = { owned_by = THIS } }
					} 
				relation = { who = THIS value = -100 }
				casus_belli = THIS
				}
			army_tradition = 0.1
			set_country_flag = manchu_ambition
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lower } }
				owner = { government = feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lower } }
				owner = { government = merchant_republic }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lower } }
				owner = { government = despotic_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lower } }
				owner = { government = noble_republic }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lower } }
				owner = { government = administrative_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lower } }
				owner = { government = administrative_republic }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lower } }
				owner = { government = absolute_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lower } }
				owner = { government = republican_dictatorship }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lower } }
				owner = { government = constitutional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lower } }
				owner = { government = enlightened_despotism }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lower } }
				owner = { government = constitutional_republic }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lower } }
				owner = { government = bureaucratic_despotism }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lower } }
				owner = { government = theocratic_government }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lower } }
				owner = { government = religious_order }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lower } }
				owner = { government = tribal_despotism }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lower } }
				owner = { government = tribal_democracy }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lower } }
				owner = { government = tribal_federation }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lower } }
				owner = { government = early_feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lower } }
				owner = { government = transitional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lower } }
				owner = { government = revolutionary_empire }
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lowest } }
				owner = { government = noble_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lowest } }
				owner = { government = religious_order-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower }
				}
			}
		ai_will_do = { factor = 1 }
		}

	qing_empire = {
		potential = {
			NOT = { exists = QNG }
			culture_group = altaic
			any_owned_province = { culture_group = chinese }
			has_country_flag = manchu_ambition
			NOT = { has_country_flag = formed_QNG }
			}
		allow = {
			NOT = { is_subject = yes }
			num_of_cities = 12
			MIL = 7 #Strong ruler
			NOT = { ally = { tag = MNG } }
			OR = { 
				MNG = { NOT = { stability = 0 } } # Ming has negative stab
				MNG = { war_exhaustion = 8 }
				NOT = { exists = MNG }
				}
			war = no
			}
		effect = {
			set_country_flag = formed_QNG

			QNG = {
				add_core = 690 # Qingzhou
				add_core = 691 # Shandong
				add_core = 695 # Shuntian
				add_core = 687 # Runing
				add_core = 688 # Henan
				add_core = 692 # Zhending
				add_core = 693 # Taiyuan
				add_core = 694 # Pingyang
				add_core = 696 # Baoding
				add_core = 697 # Datong
				}
			random_owned = { base_tax = 1 }
			centralization_decentralization = -1
			prestige = 0.05
			random_owned = {
				limit = { owner = { NOT = { government = administrative_monarchy-upper } } }
				owner = { government = administrative_monarchy-upper }
			}
			random_owned = { 
				limit = { owner = { NOT = { religion = confucianism } } }
				owner = { religion = confucianism }
			}
			change_tag = QNG
			random_owned = { 
				limit = { province_id = 695 owned_by = THIS }
				owner = { capital = 695 } 
				}
			}
		ai_will_do = {
			factor = 1
			}
	}
	qing_empire_2 = {
		potential = {
			NOT = { exists = QNG }
			has_country_flag = formed_QNG
			}
		allow = {
			}
		effect = {
			change_tag = QNG
			}
		ai_will_do = {
			factor = 0
			}
	}
}
