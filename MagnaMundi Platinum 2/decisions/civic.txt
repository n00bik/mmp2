country_decisions = {

	licensing_of_the_press_act = {
		potential = {
			not = { has_country_modifier = the_licensing_of_the_press_act }
			or = {
				technology_group = western_europe
				technology_group = eastern_europe
			}

			government_tech = 10

		}
		allow = {
		
			innovative_narrowminded = 1
			not = { serfdom_freesubjects = 1 }
			
			OR = {
				adm = 7
				
				AND = {
					adm = 6
					statesman = 4
				}

				AND = {
					adm = 6
					spymaster = 5
				}

				AND = {
					adm = 6
					high_judge = 3
				}

				AND = {
					adm = 6
					inquisitor = 3
				}

			}
			
		}
		effect = {
			stability = -1
			innovative_narrowminded = 1
			serfdom_freesubjects = -1
			add_country_modifier = {
				name = "the_licensing_of_the_press_act"
				duration = -1
			}			
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { num_of_cities = 10 }
			}			

			modifier = {
				factor = 0
				NOT = { stability = 3 }
			}
			
			modifier = {
				factor = 0
				NOT = { war = yes }
			}
			
			modifier = {
				factor = 0
				badboy = 0.75
			}
			
		}
	}
	tenures_abolition_act = {
		potential = {
			not = { has_country_modifier = the_tenures_abolition_act }
			OR = {
				government = feudal_monarchy
				AND = {
					has_global_flag = atage
					OR = {
						government = feudal_monarchy-lower
						government = feudal_monarchy-lowest
						government = feudal_monarchy-upper
					}
				}
				AND = {
					government = electoral_government
					has_country_flag = elector_ex_feudal
				}
			}
			government_tech = 10
		}
		allow = {
			not = { centralization_decentralization = -2 }
			not = { serfdom_freesubjects = -2 }
			
			
			OR = {
				adm = 7
				
				AND = {
					adm = 6
					statesman = 4
				}

				AND = {
					adm = 6
					philosopher = 5
				}

				AND = {
					adm = 6
					high_judge = 3
				}

				AND = {
					adm = 6
					inquisitor = 3
				}

			}
		}
		effect = {
			serfdom_freesubjects = 2
			innovative_narrowminded = -1
			add_country_modifier = {
				name = "the_tenures_abolition_act"
				duration = -1
			}
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { num_of_cities = 10 }
			}			

			modifier = {
				factor = 0
				NOT = { stability = 3 }
			}
			
			modifier = {
				factor = 0
				NOT = { war = yes }
			}
			
			modifier = {
				factor = 0
				NOT = { ADM = 8 }
			}
			
		}
			
			
	}
	dissolution_act = {
		potential = {
			not = { has_country_modifier = the_dissolution_act }
			OR = {
				government = constitutional_monarchy
				government = constitutional_republic
				government = revolutionary_republic
				AND = {
					has_global_flag = atage
					OR = {
						government = constitutional_monarchy-lower
						government = constitutional_republic-lower
						government = revolutionary_republic-lower
						government = constitutional_monarchy-lowest
						government = constitutional_republic-lowest
						government = revolutionary_republic-lowest
						government = constitutional_monarchy-upper
						government = constitutional_republic-upper
						government = revolutionary_republic-upper
					}
				}
				AND = {
					has_country_flag = elector_ex_constitutional
					government = electoral_government
				}
			}
			government_tech = 8
		}
		
		allow = {
			not = { centralization_decentralization = -2 }
			serfdom_freesubjects = -1
			
			ADM = 5
			advisor = statesman
			
			NOT = { advisor = theologian }
			NOT = { idea = church_attendance_duty }
			NOT = { idea = ecumenism }
			NOT = { idea = divine_supremacy }
			NOT = { idea = deus_vult }

			capital_scope = {
				has_province_modifier = parliament
				controlled_by = owner
				NOT = { has_province_modifier = conflict_with_the_estates }
				NOT = { has_province_modifier = pending_poc_resolution }
				NOT = { has_province_modifier = parliament_dissolved }
			}
			
		}
		effect = {
			serfdom_freesubjects = 1
			badboy = -3
			stability = -1
			
			add_country_modifier = {
				name = "the_dissolution_act"
				duration = -1
			}
			change_variable = { which = power_of_parliament value = 2 }
		}
			
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { num_of_cities = 6 }
			}			

			modifier = {
				factor = 0
				NOT = { stability = 3 }
			}
			
			modifier = {
				factor = 0
				NOT = { war = yes }
			}
			
			modifier = {
				factor = 0
				NOT = { ADM = 6 }
			}

			modifier = {
				factor = 0
				check_variable = { which = power_of_parliament value = 5 }
			}
			
		}
			
	}
	witchcraft_act = {
		potential = {
			not = { has_country_modifier = the_witchcraft_act }
			religion_group = christian
			year = 1500
			not = { year = 1700 }
		}
		allow = {
		
			OR = {
				idea = church_attendance_duty
				idea = divine_supremacy
				idea = deus_vult
			}
			
			theologian = 5
			innovative_narrowminded = 2
			NOT = { stability = 0 }
		}
		effect = {
			stability = 1
			innovative_narrowminded = 1
			add_country_modifier = {
				name = "the_witchcraft_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0
				NOT = { ADM = 6 }				
			}
			
			modifier = {
				factor = 0
				badboy = 0.5
			}
			
		}
	}
	education_act = {
		potential = {
			has_country_modifier = the_school_establishment_act
			not = { has_country_modifier = the_education_act }
			religion_group = christian
		}
		allow = {
			not = { innovative_narrowminded = 0 }
			statesman = 5
			adm = 7
		}
		effect = {
		
			treasury = -50
		
			innovative_narrowminded = -1
			add_country_modifier = {
				name = "the_education_act"
				duration = -1
			}
		}

		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0
				NOT = { treasury = 150 }				
			}
			
			modifier = {
				factor = 0
				badboy = 0.5
			}
			
		}

	}
	abolish_slavery_act = {
		potential = {
			government_tech = 30
			not = { has_country_modifier = the_abolish_slavery_act }
			slaves = 1
		}
		allow = {
			dip = 6
			not = { badboy = 0.1 }
		}
		effect = {
			prestige = 0.05
			innovative_narrowminded = -1
			add_country_modifier = {
				name = "the_abolish_slavery_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.0
				slaves = 2
			}
			modifier = {
				factor = 0.0
				OR = {
					coffee = 10
					tobacco = 10
					sugar = 10
					cotton = 10
				}
			}
		}
	}
	judiciary_act = {
		potential = {
			not = { has_country_modifier = the_judiciary_act }
			not = { centralization_decentralization = -2 }
		}
		allow = {
			government_tech = 20
			adm = 7
			capital_scope = {
				has_building = courthouse
				has_building = constable
			}
		}
		effect = {
		
			treasury = -50
		
			add_country_modifier = {
				name = "the_judiciary_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { treasury = 150 }
			}
			
		}
	}
	combination_act = {
		potential = {
			government_tech = 35
			not = { has_country_modifier = the_combination_act }
			not = { serfdom_freesubjects = -2 }
			not = { idea = humanist_tolerance }
		}
		allow = {
			innovative_narrowminded = 2
			adm = 6
		}
		effect = {
			mercantilism_freetrade = -1
			add_country_modifier = {
				name = "the_combination_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	school_establishment_act = {
		potential = {
			not = { has_country_modifier = the_school_establishment_act }
			government_tech = 16
		}
		allow = {
			not = { innovative_narrowminded = -2 }
			capital_scope = { has_building = university }
		}
		effect = {
			add_country_modifier = {
				name = "the_school_establishment_act"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	revoke_restraint_of_appeals = {
		potential = {
			has_country_modifier = the_statue_in_restraint_of_appeals
			not = { has_country_modifier = revocation_of_restraint_of_appeals }
			religion = catholic
			exists = PAP
		}
		allow = {
			dip = 7
			advisor = theologian
			relation = { who = PAP value = 100 }
		}
		effect = {
			remove_country_modifier = the_statue_in_restraint_of_appeals
			relation = { who = PAP value = 50 }
			add_country_modifier = {
				name = "revocation_of_restraint_of_appeals"
				duration = -1
			}			
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					NOT = { relation = { who = PAP value = 150 } }
					num_of_cities = 5
				}
			}
		}
	}
}

province_decisions = {
	land_enclosure = {
		potential = {
			base_tax = 5
			NOT = { is_overseas = yes }
			owner = { government_tech = 10 }
			not = { has_province_modifier = land_inclosure }
			not = { any_neighbor_province = { has_province_modifier = land_inclosure } }
			owner = { not = { has_country_flag = peasant_war } }
			owner = { not = { has_country_flag = peasant_war_won } }
			not = { has_province_flag = peasant_protests_succeeded }
			not = { has_province_flag = peasant_war_in_progress }
			not = { has_province_flag = peasant_war_captured }
		}
		allow = { 
			controlled_by = owner 
			
			OR = {
				owner = { adm = 7 }
				owner = { advisor = sheriff }
				owner = { advisor = high_judge }
				owner = { advisor = alderman }
			}
			
			owner = { stability = 2 }
		}
		
		effect = {
			add_province_modifier = {
				name = "land_inclosure"
				duration = -1
			}
			revolt_risk = 12
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.0
				nationalism = 1
			}
			modifier = {
				factor = 0.0
				revolt_risk = 2
			}
			modifier = {
				factor = 0.0
				has_owner_religion = no
			}
			
			modifier = {
				factor = 0.0
				owner = { war = yes }
			}
			
			modifier = {
				factor = 0.0
				NOT = { base_tax = 10 }
			}

		}
	}
	

	spy_agency = {
		potential = {
			owner = {
				idea = espionage
				not = { any_owned_province = { has_province_modifier = spy_agencies } }
			}
			not = { has_province_modifier = spy_agencies }
			any_neighbor_province = { not = { owned_by = this } }
		}
		allow = {
			owner = { 

				not = { serfdom_freesubjects = 2 }			

				OR = {
					ADM = 7
					spymaster = 4
					inquisitor = 5
				}
			}
		}
		effect = {
			owner = { 
				treasury = -20
			}
			add_province_modifier = {
				name = "spy_agencies"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				owner = {
					NOT = { treasury = 200 }
				}
			}

		}
	}
	

	
	yearly_provincial_festival = {
		potential = {
			not = { has_province_modifier = provincial_festivals }
			owner = { war = no }
			is_colony = no
			is_core = this
			not = { any_neighbor_province = { has_province_modifier = provincial_festivals owned_by = THIS } }
			not = { any_neighbor_province = { is_capital = yes owned_by = THIS } }
		}
		allow = {
			base_tax = 7
			owner = {
				stability = 1
			}

		}
		effect = {
			owner = { treasury = -50 }
			add_province_modifier = {
				name = "provincial_festivals"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.0
				owner = { not = { treasury = 100 } }
			}
			modifier = {
				factor = 0.0
				has_owner_culture = yes
			}
			modifier = {
				factor = 0.0
				has_owner_religion = yes
			}
			modifier = {
				factor = 0.0
				NOT = { revolt_risk = 3 }
			}
			modifier = {
				factor = 0.0
				nationalism = 2 # Modifiers don't affect this minimum revolt risk
			}
		}
	}

	provincial_court_system = {
		potential = {
			not = { has_province_modifier = provincial_court_systems }
			owner = { government_tech = 15 }
			not = { any_neighbor_province = { has_province_modifier = provincial_court_systems owned_by = THIS } }
			not = { any_neighbor_province = { is_capital = yes owned_by = THIS } }
			is_core = this
		}
		allow = {
			has_building = courthouse
			
			owner = { 
			
				OR = {
					ADM = 7
					
					AND = {
						ADM = 6
						advisor = high_judge
					}				
				}
			}			
			
		}
		effect = {
			owner = { treasury = -40 }
			add_province_modifier = {
				name = "provincial_court_systems"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0.0
				owner = { not = { treasury = 120 } }
			}
			modifier = {
				factor = 0.0
				has_owner_culture = yes
			}
			modifier = {
				factor = 0.0
				has_owner_religion = yes
			}
			modifier = {
				factor = 0.0
				NOT = { revolt_risk = 3 }
			}
			modifier = {
				factor = 0.0
				nationalism = 2 # Modifiers don't affect this minimum revolt risk
			}
		}
	}
	town_hall = {
		potential = {
			not = { has_province_modifier = city_hall }
			owner = { idea = cabinet }
			is_capital = yes
		}
		allow = {
			owner = {
				or = {
					adm = 6
					advisor = statesman
				}
			}
		}
		effect = {
		
			owner = {
				treasury = -100
			}
		
			add_province_modifier = {
				name = "city_hall"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1

            modifier = {
                factor = 0
                owner = { NOT = { num_of_cities = 8 } }
            }

			modifier = {
				factor = 0.0
				NOT = { revolt_risk = 5 }
			}

			modifier = {
				factor = 0.0
				owner = { 
					not = { treasury = 250 } 
				}
			}	

			modifier = {
				factor = 0.0
				owner = { 
					OR = {
						has_country_flag = minor_holding
						has_country_flag = small_state
					}
				}
			}

			modifier = {
				factor = 0.0
				owner = { 
					war = yes
				}
			}			

		}
	}
	
	embassy = {
		potential = {	
			owner = { not = { any_owned_province = { has_province_modifier = embassies } } }
			any_neighbor_province = {
				not = { owned_by = this }
				owner = { relation = { who = this value = 0 } }
			}
		}
		allow = {
			owner = { 
			
				NOT = { badboy = 0.5 }
			
				OR = {
					dip = 7
					
					AND = {
						DIP = 6
						advisor = diplomat
					}				
				}
			}
		}
			
		effect = {
			random_neighbor_province = {
				limit = { not = { owned_by = this } }
				owner = { 
					relation = { who = this value = 50 } 
					treasury = -5
				}
			}
			add_province_modifier = {
				name = "embassies"
				duration = 3650
			}
		}
			
		ai_will_do = {
			factor = 1

			modifier = {
				factor = 0.0
				owner = { 
					not = { treasury = 50 } 
				}
			}	
		}
			
	}
	
}
