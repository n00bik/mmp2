country_decisions = {

	make_st_petersburg_capital = {
		potential = {
			tag = RUS
			owns = 33
			NOT = { capital = 33 }
			NOT = { has_country_flag = relocated_capital_st_petersburg }
			NOT = { technology_group = eastern_europe }
			NOT = { has_country_modifier = opening_society }
			NOT = { has_country_modifier = entrenched_past }
			NOT = { has_country_modifier = limited_social_mobility }
		}
		allow = {
			is_core = 33
			NOT = { has_country_modifier = emerging_middle_class }
			NOT = { has_country_modifier = traditional_values }
			NOT = { has_country_modifier = open_society }
			war = no

		}
		effect = {
			set_country_flag = relocated_capital_st_petersburg
			capital_scope = { base_tax = -2 change_manpower = -1 }
			treasury = -100
			capital = 33
			prestige = 0.1
			33 = {
				change_province_name = "St. Petersburg"
				base_tax = 5
				change_manpower = 2
				culture = russian
			 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { treasury = 175 }
			}
		}
	}
	establish_russian_patriarchate = {
		potential = {
			tag = RUS
			NOT = { has_country_modifier = russian_patriarchate }
			religion = orthodox
		}
		allow = {
			war = no
			NOT = { 151 = { owner = { religion = orthodox } } }
			prestige = 0.2
			innovative_narrowminded = 2
		}
		effect = {
			add_country_modifier = {
				name = "russian_patriarchate"
				duration = -1
				}
			}
		ai_will_do = {
			factor = 1
		}
	}
	
	
}