country_decisions = {

	russian_nation = {
		potential = {
			NOT = { exists = RUS }
			NOT = { tag = PAP }
			culture_group = ruthenian
			any_owned_province = { culture = russian }
			NOT = { has_country_flag = russian_nation_declined }
			}
		allow = {
			NOT = { is_subject = yes }
			owns = 295			# Moskva
			owns = 310			# Novgorod
			OR = {
				owns = 294		# Tver
				owns = 308		# Yaroslavl	
				}
			OR = {
				owns = 289		# Chernigov
				owns = 293		# Smolensk
				}
			OR = {
				owns = 306		# Nijni-Novgorod
				owns = 307		# Vladimir	
			}
			war = no
			}
		effect = {
			random_owned = { 
				limit = { culture = russian owner = { NOT = { accepted_culture = russian } } }
				owner = { add_accepted_culture = russian }
				}
			random_owned = { 
				limit = { culture = belarussian owner = { NOT = { accepted_culture = belarussian } } }
				owner = { add_accepted_culture = belarussian }
				}
			random_owned = { 
				limit = { culture = ukrainian owner = { NOT = { accepted_culture = ukrainian } } }
				owner = { add_accepted_culture = ukrainian }
				}
			RUS = {
				add_core = 32		# Kexholm
				add_core = 33		# Neva
				add_core = 34		# Ingermanland
				add_core = 274		# Pskov
				add_core = 275		# Polotsk
				add_core = 288		# Lugansk
				add_core = 289		# Chernigov
				add_core = 290		# Poltava
				add_core = 291		# Kharkov
				add_core = 292		# Mogilyov
				add_core = 293		# Smolensk
				add_core = 294		# Tver
				add_core = 295		# Moskva
				add_core = 296		# Kaluga
				add_core = 297		# Orel
				add_core = 298		# Kurks
				add_core = 299		# Voronezh
				add_core = 300		# Tula
				add_core = 301		# Novgorod
				add_core = 302		# Tambow
				add_core = 303		# Saratow
				add_core = 304		# Pensa
				add_core = 305		# Simbirsk
				add_core = 306		# Nijni-Novgorod
				add_core = 307		# Vladimir
				add_core = 308		# Yaroslavl
				add_core = 309		# Olonets
				add_core = 310		# Novgorod
				add_core = 311		# Kholm
				add_core = 312		# Beloozero
				add_core = 313		# Archangelsk
				add_core = 314		# Ustyug
				add_core = 467		# Bogutjar
				add_core = 469		# Mordvar
				add_core = 1077		# Solikamsk
				add_core = 1079		# Viatka
				add_core = 1080		# Idnakar
				add_core = 1081		# Bolgar
				add_core = 1082		# Kazan
				add_core = 1083		# Zavolochye
				add_core = 1752		# Rzhev
				add_core = 1753		# Kostroma
				add_core = 1754		# Orel
				add_core = 1755		# Ustyug
				}
			centralization_decentralization = -1
			merchants = 1
			random_owned = {
				limit = { culture = russian }
				base_tax = 1
			}
			prestige = 0.05
			any_country = { #This threatens other Russian nations
				limit = { 
					OR = {
						culture_group = ruthenian
						culture_group = turkic
						}
					NOT = { capital_scope = { owned_by = THIS } }
					num_of_cities = 1
					}
				relation = { who = THIS value = -100 }
				casus_belli = THIS 
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lower } }
				owner = { government = feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lower } }
				owner = { government = merchant_republic }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lower } }
				owner = { government = despotic_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lower } }
				owner = { government = noble_republic }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lower } }
				owner = { government = administrative_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lower } }
				owner = { government = administrative_republic }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lower } }
				owner = { government = absolute_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lower } }
				owner = { government = republican_dictatorship }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lower } }
				owner = { government = constitutional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lower } }
				owner = { government = enlightened_despotism }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lower } }
				owner = { government = constitutional_republic }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lower } }
				owner = { government = bureaucratic_despotism }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lower } }
				owner = { government = theocratic_government }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lower } }
				owner = { government = religious_order }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lower } }
				owner = { government = tribal_despotism }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lower } }
				owner = { government = tribal_democracy }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lower } }
				owner = { government = tribal_federation }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lower } }
				owner = { government = early_feudal_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lower } }
				owner = { government = transitional_monarchy }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lower } }
				owner = { government = revolutionary_empire }
				}
			random_owned = { 
				limit = { owner = { government = feudal_monarchy-lowest } }
				owner = { government = feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = merchant_republic-lowest } }
				owner = { government = merchant_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = despotic_monarchy-lowest } }
				owner = { government = despotic_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = noble_republic-lowest } }
				owner = { government = noble_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_monarchy-lowest } }
				owner = { government = administrative_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = administrative_republic-lowest } }
				owner = { government = administrative_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = absolute_monarchy-lowest } }
				owner = { government = absolute_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = republican_dictatorship-lowest } }
				owner = { government = republican_dictatorship-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_monarchy-lowest } }
				owner = { government = constitutional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = enlightened_despotism-lowest } }
				owner = { government = enlightened_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = constitutional_republic-lowest } }
				owner = { government = constitutional_republic-lower }
				}
			random_owned = { 
				limit = { owner = { government = bureaucratic_despotism-lowest } }
				owner = { government = bureaucratic_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = theocratic_government-lowest } }
				owner = { government = theocratic_government-lower }
				}
			random_owned = { 
				limit = { owner = { government = religious_order-lowest } }
				owner = { government = religious_order-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_despotism-lowest } }
				owner = { government = tribal_despotism-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_democracy-lowest } }
				owner = { government = tribal_democracy-lower }
				}
			random_owned = { 
				limit = { owner = { government = tribal_federation-lowest } }
				owner = { government = tribal_federation-lower }
				}
			random_owned = { 
				limit = { owner = { government = early_feudal_monarchy-lowest } }
				owner = { government = early_feudal_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = transitional_monarchy-lowest } }
				owner = { government = transitional_monarchy-lower }
				}
			random_owned = { 
				limit = { owner = { government = revolutionary_republic-lowest } }
				owner = { government = revolutionary_empire-lower }
				}
			change_tag = RUS
			any_country = {
				limit = {
					exists = no
					NOT = { tag = RUS }
					culture_group = ruthenian
					capital_scope = { owner = { culture_group = ruthenian } }
					NOT = { has_country_flag = union_inherited }
					NOT = { has_country_flag = integrated_vassal }
				}
				country_event = 112982	# general core removal
			}
			RUS = { country_event = 295020 }
			}
		ai_will_do = {
			factor = 1
		}
	}
	
}