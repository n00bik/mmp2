country_decisions = {

	cw_populist = {
		potential = {

			has_country_flag = civil_war
			NOT = { has_country_flag = cw_populist_flag2 }

		}

		allow = {
		}

		effect = {
			stability = +1
			
			years_of_income = -0.25


			set_country_flag = cw_populist_flag1
			set_country_flag = cw_populist_flag2
		}

		ai_will_do = {
			factor = 0

		}
	}


	cw_blodlust = {

		potential = {

			OR = { 
				has_country_flag = total_war
				has_country_flag = civil_war
			}

			NOT = { has_country_flag = cw_bloodlust }

		}

		allow = {
		}

		effect = {


			set_country_flag = cw_bloodlust					

			random_owned = { 
				limit = { 
					owner = { 
						 has_country_flag = minor_holding
					} 
				}

				owner = { 
					manpower = +2
				}
			}


			random_owned = { 
				limit = { 
					owner = { 
						 has_country_flag = small_state 
					} 
				}

				owner = { 
					manpower = +5
				}
			}

			random_owned = { 
				limit = { 
					owner = { 
						 has_country_flag = medium_power 
					} 
				}

				owner = { 
					manpower = +8
				}
			}

			random_owned = { 
				limit = { 
					owner = { 
						 has_country_flag = major_power 
					} 
				}

				owner = { 
					manpower = +12
				}
			}

			random_owned = { 
				limit = { 
					owner = { 
						 has_country_flag = great_power 
					} 
				}

				owner = { 
					manpower = +15
				}
			}




		}

		ai_will_do = {
			factor = 0

		}
	}




}


