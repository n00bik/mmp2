###################################################
#
# ADVISORS FOR THE REGION 'SCANDINAVIA'
#
# CURRENT TOTAL IS = 85
#
###################################################
#province list:
###################################################
#Scandinavia:
###################################################
#1 - Uppland, contains Stockholm, Uppsala & Nyk�ping.
#2 - �sterg�tland, contains Norrk�ping, Link�ping etc.
#3 - Sm�land, also contains �land. Kalmar, J�nk�ping etc..
#4 - Bergslagen, contains Falun, V�ster�s etc.
#5 - V�rmland+N�rke+Dal, contains Karlstad & �rebro
#6 - Sk�ne, contains entire Sk�ne-Blekinge
#7 - V�sterg�tland, contains G�teborg, Skara, Sk�vde etc.
#8 - Dalaskogen, northwesten part of Dalarna, including Mora and entire Siljan area.
#9 - H�lsingland-G�strikland-Medelpad, from G�vle to N�s
#10 - J�mtland+H�rjedalen
#11 - V�sterbotten-�ngermanland, from Sundsvall/H�rn�sand over Ume� to Torne�
#12 - Sjaelland, incl. K�benhavn, Roskilde, Helsing�r, Ringsted
#13 - S�nderjylland-Slesvig, incl. Ribe, Slesvig and Flensborg
#14 - Fyn-Langeland-Lolland-Falster, incl. Odense, Nyborg and Nyk�bing
#15 - Jylland, incl. Aarhus, Aalborg, Viborg and Kolding
#16 - Bohusl�n, contains Marstrand, Uddevalla, Str�mstad.
#17 - Akershus, incl. Oslo (Christiania), Sarpsborg, T�nsberg
#18 - Lapland.. the far north wastelands..
#19 - �sterbotten, down to Vasa.
#20 - Tr�ndelag, incl. Trondheim, Frosta, R�ros, Steinviksholm
#21 - H�logaland, incl. Steig, V�gan, Trondenes, Andenes, Alstahaug
#22 - Eisdiva, Oplandene, incl. Hamar, Lillehammer
#23 - Bergenshus, incl. Bergen
#24 - Agder, incl. Stavanger, Kristiansand
#25 - Gotland, Visby etc
#26 - Halland, contains Halmstad, Varberg.
#27 - Finland, incl. �bo.
#28 - Tavastland, incl. Tavastehus.
#29 - Nyland, incl. Raseborg, Borg�, Helsingfors.
#30 - Viborgs l�n, incl. Viborg, Villmansstrand, and Kivin�bb.
#31 - Savolaks, incl. Nyslott(Olofsborg).
#315 - Finnmark (Vard�)
###################################################
#Baltic:
#####################################################
#32 - Keksholms (Sordavala and Keksholm)
#35 - �sel (Dag�, iSonnenburg and Arensburg)
#36 - Estland (Reval, Hapsal, Weissenstein, Wesenberg, Pernau and Narva)
#37 - Livland (Dorpat, Fellin, Karkus,Wolmar, Lemsal and Wenden)
#38 - Riga Riga and D�nam�nde) 
#39 - Kurland (Mitau, Libau, Windau, Bauske and Kokenhusen)
####################################################
#Advisor types:
####################################################
#philosopher
#natural_scientist
#artist
#statesman
#treasurer 
#naval_reformer
#army_reformer 
#trader
#theologian 
#spymaster 
#colonial_governor 
#diplomat
##################################################     
# Era #1  1399 - 1540  (11/25 used currently)
##################################################     

advisor = {
	name = "Claudius Clavus" #Danish Geographer
	location = 14 #
	skill = 3
	type = natural_scientist
	date = 1412.1.1 #25 years old
	death_date = 1447.1.1 # ?
}

advisor = {
	name = "Johann Snell" #started book-printing in Denmark and Sweden
	location = 1 #Stockholm (he could also be placed in Denmark)
	skill = 3
	type = trader
	date = 1483.12.20 #printed the first book in Sweden (?)
	death_date = 1519.1.1 #dies
}

advisor = {
	name = "Mogens G�ye" #Danish statesman and Steward of the Realm
	location = 12 #Sjaelland
	skill = 3
	type = diplomat #could also be statesman
	date = 1514.4.6	#30 years before death
	death_date = 1544.4.6 #dies
}

advisor = {
	name = "Olaus Magnus" #Swedish ecclesiastic and writer
	location = 2 #Link�ping
	skill = 2
	type = theologian
	date = 1520.10.1 #30 years old
	death_date = 1557.8.1 #dies
}

advisor = {
	name = "Hans Tausen" #protagonist of the Danish Reformation
	location = 14 #Fyn
	skill = 5
	type = theologian
	date = 1524.1.1	#30 years old.
	death_date = 1561.1.1 #dies
}

advisor = {
	name = "Johan Friis" #Danish statesman
	location = 6 #Sk�ne
	skill = 3
	type = statesman
	date = 1524.1.1	#30 years old
	death_date = 1570.12.5 #dies
}

advisor = {
	name = "Olaus Petri" #a clergyman, writer and a main character of the Protestant reformation in Sweden
	location = 5 #�rebro
	skill = 3
	type = theologian
	date = 1525.1.1	#32 years old.
	death_date = 1552.4.19 #dies
}

advisor = {
	name = "Laurentius Petri" #Swedish clergyman and the first Evangelical Lutheran Archbishop of Sweden
	location = 5 #�rebro
	skill = 4
	type = theologian
	date = 1529.1.1	#30 years old.
	death_date = 1573.10.27 #dies
}

advisor = {
	name = "Conrad von Pyhy" #German who worked for Gustav Vasa
	location = 1 #Stockholm
	skill = 1
	type = statesman
	date = 1530.1.1	#~30 years old.
	death_date = 1553.1.1 #dies
}

advisor = {
	name = "Peder Swart" #Swedish Chronicler and bisschop
	location = 1 #Stockholm
	skill = 4
	type = artist
	date = 1532.1.1	#30 years before death
	death_date = 1562.1.1 #dies
}

advisor = {
	name = "Mikael Agricola" #Finnish clergyman who became de facto founder of written Finnish
	location = 29 #Nyland
	skill = 2
	type = theologian #could also be artist
	date = 1540.1.1	#30 years old
	death_date = 1557.4.9 #dies
}

##################################################      
# Era #2  1540 - 1630   (19/25 used currently)
##################################################      

advisor = {
	name = "Peder Oxe" #Danish finance minister and Steward of the Realm
	location = 12 #Sjaelland
	skill = 3
	type = treasurer
	date = 1550.1.7	#30 years old
	death_date = 1575.10.24 #dies
}

advisor = {
	name = "J�ran Persson" #King Eric XIV of Sweden's most trusted counsellor and head of spies
	location = 4 #Sala
	skill = 2
	type = spymaster
	date = 1555.1.1	#25 years old
	death_date = 1568.9.1 #forced to abdicate in september
}

advisor = {
	name = "Nils Gyllenstierna" #Swedish Lord High Chancellor
	location = 3 #Sm�land
	skill = 2
	type = diplomat
	date = 1556.1.1	#30 years old
	death_date = 1601.1.1 #dies
}

advisor = {
	name = "Heinrich Rantzau" #d�nischer Statthalter 
	location = 13 #Slesvig
	skill = 3
	type = statesman
	date = 1556.3.11 #30 years old
	death_date = 1598.1.1 #retires
}

advisor = {
	name = "Erik Sparre" #spokesman of the Swedish nobles and proponent of Sigismund 
	location = 3 #Sm�land <- not sure
	skill = 3
	type = diplomat 
	date = 1570.1.1 #20 years old
	death_date = 1600.1.1 #dies
}

advisor = {
	name = "Hogenskild Bielke" #Swedish nobleman nicknamed 'The old fox'
	location = 1 #Uppland <- not sure.
 	skill = 2
	type = statesman
	date = 1570.1.1 #32 years old
	death_date = 1605.1.1 #dies
}

advisor = {
	name = "J�rgen Eriksen" #Bishop of Stavanger 1571-1604
	location = 24 #Agder
	skill = 2
	type = theologian 
	date = 1571.1.1 #30 years old
	death_date = 1604.1.1 #retires
}

advisor = {
	name = "Anders Vedel" #Anders S�rensen Vedel, priest and historian, translated the 'Gesta Danorum'
	location = 14 #Vejle
	skill = 3
	type = artist
	date = 1572.11.9 #30 years old
	death_date = 1616.2.13 #dies
}

advisor = {
	name = "Tycho Brahe" #credited with the most accurate astronomical observations of his time
	location = 6 #Sk�ne
	skill = 5
	type = natural_scientist
	date = 1576.12.14	#30 years old
	death_date = 1601.10.24 #dies
}

advisor = {
	name = "Thomas Fincke" # Danish mathematician and physicist
	location = 13 #Flensburg
	skill = 4
	type = natural_scientist
	date = 1591.1.6	#30 years old
	death_date = 1656.4.24 #dies
}

advisor = {
	name = "Johannes Bureus" #Johannes Thomae Bureus Agrivillensis, tutor and advisor of King Gustavus Adolphus
	location = 1 #�kerby ?
	skill = 4
	type = artist
	date = 1598.3.25 #30 years old
	death_date = 1652.1.1 #dies
}

advisor = {
	name = "Johan Skytte" #Baron Johan Skytte, GIIA's teacher, social and political reformer 
	location = 1 #Sweden
	skill = 5
	type = statesman
	date = 1607.1.7	#30 years old
	death_date = 1645.3.25 #dies
}

advisor = {
	name = "Johannes Messenius" #Swedish historian, dramatist and university professor
	location = 2 #Vadstena
	skill = 1
	type = philosopher #could also be artist
	date = 1609.7.1	#30 years old
	death_date = 1636.1.1 #dies
}

advisor = {
	name = "Axel Oxenstierna" #made Sweden a great power
	location = 1 #Uppland
	skill = 6
	type = statesman
	date = 1613.6.16 #30 years old
	death_date = 1654.8.28 #dies
}

advisor = {
	name = "Bartholin den �ldre" #Caspar Bartholin, a polymath, first to describe the workings of the olfactory nerve
	location = 6 #Malm�
	skill = 4
	type = natural_scientist
	date = 1615.1.1	#30 years old
	death_date = 1629.7.13 #dies
}

advisor = { 
	name = "Ole Worm" #Danish physician and natural philosopher
	location = 15 #Aarhus
	skill = 3
	type = philosopher #could also be natural_scientist
	date = 1618.5.13 #30 years old
	death_date = 1654.8.31 #dies
}

advisor = {
	name = "Johan Adler-Salvius" #diplomat under Axel Oxenstierna
	location = 1 #Str�ngn�s
	skill = 3
	type = diplomat
	date = 1620.1.1	#30 years old
	death_date = 1652.8.24 #dies
}

advisor = {
	name = "Johannes M. Gothus" #Johannes Matthi� Gothus, personal teacher Christina IV
	location = 1 #Stockholm
	skill = 1
	type = statesman
	date = 1622.1.1	#30 years old
	death_date = 1670.2.18 #dies
}

advisor = {
	name = "Georg Stiernhielm" #Swedish pioneer of linguistics
	location = 4 #Vika socken, Dalarne
	skill = 3
	type = philosopher
	date = 1628.7.8	#30 years old
	death_date = 1672.4.22 #dies
}

##################################################     
# Era #3  1630 - 1720  (26/25 used currently)
##################################################     

#also present as general for Denmark
advisor = {
	name = "Anders Bille" #Danish general, his reforms, while being good, failed to stop the Swedes
	location = 12 #Sjaelland
	skill = 2
	type = army_reformer
	date = 1630.3.19 #30 years old
	death_date = 1657.11.10 #dies
}

advisor = {
	name = "Peder Winstrup" #Bishop of Lund
	location = 6 #Sk�ne
	skill = 3
	type = theologian
	date = 1635.1.1	#30 years old
	death_date = 1679.1.1 #retires
}

advisor = {
	name = "Corfitz Ulfeldt" #Danish statesman and diplomat, son of the chancellor Jacob Ulfeldt
	location = 12 #K�benhavn
	skill = 3
	type = diplomat
	date = 1636.7.10 #30 years old
	death_date = 1664.1.1 #dies
}

advisor = {
	name = "Per Brahe den yngre" #The younger. Chamberlain of GAII, Governor of Finland, Regent of Sweden
	location = 1 #Uppsala
	skill = 3
	type = statesman  
	date = 1637.4.4 #35 years, named governor of Finland.
	death_date = 1680.9.2 #dies
}

advisor = {
	name = "Hannibal Sehested" #Danish statesman and Governor of Norway
	location = 35 #�sel, Arensburg
	skill = 4 #reorganised the fiscal system of Norway
	type = treasurer
	date = 1639.1.1 #30 years old
	death_date = 1666.9.23 #dies
}

advisor = {
	name = "Gabriel Marselis" #Bailed the danish king out of the cost of the thirty years war
	location = 12 #K�benhavn
	skill = 3
	type = trader
	date = 1639.1.1 #30 years old
	death_date = 1673.4.5 #dies
}

advisor = {
	name = "Joachim Gersdorff" #Joachim Gersdorff de Tundbyholm, Danish statesman
	location = 12 #Sjaelland
	skill = 3
	type = statesman
	date = 1641.1.1 #30 years old 
	death_date = 1661.4.19 #dies
}

advisor = {
	name = "Rasmus Bartholin" #Danish scientist and physician, discovery of the double refraction of a light ray 
	location = 12 #Sjaelland
	skill = 2
	type = natural_scientist
	date = 1655.8.13 #30 years old
	death_date = 1698.11.4 #dies
}

advisor = {
	name = "David K. Ehrenstrahl" #David Kl�cker Ehrenstrahl, Swedish court painter
	location = 1 #Stockholm
	skill = 3
	type = artist
	date = 1659.9.23 #30 years old
	death_date = 1698.1.1 #dies
}

#also present as an admiral for Denmark
advisor = {
	name = "Niels Juel" #Danish admiral, raised Danish sea-power by a new system of naval tactics
	location = 17 #Oslo
	skill = 5
	type = naval_reformer
	date = 1659.5.8 #30 years old
	death_date = 1697.4.8 #dies
}

advisor = {
	name = "Olof Rudbeck" #Swedish scientist and writer, professor of medicine
	location = 4 #V�ster�s
	skill = 4
	type = natural_scientist
	date = 1660.1.15 #30 years old
	death_date = 1702.1.1 #dies
}

advisor = {
	name = "Johan Gyllenstierna" #Johan G�ransson Gyllenstierna, influental Swedish statesman
	location = 1 #Sweden, exact location unknown
	skill = 5
	type = statesman
	date = 1665.2.18 #30 years old
	death_date = 1680.6.10 #dies
}

advisor = {
	name = "Peder Griffenfeldt" #Danish statesman, trusted royal counsellor
	location = 12 #Sjaelland
	skill = 4
	type = statesman
	date = 1665.8.24 #30 years old
	death_date = 1699.3.12 #dies
}

advisor = {
	name = "Diderik Buxtehude" #Diderik Hansen Buxtehude, German-Danish organist and composer 
	location = 12 #Sjaelland
	skill = 3
	type = artist
	date = 1667.1.1 #30 years old 
	death_date = 1707.5.9 #dies
}

advisor = {
	name = "Nicholas Steno" #pioneer both in anatomy and in geology
	location = 12 #K�benhavn
	skill = 3
	type = natural_scientist
	date = 1668.1.10 #30 years old
	death_date = 1686.11.26 #dies
}

advisor = {
	name = "Georg Mohr" #Danish mathematician
	location = 12 #K�benhavn
	skill = 3
	type = natural_scientist
	date = 1670.4.1 #30 years old
	death_date = 1697.1.27 #dies
}

advisor = {
	name = "Urban Hj�rne" #Famous writer and scientist, stopped the witch hunts in Sweden
	location = 1 #Uppsala, studied there (born in Ingermanland)
	skill = 3
	type = artist #could also be natural_scientist
	date = 1671.12.20	#30 years old
	death_date = 1724.3.10 #dies
}

advisor = {
	name = "Hans Wachtmeister" #swedish admiral
	location = 1 #Sweden, exact location unknown
	skill = 4
	type = naval_reformer
	date = 1671.12.24	#30 years old
	death_date = 1714.1.1 #dies
}

advisor = {
	name = "Ole R�mer" #Danish astronomer who made the first quantitative measurements of the speed of light
	location = 15 #Aarhus
	skill = 4
	type = natural_scientist
	date = 1674.9.19	#30 years old
	death_date = 1710.9.19 #dies
}

advisor = {
	name = "Carl Piper" #Swedish statesman, practically prime minister for a while
	location = 1 #Stockholm
	skill = 3
	type = statesman
	date = 1677.7.29 #30 years old
	death_date = 1716.5.29 #dies
}

advisor = {
	name = "Petter Dass" #foremost poet and psalmist of his generation.
	location = 21 #H�logaland
	skill = 2
	type = theologian
	date = 1677.1.1 #30 years old 
	death_date = 1707.9.1 #dies
}

advisor = { 
	name = "Bartholin den yngre" #Caspar Bartholin, Danish anatomist who first described the "Bartholin's gland
	location = 12 #Sjaelland
	skill = 3
	type = natural_scientist
	date = 1685.1.1 #30 years old 
	death_date = 1738.1.1 #dies
}

advisor = {
	name = "Christopher Polhem" #Swedish scientist, inventor and industrialist
	location = 25 #Gotland
	skill = 4
	type = natural_scientist
	date = 1691.12.18	#30 years old
	death_date = 1751.8.30 #dies
}

advisor = {
	name = "Ludvig Holberg" #Danish-Norwegian writer and playwright 
	location = 23 #Bergen 
	skill = 3
	type = artist
	date = 1714.12.3 #30 years old
	death_date = 1754.1.27 #dies
}

advisor = {
	name = "Hans Egede" #Norwegian Lutheran missionary, Apostle of Greenland
	location = 12 #Sjaelland
	skill = 1
	type = colonial_governor #could also be theologian
	date = 1716.1.31 #30 years old
	death_date = 1758.11.5 #dies
}

advisor = {
	name = "Emanuel Swedenborg" #Swedish scientist, philosopher, mystic, and theologian
	location = 1 #Stockholm
	skill = 3
	type = theologian #could also be natural_scientist
	date = 1718.1.29 #30 years old
	death_date = 1772.3.29 #dies
}

##################################################
# Era #4  1720 - 1810  (30/25 used currently)
##################################################

advisor = {
	name = "Erik Tolstadius" #vocal pietist leader
	location = 1 #Stockholm
	skill = 3
	type = theologian
	date = 1723.2.21 #30 years old
	death_date = 1759.1.1 #dies
}

advisor = {
	name = "Carl Gustaf Tessin" #Swedish politician, leader of the Hat Party
	location = 1 #Stockholm
	skill = 2
	type = statesman
	date = 1725.9.5 #30 years old
	death_date = 1770.1.7 #dies
}

advisor = {
	name = "Anders Celsius" #Founder of the Uppsala Astronomical Observatory, known for the Celsius temperature scale
	location = 9 #H�lsingland
	skill = 4
	type = natural_scientist
	date = 1731.11.27 #30 years old
	death_date = 1744.4.25 #dies
}

advisor = {
	name = "Thomas Plomgren" #Richest merchant in Sweden at that time
	location = 1 #Stockholm
	skill = 3
	type = trader
	date = 1732.8.16 #30 years old
	death_date = 1754.1.1 #dies
}

advisor = {
	name = "Carolus Linnaeus" #Swedish zoologist who laid the foundations for the modern scheme of nomenclature
	location = 1 #Uppsala
	skill = 5
	type = natural_scientist
	date = 1737.5.23 #30 years old
	death_date = 1778.1.10 #dies
}

advisor = {
	name = "Olof von Dahlin" #Swedish poet
	location = 7 #Halland, Vinbergs pr�stg�rd
	skill = 3
	type = artist
	date = 1738.8.29 #30 years old
	death_date = 1763.8.12 #dies
}

advisor = {
	name = "Adam Gottlob Moltke" #Danish statesman and diplomat. Inseparable companion of king Fredrick V.
	location = 12 #K�benhavn
	skill = 3
	type = statesman
	date = 1740.10.10 #30 years old
	death_date = 1792.9.25 #dies
}

advisor = {
	name = "Anders Berch" #First Professor in Economics in the world
	location = 8 #Dalarne
	skill = 3
	type = treasurer
	date = 1741.3.4 #30 years old
	death_date = 1774.12.14 #dies
}

advisor = {
	name = "Johan Bernstorff" #Johann Hartwig Ernst Bernstorff. Minister of foreign affairs, successfully kept Denmark at peace.
	location = 12 #K�benhavn
	skill = 4
	type = diplomat
	date = 1742.5.13 #30 years old
	death_date = 1772.2.18 #dies
}

advisor = {
	name = "Anders J. von H�pken" #one of Arvid Horn's most determined opponents and a founder of the Hat party 
	location = 1 #Stockholm
	skill = 2
	type = statesman
	date = 1742.3.31 #30 years old
	death_date = 1789.1.1 #dies
}

advisor = {
	name = "Pehr V. Wargentin" #Pehr Vilhelm Wargentin, Swedish astronomer, made studies on the moons of Jupiter 
	location = 10 #J�mtland
	skill = 2
	type = natural_scientist
	date = 1747.9.22 #30 years old
	death_date = 1783.12.13 #dies
}

advisor = {
	name = "Alexander Roslin" #Swedish painter
	location = 1 #Stockholm
	skill = 2
	type = artist
	date = 1748.1.1 #30 years old
	death_date = 1798.1.1 #dies
}

advisor = {
	name = "Johan Ernst Gunnerus" #Archbishop of Trondheim, naturalist
	location = 20 #Tr�ndelag
	skill = 2
	type = natural_scientist #could also be theologian
	date = 1748.1.1 #30 years old
	death_date = 1773.9.23 #dies
}

#also present as a general for Sweden
advisor = {
	name = "Augustin Ehrensv�rd" #Military architect, whose life work was Sveaborg
	location = 29 #Nyland
	skill = 3
	type = fortification_expert
	date = 1748.1.1 #Beginning of building Sveaborg, 38 years old
	death_date = 1772.1.1 #dies
}

advisor = {
	name = "Henric af Chapman" #Fredric Henric af Chapman, Swedish naval ship architect
	location = 7 #G�teborg
	skill = 3
	type = naval_reformer
	date = 1751.9.9 #30 years old
	death_date = 1808.8.19 #dies
}

advisor = {
	name = "Peter Ascanius" #Naturalist and geologist, student of Linneaus
	location = 23 #Bergenhus
	skill = 2
	type = natural_scientist
	date = 1753.1.1 #30 years old
	death_date = 1803.1.1 #dies
}

advisor = {
	name = "Georg Oeder" #Georg Christian Oeder. First director of the Danish Botanical Garden and reformed Danish 'lando'
	location = 12 #K�benhavn
	skill = 2
	type = treasurer #could also be natural_scientist
	date = 1758.2.3 #30 years old
	death_date = 1791.1.1 #dies
}

advisor = {
	name = "Ove H�egh-Guldberg" #Danish statesman, historian and de facto prime minister of Denmark, archconservative
	location = 15 #Jylland
	skill = 1
	type = statesman
	date = 1761.9.1 #30 years old
	death_date = 1808.2.7 #dies
}

advisor = {
	name = "Anders Chydenius" #Finnish classical liberalist economist, priest and politician
	location = 19 #Nederveteli, where he initially worked as priest and doctor
	skill = 2 #Some of his ideas were too revolutionary for the time
	type = trader #because of his ideas about free trade
	date = 1765.1.1 #36 years, was then sent to the riksdag
	death_date = 1803.2.1 #dies
}

advisor = {
	name = "Johann Struensee" #Johann Friedrich, Graf von Struensee. German doctor, became de facto regent of Denmark
	location = 12 #K�benhavn
	skill = 4
	type = statesman
	date = 1767.8.5 #30 years old
	death_date = 1772.4.28 #dies
}

advisor = {
	name = "Henric Liljensparre" #chief of police in Stockholm 
	location = 2 #Norrk�ping
	skill = 2
	type = spymaster
	date = 1768.7.22 #30 years old
	death_date = 1814.1.1 #dies
}

advisor = {
	name = "Carl Michael Bellman" #Swedish poet and composer, central figure in the Swedish song tradition  
	location = 1 #Stockholm
	skill = 5
	type = artist
	date = 1770.2.4 #30 years old
	death_date = 1795.2.11 #dies
}

advisor = {
	name = "Carl Wilhelm Scheele" #German-Swedish chemist, discovered oxygen before Joseph Priestley.  
	location = 1 #Stockholm
	skill = 3
	type = natural_scientist
	date = 1772.12.9 #30 years old
	death_date = 1786.5.21 #dies
}

advisor = {
	name = "Nikolaj Abildgaard" #Nikolaj Abraham Abildgaard, his works are scarcely known out of Denmark
	location = 12 #K�benhavn
	skill = 2
	type = artist
	date = 1773.9.11 #30 years old
	death_date = 1809.6.4 #dies
}

advisor = {
	name = "Caspar Wessel" #Mathemathican, forgotten inventor of the complex plane
	location = 17 #Akershus
	skill = 3
	type = natural_scientist
	date = 1775.6.8 #30 years old
	death_date = 1818.3.25 #dies
}

advisor = {
	name = "Christian Reventlow" #Christian Ditlev Frederik, Count Reventlow. Danish statesman and reformer
	location = 12 #K�benhavn
	skill = 3
	type = statesman
	date = 1778.1.1 #30 years old
	death_date = 1827.10.11 #dies
}

advisor = {
	name = "Johan H. Kellgren" #Johan Henric Kellgren. Swedish poet
	location = 1 #Stockholm
	skill = 1
	type = artist
	date = 1781.12.1 #30 years old
	death_date = 1795.1.1 #dies
}

advisor = {
	name = "Axel von Fersen" #Swedish statesman and diplomat
	location = 1 #Stockholm
	skill = 3
	type = diplomat
	date = 1785.9.4 #30 years old
	death_date = 1810.6.20 #dies
}

advisor = {
	name = "Leopold" #Carl Gustaf af Leopold, Swedish court poet 
	location = 2 #Norrk�ping
	skill = 2
	type = artist
	date = 1786.3.26 #30 years old
	death_date = 1829.1.1 #dies
}

advisor = {
	name = "Bengt Lidner" #Swedish poet
	location = 7 #G�teborg
	skill = 2
	type = artist
	date = 1787.3.16 #30 years old
	death_date = 1793.1.4 #dies
}

advisor = {
	name = "Peter A. Heiberg" #Peter Andreas Heiberg, Danish author and philologist
	location = 12 #Sjaelland
	skill = 1
	type = artist
	date = 1788.11.16 #30 years old
	death_date = 1841.4.30 #died
}

advisor = {
	name = "Carl L�wenhielm" #Swedish military officer, diplomat and politician
	location = 1 #Uppland
	skill = 2
	type = diplomat
	date = 1802.1.1 #30 years old
	death_date = 1861.1.1 #died
}

##################################################  
#End of file
##################################################  

