# Livonian Order become an autonomous branch of the Teutonic Order
vassal = {
	first = TEU
	second = LIV
	start_date = 1236.1.1 #Battle of Schaulen
	end_date = 1525.1.1 #Prussia is secularized
}

alliance = {
	first = TEU
	second = LIV
	start_date = 1236.1.1 #Battle of Schaulen
	end_date = 1525.1.1 #Prussia is secularized
}