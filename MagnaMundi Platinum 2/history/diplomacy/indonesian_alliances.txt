# Majapahit
union = {
	first = MAJ
	second = PLB
	start_date = 1377.1.1
	end_date = 1453.1.1
}

alliance = {
	first = MAJ
	second = PLB
	start_date = 1377.1.1
	end_date = 1453.1.1
}

# Siamese vassal
vassal = {
	first = AYU
	second = SUK
	start_date = 1399.1.1
	end_date = 1438.1.1
}

vassal = {
	first = AYU
	second = KHM
	start_date = 1474.1.1
	end_date = 1557.1.1
}