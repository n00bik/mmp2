#Mongols

#alliance = { 
#	first = OIR
#	second = MCH
#	start_date = 1448.1.1
#	end_date = 1454.1.1
#}

union = {
	first = OIR
	second = KHA
	start_date = 1402.1.1 #Orug Temur proclaims himself as the Great Khan
	end_date = 1403.1.1 #Defeated by Oljei Khan
}

# Most of Mongolia is united under Esen Khan
royal_marriage = {
	first = KHA
	second = OIR
	start_date = 1439.1.1
	end_date = 1451.8.1
}

vassal = {
	first = OIR
	second = KHA
	start_date = 1438.1.1
	end_date = 1451.8.1
}

#For 1453 start
#vassal = {
#	first = OIR
#	second = KHA
#	start_date = 1452.1.1
#	end_date = 1453.6.1
#}

union = {
	first = OIR
	second = KHA
	start_date = 1452.1.1
	end_date = 1454.6.1
}

alliance = {
	first = OIR
	second = KHA
	start_date = 1452.1.1
	end_date = 1454.6.1
}

vassal = {
	first = KHA
	second = GEO
	start_date = 1242.1.1 #Georgia surrenders
	end_date = 1327.1.1 #Georgia reasserts her independence
	}

#Not historical, only in for gameplay purposes
#alliance = {
#	first = MCH
#	second = KHA
#	start_date = 1399.1.1
#	end_date = 1636.1.1
#}
#alliance = {
#	first = MCH
#	second = OIR
#	start_date = 1399.1.1
#	end_date = 1636.1.1
#}
	
