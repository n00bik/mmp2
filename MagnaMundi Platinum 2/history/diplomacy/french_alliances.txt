# Burgundy - Brittany alliance for balance
alliance = {
	first = BUR
	second = BRI
	start_date = 1399.1.1
	end_date = 1477.1.5
}

# Provence and Lorraine under Ren� of Anjou
union = {
	first = PRO
	second = LOR
	start_date = 1434.11.16
	end_date = 1452.3.27
}

alliance = {
	first = PRO
	second = LOR
	start_date = 1434.11.16
	end_date = 1452.3.27
}

# Foix, a French vassal
vassal = {
	first = FRA
	second = FOI
	start_date = 1399.1.1
	end_date = 1479.2.13
}

# Orleanais, a French vassal
vassal = {
	first = FRA
	second = ORL
	start_date = 1399.1.1
	end_date = 1574.5.3
}

# Armagnac, a French vassal
vassal = {
	first = FRA
	second = AMG
	start_date = 1399.1.1
	end_date = 1549.12.22
}

# Provence, a French vassal
vassal = {
	first = FRA
	second = PRO
	start_date = 1399.1.1
	end_date = 1481.1.1
}

# Auvergne, a French vassal
vassal = {
	first = FRA
	second = AUV
	start_date = 1399.1.1
	end_date = 1589.1.5
}

# Bourbonnais, a French vassal
vassal = {
	first = FRA
	second = BOU
	start_date = 1399.1.1
	end_date = 1537.1.5
}

# France and Genoa
union = {
	first = FRA
	second = GEN
	start_date = 1458.1.1
	end_date = 1461.1.1
}

alliance = {
	first = FRA
	second = GEN
	start_date = 1458.1.1
	end_date = 1461.1.1
}

# Switzerland and Austria (League of Constance)
alliance = {
	first = SWI
	second = HAB
	start_date = 1474.3.30
	end_date = 1477.1.6
}	

# Switzerland and Lorraine (League of Constance)
alliance = {
	first = SWI
	second = LOR
	start_date = 1474.4.18
	end_date = 1477.1.6
}

# Auld Alliance
alliance = {
	first = FRA
	second = SCO
	start_date = 1399.1.1
	end_date = 1560.1.1
}

# Charles VII and Marie of Anjou
royal_marriage = {
	first = FRA
	second = NAP
	start_date = 1422.10.22
	end_date = 1461.7.22
}

# Amadeo IX of Savoy and Yolande of Valois
royal_marriage = {
	first = SAV
	second = FRA
	start_date = 1453.1.1
	end_date = 1472.3.30
}