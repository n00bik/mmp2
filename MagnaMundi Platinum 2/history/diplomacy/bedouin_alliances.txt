alliance = {
	first = HED
	second = ADE
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = HED
	second = NAJ
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = ADE
	second = NAJ
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = OMA
	second = HED
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = OMA
	second = ADE
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = NAJ
	second = OMA
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = ALH
	second = HED
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = ALH
	second = ADE
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = NAJ
	second = ALH
	start_date = 1444.1.1
	end_date = 1477.12.31
}

alliance = {
	first = OMA
	second = ALH
	start_date = 1444.1.1
	end_date = 1477.12.31
}