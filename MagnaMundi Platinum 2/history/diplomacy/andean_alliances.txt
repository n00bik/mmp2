#Inca dominance over the altiplano
vassal = {
	first = CUZ
	second = ICA
	start_date = 1444.1.1
	end_date = 1471.1.1 #Chinchas is annexed
}
vassal = {
	first = INC
	second = ICA
	start_date = 1471.1.1
	end_date = 1479.1.1 #Chinchas is annexed
}
vassal = {
	first = INC
	second = AYM
	start_date = 1472.1.1 #The Inca defeat an Aymara rebellion
	end_date = 1485.1.1 #The Aymara kingdoms are incorporated into the empire
}