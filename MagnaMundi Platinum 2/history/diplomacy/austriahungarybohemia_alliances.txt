# Silesia, a Bohemia vassal
vassal = {
	first = BOH
	second = SIL
	start_date = 1399.1.1
	end_date = 1526.8.29
}

# Brandenburg-Ansbach (Friedrich I)
union = {
	first = BRA
	second = ANS
	start_date = 1415.1.1
	end_date = 1440.9.21
}

# Brandenburg-Ansbach (Albrecht Achilles)
union = {
	first = BRA
	second = ANS
	start_date = 1470.2.10
	end_date = 1486.3.12
}

# Luxembourg-Brandenburg
union = {
	first = LUX
	second = BRA
	start_date = 1397.1.1
	end_date = 1411.1.18
}

# Hungary-Brandenburg
union = {
	first = HUN
	second = BRA
	start_date = 1411.1.18
	end_date = 1417.4.18
}

# Hungary-Bohemia # Sigismund
union = {
	first = HUN
	second = BOH
	start_date = 1419.8.16
	end_date = 1437.12.9
}

# Lower Austria-Hungary, Albrecht V
union = {
	first = HAB
	second = HUN
	start_date = 1437.12.9
	end_date = 1439.10.27
}

# Poland-Hungary
union = {
	first = POL
	second = HUN
	start_date = 1439.10.27
	end_date = 1444.11.11
}

# Lower Austria-Hungary, Ladislas
union = {
	first = HAB
	second = HUN
	start_date = 1452.1.1
	end_date = 1457.11.24
}

# Lower Austria-Bohemia, Ladislas is crowned king of Bohemia
union = {
	first = HAB
	second = BOH
	start_date = 1453.10.28
	end_date = 1457.11.24
}

# Brandenburg-Ansbach (Albrecht Achilles)
alliance = {
	first = BRA
	second = ANS
	start_date = 1470.2.10
	end_date = 1486.3.12
}

# Brandenburg-Ansbach (Friedrich I)
alliance = {
	first = BRA
	second = ANS
	start_date = 1415.1.1
	end_date = 1440.9.21
}

# Luxembourg-Brandenburg
alliance = {
	first = LUX
	second = BRA
	start_date = 1397.1.1
	end_date = 1411.1.18
}

# Hungary-Brandenburg
alliance = {
	first = HUN
	second = BRA
	start_date = 1411.1.18
	end_date = 1417.4.18
}

# Hungary-Bohemia
alliance = {
	first = HUN
	second = BOH
	start_date = 1419.8.16
	end_date = 1439.10.27
}

# Hungary-Bohemia, Ladislas is crowned king of Bohemia
alliance = {
	first = HUN
	second = BOH
	start_date = 1453.10.28
	end_date = 1457.11.24
}

# Lower Austria-Hungary, Albrecht V
alliance = {
	first = HAB
	second = HUN
	start_date = 1437.12.9
	end_date = 1439.10.27
}

# Poland-Hungary
alliance = {
	first = POL
	second = HUN
	start_date = 1439.10.27
	end_date = 1444.11.11
}

# Lower Austria-Hungary, Ladislas
alliance = {
	first = HAB
	second = HUN
	start_date = 1452.1.1
	end_date = 1457.11.24
}

# Vladislav Jagielo and Ana Cilli
royal_marriage = {
	first = POL
	second = HAB
	start_date = 1402.1.29
	end_date = 1416.5.21
}

# Albert V and Elisabeth
royal_marriage = {
	first = HAB
	second = HUN
	start_date = 1421.8.9
	end_date = 1437.12.9
}

# Matthias Corvinus of Hungary and Beatrice of Naples
royal_marriage = {
	first = HUN
	second = NAP
	start_date = 1476.1.1
	end_date = 1490.4.6
}

# Friedrich III ~ Eleanor (daughter of King Duarte I of Portugal)
royal_marriage = {
	first = HAB
	second = POR
	start_date = 1453.1.1
	end_date = 1467.9.3
}
