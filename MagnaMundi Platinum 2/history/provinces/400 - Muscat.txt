# # # # # # # # # # # # # #                                                 
#400 - Muscat                                                            
                                                              
owner = OMA                                                            
controller = OMA                                                            
culture = bedouin_arabic                                                            
religion = shiite                                                            
capital = "Muscat"                                                            
trade_goods = fish #Dominated by trade, dates and fishing                                                      
hre = no                                                            
base_tax = 3                                                            
manpower = 2                                                            
citysize = 2980                                                            
add_core = OMA  
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX

#East Africa
discovered_by = ADA
discovered_by = ETH
discovered_by = NUB
discovered_by = SOF
discovered_by = ZAN                                                            

discovered_by = indian discovered_by = india

1450.1.1  = { citysize = 3500 }                                                       
