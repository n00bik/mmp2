# # # # # # # # # # # # # # # # # # #                                            
#938 - Pamlico                                                            
                                                              
culture = catawba                                                            
religion = midewiwin                                                            
capital = "Pamlico"                                                            
trade_goods = unknown #tobacco                                                           
hre = no                                                            
base_tax = 4                                                            
manpower = 1                                                            
native_size = 10                                                            
native_ferocity = 2                                                            
native_hostileness = 8                                                            
                                                              
