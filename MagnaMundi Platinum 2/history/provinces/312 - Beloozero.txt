# # # # # # # # # # # # # # # # # # #                                            
#312 - Beloozero                                                            
                                                              
owner = RUS # actually a Belozersk duchy from 1238                                                     
controller = RUS                                                            
culture = russian                                                            
religion = orthodox                                                            
hre = no                                                            
base_tax = 4 # down from 6, Vologda went up to compensate                                                   
trade_goods = naval_supplies                                                            
manpower = 4                                                            
capital = "Beloozero"                                                            
citysize = 3100                                                            
fort1 = yes                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ                                                            
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB                                                            
discovered_by = TIM
discovered_by = TOX
                                                              
1375.1.1  = { owner = MOS                                                        
  controller = MOS                                                          
  add_core = MOS                                                          
     }                                                         
1450.1.1  = { citysize = 3840 }                                                       
