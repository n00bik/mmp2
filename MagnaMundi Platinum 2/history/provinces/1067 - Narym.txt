# # # # # # # #                                                       
#1067 - Narym                                                            
                                                              
culture = yakut                                                            
religion = shamanism                                                            
capital = "Narym"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 3                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 2                                                            
native_hostileness = 3                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                              
