# # # # # # # # # # # # # # # #                                               
#1176 - Roggeveld                                                            
                                                              
culture = khoisan                                                            
religion = fetishist                                                            
capital = "Roggeveld"                                                            
trade_goods = unknown #gold                                                           
hre = no                                                            
base_tax = 3                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 0.5                                                            
native_hostileness = 1                                                            
                                                              
