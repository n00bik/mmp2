# # # # # # # # # # # # # # # # #                                              
#866 - Guaycura                                                            
                                                              
culture = piman                                                            
religion = animism                                                            
capital = "Guaycura"                                                            
trade_goods = unknown #fish                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 2                                                            
                                                              
