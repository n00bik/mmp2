# # # # # # # # # # # # # # #                                                
#410 - Iraq-i-Arab                                                            
                                                              
owner = JAI                                                            
controller = JAI                                                            
culture = al_iraqiya_arabic                                                            
religion = sunni                                                            
capital = "Bagdad"                                                            
trade_goods = hemp                                                            
hre = no                                                            
base_tax = 10                                                            
manpower = 4                                                            
citysize = 40150                                                            
fort1 = yes                                                            
temple = yes                                                            
add_core = IRQ            
discovered_by = indian discovered_by = india
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                              
1344.1.1 = { #Effective Jalayarid independence from the Ilkhanate                                                      
 owner = JAI                                                           
 controller = JAI                                                           
 }                                                             
1393.1.1 = { #Timur's campaigns against the Jalayarids drive them out of Baghdad                                                  
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
#1401.6.1   = { controller = TIM }                                                      
1402.1.1   = { controller = JAI }                                                      
1403.1.1   = { controller = TIM }                                                      
1405.1.1  = { controller = JAI owner = JAI }                                                    
1411.1.1   = { owner = QAR                                                       
  controller = QAR                                                          
  add_core = QAR                                                          
      } # Qara Koyunlu                                                     
1469.1.1   = { owner = AKK                                                       
  controller = AKK                                                          
  remove_core = QAR
    }                                                          
1474.1.1   = { revolt_risk = 4 } # Rebellion                                                    
1475.1.1   = { revolt_risk = 0 }                                                      
