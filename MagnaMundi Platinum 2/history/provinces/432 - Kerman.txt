# # # # # # # # # # # # # # # #                                               
#432 - Kerman                                                            
                                                              
owner = KHO                                                            
controller = KHO                                                            
culture = persian                                                            
religion = sunni                                                            
capital = "Kerman"                                                            
trade_goods = spices                                                            
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
citysize = 8102                                                            
add_core = KHO                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                            
discovered_by = indian discovered_by = india                                                           
                                                              
1370.1.1 = { #Approximate date of conquest                                                        
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
1405.1.1 = { #Timur dies and his empire breaks up                                                     
 owner = KHO                                                           
 controller = KHO                                                           
 }                                                             
1409.1.1 = { #Shah Rukh reunites the empire                                                       
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
1449.10.27 = { #Ulugh Beg's death splits the empire again                                                     
 owner = KHO                                                           
 controller = KHO                                                           
 }                                                             
1450.1.1 = { citysize = 8600 }                                                        
1459.4.1 = {                                                            
 owner = QAR                                                           
 controller = QAR                                                           
 } #The Qara Koyunlu invade Khorasan                                                        
1469.8.27 = {                                                            
 owner = KHO                                                           
 controller = KHO                                                           
 } #The empire is divided after Abu Sa'id dies                                                     
