# # # # # # # # # # # # #                                                  
#998 - Ungava                                                            
                                                              
culture = inuit                                                            
religion = animism                                                            
capital = "Ungava"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 1                                                            
                                                              
