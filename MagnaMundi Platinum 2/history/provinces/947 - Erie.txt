# # # # # # # # # # # # # # # # # # #                                            
#947 - Erie                                                            
                                                              
culture = iroquois                                                            
religion = midewiwin                                                            
capital = "Erie"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 3                                                            
manpower = 5                                                            
native_size = 5                                                            
native_ferocity = 0.5                                                            
native_hostileness = 4                                                            
                                                              
