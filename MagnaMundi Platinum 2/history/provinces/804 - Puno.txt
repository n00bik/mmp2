# # # # # # # # # # # # # # # # # # # # #                                          
#804 - Ayaviri                                                            
                                                              
owner = CUZ                                                            
controller = CUZ                                                            
culture = aymara                                                            
religion = animism                                                            
capital = "Ayaviri"                                                            
trade_goods = gold                                                            
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
citysize = 1250
add_core = CUZ                                                            
discovered_by = INC                                                            
discovered_by = CUZ                                                            
discovered_by = QTO                                                            
discovered_by = ICA                                                            
discovered_by = CHM                                                            
discovered_by = HUA                                                            
discovered_by = AYM                                                            
                                                              
1450.1.1  = { citysize = 1950 }
1471.1.1 = {                                                            
 owner = INC                                                           
 controller = INC                                                           
 add_core = INC                                                           
 remove_core = CUZ                                                           
 base_tax = 3                                                           
 manpower = 2                                                           
 }                                                             
