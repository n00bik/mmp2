# # # # # # # # # # # # # # # # # # # # #                                          
#1813 - Thessaly                                                            
                                                              
owner = BYZ                                                            
controller = BYZ                                                            
culture = greek                                                            
religion = orthodox                                                            
capital = "Larissa"                                                            
trade_goods = leather                                                            
hre = no                                                            
base_tax = 5                                                            
manpower = 2                                                            
citysize = 26887                                                            
fort1 = yes                                                            
add_core = BYZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                              
1204.3.1 = { #Fourth Crusade                                                          
 owner = GRE #Thessaly                                                          
 controller = GRE                                                           
 }                                                             
1215.1.1 = { #Conquered by the Despotate of Epirus                                                      
 owner = ALB                                                           
 controller = ALB                                                           
 }                                                             
1239.1.1 = { #Reconquered by Thessaly                                                         
 owner = GRE                                                           
 controller = GRE                                                           
 }                                                             
1241.1.1 = { #Epirus reconquers it                                                         
 owner = ALB                                                           
 controller = ALB                                                           
 }                                                             
1268.1.1 = { #Ruled by a different family                                                       
 owner = GRE                                                           
 controller = GRE                                                           
 }                                                             
1335.1.1 = { #Byzantines take advantage of Epirote invasion to conquer Thessaly                                                   
 owner = BYZ                                                           
 controller = BYZ                                                           
 }                                                             
1348.1.1 = { #Incorporated into the Serbian Empire                                                       
 owner = SER                                                           
 controller = SER                                                           
 }                                                             
1373.1.1 = { #Angeloi conquest                                                          
 owner = BYZ                                                           
 controller = BYZ                                                           
 }                                                             
1394.1.1 = { #Conquered by Ottomans                                                         
 owner = TUR                                                           
 controller = TUR                                                           
 }                                                             
1402.1.1  = { revolt = { type = pretender_rebels size = 0 } controller = REB } # Interregnum                                           
1403.1.1  = { revolt = {} owner = BYZ controller = BYZ } #Given to Byzantium in return for aid                                          
1423.1.1  = { owner = VEN #Thessalonicans ask the Venetians to take over                                                 
  controller = VEN                                                          
  add_core = VEN                                                          
     } # Sold to Venice                                                     
1430.3.29 = { owner = TUR                                                         
  controller = TUR                                                          
  add_core = TUR                                                          
  remove_core = VEN                                                          
     } # To the Ottoman Empire                                                    
1453.5.29  = { remove_core = BYZ } #Fall of the empire                                                   
