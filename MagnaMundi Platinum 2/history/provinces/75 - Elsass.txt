# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #                                
# 75 Elsass - Principal cities: Strasbourg                                                        
                                                              
owner = ALS                                                            
controller = ALS                                                            
capital = "Stra�burg"                                                            
citysize = 7000                                                            
culture = swabian                                                            
religion = catholic                                                            
hre = yes                                                            
base_tax = 7                                                            
trade_goods = wine                                                            
manpower = 5                                                            
add_core = ALS                                                            
fort1 = yes                                                            
marketplace = yes # Trade Center                                                         
temple = yes # La Notre Dame de Strasbourg (finished 1439)                                                    
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1450.1.1   = { citysize = 8000 }                                                      
1467.6.15  = { add_core = BUR } # Charles the Bold ascends and lays claims on the Alsace                                            
1469.1.1   = { owner = BUR controller = BUR } # Upper Alsace given to Charles the Bold by Archduke Sigismund                                        
1474.1.1   = { revolt_risk = 7 } # Breisgau and Upper Alsace revolt against Charles the Bold                                            
1474.5.9   = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # Rebels arrest & execute Charles' puppet von Hagenbach                                   
1477.1.5   = { revolt = {} owner = ALS controller = ALS remove_core = BUR revolt_risk = 0 } # Charles the Bold dies, Alsace re-established                                   
