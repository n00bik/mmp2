# # # # # # # # # #                                                     
#1039 - Verkhoyansk                                                            
                                                              
culture = inuit                                                            
religion = animism                                                            
capital = "Verkhoyansk"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 3                                                            
                                                              
