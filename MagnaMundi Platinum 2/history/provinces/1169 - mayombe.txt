# # # # # # # # # #                                                     
#1169 - Mayombe                                                            
                                                              
owner = KON                                                            
controller = KON                                                            
culture = kongolese                                                            
religion = fetishist                                                            
capital = "Mayombe"                                                            
trade_goods = slaves                                                            
hre = no                                                            
base_tax = 3                                                            
manpower = 5                                                            
citysize = 1480                                                            
add_core = KON                                                            
#Central Africa
discovered_by = LOA
discovered_by = KON
                                                              
1450.1.1 = { citysize = 1670 }                                                        
