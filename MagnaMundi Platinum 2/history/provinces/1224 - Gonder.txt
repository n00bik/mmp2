# # # # # # # # # # # # # # # # # # # # # # # # # # #                                    
#1224 - Gonder                                                            
                                                              
owner = ETH                                                            
controller = ETH                                                            
add_core = ETH                                                            
culture = ethiopian                                                            
religion = oriental                                                        
capital = "Amba Geshen"                                                           
citysize = 6000                                                            
manpower = 2                                                            
trade_goods = indigo #grain                                                           
hre = no                                                            
base_tax = 2                                                            
#East Africa
discovered_by = ADA
discovered_by = ETH
discovered_by = NUB
discovered_by = SOF
discovered_by = ZAN                                                            

#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1450.1.1   = { citysize = 7000 }                                                      
1475.1.1   = { revolt_risk = 5 } #Repression against the Falasha by Baeda Maryam                                               
1476.1.1   = { revolt_risk = 0 }                                                      
