# # # # # # # # # # # # # # # # # #                                             
#1220 - Dar Ja'al                                                           
                                                              
culture = nubian                                                            
religion = fetishist                                                            
capital = "Al Damar"                                                           
trade_goods = unknown
hre = no                                                            
base_tax = 1   
native_size = 50                                                            
native_ferocity = 4.5                                                            
native_hostileness = 9                                                            
#East Africa
discovered_by = ADA
discovered_by = ETH
discovered_by = NUB
discovered_by = SOF
discovered_by = ZAN                                                            

