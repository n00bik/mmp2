# # # # # # # # # # # # # # # # # # # # # # # # #                                      
#265 - Moravia                                                            
                                                              
owner = BOH                                                            
controller = BOH                                                            
capital = "Brunn"                                                            
culture = czech                                                            
religion = catholic                                                            
trade_goods = wine                                                            
hre = yes                                                            
base_tax = 3                                                            
manpower = 2                                                            
citysize = 9000                                                            
fort1 = yes                                                            
add_core = BOH                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1419.12.1 = { revolt = { type = heretic_rebels size = 2 } controller = REB } # Hussite Wars, Battle of Nekmir
1436.7.5  = { revolt = {} controller = BOH } # Peace is signed in Jihlava
1457.1.1  = { revolt_risk = 5 regimental_camp = yes } # George of Podiebrand had to secure recognition from the German and Catholic towns                                      
1464.1.1  = { revolt_risk = 1 } # The Catholic nobility is still undermines the crown.                                              
1471.1.1  = { revolt_risk = 0 }                                                       
1474.1.1 = { #Hungary wins Silesia, Moravia and Lusatia from Bohemia in the crusades                                                 
 owner = HUN                                                           
 controller = HUN                                                           
 }                                                             
