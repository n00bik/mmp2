# # # # # # # # # # # # # # # # # # # #                                           
#916 - Kaskaskia                                                            
                                                              
culture = illinois                                                            
religion = midewiwin                                                            
capital = "Kaskaskia"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
native_size = 10                                                            
native_ferocity = 1                                                            
native_hostileness = 0                                                            
                                                              
