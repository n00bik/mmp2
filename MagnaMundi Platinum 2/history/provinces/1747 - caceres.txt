# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #                           
#1747 - Caceres                                                            
                                                              
owner = CAS  #Enrique III of Castille                                                       
controller = CAS                                                            
add_core = CAS                                                            
culture = castillian                                                            
religion = catholic                                                            
hre = no                                                            
base_tax = 3                                                            
trade_goods = wool                                                            
manpower = 2                                                            
fort1 = yes                                                            
capital = "Caceres"                                                            
citysize = 2500                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1464.5.1   = { revolt_risk = 3 } #Nobiliary uprising against King Enrique, Castilla goes into anarchy                                             
1468.9.18  = { revolt_risk = 0 } #Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved                                   
1470.1.1   = { revolt_risk = 3 } #Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.                           
1475.6.2   = { controller = POR }                                                      
