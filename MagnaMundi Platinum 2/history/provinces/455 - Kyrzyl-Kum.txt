#455 - Kyrzyl-Kum # # # # # # # # # # # # # # # # # # #                                         
                                                              
owner = TOX                                                            
controller = TOX                                                            
culture = khazak	# uzbehk                                                           
religion = sunni                                                            
capital = "Kyzyl Kum"                                                           
trade_goods = salt                                                            
hre = no                                                            
base_tax = 1                                                            
manpower = 2                                                            
citysize = 1032                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX

1370.1.1 = { #Approximate date of conquest                                                        
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
1405.1.1 = { #Timur dies and his empire breaks up                                                     
 owner = TOX                                                           
 controller = TOX                                                           
 }                                                             
1409.1.1 = { #Shah Rukh reunites the empire                                                       
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
1446.1.1  = { owner = SHY controller = SHY add_core = SHY }
1450.1.1  = { citysize = 1400 }                                                       
1462.11.1 = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # Besieged by Timurid rebels                                         
1463.1.1  = { revolt = {} controller = SHY }                                                    
