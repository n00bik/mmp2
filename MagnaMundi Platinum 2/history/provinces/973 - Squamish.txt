# # # # # # # # # # # # # # # # #                                              
#973 - Squamish                                                            
                                                              
culture = salish                                                            
religion = midewiwin                                                            
capital = "Squamish"                                                            
trade_goods = unknown #naval_supplies                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 6                                                            
                                                              
