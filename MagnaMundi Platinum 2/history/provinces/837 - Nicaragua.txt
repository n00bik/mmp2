# # # # # # # # # # # # # # # #                                               
#837 - Nicaragua                                                            
                                                              
culture = carib                                                            
religion = animism                                                            
capital = "Nicaragua"                                                            
trade_goods = unknown #coffee                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 20                                                            
native_ferocity = 2                                                            
native_hostileness = 5                                                            
discovered_by = MAY                                                            
                                                              
