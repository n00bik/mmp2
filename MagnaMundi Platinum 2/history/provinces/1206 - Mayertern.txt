# # # # # # # # # # # # # # # # # # # # # # # # #                                      
#1206 - Majerteen                                                            
                                                              
culture = somali                                                            
religion = sunni                                                            
capital = "Hobyo"                                                            
trade_goods = unknown #wool                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 50                                                            
native_ferocity = 4.5                                                            
native_hostileness = 9                                                            
#East Africa
discovered_by = ADA
discovered_by = ETH
discovered_by = NUB
discovered_by = SOF
discovered_by = ZAN                                                            

#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
