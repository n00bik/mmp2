# # # # # # # # # # # # # # # # # # # # # #                                         
#1187 - Urungwe                                                            
                                                              
owner = ZIM                                                            
controller = ZIM                                                            
add_core = ZIM                                                            
culture = shona                                                            
religion = fetishist                                                            
capital = "Matarira"                                                            
citysize = 5000                                                            
manpower = 2                                                            
trade_goods = ivory                                                            
hre = no                                                            
base_tax = 2                                                            
#East Africa
discovered_by = SOF
discovered_by = ZAN                                                            
discovered_by = ZIM                                                          
                                                              
1450.1.1 = { citysize = 6000 }                                                        
