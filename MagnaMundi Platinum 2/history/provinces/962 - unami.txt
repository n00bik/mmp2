# # # # # # # # # # # # # # # # # # # # #                                          
#962 - Unami                                                            
                                                              
culture = delaware                                                            
religion = midewiwin                                                            
capital = "Unami"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 5                                                            
manpower = 1                                                            
native_size = 15                                                            
native_ferocity = 2                                                            
native_hostileness = 8                                                            
                                                              
