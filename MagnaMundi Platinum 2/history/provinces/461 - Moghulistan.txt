# # # # # # # # # # # #                                                   
#461 - Moghulistan                                                            
                                                              
owner = CHG                                                            
controller = CHG                                                            
culture = chagatai                                                            
religion = sunni                                                            
capital = "Kulja"                                                            
trade_goods = wool                                                            
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
citysize = 3120                                                            
add_core = CHG                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR
                                                            
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG                                                            
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER                                                            
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                              
1444.1.1 = { controller = OIR }                                                        
1445.1.1 = { controller = CHG }                                                        
1450.1.1 = { citysize = 3800 }
1469.1.1 = { capital = "Aksu" }                                                         
