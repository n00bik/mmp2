# # # # # # # # # # # # # # # # # #                                             
#343 - Fez                                                            
                                                              
owner = MOR                                                            
controller = MOR                                                            
culture = berber                                                            
religion = sunni                                                            
capital = "Fez"                                                            
trade_goods = cloth                                                            
hre = no                                                            
base_tax = 7                                                            
manpower = 4                                                            
citysize = 10440                                                            
fort1 = yes                                                            
add_core = MOR                                                            
temple = yes # Kariouyine mosque                                                         
discovered_by = latin discovered_by = western_europe                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR  

#West Africa
discovered_by = MAL
discovered_by = ASH
discovered_by = BEN
discovered_by = HAU
discovered_by = JOL
discovered_by = KBO
discovered_by = MSI
discovered_by = OYO
discovered_by = SON
discovered_by = SOK
discovered_by = SEG
discovered_by = YAO                                                           
                                                              
1000.1.1   = { Defendable_Position = yes }                                                      
                                                              
1450.1.1 = { citysize = 11050 }                                                        
