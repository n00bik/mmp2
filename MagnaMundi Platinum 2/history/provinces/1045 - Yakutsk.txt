# # # # # # # # # # #                                                    
#1045 - Yakutsk                                                            
                                                              
#1045 - Yakutsk                                                            
culture = yakut                                                            
religion = shamanism                                                            
capital = "Yakutsk"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 3                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 3                                                            
                                                              
