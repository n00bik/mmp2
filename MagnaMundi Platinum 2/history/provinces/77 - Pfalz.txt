# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #                             
#77 - Pfalz                                                            
                                                              
capital = "Speyer"                                                            
culture = hessian                                                            
religion = catholic                                                            
trade_goods = wine                                                            
owner = PAL                                                            
base_tax = 6                                                            
manpower = 2                                                            
fort1 = yes                                                            
citysize = 10000                                                            
controller = PAL                                                            
add_core = PAL                                                            
hre = yes                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1444.1.1   = { temple = yes marketplace = yes } # The Kaiserdom in Speyer is on of the oldest and greatest in the HRE, built over the 11th and 12th century                             
1450.1.1   = { citysize = 12000 }                                                      
