# # # # # # # # # # # # # # # # #                                              
#937 - Tuscarora                                                            
                                                              
culture = catawba                                                            
religion = midewiwin                                                            
capital = "Tuscarora"                                                            
trade_goods = unknown #tobacco                                                           
hre = no                                                            
base_tax = 3                                                            
manpower = 2                                                            
native_size = 5                                                            
native_ferocity = 0.5                                                            
native_hostileness = 4                                                            
                                                              
