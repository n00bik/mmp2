# # # # # # # # # # # # # # # #                                               
#847 - Zapotec                                                            
                                                              
owner = TLA                                                            
controller = TLA                                                            
culture = nahua                                                            
religion = teotl                                                            
capital = "Tehuantepec" #Large Zapotec town known for its assertive women                                                    
trade_goods = cotton                                                            
hre = no                                                            
base_tax = 3                                                            
manpower = 2                                                            
citysize = 8910 #1568 Survey                                                          
add_core = TLA                                                            
fort1 = yes                                                            
discovered_by = MAY                                                            
discovered_by = CHC                                                            
discovered_by = MHC                                                            
discovered_by = TLA                                                            
discovered_by = AZT                                                            
                                                              
