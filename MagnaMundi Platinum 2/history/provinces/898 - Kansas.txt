# # # # # # # # # # # #                                                   
#898 - Kansas                                                            
                                                              
culture = pawnee                                                            
religion = midewiwin                                                            
capital = "Kansas"                                                            
trade_goods = unknown #grain                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 30                                                            
native_ferocity = 3                                                            
native_hostileness = 9                                                            
                                                              
