#479 - Kerey # # # # # # # # #                                                   
                                                              
owner = GOL
controller = GOL
culture = khazak                                                            
religion = sunni                                                            
capital = "Kerey"                                                            
trade_goods = wool                                                            
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
citysize = 1340                                                            
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH                                                            
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB                                                            
discovered_by = TIM
discovered_by = TOX
                                                              
1428.1.1 = { owner = SHY controller = SHY }
1450.1.1 = { citysize = 1940 }
1466.1.1 = { owner = KZH controller = KZH add_core = KZH }                                                     
