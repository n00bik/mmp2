# # # # # # # # # # # # #                                                  
#1009 - Atikaki                                                            
                                                              
culture = assiniboine                                                            
religion = midewiwin                                                            
capital = "Manitoba"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 10                                                            
native_ferocity = 1                                                            
native_hostileness = 7                                                            
                                                              
