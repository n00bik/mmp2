# # # # # # #                                                        
#1105 - Vestbygden                                                            
                                                              
culture = inuit                                                            
religion = animism                                                            
capital = "Vestbygden"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 2                                                            
native_hostileness = 9                                                            
                                                              
