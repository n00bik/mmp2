# # # # # # # # # # # # # # # # # # #                                            
#1121 - Bure                                                            
                                                              
owner = MAL                                                            
controller = MAL                                                            
culture = mali                                                            
citysize = 14000                                                            
manpower = 2                                                            
religion = fetishist                                                            
capital = "Kurussa"                                                            
trade_goods = gold                                                            
hre = no                                                            
base_tax = 8                                                            
add_core = MAL                                                            
#West Africa
discovered_by = MAL
discovered_by = ASH
discovered_by = BEN
discovered_by = HAU
discovered_by = JOL
discovered_by = KBO
discovered_by = MSI
discovered_by = OYO
discovered_by = SON
discovered_by = SOK
discovered_by = SEG
discovered_by = YAO                                                          
                                                              
1450.1.1 = { citysize = 15000 }                                                        
