# # # # # # # # # # # # # # # # #                                              
#881 - Piro                                                            
                                                              
culture = comanche                                                            
religion = animism                                                            
capital = "Acoma"                                                            
trade_goods = unknown #salt                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 15                                                            
native_ferocity = 3                                                            
native_hostileness = 5                                                            
                                                              
