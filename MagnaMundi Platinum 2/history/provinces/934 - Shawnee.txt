# # # # # # # # # # #                                                    
#934 - Shawnee                                                            
                                                                                                                         
culture = shawnee                                                            
religion = midewiwin                                                            
capital = "Shawnee"                                                                                                                     
hre = no                                                            
base_tax = 1                                                            
manpower = 1  
trade_goods = unknown 
native_size = 12                                                            
native_ferocity = 5                                                            
native_hostileness = 9 
                                                  
