# # # # # # # # # # # # #                                                  
#723 - Xilin Gol                                                           
                                                              
owner = KHA  # The Oirads united much of Mongolia                                                    
controller = KHA                                                            
culture = mongol                                                            
religion = shamanism                                                            
capital = "Xilin Gol"                                                           
trade_goods = wool                                                            
hre = no                                                            
base_tax = 2                                                            
manpower = 3                                                            
citysize = 5525                                                            
add_core = KHA                                                            
#Asia
discovered_by = AKG
discovered_by = AKM
discovered_by = ANG
discovered_by = ANN
discovered_by = ARK
discovered_by = ASK
discovered_by = ASN
discovered_by = ASS
discovered_by = AYU
discovered_by = BHU
discovered_by = CHU
discovered_by = CSK
discovered_by = DAI
discovered_by = DAL
discovered_by = DTE
discovered_by = HJO
discovered_by = HKW
discovered_by = HTK
discovered_by = IGW
discovered_by = JAP
discovered_by = JIN
discovered_by = JTG
discovered_by = KHM
discovered_by = KNO
discovered_by = KOR
discovered_by = LNA
discovered_by = LUA
discovered_by = LXA
discovered_by = MCH
discovered_by = MGM
discovered_by = MIN
discovered_by = MIY
discovered_by = MNG
discovered_by = MRI
discovered_by = NHN
discovered_by = NNB
discovered_by = ODA
discovered_by = OTM
discovered_by = OUC
discovered_by = PEG
discovered_by = QII
discovered_by = QIN
discovered_by = QNG
discovered_by = RYU
discovered_by = RZJ
discovered_by = SHN
discovered_by = SHU
discovered_by = SMZ
discovered_by = SST
discovered_by = STM
discovered_by = STO
discovered_by = SUK
discovered_by = TAU
discovered_by = TGW
discovered_by = TIB
discovered_by = TKD
discovered_by = TKI
discovered_by = USG
discovered_by = VIE
discovered_by = WUU
discovered_by = WYU
discovered_by = XIA
discovered_by = YMN
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX

discovered_by = indian discovered_by = india                                                            
                                                              
1450.1.1 = { citysize = 5970 }
1452.1.1 = { controller = OIR owner = OIR }                                                     
1453.6.1 = { revolt_risk = 5 }                                                        
1455.1.1 = { controller = KHA owner = KHA revolt_risk = 3 }                                                  
