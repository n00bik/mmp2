# # # # # # # # # # # # # # # # # # # # # # # # # # #                                    
# 174 Gascogne - Principal cities: Bordeaux                                                        
                                                              
owner = ENG                                                            
controller = ENG                                                            
capital = "Bordeaux"                                                            
citysize = 15500                                                            
culture = gascon                                                            
religion = catholic                                                            
hre = no                                                            
base_tax = 6                                                            
trade_goods = wine                                                            
manpower = 5                                                            
add_core = ENG                                                            
add_core = FRA                                                            
add_core = AMG
fort1 = yes                                                            
marketplace = yes                                                            
#university = yes # L'Université de Bordeaux                                                        
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1450.1.1   = { citysize = 17500 }                                                      
1451.6.30  = { owner = FRA controller = FRA }    
1452.10.23  = { controller = ENG }    
1453.7.18  = { controller = FRA }                                                    
1466.1.1   = { temple = yes } # Cathedral of Bordeaux finished                                                 
1475.8.29  = { remove_core = ENG } # Treaty of Picquigny
