# # # # # # # # # # # # # # #                                                
#1099 - Socotra                                                            
                                                              
owner = ADE                                                            
controller = ADE                                                            
culture = somali                                                            
religion = oriental #Nestorian                                                       
capital = "Socotra"                                                            
trade_goods = fish                                                            
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
citysize = 1000                                                            
add_core = ADE
discovered_by = indian discovered_by = india
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
#East Africa
discovered_by = ADA
discovered_by = ETH
discovered_by = NUB
discovered_by = SOF
discovered_by = ZAN                                                            
                                                              
1000.1.1   = { Defendable_Position = yes }                                                      

1450.1.1 = { citysize = 1200 }
