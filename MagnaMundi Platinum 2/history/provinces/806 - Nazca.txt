# # # # # # # # # # # # # # # # # # # # #                                          
#806 - Nazca                                                            
                                                              
owner = ICA                                                            
controller = ICA                                                            
culture = nazca                                                            
religion = animism                                                            
capital = "Nazca"                                                            
trade_goods = fish                                                            
hre = no                                                            
base_tax = 3                                                            
manpower = 2                                                            
citysize = 1850                                                            
add_core = ICA                                                            
discovered_by = INC                                                            
discovered_by = CUZ                                                            
discovered_by = QTO                                                            
discovered_by = ICA                                                            
discovered_by = CHM                                                            
discovered_by = HUA                                                            
discovered_by = AYM                                                            
                                                              
1450.1.1  = { citysize = 2100 }
1475.1.1 = {                                                            
  owner = INC                                                          
  controller = INC                                                          
  }                                                            
