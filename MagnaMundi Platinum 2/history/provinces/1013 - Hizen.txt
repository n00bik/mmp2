# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #                                 
#1013 - Hizen (including Chikuzen)                                                          
                                                              
owner = SHN                                                            
controller = SHN                                                            
culture = kyushu                                                            
religion = shinto                                                            
capital = "Fukuoka"                                                            
trade_goods = fish #or tea, which grows well there; Hizen is mountainous and is not self-sufficient for rice and beans                                           
hre = no                                                            
base_tax = 9                                                            
manpower = 6                                                            
citysize = 7200 #50000 in 1884 (Nagasaki has 40000)                                                      
add_core = SHN                                                            
add_core = JAP                                                            
add_core = RZJ                                                            
fort1 = yes                                                            
#Asia
discovered_by = AKG
discovered_by = AKM
discovered_by = ANG
discovered_by = ANN
discovered_by = ARK
discovered_by = ASK
discovered_by = ASN
discovered_by = ASS
discovered_by = AYU
discovered_by = BHU
discovered_by = CHU
discovered_by = CSK
discovered_by = DAI
discovered_by = DAL
discovered_by = DTE
discovered_by = HJO
discovered_by = HKW
discovered_by = HTK
discovered_by = IGW
discovered_by = JAP
discovered_by = JIN
discovered_by = JTG
discovered_by = KHM
discovered_by = KNO
discovered_by = KOR
discovered_by = LNA
discovered_by = LUA
discovered_by = LXA
discovered_by = MCH
discovered_by = MGM
discovered_by = MIN
discovered_by = MIY
discovered_by = MNG
discovered_by = MRI
discovered_by = NHN
discovered_by = NNB
discovered_by = ODA
discovered_by = OTM
discovered_by = OUC
discovered_by = PEG
discovered_by = QII
discovered_by = QIN
discovered_by = QNG
discovered_by = RYU
discovered_by = RZJ
discovered_by = SHN
discovered_by = SHU
discovered_by = SMZ
discovered_by = SST
discovered_by = STM
discovered_by = STO
discovered_by = SUK
discovered_by = TAU
discovered_by = TGW
discovered_by = TIB
discovered_by = TKD
discovered_by = TKI
discovered_by = USG
discovered_by = VIE
discovered_by = WUU
discovered_by = WYU
discovered_by = XIA
discovered_by = YMN                                                            
                                                              
1433.3.1 = { controller = OUC }                                                        
1433.10.1 = { owner = OUC } #Shoni defeated, fled to Tsushima                                                   
1446.1.1 = { owner = SHN controller = SHN } #allowed to return to Dazaifu by Shogun                                              
1450.1.1   = { citysize = 8800 controller = OUC }                                                   
1450.2.1 = { owner = OUC } #Shoni driven out of Dazaifu, fled to Hizen                                                
#1465.1.1 = { owner = SHN controller = SHN } #Shoni Noriyori appointed as Chikuzen Shugo                                               
1469.8.1 = { owner = SHN controller = SHN } #drove out Ouchi from Chikuzen                                                
