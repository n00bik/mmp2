# # # # # # # # # # #                                                    
#1071 - Tara                                                            
                                                              
owner = GOL
controller = GOL
culture = sibir                                                            
religion = sunni                                                            
capital = "Tara"                                                            
trade_goods = grain                                                            
hre = no                                                            
base_tax = 5                                                            
manpower = 1                                                            
citysize = 1000                                                            
add_core = GOL                                                
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ                                                            
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                              
1428.1.1 = { owner = SHY controller = SHY remove_core = GOL }
1450.1.1 = { citysize = 1400 } # Estimated
1468.1.1 = { owner = SIB controller = SIB add_core = SIB } # Sibir khanate formed
