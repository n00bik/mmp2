# # # # # # # # # # # # # # # # # # # # #                                          
#778 - Rio de la Plata                                                         
                                                              
culture = guajiro                                                            
religion = animism                                                            
capital = "Rio de la Plata"                                                         
trade_goods = unknown #fish                                                           
hre = no                                                            
base_tax = 6                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 1                                                            
                                                              
