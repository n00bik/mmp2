# # # # # # # # # #                                                     
#649 - Ternate                                                            
                                                              
culture = papuan                                                            
religion = animism                                                            
capital = "Ternate"                                                            
trade_goods = unknown #spices                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
native_size = 5                                                            
native_ferocity = 0.5                                                            
native_hostileness = 1                                                            
                                                              
