# # # # # # # # # # # # # #                                                 
#446 - Herat                                                            
                                                              
owner = KHO                                                            
controller = KHO                                                            
culture = pashtun                                                            
religion = sunni                                                            
capital = "Herat"                                                            
trade_goods = wool                                                            
hre = no                                                            
cot = no                                                            
base_tax = 5                                                            
manpower = 6                                                            
citysize = 4310                                                            
add_core = KHO                                                            
fort1 = yes                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX
                                                            
discovered_by = indian discovered_by = india                                                           
                                                              
1389.1.1 = { #Conquest of Herat from the Kartids                                                      
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
1405.1.1 = { #Timur dies and his empire breaks up                                                     
 owner = KHO                                                           
 controller = KHO                                                           
 }                                                             
1409.1.1 = { #Shah Rukh reunites the empire                                                       
 owner = TIM                                                           
 controller = TIM                                                           
 }                                                             
1447.3.15 = { add_core = TOX add_core = QAR }
1449.10.27 = { #Ulugh Beg's death splits the empire again                                                     
 owner = KHO                                                           
 controller = KHO                                                           
 }                                                             
1450.1.1  = { citysize = 4800 }                                                       
1457.7.1 = { controller = QAR } #Jahan Shah captures Herat                                                    
1458.1.1 = { controller = TOX } #Abu Sa'id takes advantage and invades                                                  
1459.4.1 = {                                                            
 owner = TIM                                                           
 controller = TIM                                                           
 } #Abu Sa'id reunites the empire at the battle of Sarakhs                                                   
1460.1.1  = { revolt_risk = 5 } # Sieged by Timurid Transox rebels                                                 
1465.1.1  = { revolt_risk = 0 } # Estimated                                                     
1469.8.27 = {                                                            
 owner = KHO                                                           
 controller = KHO                                                           
 remove_core = QAR                                                           
 } #The empire is divided after Abu Sa'id dies                                                     
1470.7.1 = { controller = REB } #Yadigar Muhammad captures Herat                                                    
1470.8.15 = { controller = KHO }                                                        
