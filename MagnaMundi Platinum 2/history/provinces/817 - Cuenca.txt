# # # # # # # # # # # # # # # # #                                              
#817 - Guayas                                                            
                                                              
owner = QTO                                                            
controller = QTO                                                            
culture = cara                                                            
religion = animism                                                            
capital = "Saraguro"                                                            
trade_goods = maize #grain                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
citysize = 1500                                                            
add_core = QTO                                                            
discovered_by = INC                                                            
discovered_by = CUZ                                                            
discovered_by = QTO                                                            
discovered_by = ICA                                                            
discovered_by = CHM                                                            
discovered_by = HUA                                                            
discovered_by = AYM                                                            
                                                              
1463.1.1 = {                                                            
  owner = CUZ                                                          
  controller = CUZ                                                          
 base_tax = 3                                                           
 manpower = 2                                                           
  }                                                            
1471.1.1 = {                                                            
  owner = INC                                                          
  controller = INC                                                          
  }                                                            
