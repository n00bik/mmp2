# # # # # # # # # # # # # # # # # # # # # # # # # #                                     
#1028 - Musashi                                                            
                                                              
owner = JAP                                                            
controller = JAP                                                            
culture = azuma                                                            
religion = shinto                                                            
capital = "Kamakura" #later Edo (Tokyo)                                                         
trade_goods = rice #later silk                                                          
hre = no                                                            
base_tax = 4                                                            
manpower = 3                                                            
citysize = 81800 #565000 in 1884                                                         
add_core = HJO                                                            
add_core = JAP                                                            
#Asia
discovered_by = AKG
discovered_by = AKM
discovered_by = ANG
discovered_by = ANN
discovered_by = ARK
discovered_by = ASK
discovered_by = ASN
discovered_by = ASS
discovered_by = AYU
discovered_by = BHU
discovered_by = CHU
discovered_by = CSK
discovered_by = DAI
discovered_by = DAL
discovered_by = DTE
discovered_by = HJO
discovered_by = HKW
discovered_by = HTK
discovered_by = IGW
discovered_by = JAP
discovered_by = JIN
discovered_by = JTG
discovered_by = KHM
discovered_by = KNO
discovered_by = KOR
discovered_by = LNA
discovered_by = LUA
discovered_by = LXA
discovered_by = MCH
discovered_by = MGM
discovered_by = MIN
discovered_by = MIY
discovered_by = MNG
discovered_by = MRI
discovered_by = NHN
discovered_by = NNB
discovered_by = ODA
discovered_by = OTM
discovered_by = OUC
discovered_by = PEG
discovered_by = QII
discovered_by = QIN
discovered_by = QNG
discovered_by = RYU
discovered_by = RZJ
discovered_by = SHN
discovered_by = SHU
discovered_by = SMZ
discovered_by = SST
discovered_by = STM
discovered_by = STO
discovered_by = SUK
discovered_by = TAU
discovered_by = TGW
discovered_by = TIB
discovered_by = TKD
discovered_by = TKI
discovered_by = USG
discovered_by = VIE
discovered_by = WUU
discovered_by = WYU
discovered_by = XIA
discovered_by = YMN                                                           
temple = yes #Engaku-ji                                                           
fort1 = yes                                                            
                                                              
1000.1.1   = { Defendable_Position = yes }                                                      
                                                              
1336.1.1 = { owner = USG controller = USG } #Should be the Ota, vassals of the Uesugi                                             
1386.1.1 = { add_core = USG }                                                        
1450.1.1 = { citysize = 99800 }                                                        
1454.1.1 = { revolt_risk = 5 } #Fighting between the three branches of the Uesugi family as they split and expand                                          
1457.1.1 = { capital = "Edo" fort2 = yes } #Edo castle is built by Ota Dokan                                              
