# # # # # # # # # # #                                                    
#1054 - Ilimsk                                                            
                                                              
culture = yakut                                                            
religion = shamanism                                                            
capital = "Ilimsk"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
native_size = 10                                                            
native_ferocity = 1                                                            
native_hostileness = 3                                                            
                                                              
