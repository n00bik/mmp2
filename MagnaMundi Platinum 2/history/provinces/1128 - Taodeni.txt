# # # # # # # # # # # # # # # # # # # # # # # #                                       
#1128 - Taodeni                                                            
                                                              
culture = tuareg                                                            
religion = sunni                                                            
capital = "Tawdani"                                                            
trade_goods = unknown #salt                                                           
hre = no                                                            
base_tax = 2                                                            
native_size = 60                                                            
native_ferocity = 4.5                                                            
native_hostileness = 10    

#West Africa
discovered_by = MAL
discovered_by = ASH
discovered_by = BEN
discovered_by = HAU
discovered_by = JOL
discovered_by = KBO
discovered_by = MSI
discovered_by = OYO
discovered_by = SON
discovered_by = SOK
discovered_by = SEG
discovered_by = YAO                                                           
                                                              
# TODO: Morocco expansion events ?
discovered_by = MOR                                                              
