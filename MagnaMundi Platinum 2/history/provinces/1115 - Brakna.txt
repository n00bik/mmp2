# # # #                                                           
#1115 - Brakna                                                            
                                                              
culture = tuareg                                                            
capital = "Aligh"                                                            
religion = sunni                                                            
trade_goods = unknown #wool                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 50                                                            
native_ferocity = 4.5                                                            
native_hostileness = 9                                                            
#West Africa
discovered_by = MAL
discovered_by = ASH
discovered_by = BEN
discovered_by = HAU
discovered_by = JOL
discovered_by = KBO
discovered_by = MSI
discovered_by = OYO
discovered_by = SON
discovered_by = SOK
discovered_by = SEG
discovered_by = YAO                                                         

