# # # # # # # # # # # # # # # # # # # # # # # # # # # #                                   
#1811 - Ionia                                                            
                                                              
owner = BYZ
controller = BYZ
culture = greek                                                            
religion = orthodox                                                            
capital = "Aivali"                                                            
trade_goods = olive                                                            
hre = no                                                            
base_tax = 5                                                            
manpower = 2                                                            
citysize = 9010                                                            
fort1 = yes                                                            
add_core = BYZ                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            

#Nomads
discovered_by = AKK
discovered_by = AST
discovered_by = CHG
discovered_by = CRI
discovered_by = FGA
discovered_by = GOL
discovered_by = KAB
discovered_by = KAZ
discovered_by = KHA
discovered_by = KHI
discovered_by = KHO
discovered_by = KOK
discovered_by = KZH
discovered_by = NOG
discovered_by = OIR
discovered_by = PER
discovered_by = QAR
discovered_by = SHY
discovered_by = SIB
discovered_by = TIM
discovered_by = TOX

discovered_by = latin discovered_by = western_europe                                                            
                                                              
1071.1.1 =  { #Battle of Manzikert                                                        
	owner = TUR #Seljuk Turks                                                         
	controller = TUR                                                           
	}                                                             
1097.1.1 = { owner = BYZ controller = BYZ } #First Crusade recaptures the Anatolian coast for Byzantium                                             
1282.1.1 = { owner = TUR controller = TUR }                                                     
1307.1.1 = { owner = AYD controller = AYD add_core = AYD add_core = TUR } #Beylik of Aydinoglu founded                                           
1390.1.1 = { remove_core = BYZ } #Philadelphia, the last Byzantine holding in Asia Minor, is conquered by the Turks                                           
1402.1.1  = { #Briefly conquered by the Ottomans                                                      
 owner = TUR                                                           
 controller = TUR                                                           
 }                                                             
1403.1.1 = { #The Timurids gave Aydin back tot he Beylik                                                    
 owner = AYD                                                           
 controller = AYD                                                           
 }                                                             
1425.1.1 = {                                                            
 owner = TUR                                                           
 controller = TUR                                                           
 remove_core = AYD                                                           
 } # Incorporated into the Ottoman Empire                                                       
1450.1.1  = { citysize = 9640 }                                                       
