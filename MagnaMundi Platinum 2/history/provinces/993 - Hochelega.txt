# # # # # # # # # # # # # # # # #                                              
#993 - Hochelega                                                            
                                                              
culture = algonquin                                                            
religion = midewiwin                                                            
capital = "Hochelega"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 6                                                            
                                                              
