# # # # # # # # # # # # # # # # # # # # #                                          
#895 - Arkansas                                                            
                                                              
culture = caddo                                                            
religion = midewiwin                                                            
capital = "Arkansas"                                                            
trade_goods = unknown #cotton                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
native_size = 25                                                            
native_ferocity = 2                                                            
native_hostileness = 8                                                            
                                                              
