# # # # # # # # # # #                                                    
#1171 - Ndongo                                                            
                                                              
owner = KON                                                            
controller = KON                                                            
culture = kongolese                                                            
religion = fetishist                                                            
capital = "Ndongo"                                                            
trade_goods = palm                                                            
hre = no                                                            
base_tax = 4                                                            
manpower = 5                                                            
citysize = 1050                                                            
add_core = KON                                                            
#Central Africa
discovered_by = LOA
discovered_by = KON
                                                              
1450.1.1 = { citysize = 1600 }                                                        
