# # # # # # # # # #                                                     
#1035 - Penchisky                                                            
                                                              
culture = inuit                                                            
religion = animism                                                            
capital = "Penchisky"                                                            
trade_goods = unknown #fish                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1                                                            
native_hostileness = 3                                                            
                                                              
