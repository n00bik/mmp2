# # # # # # # # # # # #                                                   
#647 - Ceram                                                            
                                                              
culture = papuan                                                            
religion = animism                                                            
capital = "Ceram"                                                            
trade_goods = unknown #spices                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 0.5                                                            
native_hostileness = 3                                                            
discovered_by = MAJ                                                            
                                                              
