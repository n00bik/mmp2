# # # # # # # # # # # # # # #                                                
#871 - Pomo                                                            
                                                              
culture = nez_perce                                                            
religion = animism                                                            
capital = "Pomo"                                                            
trade_goods = unknown #gold                                                           
hre = no                                                            
base_tax = 5                                                            
manpower = 1                                                            
native_size = 20                                                            
native_ferocity = 1                                                            
native_hostileness = 4                                                            
                                                              
