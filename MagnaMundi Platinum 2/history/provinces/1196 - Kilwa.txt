# # # # # # # # # # # # # # # # # # # # # # # #                                       
#1196 - Kilwa                                                            
                                                              
owner = ZAN                                                            
controller = ZAN                                                            
add_core = ZAN                                                            
culture = swahili                                                            
religion = sunni                                                            
capital = "Kilwa Kisiwani"                                                           
citysize = 14500                                                            
manpower = 2                                                            
trade_goods = gold                                                            
hre = no                                                            
base_tax = 3                                                            
#East Africa
discovered_by = ADA
discovered_by = ETH
discovered_by = NUB
discovered_by = SOF
discovered_by = ZAN                                                            
discovered_by = ZIM                                                          
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1450.1.1  = { citysize = 16000 }                                                       
