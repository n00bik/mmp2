# # # # # # # # # # # # # # # # #                                              
#907 - Dakota                                                            
                                                              
culture = sioux                                                            
religion = midewiwin                                                            
capital = "Dakota"                                                            
trade_goods = unknown #grain                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
native_size = 25                                                            
native_ferocity = 2                                                            
native_hostileness = 9                                                            
                                                              
