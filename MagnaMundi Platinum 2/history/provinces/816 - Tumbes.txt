# # # # # # # # # # # # # # # # #                                              
#816 - Sican                                                            
                                                              
owner = CHM                                                            
controller = CHM                                                            
culture = moche                                                            
religion = inti                                                            
capital = "Tumbes"                                                            
trade_goods = maize #grain                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
citysize = 1500                                                            
add_core = CHM                                                            
discovered_by = INC                                                            
discovered_by = CUZ                                                            
discovered_by = QTO                                                            
discovered_by = ICA                                                            
discovered_by = CHM                                                            
discovered_by = HUA                                                            
discovered_by = AYM                                                            
                                                              
1450.1.1  = { citysize = 1800 }
