# # # # # # # # # # # # # # # # # # # #                                           
#241 - Glamorgan                                                            
                                                              
owner = ENG                                                            
controller = ENG                                                            
culture = welsh                                                            
religion = catholic                                                            
hre = no                                                            
base_tax = 4                                                            
trade_goods = iron                                                            
manpower = 2                                                            
capital = "Carmarthen"                                                            
citysize = 1200
add_core = ENG                                                            
fort1 = yes                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
                                                              
1399.1.1  = { revolt_risk = 20 } # Buildup to the Welsh Revolt
1400.9.16 = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # The Welsh Revolt
1415.1.1  = { revolt_risk = 0 revolt = {} controller = ENG } # English rule is restored
1450.1.1  = { citysize = 1500 }
1453.1.1  = { revolt_risk = 5 } #Start of the War of the Roses                                                
1461.6.1  = { revolt_risk = 2 } #Coronation of Edward IV                                                   
1467.1.1  = { revolt_risk = 5 } #Rivalry between Edward IV & Warwick                                                 
1471.1.1  = { revolt_risk = 8 } #Unpopularity of Warwick & War with Burgundy                                                
1471.3.1  = { revolt_risk = 0 revolt = { type = revolutionary_rebels size = 2 } controller = REB }
1471.5.4  = { revolt = {} revolt_risk = 2 controller = ENG } # Murder of Henry VI & Restoration of Edward IV

