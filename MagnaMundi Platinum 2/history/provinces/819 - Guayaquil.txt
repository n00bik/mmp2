# # # # # # # # # # # # # # # # # #                                             
#819 - Quevedo                                                            
                                                              
culture = cara                                                            
religion = animism                                                            
capital = "Quevedo"                                                            
trade_goods = maize #grain                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
owner = QTO                                                            
controller = QTO                                                            
add_core = QTO                                                            
citysize = 1000                                                            
discovered_by = INC                                                            
discovered_by = CUZ                                                            
discovered_by = QTO                                                            
discovered_by = ICA                                                            
discovered_by = CHM                                                            
discovered_by = HUA                                                            
discovered_by = AYM                                                            
                                                              
