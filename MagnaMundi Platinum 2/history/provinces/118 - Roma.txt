# # # # # # # # # # # # # # # # # #                                             
#118 - Roma                                                            
                                                              
owner = PAP                                                            
controller = PAP                                                            
culture = umbrian                                                            
religion = catholic                                                            
hre = no                                                            
base_tax = 12                                                            
trade_goods = olive                                                            
manpower = 8                                                            
fort1 = yes                                                            
fort2 = yes                                                            
capital = "Roma"                                                            
citysize = 47000                                                            
temple = yes                                                            
marketplace = yes                                                            
add_core = PAP                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
                                                              
1408.1.1   = { owner = NAP controller = NAP } # Ladislas takes control of Ancona                                             
1414.8.6   = { owner = PAP controller = PAP }                                                   
1450.1.1   = { citysize = 50000 }                                                      
