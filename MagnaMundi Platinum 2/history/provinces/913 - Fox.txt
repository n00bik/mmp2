# # # # # # # # # # # # # # # # # # #                                            
#913 - Fox                                                            
                                                              
culture = algonquin                                                            
religion = midewiwin                                                            
capital = "Fox"                                                            
trade_goods = unknown #iron                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
native_size = 10                                                            
native_ferocity = 2                                                            
native_hostileness = 5                                                            
                                                              
