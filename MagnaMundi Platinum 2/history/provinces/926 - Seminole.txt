# # # # # # # # # # # # # # # #                                               
#926 - Seminole                                                            
                                                              
culture = calusa                                                            
religion = midewiwin                                                            
capital = "Seminole"                                                            
trade_goods = unknown #fur                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 10                                                            
native_ferocity = 2                                                            
native_hostileness = 8                                                            
                                                              
