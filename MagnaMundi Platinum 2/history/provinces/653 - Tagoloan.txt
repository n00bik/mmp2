# # # # # # # # #                                                      
#653 - Tagoloan                                                            
                                                              
culture = filipino                                                            
religion = animism                                                            
capital = "Tagoloan"                                                            
trade_goods = unknown #spices                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
native_size = 50                                                            
native_ferocity = 3                                                            
native_hostileness = 9                                                            
                                                              
1457.1.1 = { discovered_by = SUL                                                         
  owner = SUL                                                          
  controller = SUL                                                          
  add_core = SUL                                                          
  citysize = 5200                                                          
  trade_goods = clove                                                          
    } # Sultanate of Sulu                                                      
