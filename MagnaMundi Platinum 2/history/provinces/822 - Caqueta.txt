# # # # # # # # # # # # # #                                                 
#822 - Caranqui                                                            
                                                              
culture = cara                                                            
religion = animism                                                            
capital = "Florencia"                                                            
trade_goods = unknown #gold                                                           
hre = no                                                            
base_tax = 2                                                            
manpower = 1                                                            
discovered_by = INC                                                            
discovered_by = CUZ                                                            
discovered_by = QTO                                                            
discovered_by = ICA                                                            
discovered_by = CHM                                                            
discovered_by = HUA                                                            
discovered_by = AYM                                                            
                                                              
