# # # # # # # # # # # # # # # # # # #                                            
#987 - Niagara                                                            
                                                                                                                        
culture = wyandot                                                            
religion = midewiwin                                                            
capital = "Niagara"                                                            
trade_goods = unknown # fur
hre = no                                                            
base_tax = 4                                                            
manpower = 2                                                            
native_size = 12                                                           
native_ferocity = 4                                                          
native_hostileness = 9                                                          
                                                                                                                   
