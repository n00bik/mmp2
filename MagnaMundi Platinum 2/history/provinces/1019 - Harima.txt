# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #                               
#1019 - Aki                                                            
                                                              
owner = JAP                                                            
controller = JAP                                                            
culture = yamato                                                            
religion = shinto                                                            
capital = "Hiroshima"                                                            
trade_goods = fish                                                            
hre = no                                                            
base_tax = 4                                                            
manpower = 3                                                            
citysize = 11000 #76000 in 1884                                                         
fort1 = yes                                                            
temple = yes #Itsukushima Jinja                                                          
add_core = JAP                                                            
#Asia
discovered_by = AKG
discovered_by = AKM
discovered_by = ANG
discovered_by = ANN
discovered_by = ARK
discovered_by = ASK
discovered_by = ASN
discovered_by = ASS
discovered_by = AYU
discovered_by = BHU
discovered_by = CHU
discovered_by = CSK
discovered_by = DAI
discovered_by = DAL
discovered_by = DTE
discovered_by = HJO
discovered_by = HKW
discovered_by = HTK
discovered_by = IGW
discovered_by = JAP
discovered_by = JIN
discovered_by = JTG
discovered_by = KHM
discovered_by = KNO
discovered_by = KOR
discovered_by = LNA
discovered_by = LUA
discovered_by = LXA
discovered_by = MCH
discovered_by = MGM
discovered_by = MIN
discovered_by = MIY
discovered_by = MNG
discovered_by = MRI
discovered_by = NHN
discovered_by = NNB
discovered_by = ODA
discovered_by = OTM
discovered_by = OUC
discovered_by = PEG
discovered_by = QII
discovered_by = QIN
discovered_by = QNG
discovered_by = RYU
discovered_by = RZJ
discovered_by = SHN
discovered_by = SHU
discovered_by = SMZ
discovered_by = SST
discovered_by = STM
discovered_by = STO
discovered_by = SUK
discovered_by = TAU
discovered_by = TGW
discovered_by = TIB
discovered_by = TKD
discovered_by = TKI
discovered_by = USG
discovered_by = VIE
discovered_by = WUU
discovered_by = WYU
discovered_by = XIA
discovered_by = YMN                                                           
                                                              
1336.1.1 = { owner = TKD controller = TKD add_core = TKD }                                                  
1351.1.1 = { revolt_risk = 5 add_core = MRI } #The Mori were in open rebellion against the shugo as they consolidated their holdings there                                      
1360.1.1 = { revolt_risk = 0 }                                                        
1382.1.1 = { remove_core = TKD } #End of the Nambokucho period, Aki is divided into three parts                                             
1394.1.1 = { owner = AKG controller = AKG }                                                     
1404.1.1 = { owner = YMN controller = YMN add_core = YMN }                                                  
1450.1.1 = { citysize = 13400 }                                                        
1455.1.1 = { revolt_risk = 5 } #Mori Hiromoto begins a campaign against the other branches of his family with shogunal approval and support from the Takeda                                    
1461.1.1 = { revolt_risk = 0 } #Support is removed and Mori is chastized                                                 
#1471.1.1 = { owner = MRI controller = MRI } #Mori Toyomoto seizes control of Aki from the Bakufu                                            
