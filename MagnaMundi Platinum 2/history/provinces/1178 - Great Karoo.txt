# # # # # # # # # # # # # # # #                                               
#1178 - Great Karoo                                                           
                                                              
culture = khoisan                                                            
religion = fetishist                                                            
capital = "Great Karoo"                                                           
trade_goods = unknown #grain                                                           
hre = no                                                            
base_tax = 4                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 0.5                                                            
native_hostileness = 1                                                            
                                                              
