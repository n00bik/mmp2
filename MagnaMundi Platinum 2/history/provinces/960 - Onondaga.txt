# # # # # # # # # # # # # # # # # # #                                            
#960 - Onondaga

culture = iroquois                                                            
religion = midewiwin                                                            
capital = "Onondaga"
hre = no                                                            
base_tax = 3                                                            
manpower = 2                                                            
trade_goods = unknown 
native_size = 13                                                            
native_ferocity = 4                                                          
native_hostileness = 9

