# # # # # # # # # # # # # # # # #                                              
#995 - Atiamekw                                                            
                                                              
culture = cree_montagnais                                                            
religion = midewiwin                                                            
capital = "Atikamekw"                                                            
trade_goods = unknown #fish                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 1.5                                                            
native_hostileness = 2                                                            
                                                              
