# # # # # # # # # # #                                                    
#368 - Madeira                                                            
                                                              
culture = berber                                                            
religion = animism                                                            
capital = "Madeira"                                                            
trade_goods = unknown                                                            
hre = no                                                            
base_tax = 7                                                            
manpower = 1                                                            
native_size = 0
native_ferocity = 0
native_hostileness = 0                                                            
                                                              
1000.1.1   = { Defendable_Position = yes }                                                      
                                                              
1419.1.1 = { discovered_by = latin discovered_by = western_europe } # Jo�o Gon�alves Zarco                                                    
1420.1.1 = { owner = POR                                                         
      controller = POR                                                      
      add_core = POR                                                      
      citysize = 500                                                      
      culture = portugese                                                      
      religion = catholic                                                      
      trade_goods = sugar                                                      
  discovered_by = POR                                                          
  discovered_by = MOR                                                          
  discovered_by = GEN                                                          
  discovered_by = ARA                                                          
  discovered_by = CAS                                                          
  discovered_by = ALG                                                          
  discovered_by = TUN                                                          
  discovered_by = TRP                                                          
  discovered_by = FRA                                                          
  discovered_by = ENG                                                          
    }                                                       
1425.1.1  = { discovered_by = muslim discovered_by = near_east }                                                       
1450.1.1  = { citysize = 3500 }                                                       
