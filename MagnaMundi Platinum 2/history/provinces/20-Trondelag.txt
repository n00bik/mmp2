# # # # # # # # # # # # # # # # # # # # # # # #                                       
#Tr�ndelag, incl. Trondheim, Frosta, R�ros, Steinviksholm                                                         
                                                              
owner = NOR                                                            
controller = NOR                                                            
add_core = NOR                                                            
culture = norwegian                                                            
religion = catholic                                                            
hre = no                                                            
base_tax = 5                                                            
trade_goods = fish                                                            
manpower = 2                                                            
capital = "Trondheim"                                                            
citysize = 2000 # Estimated                                                          
temple = yes                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
                                                              
1450.1.1   = { citysize = 1150 }                                                      
1470.1.1   = { citysize = 1250 }                                                      
