# # # # # # # # # # # # # # # # # # # # #                                          
#274 - Pskov                                                            
                                                              
owner = NOV                                                            
controller = NOV                                                            
culture = russian                                                            
religion = orthodox                                                            
hre = no                                                            
base_tax = 5                                                            
trade_goods = millet                                                            
manpower = 5                                                            
capital = "Pskov"                                                            
citysize = 2000                                                            
fort1 = yes                                                            
fort2 = yes
temple = yes  # Trinity cathedral, founded in 1138                                                     
add_core = NOV                                                            
discovered_by = latin discovered_by = western_europe                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
                                                              
1268.2.18 = { #Battle of Rakovor                                                         
 owner = PSK                                                           
 controller = PSK                                                           
 add_core = PSK                                                           
 }                                                             
1348.1.1 = { #Battle of Bolotovo                                                         
 remove_core = NOV                                                           
 }                                                             
1470.1.1  = { revolt_risk = 5 } # Struggles between the boyars and smerds                                                
