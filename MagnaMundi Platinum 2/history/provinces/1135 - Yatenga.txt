# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #                              
#1135 - Yatenga                                                            
                                                              
culture = mossi                                                           
religion = fetishist                                                            
capital = "Gursi"                                                            
trade_goods = unknown #grain                                                           
hre = no                                                            
base_tax = 1                                                            
native_size = 60                                                            
native_ferocity = 4.5                                                            
native_hostileness = 9                                                            
#West Africa
discovered_by = MAL
discovered_by = ASH
discovered_by = BEN
discovered_by = HAU
discovered_by = JOL
discovered_by = KBO
discovered_by = MSI
discovered_by = OYO
discovered_by = SON
discovered_by = SOK
discovered_by = SEG
discovered_by = YAO                                                          
                                                              
1444.1.1 = {
    owner = MSI
    controller = MSI
    add_core = MSI
    trade_goods = millet #grain                                                           
    manpower = 2
    citysize = 1500
}                                                              
1450.1.1  = { citysize = 2000 }
1460.1.1  = { revolt_risk = 3 } #Songhai raids devastate region                                                
1468.1.1  = { revolt_risk = 3 } #Songhai raids devastate region                                                   
