# # # # # # # # # # # # # # # # # # # # #                                          
#1148 - Gobir                                                            
                                                              
owner = HAU                                                            
controller = HAU                                                            
add_core = HAU                                                            
culture = hausa                                                           
citysize = 21000
manpower = 3                                                            
religion = sunni                                                            
capital = "Birnin Lalle"                                                           
trade_goods = carmine                                                            
hre = no                                                            
base_tax = 2                                                            
#West Africa
discovered_by = MAL
discovered_by = ASH
discovered_by = BEN
discovered_by = HAU
discovered_by = JOL
discovered_by = KBO
discovered_by = MSI
discovered_by = OYO
discovered_by = SON
discovered_by = SOK
discovered_by = SEG
discovered_by = YAO                                                       

1450.1.1 = { citysize = 25000 }
                                                              
