# # # # # # # # # # # # # # # # # # #                                            
#912 - Sauk                                                            
                                                              
culture = ojibwa                                                            
religion = midewiwin                                                            
capital = "Sauk"                                                            
trade_goods = unknown #fish                                                           
hre = no                                                            
base_tax = 1                                                            
manpower = 1                                                            
native_size = 5                                                            
native_ferocity = 2                                                            
native_hostileness = 5                                                            
                                                              
