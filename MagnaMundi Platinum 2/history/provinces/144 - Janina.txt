# # # # # # # # # # # # # # # # # # # # # #                                         
#144 - Epirus                                                            
                                                              
owner = ALB                                                            
controller = ALB                                                            
culture = greek                                                            
religion = orthodox                                                            
capital = "Ioannina"                                                            
trade_goods = wool                                                            
hre = no                                                            
base_tax = 2                                                            
manpower = 2                                                            
citysize = 3500
fort1 = yes                                                            
add_core = ALB                                                            
#Near East
discovered_by = ADE
discovered_by = ALG
discovered_by = ALH
discovered_by = ARB
discovered_by = DUR
discovered_by = GRA
discovered_by = HED
discovered_by = MAM
discovered_by = MOR
discovered_by = NAJ
discovered_by = OMA
discovered_by = SHI
discovered_by = SHR
discovered_by = TRP
discovered_by = TUN

#Ottomans
discovered_by = AYD
discovered_by = CND
discovered_by = DUL
discovered_by = KAR
discovered_by = TUR                                                            
#Eastern Europe
discovered_by = ACH
discovered_by = ALB
discovered_by = ARM
discovered_by = ATH
discovered_by = BOS
discovered_by = BUL
discovered_by = BYZ
discovered_by = CRO
discovered_by = CYP
discovered_by = GEO
discovered_by = GRE
discovered_by = HCD
discovered_by = HUN
discovered_by = LIT
discovered_by = MAZ
discovered_by = MOE
discovered_by = MOL
discovered_by = MON
discovered_by = MOS
discovered_by = NAX
discovered_by = NOV
discovered_by = POL
discovered_by = PSK
discovered_by = RAG
discovered_by = RIG
discovered_by = RUS
discovered_by = RYA
discovered_by = SER
discovered_by = TRA
discovered_by = TRE
discovered_by = TVE
discovered_by = WAL
discovered_by = YAR
discovered_by = ZAZ                                                            
discovered_by = latin discovered_by = western_europe                                                            
                                                              
1081.1.1 = { #Norman conquest                                                          
 owner = SIC                                                           
 controller = SIC                                                           
 add_core = BYZ                                                           
 }                                                             
1083.1.1 = { #Byzantines reconquer it with Venetian help                                                      
 owner = BYZ                                                           
 controller = BYZ                                                           
 }                                                             
1204.4.13 = { #Fourth crusade creates the Despotate of Epirus                                                     
 owner = ALB                                                           
 controller = ALB                                                           
 }                                                             
1230.1.1 = { #Conquered by Bulgaria                                                         
 owner = BUL                                                           
 controller = BUL                                                           
 }                                                             
1246.1.1 = { #Revival of the Byzantine Empire                                                       
 owner = BYZ                                                           
 controller = BYZ                                                           
 }                                                             
1257.1.1 = { #The Two Sicilies take control                                                       
 owner = SIC                                                           
 controller = SIC                                                           
 }                                                             
1272.2.1 = { #Angevin kingdom (principality) of Albania                                                       
 owner = NAP                                                           
 controller = NAP                                                           
 }                                                             
1281.1.1 = { #Byzantine revival                                                          
 owner = BYZ                                                           
 controller = BYZ                                                           
 }                                                             
1344.1.1 = { #Serbian Empire of Stefen Dusan (approximate date)                                                     
 owner = SER                                                           
 controller = SER                                                           
 }                                                             
1355.1.1 = { #Death of Dusan and breakup of his empire                                                    
 owner = ALB                                                           
 controller = ALB                                                           
 remove_core = BYZ                                                           
 }                                                             
1396.1.1 = { #Conquest of most of Albania                                                       
 owner = TUR                                                           
 controller = TUR                                                           
 }                                                             
1402.1.1   = { revolt = { type = pretender_rebels size = 0 } controller = REB } # Interregnum                                          
1410.1.1   = { revolt = {} revolt = { type = pretender_rebels size = 0 } controller = REB }                                         
1413.1.1   = { revolt = {} controller = TUR }                                                   
1446.1.1 = { add_core = TUR }                                                        
1450.1.1   = { citysize = 4000 }                                                      
