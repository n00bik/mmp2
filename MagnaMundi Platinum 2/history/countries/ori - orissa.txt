government = despotic_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 0
mercantilism_freetrade = -4
offensive_defensive = -2
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = oriya
religion = hinduism
technology_group = indian
capital = 552	# Cuttack

1000.1.1 = { set_country_flag = idea_superior_seamanship }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = limited_social_mobility }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = mixed_country } #Famous for their war elephants

#Rajas of Utkala/Orissa
1378.1.1 = {
	monarch = {
		name = "Narasimhadeva IV"
		adm = 4
		dip = 4
		mil = 6
	}
}

1414.1.1 = {
	monarch = {
		name = "Bhanudeva IV"
		adm = 3
		dip = 5
		mil = 5
	}
}

1434.1.1 = {
	monarch = {
		name = "Kapilesvara Gajapati"
		adm = 4
		dip = 5
		mil = 6
	}
}

1466.1.1 = {
	monarch = {
		name = "Purushottama"
		adm = 6
		dip = 5
		mil = 3
	}
}

