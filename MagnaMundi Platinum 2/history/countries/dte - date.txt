government = feudal_monarchy-lowest
aristocracy_plutocracy = -4
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = -2
land_naval = -3
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = azuma
religion = shinto
technology_group = chinese
capital = 1026	# Mutsu

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = idea_grand_army }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = tribute_prevention }

1336.1.1 = { 	#Mutsu was perhaps the strongest center of southern support and the Date clan did support them
	set_country_flag = disloyal_retainer

	set_country_flag = southern_court_supporter 
	} 

1353.1.1 = {
	monarch = {
		name = "Date Masamune"
		adm = 5
		dip = 7
		mil = 7
	}
}

1439.1.1 = {
	monarch = {
		name = "Date Mochimune"
		adm = 5
		dip = 5
		mil = 6
	}
}

1469.1.1 = {
	monarch = {
		name = "Date Narimune"
		adm = 5
		dip = 5
		mil = 5
	}
}

