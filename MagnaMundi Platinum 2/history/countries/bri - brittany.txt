government = feudal_monarchy-lower

aristocracy_plutocracy = -2
centralization_decentralization = 2
innovative_narrowminded = -2
mercantilism_freetrade = 3
offensive_defensive = 2
land_naval = 2
quality_quantity = 2
serfdom_freesubjects = -2
primary_culture = breton
religion = catholic
technology_group = latin
capital = 172	# Nantes

1000.1.1 = { set_country_flag = idea_sea_hawks }
1000.1.1 = { set_country_flag = idea_grand_navy }
1000.1.1 = { set_country_flag = idea_national_trade_policy }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = medium_power }

1345.9.16 = {
	monarch = {
		name = "Jean V"
		adm = 5
		dip = 5
		mil = 7
	}
}

1399.11.1 = {
	monarch = {
		name = "Jean VI"
		adm = 4
		dip = 5
		mil = 5
	}
}

1442.8.29 = {
	monarch = {
		name = "Fran�ois I"
		adm = 4
		dip = 4
		mil = 5
	}
}

1450.7.20 = {
	monarch = {
		name = "Pierre II"
		adm = 5
		dip = 5
		mil = 6
	}
}

1457.9.23 = {
	monarch = {
		name = "Artur III"
		adm = 6
		dip = 5
		mil = 7
		leader = { name = "Arthur de Richemont"	type = general	rank = 0	fire = 4	shock = 4	manuever = 4	siege = 1 }
	}
}

1458.12.26 = { aristocracy_plutocracy = -2 innovative_narrowminded = -3 mercantilism_freetrade = -1 }#Fran�ois II didn't take his duties seriouly and lefte the nobles to rule his Duchy, but was a maecenas of commerce and culture

1458.12.27 = {
	monarch = {
		name = "Fran�ois II"
		adm = 6
		dip = 6
		mil = 4
	}
}

