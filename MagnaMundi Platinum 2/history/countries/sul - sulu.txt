government = early_feudal_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 0
mercantilism_freetrade = 1
offensive_defensive = -2
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = sunni
primary_culture = filipino
capital = 651	# Jolo

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_superior_seamanship }
1000.1.1 = { set_country_flag = idea_sea_hawks }

1000.1.1 = { set_country_flag = inf_country }

1450.1.1 = {
	monarch = {
		name = "Sharif ul Hashim"
		dip = 6
		adm = 6
		mil = 4
	}
}

