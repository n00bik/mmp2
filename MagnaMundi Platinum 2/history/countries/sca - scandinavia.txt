government = administrative_monarchy

aristocracy_plutocracy = 0
centralization_decentralization = 3
innovative_narrowminded = -1
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = 0
quality_quantity = -2
serfdom_freesubjects = 3
technology_group = latin
primary_culture = swedish
religion = protestant
add_accepted_culture = finnish
capital = 1	#Stockholm

