#######################################
# Timurid Empire
#######################################
government = early_feudal_monarchy-upper

aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
offensive_defensive = -1
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -5
technology_group = muslim
primary_culture = turkmeni
religion = sunni
capital = 454	# Samarkand

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_patron_of_art }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = major_power }

1370.1.1 = {
	monarch = {
		name = "Timur"
		adm = 7	
		dip = 7	
		mil = 8
		leader = { name = "Timur"	type = general	rank = 1	fire = 6	shock = 6	manuever = 6	siege = 2 }
	}
}

1383.1.1 = { leader = {	name = "Miran Shah"	type = general	rank = 1	fire = 3	shock = 3	manuever = 4	siege = 0	death_date = 1408.1.1 } }

1409.1.1 = {
	monarch = {
		name = "Shah Rukh"
		adm = 4
		dip = 4	
		mil = 4
	}
	capital = 446 # Herat
}

1447.1.1 = {
	monarch = {
		name = "Ulugh Beg"
		adm = 4
		dip = 4	
		mil = 4
	}
    capital = 454 # Samarkand
}

1447.3.15 = { set_country_flag = civil_war } #Ulugh Beg's reign disputed by pretenders and the empire split in three

1451.6.23 = { clr_country_flag = civil_war }

###########################################################################
# The Timurid Empire ceases to exist de facto after Sa'id's death in 1469 #
###########################################################################

1453.1.1 = { set_country_flag = total_war_series set_country_flag = total_war }

1459.4.1 = {
	monarch = {
		name = "Ab� Sa'id"
		adm = 3	
		dip = 5	
		mil = 7	
		leader = {	name = "Ab� Sa'id"             	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0 }
	}
	capital = 446 # Herat
}