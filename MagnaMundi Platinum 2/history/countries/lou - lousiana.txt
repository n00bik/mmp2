government = constitutional_republic-lower

aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -2
technology_group = latin
primary_culture = english
religion = reformed
capital = 922	# Mobile

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_bill_of_rights }

1000.1.1 = { set_country_flag = mixed_country }

