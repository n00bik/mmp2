government = feudal_monarchy

aristocracy_plutocracy = -2
centralization_decentralization = -4
innovative_narrowminded = 2
mercantilism_freetrade = -1
offensive_defensive = 2
land_naval = 0
quality_quantity = -1
serfdom_freesubjects = 1
primary_culture = castillian
add_accepted_culture = andalucian
religion = catholic
technology_group = latin
capital = 215	# Castilla La Vieja

historical_friend = ARA

1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_excellent_shipwrights }
1000.1.1 = { set_country_flag = idea_divine_supremacy }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = medium_power }

1390.10.9 = {
	monarch = {
		name = "Enrique III"
		adm = 6
		dip = 5
		mil = 5
	}
}

1406.12.26 = {
	monarch = {
		name = "Juan II"
		adm = 4
		dip = 4
		mil = 5
	}
}

1422.1.1 = { leader = {	name = "�lvaro de Luna"	type = general	rank = 1	fire = 2	shock = 2	manuever = 3	siege = 0	death_date = 1453.6.2 } }

1450.1.1 = { decision = act_of_uniformity decision = blasphemy_act }

1454.7.20 = {
	monarch = {
		name = "Enrique IV"
		adm = 3
		dip = 3
		mil = 3
	}
}

1456.1.1 = { set_country_flag = renaissance }

1469.10.19 = { set_country_flag = happened_161000a }

1474.12.11 = { capital = 215 } # Castilla La Vieja

1474.12.11 = {
	monarch = {
		name = "Isabel I"
		adm = 8
		dip = 9
		mil = 6
		female = yes
	}
}

