government = feudal_monarchy-lowest
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
technology_group = latin
religion = catholic
primary_culture = lombard
capital = 113	# Ferrara

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_espionage }
1000.1.1 = { set_country_flag = idea_smithian_economics }

1000.1.1 = { set_country_flag = small_state }

1000.1.1 = { set_country_flag = inf_country }

1393.1.1 = {
	set_country_flag = union_setup_pre_game
	set_country_flag = union_succeed_I
}
