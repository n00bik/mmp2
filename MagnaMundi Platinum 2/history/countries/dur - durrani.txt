government = despotic_monarchy-lower

aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
offensive_defensive = -1
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -5
technology_group = muslim
religion = sunni
primary_culture = baluchi
capital = 447	# Kandahar

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = rejecting_change }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }

