government = feudal_monarchy

aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = 3
quality_quantity = -2
serfdom_freesubjects = 2
primary_culture = norwegian
religion = catholic
technology_group = latin
historical_friend = DAN
capital = 17	#Akershus

1000.1.1 = { set_country_flag = idea_superior_seamanship }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_bill_of_rights }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = medium_power }

1397.6.17 = {
	set_country_flag = union_setup_pre_game
	set_country_flag = union_succeed_A
}

1436.1.1 = { leader = {	name = "Amund Bolt"            	type = general	rank = 3	fire = 1	shock = 2	manuever = 2	siege = 0	death_date = 1440.1.1 } }

