government = feudal_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 2
innovative_narrowminded = -1
mercantilism_freetrade = 3
offensive_defensive = -3
land_naval = 4
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = occitain
add_accepted_culture = cosmopolitan_french
religion = catholic
technology_group = latin
capital = 201	# Aix-en-Provence

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_superior_seamanship }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = medium_power }

1399.8.1 = {
	monarch = {
		name = "Louis II"
		adm = 3
		dip = 5
		mil = 5
	}
}

1417.1.1 = {
	monarch = {
		name = "Louis III"
		adm = 4
		dip = 5
		mil = 4
	}
}

1434.11.16 = {
	monarch = {
		name = "Ren� I"
		adm = 6
		dip = 8
		mil = 5
	}
}
