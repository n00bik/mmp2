government = theocratic_government-lower

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = hessian
religion = catholic
elector = yes
technology_group = latin
capital = 78	# Mainz

1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = mixed_country }
1000.1.1 = { set_country_flag = ic_possible }
1000.1.1 = { set_country_flag = minor_holding }

1396.1.1 = {
	monarch = {
		name = "Johann II"
		adm = 5
		dip = 6
		mil = 4
	}
}

1419.1.1 = {
	monarch = {
		name = "Konrad III"
		adm = 4
		dip = 5
		mil = 5
	}
}

1434.6.11 = {
	monarch = {
		name = "Dietrich I"
		adm = 5
		dip = 6
		mil = 6
	}
}

1459.5.5 = {
	monarch = {
		name = "Dieter I"
		adm = 5
		dip = 4
		mil = 7
	}
}

1461.8.22 = {
	monarch = {
		name = "Adolf II" # aka "Adolf III"
		adm = 6
		dip = 5
		mil = 5
	}
}

1470.1.1 = { set_country_flag = has_expelled_jews }

