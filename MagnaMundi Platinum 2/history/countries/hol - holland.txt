government = feudal_monarchy-lowest
aristocracy_plutocracy = -1
centralization_decentralization = -1
innovative_narrowminded = -2
mercantilism_freetrade = 4
offensive_defensive = 1
land_naval = 2
quality_quantity = -2
serfdom_freesubjects = 5
technology_group = latin
religion = catholic
primary_culture = dutch
capital = 97	# Amsterdam

1000.1.1 = { set_country_flag = idea_shrewd_commerce_practise }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_naval_provisioning }

1000.1.1 = { set_country_flag = mixed_country }
1000.1.1 = { set_country_flag = small_state }

1299.11.10 = {
	set_country_flag = union_setup_pre_game
	set_country_flag = union_succeed_L
}

1428.7.3 = {
	clr_country_flag = union_setup_pre_game
	clr_country_flag = union_succeed_L
}
