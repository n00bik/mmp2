government = the_demesne
aristocracy_plutocracy = -5	# COUNTING ELECTORS' VOTES FOR HEREDITARY EMPEROR  (starts counting at -5)
centralization_decentralization = -5	# MAGNITUDE OF RESPONSE
innovative_narrowminded = 0
mercantilism_freetrade = 0
offensive_defensive = 0
land_naval = 0
quality_quantity = 0
serfdom_freesubjects = 0		#Index of religious conflict (may enable "tolerance", if >=0)
# SLIDER OVERFLOW
# RLD = { serfdom_freesubjects = 0 }	--> counts provinces gained/lost in Imperial War against Ottomans
# TREASURY keeps track of support for tolerance
technology_group = latin
primary_culture = bavarian
religion = catholic
capital = 133	# Linz

# VARIABLES @ EMP
#supporters_emperor	# counts number of supports_emperor (max 3)
#supporters_rival	# counts number of supports_rival (max 3)
#electors_theocratic	# counts theocratic electors
#electors_temporal	# counts temporal electors

# @ player country level
#hre_standing (0-10)

1000.1.1 = {
	monarch = {
		name = "Imperial Magistrate"
		adm = 8
		dip = 3
		mil = 3
	}
}

