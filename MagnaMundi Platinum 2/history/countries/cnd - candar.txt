government = despotic_monarchy-lowest

aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 1
mercantilism_freetrade = -4
offensive_defensive = -2
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = turkish
religion = sunni
technology_group = eastern
unit_type = muslim
capital = 328	# Sinop

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_patron_of_art }

1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }

1000.1.1 = { set_country_flag = small_state }

1000.1.1 = { set_country_flag = inf_country }

1383.1.1 = { capital = 328 } #Suleyman II seizes Kastamonu and becomes an Ottoman vassal; Bayezid moves to Sinop.

1385.1.1 = {
	monarch = {
		name = "Isfendiyar"
		adm = 6
		dip = 5
		mil = 4
	}
}

1440.1.29 = {
	monarch = {
		name = "Ibr�him II"
		adm = 4
		dip = 6
		mil = 6
	}
}

1443.1.1 = {
	monarch = {
		name = "Ism�il"
		adm = 5
		dip = 6
		mil = 5
	}
}

1461.1.1 = {
	monarch = {
		name = "Ahmed"
		adm = 6
		dip = 4
		mil = 3
	}
}

# Annexed by the Ottoman Empire at the end 1461
