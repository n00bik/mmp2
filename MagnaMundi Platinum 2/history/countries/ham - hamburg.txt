government = imperial_city

aristocracy_plutocracy = 3
centralization_decentralization = 4
innovative_narrowminded = -2
mercantilism_freetrade = 3
offensive_defensive = 0
land_naval = 2
quality_quantity = 0
serfdom_freesubjects = 2
technology_group = latin
religion = catholic
primary_culture = hannoverian
historical_friend = NOV
historical_friend = HCB
historical_friend = HCD
historical_friend = HCL
capital = 44	# Hamburg

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_smithian_economics }
1000.1.1 = { set_country_flag = idea_superior_seamanship }

1000.1.1 = { set_country_flag = hanseatic_league }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = ic_possible }
1000.1.1 = { set_country_flag = small_state }

1390.1.1 = {
	monarch = {
		name = "Marquardus Schreye"
		adm = 6
		dip = 5
		mil = 5
	}
}

1413.1.1 = {
	monarch = {
		name = "Hinricus de Monte"
		adm = 7
		dip = 8
		mil = 3
	}
}

1439.1.1 = {
	monarch = {
		name = "Hinricus Koting"
		adm = 8
		dip = 8
		mil = 7
	}
}

1458.1.1 = {
	monarch = {
		name = "Hinrick Leseman"
		adm = 8
		dip = 7
		mil = 7
	}
}

1466.1.1 = {
	monarch = {
		name = "Dr. Hinrick Murmester"
		adm = 7
		dip = 6
		mil = 4
	}
}

