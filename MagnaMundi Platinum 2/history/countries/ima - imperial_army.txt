government = reichsarmee
aristocracy_plutocracy = -5
centralization_decentralization = -5
innovative_narrowminded = 5
mercantilism_freetrade = 0
offensive_defensive = -5
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = -5
technology_group = latin
primary_culture = bavarian
religion = catholic
capital = 1776	# Ffm

1000.1.1 = { set_country_flag = idea_napoleonic_warfare }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }

1000.1.1 = {
	monarch = {
		name = "Field Marshall of the Empire"
		adm = 3
		dip = 5
		mil = 8
	}
}
