government = feudal_monarchy-lowest

aristocracy_plutocracy = -1
centralization_decentralization = -5
innovative_narrowminded = -4
mercantilism_freetrade = 2
offensive_defensive = -2
land_naval = -2
quality_quantity = 2
serfdom_freesubjects = 0
technology_group = latin
primary_culture = gascon
religion = catholic
capital = 175	# Bayonne

1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_military_drill }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = small_state }

1391.7.25 = {
	monarch = {
		name = "Bernard VII"
		adm = 4
		dip = 4
		mil = 3
	}
}

1418.6.12 = {
	monarch = {
		name = "Jean IV"
		adm = 4
		dip = 5
		mil = 5
	}
}

1450.11.6 = {
	monarch = {
		name = "Jean V"
		adm = 5
		dip = 3
		mil = 4
		leader = {	name = "Jean V"   	type = general	rank = 0	fire = 2	shock = 2	manuever = 2	siege = 0 }
	}
}

1468.1.1 = { set_country_flag = renaissance }

1473.2.10 = { offensive_defensive = 3 } # Charles I is imprissoned by Louis XI

1473.2.10 = {
	monarch = {
		name = "Charles I"
		adm = 4
		dip = 4
		mil = 4
	}
}

