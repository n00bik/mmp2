government = feudal_monarchy-lowest

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 1
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -2
technology_group = latin
religion = catholic
primary_culture = austrian
capital = 73	# Innsbruck

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_military_drill }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = small_state }

1386.7.10 = {
	monarch = {
		name = "Wilhelm I"
		adm = 4
		dip = 5
		mil = 4
	}
}

1406.7.16 = {
	monarch = {
		name = "Friedrich IV"
		adm = 4
		dip = 5
		mil = 4
	}
}

1439.1.1 = {
	monarch = {
		name = "Sigismund I"
		adm = 5
		dip = 5
		mil = 6
	}
}

1454.1.1 = {
	monarch = {
		name = "Friedrich V"
		adm = 6
		dip = 4
		mil = 3
	}
}

