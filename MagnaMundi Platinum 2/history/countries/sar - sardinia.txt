government = feudal_monarchy

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
technology_group = latin
religion = catholic
primary_culture = sardinian
capital = 127	# Cagliari

1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_national_trade_policy }
1000.1.1 = { set_country_flag = idea_bill_of_rights }

1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = small_state }

1387.1.1 = {
	monarch = { 
		name = "Mariano V"
		dip = 6
		mil = 5
		adm = 6
	}
}

1407.1.1 = {
	monarch = { 
		name = "Guglielmo I"
		dip = 5
		mil = 4
		adm = 4
	}
}

