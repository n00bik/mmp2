government = feudal_monarchy

aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = 0
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = central_thai
religion = buddhism
technology_group = chinese
capital = 600	# Ayutthaya

1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_national_conscripts }

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }

1371.1.1 = { set_country_flag = tribute_accepted }

1395.1.1 = {
	monarch = {
		name = "Ram Raja"
		adm = 5
		dip = 4
		mil = 4
	}
}

1408.1.1 = {
	monarch = {
		name = "Int'araja I"
		adm = 3
		dip = 5
		mil = 5
	}
}

1424.1.1 = {
	monarch = {
		name = "Boromoraja II"
		adm = 4
		dip = 5
		mil = 6
		leader = { name = "Boromoraja II"      	type = general	rank = 0	fire = 2	shock = 4	manuever = 3	siege = 1 }
	}
}

1448.1.1 = {
	monarch = {
		name = "Boromo Trailokanat"
		adm = 9
		dip = 5
		mil = 5
	}
}

