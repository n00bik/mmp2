government = early_feudal_monarchy-lower

aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 5
mercantilism_freetrade = -5
offensive_defensive = -5
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = nazca
religion = animism
technology_group = new_world
capital = 806 #Ica

1000.1.1 = { set_country_flag = idea_divine_supremacy }
1000.1.1 = { set_country_flag = idea_bureaucracy }
1000.1.1 = { set_country_flag = idea_grand_army }

1000.1.1 = { set_country_flag = hierarchial_society }
1000.1.1 = { set_country_flag = inward_thinking }
1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = limited_social_mobility }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = rejecting_change }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = innocent }
1000.1.1 = { set_country_flag = subsistence_farmers }

1000.1.1 = { set_country_flag = native_country }
1000.1.1 = { set_country_flag = isolated }

1000.1.1 = {
	monarch = {
		name = "Katari" #Actually the name of a miner who declared himself ruler of the Chichas, but it's the best I could do...
		adm = 4
		dip = 4
		mil = 4
	}
}

