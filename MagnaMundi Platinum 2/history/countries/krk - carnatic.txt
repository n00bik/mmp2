government = administrative_republic-lowest

aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = 0
land_naval = 1
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = indian
religion = hinduism
primary_culture = telegu
add_accepted_culture = tamil
capital = 537	# Thanjavur

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_grand_army }
1000.1.1 = { set_country_flag = idea_national_trade_policy }

1000.1.1 = { set_country_flag = limited_social_mobility }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }

#Nawabs of the Carnatic
