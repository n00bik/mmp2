government = feudal_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -5
offensive_defensive = 0
land_naval = -3
quality_quantity = 0
serfdom_freesubjects = -4
technology_group = latin
primary_culture = bavarian
religion = catholic
capital = 65	#M�nchen!

1000.1.1 = { set_country_flag = idea_bureaucracy }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = medium_power }

1397.1.1 = {
	monarch = {
		name = "Ernst I"
		adm = 5
		dip = 7
		mil = 5
    	}
}

1438.7.3 = {
	monarch = {
		name = "Albrecht III"
		adm = 5	
		dip = 5
		mil = 5
    	}
}

1440.1.1 = { set_country_flag = has_expelled_jews }

1460.2.28  = {
	monarch = {
		name = "Johann IV"
		adm = 4	
		dip = 3	
		mil = 4
	}
}

1463.11.20 = {
	monarch = {
		name = "Siegmund I"
		adm = 3
		dip = 3
		mil = 3
	}
}

1467.9.4   = {
	monarch = {
		name = "Albrecht IV der Weise"
		adm = 8
		dip = 6
		mil = 5
	}
}

1474.1.1 = { set_country_flag = renaissance }

