government = feudal_monarchy-lower

aristocracy_plutocracy = -1
centralization_decentralization = -5
innovative_narrowminded = -1
mercantilism_freetrade = 3
offensive_defensive = 3
land_naval = -3
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = cosmopolitan_french
religion = catholic
technology_group = latin
capital = 190	# Bourges

1000.1.1 = { set_country_flag = idea_humanist_tolerance }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = small_state }

1356.1.1 = {
	monarch = {
		name = "Louis II"
		adm = 5
		dip = 4
		mil = 5
	}
}

1410.1.1 = {
	monarch = {
		name = "Jean I"
		adm = 5
		dip = 5
		mil = 5
	}
}

1434.2.5 = {
	monarch = {
		name = "Charles I"
		adm = 6
		dip = 6
		mil = 6
	}
}

1456.12.5 = {
	monarch = {
		name = "Jean II"
		adm = 6
		dip = 5
		mil = 4
	}
}

1477.1.1 = { set_country_flag = renaissance }

