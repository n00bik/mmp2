government = feudal_monarchy-lowest

aristocracy_plutocracy = -1
centralization_decentralization = -1
innovative_narrowminded = 2
mercantilism_freetrade = 1
offensive_defensive = 4
land_naval = -4
quality_quantity = -1
serfdom_freesubjects = 1
technology_group = latin
religion = catholic
primary_culture = gascon
capital = 176	# Pau

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_smithian_economics }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = small_state }

1398.1.1 = {
	monarch = {
		name = "Isabelle"
		female = yes
		dip = 6
		adm = 5
		mil = 4
	}
}

1413.1.1 = {
	monarch = {
		name = "Jean I"
		dip = 4
		adm = 5
		mil = 5
	}
}

1436.1.1 = {
	monarch = {
		name = "Gaston IV"
		dip = 7
		adm = 7
		mil = 6
	}
}

1468.1.1 = { set_country_flag = renaissance }

1472.7.26 = {
	monarch = {
		name = "Fran�ois I Ph�bus"
		dip = 7
		adm = 7
		mil = 6
	}
}

