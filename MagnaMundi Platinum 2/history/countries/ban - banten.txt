government = early_feudal_monarchy-lower

aristocracy_plutocracy = 0
centralization_decentralization = 3
innovative_narrowminded = 0
mercantilism_freetrade = 1
offensive_defensive = 0
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = 2
technology_group = indian
primary_culture = javanese
religion = hinduism
capital = 624	# Bandung

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_superior_seamanship }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = inf_country }

