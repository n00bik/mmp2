government = feudal_monarchy-lower
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = -1
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = khmer
religion = buddhism
technology_group = chinese
capital = 604	# Phnom Penh

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_grand_army }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }

1389.1.1 = {
	monarch = {
		name = "Ponthea Yat"
		adm = 4
		dip = 4
		mil = 4
	}
}

1404.1.1 = {
	monarch = {
		name = "Narayana Ramadhipati"
		adm = 6
		dip = 5
		mil = 6
	}
}

1429.1.1 = {
	monarch = {
		name = "Sri Bodhya"
		adm = 4
		dip = 5
		mil = 4
	}
}

1444.1.1 = {
	monarch = {
		name = "Dharmarajadhiraja"
		adm = 6
		dip = 5
		mil = 3
	}
}

