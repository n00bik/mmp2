government = tribal_despotism-lower

aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
offensive_defensive = -1
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = nogai
religion = sunni
technology_group = eastern
unit_type = muslim
capital = 470	# Saraycik

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = rejecting_change }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = mixed_country }
1000.1.1 = { set_country_flag = horse_nomad }
1000.1.1 = { set_country_flag = medium_power }

1000.1.1 = { set_country_flag = horse_nomad_setup }

1392.1.1 = {
	monarch = {
		name = "Edigu"
		adm = 3
		dip = 6
		mil = 5
	}
}

1412.1.1 = {
	monarch = {
		name = "Batir"
		adm = 5
		dip = 5
		mil = 5
	}
}

1419.1.1 = {
	monarch = {
		name = "Mansur"
		adm = 6
		dip = 5
		mil = 5
	}
}

1427.1.1 = {
	monarch = {
		name = "Ghazi"
		adm = 4
		dip = 6
		mil = 5
	}
}

1428.1.1 = {
	monarch = {
		name = "Vakkas"
		adm = 5
		dip = 5
		mil = 4
	}
}

1447.1.1 = {
	monarch = {
		name = "Khorezmi"
		adm = 6
		dip = 5
		mil = 5
	}
}

1473.1.1 = {
	monarch = {
		name = "Abbas"
		adm = 5
		dip = 5
		mil = 5
	}
}

