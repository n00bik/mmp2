government = feudal_monarchy-lowest
aristocracy_plutocracy = -5
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = -2
land_naval = -1
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = yamato
religion = shinto
technology_group = chinese
capital = 1023	# Echizen

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_bureaucracy }
1000.1.1 = { set_country_flag = idea_patron_of_art }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = tribute_prevention }

1336.1.1 = { #served the Shiba, a related clan of the Ashikaga
	set_country_flag = retainer
	set_country_flag = northern_court_supporter 
	}

1450.1.1 = {
	monarch = {
		name = "Asakura Toshikage"
		adm = 5
		dip = 5
		mil = 5
	}
}

