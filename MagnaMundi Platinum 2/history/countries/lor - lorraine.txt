government = feudal_monarchy-lower

aristocracy_plutocracy = -1
centralization_decentralization = -4
innovative_narrowminded = -1
mercantilism_freetrade = 3
offensive_defensive = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = 4
primary_culture = burgundian
religion = catholic
technology_group = latin
capital = 189	# Nancy

1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = mixed_country }
1000.1.1 = { set_country_flag = medium_power }

1390.1.1 = {
	monarch = {
		name = "Charles II"
		adm = 4
		dip = 4
		mil = 6
	}
}

1431.1.1 = {
	monarch = {
		name = "Rene I"
		adm = 5
		dip = 6
		mil = 5
	}
}

1434.11.16 = {
	set_country_flag = union_setup_pre_game
	set_country_flag = union_succeed_B
}

1452.3.27 = {
	clr_country_flag = union_setup_pre_game
	clr_country_flag = union_succeed_B
	monarch = {
		name = "Jean II"
		adm = 4
		dip = 7
		mil = 5
	}
}

1470.12.16 = {
	monarch = {
		name = "Nicolas I"
		adm = 4
		dip = 7
		mil = 5
	}
}

1473.7.24 = {
	monarch = {
		name = "Ren� II"
		adm = 4
		dip = 7
		mil = 5
	}
}

1477.1.1 = { set_country_flag = renaissance }

