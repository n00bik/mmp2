#Michoacan, the Tarascan kingdom
government = tribal_despotism

aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 5
mercantilism_freetrade = 0
offensive_defensive = -5
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = nahua
religion = teotl
technology_group = new_world
capital = 854 #Sayulcas (Tzinzuntzan)

1000.1.1 = { set_country_flag = idea_battlefield_commisions }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_smithian_economics }

1000.1.1 = { set_country_flag = hierarchial_society }
1000.1.1 = { set_country_flag = inward_thinking }
1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = limited_social_mobility }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = rejecting_change }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = innocent }
1000.1.1 = { set_country_flag = subsistence_farmers }

1000.1.1 = { set_country_flag = native_country }
1000.1.1 = { set_country_flag = isolated }
1000.1.1 = { set_country_flag = tribal_setup }

1375.1.1 = { 
	monarch = { 
		name = "Vacusecha"
		adm = 4
		mil = 4
		dip = 4
		}
	}
1399.1.1 = { #guesstimate
	monarch = { 
		name = "Tariacuri"
		adm = 6 #Founded Tzintzuntzani
		mil = 6 #Conquered a kingdom
		dip = 5
		}
	}
1450.1.1 = { 
	monarch = { 
		name= "Hiripan" #continued expansion
		adm = 6 #Created a bureaucracy
		mil = 7 #Continued expanding
		dip = 5
		}
	}
1470.1.1 = { #brother of Hiripan
	monarch = { 
		name = "Tangaxoan I"
		adm = 5
		mil = 5
		dip = 5
		}
	}
