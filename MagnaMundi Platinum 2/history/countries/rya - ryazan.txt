government = despotic_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 2
innovative_narrowminded = 1
mercantilism_freetrade = -1
offensive_defensive = 2
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 301	# Ryazan

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_grand_army }

1000.1.1 = { set_country_flag = inf_country }

1372.1.1 = {
	monarch = {
		name = "Oleg Ivanovich"
		adm = 4
		dip = 6
		mil = 4
	}
}

1402.1.1 = {
	monarch = {
		name = "Fyodor Olegovich"
		adm = 5
		dip = 6
		mil = 5
	}
}

1427.1.1 = {
	monarch = {
		name = "Ivan Fyodorovich"
		adm = 6
		dip = 6
		mil = 4
	}
}

1430.1.1 = {
	monarch = {
		name = "Ivan IV"
		adm = 6
		dip = 5
		mil = 5
		}
	}

1456.1.1 = {
	monarch = {
		name = "Vasiliy III"
		adm = 4
		dip = 7
		mil = 5
		}
	}

