government = feudal_monarchy-lowest

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = ligurian
add_accepted_culture = occitain
religion = catholic
technology_group = latin
capital = 103	# Torino

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_cabinet }
1000.1.1 = { set_country_flag = idea_patron_of_art }

1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = medium_power }

1391.1.1 = {
	monarch = {
		name = "Amedeo VIII"
		adm = 4
		dip = 4
		mil = 5
	}
}

1416.2.19 = { government = feudal_monarchy-lower } #Emperor Sigismund made the county into a duchy

1434.11.2 = {
	monarch = {
		name = "Ludovico I"
		adm = 6
		dip = 8
		mil = 5
	}
}

1465.1.30 = {
	monarch = {
		name = "Amedeo IX"
		adm = 6
		dip = 5
		mil = 3
	}
}

1471.1.1 = { set_country_flag = renaissance }

1472.3.31 = {
	monarch = {
		name = "Filiberto I"
		adm = 4
		dip = 4
		mil = 4
	}
}

