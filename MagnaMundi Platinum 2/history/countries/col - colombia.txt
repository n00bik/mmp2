government = administrative_republic

aristocracy_plutocracy = -2
centralization_decentralization = 0
innovative_narrowminded = 0
mercantilism_freetrade = -5
offensive_defensive = 0
land_naval = -1
quality_quantity = 2
serfdom_freesubjects = 0
technology_group = latin
religion = catholic
primary_culture = castillian
capital = 825	# Bogota

1000.1.1 = { set_country_flag = idea_superior_seamanship }
1000.1.1 = { set_country_flag = idea_colonial_ventures }
1000.1.1 = { set_country_flag = idea_national_trade_policy }

1000.1.1 = { set_country_flag = mixed_country }

