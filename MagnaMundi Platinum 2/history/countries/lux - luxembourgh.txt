government = feudal_monarchy-lowest
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -3
primary_culture = wallonian
religion = catholic
technology_group = latin
capital = 94	# Luxembourg

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_national_trade_policy }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = mixed_country }
1000.1.1 = { set_country_flag = small_state }

1388.1.1 = {
	monarch = {
		name = "Jobst I"
		adm = 6
		dip = 6
		mil = 7
	}
}

1411.1.18 = {
	monarch = {
		name = "Wenzel II"
		adm = 4
		dip = 4
		mil = 6
	}
}

1412.1.1 = {
	monarch = {
		name = "Anton I"
		adm = 6
		dip = 5
		mil = 4
	}
}

1415.1.1 = {
	monarch = {
		name = "Elizabeth I"
		adm = 5
		dip = 5
		mil = 4
		female = yes
	}
}

1419.1.1 = {
	monarch = {
		name = "Johann II"
		adm = 6
		dip = 3
		mil = 4
	}
}

