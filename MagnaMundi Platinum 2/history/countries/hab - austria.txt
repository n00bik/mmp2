government = feudal_monarchy-lower
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = -4
quality_quantity = 0
serfdom_freesubjects = -3
primary_culture = austrian
religion = catholic
technology_group = latin
capital = 134	# Wien
historical_friend = HUN
# Below, not Historical but helps to prevent senseless Austrian conquests in the balkans
historical_friend = ALB
historical_friend = RAG
historical_friend = BOS
historical_friend = BUL
historical_friend = CRO
historical_friend = SER

1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_cabinet }
1000.1.1 = { set_country_flag = idea_smithian_economics }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = medium_power }

1395.8.29 = {
	monarch = {
		name = "Albrecht IV"
		adm = 4
		dip = 4
		mil = 6
	}
}

1404.9.14 = {
	monarch = {
		name = "Albrecht V"
		adm = 8
		dip = 4
		mil = 6
	}
}

1439.10.27 = { emperor = yes set_country_flag = current_emperor }

1439.10.27 = {
	monarch = {
		name = "Friedrich III"
		adm = 5
		dip = 8
		mil = 4
	}
}

1450.1.1 = { decision = court_of_wards_and_liveries }

#Ladislaus is freed.

1452.1.1 = {
	monarch = {
		name = "Ladislaus Postumus"
		adm = 6
		dip = 6
		mil = 5
	}
}

1457.11.24 = {
	monarch = {
		name = "Friedrich III"
		adm = 5
		dip = 8
		mil = 4
	}
}

1463.12.2 = { 	# death of Albrecht VI
	decision = establish_habsburg_dominance
}

