government = despotic_monarchy-lowest

aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 1
mercantilism_freetrade = 2
offensive_defensive = -2
land_naval = 3
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = sunni
primary_culture = malayan
capital = 594	# Pattani

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_national_trade_policy }
1000.1.1 = { set_country_flag = idea_bill_of_rights }

1000.1.1 = { set_country_flag = traditional_values }

1000.1.1 = { set_country_flag = inf_country }

