government = early_feudal_monarchy-lower

aristocracy_plutocracy = -1
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = 0
land_naval = 1
quality_quantity = 2
serfdom_freesubjects = -1
primary_culture = malayan
technology_group = indian
religion = hinduism
capital = 622	# Palembang

1000.1.1 = { set_country_flag = idea_national_trade_policy }
1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_excellent_shipwrights }

1000.1.1 = { set_country_flag = mixed_country }

1377.1.1 = {
	set_country_flag = union_setup_pre_game
	set_country_flag = union_fail
}

1453.1.1 = {
	clr_country_flag = union_setup_pre_game
	clr_country_flag = union_fail
}

1453.1.1 = { #should really be 1455
	monarch = {
		name = "Ario Abdillah"
		adm = 7
		dip = 7
		mil = 5
	}
}

