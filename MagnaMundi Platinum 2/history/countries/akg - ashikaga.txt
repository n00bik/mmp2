government = feudal_monarchy-upper
aristocracy_plutocracy = -5
centralization_decentralization = 4
innovative_narrowminded = 3
mercantilism_freetrade = -5
offensive_defensive = 0
land_naval = 1
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = yamato
religion = shinto
technology_group = chinese
capital = 1020	# Kyoto

1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_cabinet }
1000.1.1 = { set_country_flag = idea_grand_army }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = tribute_prevention }

1333.1.1 = { 
	monarch = { 
		name = "Ashikaga Takauji"
		adm = 5 
		dip = 4 #Helped form coalition against shogun, but also gave too much away; caused split between northern and southern courts
		mil = 8 #A general who seized power and established a new shogunate, defeated the Hojo several times, but driven out of Kyoto
		}
	}
1336.1.1 = { set_country_flag = northern_court_supporter }
1338.1.1 = { #Ashikaga seizes power from emperor Go-Daigo
	government = feudal_monarchy-upper 
	set_country_flag = shogun
	}
1358.6.7 = { 
	monarch = { 
		name = "Ashikaga Yoshiakira"
		adm = 4 #Many lords defect after Takauji died
		dip = 6 #Raised as a hostage and acted as a diplomat before his father became shogun
		mil = 4
		}
	}
1367.12.28 = { 
	monarch = { 
		name = "Ashikaga Yoshimitsu" #child ruler
		adm = 5
		dip = 7 #The southern court gave up its struggle in 1392, gained important defectors from them, established embassy with China
		mil = 6 #Pacified Kyushu
		}
	}
#1383 Akamatsu rebellion
#1391 Meitoku War (Yamana attacks Kyoto)
#1402 Uprising in Mutsu
1408.5.31 = { #Became shogun in 1394, but his father continued to be the real power until his death 
	monarch = { 
		name = "Ashikaga Yoshimochi" #his father ceded his position to him
		adm = 6 #Japan suffered a famine during his reign, but also organized Muromachi administration
		dip = 4 #1408 was appointed king of Japan by the Chinese emperor, 1411 broke off relations with China, renewed north-south conflict
		mil = 6 #Defeated rebellions by Uesugi, fought Oei War against Korea
		}
	}
#1419 Oei War: Korean attack on Tsushima
1423.1.1 = { 
	monarch = {
		name = "Ashikaga Yoshikazu" #alcoholic youth
		adm = 3 #Terrible famine, uprisings, plague
		dip = 3 #repudiated agreement with southern court (again)
		mil = 3
		}
	}
1425.3.17 = { 
	monarch = { 
		name = "Ashikaga Yoshimochi" #returned to office when his son died
		adm = 3 #Did not name a successor
		dip = 6 #Established Tosenbugyo to regulate foreign affairs
		mil = 4 #Died in peasant uprising
		}
	}
1428.2.3 = { 
	monarch = { 
		name = "Ashikaga Yoshinori" #Considered to have ruled a "reign of terror"
		adm = 3 #Suffered multiple revolts
		dip = 3 #Resumed trade with China, suffered a revolt by Ashikaga Mochiuji (kanto kanrei), assassinated by Akamatsu Mitsuhige
		mil = 6 #Defeated uprisings in Harima and Tamba, destroyed power of the Kanto Kanrei, murdered by Akamatsu
		}
	#decline of shogunate is clear from his murder (and the response)
	}
#1433 Otomo rebels
#1433 Hieizan monks rebel
#1438/9 Eikyo war (Ashikaga Mochiuji, the ambitious Kanto Kubo, attacks Uesugi Norizane (his deputy) and is declared in rebellion; war between the shoguns)
#1441 Granted Shimazu suzerainty over Rykuku islands
1441.7.12 = { #Yoshinori was assassinated in the Kakitsu incident; a torrid tale of homosexuality and bad judgement
	monarch = { 
		name = "Ashikaga Yoshikatsu" #child ruler, age 8, died falling off a horse
		adm = 3
		dip = 3
		mil = 3
		}
	}
1443.8.16 = { 
	monarch = {
		name = "Ashikaga Yoshimasa" #child ruler, age 8
		adm = 3 #ignored politics in favor of art
		dip = 3 #had trouble siring an heir, ended up confusing the issue of inheritance and causing the Onin War
		mil = 3
	}
}
#1454.12.27 Kyotoku War: Ashikaga Shigeuji, Kanto Kubo, murdered Uesugi Noritada; the Kanto Kubo was dissolved and the Uesugi compensated
1474.1.1 = {
	monarch = {
		name = "Ashikaga Yoshihisa" 
		adm = 3 #His father was the real power
		dip = 3
		mil = 4 #Died in camp during campaign
	}
}
#1487-9 Campaign against Rokkaku Takayori of Omi
