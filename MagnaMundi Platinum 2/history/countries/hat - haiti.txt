government = republican_dictatorship-lower
aristocracy_plutocracy = -1
centralization_decentralization = -3
innovative_narrowminded = 3
mercantilism_freetrade = 0
offensive_defensive = 0
land_naval = 0
quality_quantity = 2
serfdom_freesubjects = -1
technology_group = latin
religion = catholic
primary_culture = cosmopolitan_french
capital = 488	# Port-Au-Prince

1000.1.1 = { set_country_flag = idea_smithian_economics }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_grand_army }

1000.1.1 = { set_country_flag = mixed_country }

