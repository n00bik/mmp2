government = tribal_federation-lower #various chieftains and kings ruled the Shan land
aristocracy_plutocracy = -2
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = 0
land_naval = 1
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = chinese
religion = buddhism
primary_culture = shan
capital = 583	# Lashio

1000.1.1 = { set_country_flag = idea_grand_army }
1000.1.1 = { set_country_flag = idea_smithian_economics }
1000.1.1 = { set_country_flag = idea_national_trade_policy }

1000.1.1 = { set_country_flag = subsistence_farmers }
1000.1.1 = { set_country_flag = hierarchial_society }
1000.1.1 = { set_country_flag = limited_social_mobility }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = tribal_setup }

#WRONG MONARCHS - Shan dynasty ruled Pegu, not the Shan states!

1385.1.1 = {
	monarch = {
		name = "Binya-Nwe"
		dip = 5
		mil = 4
		adm = 5
	}
}

1423.1.1 = {
	monarch = {
		name = "Binya Dhamma"
		dip = 4
		mil = 3
		adm = 6
	}
}

1426.1.1 = {
	monarch = {
		name = "Binya Rankit"
		dip = 4
		mil = 5
		adm = 5
	}
}

1446.1.1 = {
	monarch = {
		name = "Binya Waru"
		dip = 6
		mil = 3
		adm = 3
	}
}

1450.1.1 = {
	monarch = {
		name = "Binya Keng"
		dip = 5
		mil = 4
		adm = 5
	}
}

1453.1.1 = {
	monarch = {
		name = "Shengtsaubu"
		dip = 4
		mil = 4
		adm = 3
	}
}

1460.1.1 = {
	monarch = {
		name = "Dhamma Dzedi"
		dip = 4
		mil = 6
		adm = 6
	}
}

