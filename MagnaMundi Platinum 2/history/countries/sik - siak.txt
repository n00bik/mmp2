government = early_feudal_monarchy-lower

aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = 0
land_naval = 3
quality_quantity = 2
serfdom_freesubjects = -1
primary_culture = malayan
technology_group = indian
religion = sunni
capital = 620	# Siak

1000.1.1 = { set_country_flag = idea_shrewd_commerce_practise }
1000.1.1 = { set_country_flag = idea_naval_fighting_instruction }
1000.1.1 = { set_country_flag = idea_superior_seamanship }

1000.1.1 = { set_country_flag = mixed_country }

1381.1.1 = {
	monarch = {
		name = "Sri Iskander"
		adm = 4
		dip = 4
		mil = 4
	}
}
1405.1.1 = { #unknown
	monarch = {
		name = "Ibrahim"
		adm = 4
		dip = 4
		mil = 4
	}
}
1420.1.1 = { #unknown
	monarch = {
		name = "Khoja Ahmad"
		adm = 4
		dip = 4
		mil = 4
	}
}
1440.1.1 = { #unknown
	monarch = {
		name = "Jamal"
		adm = 4
		dip = 4
		mil = 4
	}
}

1458.1.1 = { #unknown
	monarch = {
		name = "Mudari"
		adm = 6
		dip = 5
		mil = 4
	}
}

