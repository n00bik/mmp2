government = despotic_monarchy-lower

aristocracy_plutocracy = -5
centralization_decentralization = 2
innovative_narrowminded = 3
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = -4
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 295	# Moskva

1000.1.1 = { set_country_flag = idea_grand_army }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = medium_power }

1389.5.19 = {
	monarch = {
		name = "Vasiliy I"
		adm = 4
		dip = 6
		mil = 5
	}
}

1420.1.1 = { leader = {	name = "Yuri Patrikeev"		type = general	rank = 1	fire = 2	shock = 4	manuever = 3	siege = 1	death_date = 1439.1.1 } }

1424.2.27 = {
	monarch = {
		name = "Vasiliy II Temny"
		adm = 6
		dip = 4
		mil = 5
	}
}

1430.1.1 = { leader = {	name = "Dmitriy Shemiaka"	type = general	rank = 2	fire = 3	shock = 4	manuever = 3	siege = 0	death_date = 1453.1.1 } }

1446.1.1 = { leader = {	name = "Fyodor Basenok"         	type = general	rank = 1	fire = 3	shock = 4	manuever = 4	siege = 0	death_date = 1460.1.1 } }

1447.1.1 = { leader = {	name = "Ivan Obolenski-Striga" 	type = general	rank = 2	fire = 2	shock = 4	manuever = 3	siege = 0	death_date = 1469.1.1 } }

1460.1.1 = { leader = {	name = "Kholmski"              	type = general	rank = 1	fire = 2	shock = 3	manuever = 3	siege = 1	death_date = 1495.1.1 } }

1460.1.1 = { centralization_decentralization = 1 land_naval = -4 quality_quantity = 0 } # The Pomjestija Reform

1462.1.1 = { set_country_flag = monastic_duty }

1462.3.28 = {
	monarch = {
		name = "Ivan III Veliky"
		adm = 6
		dip = 7
		mil = 8
	}
}

1469.1.1 = { set_country_flag = byzantine_heritage }

