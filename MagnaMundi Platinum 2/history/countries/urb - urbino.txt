government = feudal_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
technology_group = latin
primary_culture = umbrian
religion = catholic
capital = 119	# Urbino

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_espionage }

1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = small_state }

#Dukes of Urbino

1363.1.1 = {
	monarch = {
		name = "Antonio I"
		dip = 6
		adm = 5
		mil = 6
	}
}

1404.1.1 = {
	monarch = {
		name = "Gidantonio I"
		dip = 5
		adm = 4
		mil = 4
	}
}

1443.2.1 = {
	monarch = {
		name = "Oddantonio I"
		dip = 6
		adm = 3
		mil = 5
	}
}

1444.7.22 = {
	monarch = {
		name = "Federigo III"
		dip = 5
		adm = 5
		mil = 5
	}
}

1456.1.1 = { set_country_flag = renaissance }

