government = feudal_monarchy-lowest
aristocracy_plutocracy = -5
centralization_decentralization = 2
innovative_narrowminded = 3
mercantilism_freetrade = -4
offensive_defensive = 0
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = yamato
religion = shinto
technology_group = chinese
capital = 1021	# Sunpu

1000.1.1 = { set_country_flag = idea_espionage }
1000.1.1 = { set_country_flag = idea_cabinet }
1000.1.1 = { set_country_flag = idea_national_trade_policy }

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = tribute_prevention }

1336.1.1 = { #One of Ashikaga's most loyal generals
	set_country_flag = loyal_retainer
	set_country_flag = northern_court_supporter 
	} 

1337.1.1 = { 
	monarch = { 
		name = "Hosokawa Akiuji" #Shugo of Izumi in 1337
		adm = 4
		dip = 4
		mil = 4
		}
	}
1352.1.1 = { 
	monarch = { 
		name = "Hosokawa Nariuji"
		adm = 4
		dip = 4
		mil = 4
		}
	}
1358.1.1 = { 
	monarch = { 
		name = "Hosokawa Kiyouji"
		adm = 4
		dip = 5 #Kanrei
		mil = 4
		}
	set_country_flag = kanrei 
	}
1361.1.1 = { 
	clr_country_flag = kanrei
	monarch = { 
		name = "Hosokawa Yoriyuki"
		adm = 5
		dip = 3 #Frugal, drew the ire of many samurai families, asked to resign
		mil = 5 #Victorious over the Yamana
		}
	}
1368.1.1 = { set_country_flag = kanrei }
1379.1.1 = { clr_country_flag = kanrei }
1391.1.1 = { #Yoriyuki dies
	set_country_flag = kanrei
	monarch = { 
		name = "Hosokawa Yorimoto"
		adm = 4
		dip = 4
		mil = 4
		}
	}
1393.1.1 = { clr_country_flag = kanrei }
1397.1.1 = { #Yorimoto dies
	monarch = { 
		name = "Hosokawa Mitsumoto"
		adm = 4
		dip = 4
		mil = 4
		}
	}
1412.1.1 = { set_country_flag = kanrei }
1421.1.1 = { clr_country_flag = kanrei }
1426.1.1 = { #Mitsumoto dies
	monarch = { 
		name = "Hosokawa Mochiyuki"
		adm = 4
		dip = 4
		mil = 4
		}
	}
1432.1.1 = { set_country_flag = kanrei }
1442.1.1 = { #Mochiyuki dies
	clr_country_flag = kanrei
	monarch = {
		name = "Hosokawa Katsumoto"
		adm = 5
		dip = 6 #Convinced the shogun to outlaw the Yamana instead of him
		mil = 6 #Began the Onin War
	}
	leader = { name = "Hosokawa Katsumoto" type = general rank = 0 fire = 2 shock = 3 manuever = 3 siege = 0 death_date = 1473.1.1 }
}
1445.1.1 = { set_country_flag = kanrei }
1449.1.1 = { clr_country_flag = kanrei }
1452.1.1 = { set_country_flag = kanrei }
1464.1.1 = { clr_country_flag = kanrei }
1467.1.14 = { #The Onin War begins
	clr_country_flag = loyal_retainer
	set_country_flag = retainer
	government = feudal_monarchy-lower
	}
1468.1.1 = { set_country_flag = kanrei }
1473.1.1 = { #Katsumoto dies
	clr_country_flag = kanrei
	monarch = { 
		name = "Hosokawa Masamoto"
		adm = 4
		dip = 4
		mil = 4
		}
	}
