government = constitutional_monarchy

aristocracy_plutocracy = -4
centralization_decentralization = -2
innovative_narrowminded = -3
mercantilism_freetrade = -1
offensive_defensive = -3
land_naval = -2
quality_quantity = -1
serfdom_freesubjects = 3
primary_culture = cosmopolitan_french
religion = catholic
technology_group = latin
capital = 183	# Paris

