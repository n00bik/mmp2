government = feudal_monarchy-lowest
aristocracy_plutocracy = -4
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = -2
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = yamato
religion = shinto
technology_group = chinese
capital = 1785	# Awa

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_espionage }
1000.1.1 = { set_country_flag = idea_sea_hawks }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = tribute_prevention }

1336.1.1 = {
	set_country_flag = retainer
	set_country_flag = northern_court_supporter
	}

1399.1.1 = {
	monarch = {
		name = "(Regency Council)"
		regent = yes
		adm = 4
		dip = 4
		mil = 4
		}
}

1467.1.1 = { #Approximate
	monarch = {
		name = "Miyoshi Nagayuki"
		adm = 5
		dip = 5
		mil = 5
		}
	}
1467.1.14 = { #The Onin War begins
	clr_country_flag = retainer
	set_country_flag = disloyal_retainer

	}
