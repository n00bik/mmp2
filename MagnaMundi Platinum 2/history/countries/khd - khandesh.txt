government = despotic_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 0
mercantilism_freetrade = -4
offensive_defensive = -2
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = sunni
primary_culture = marathi
capital = 527	# Burhanpur

1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_grand_army }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }

1399.1.1 = {
	monarch = {
		name = "Nasir Khan"
		adm = 5
		dip = 4
		mil = 5
	}
}

1437.9.20 = {
	monarch = {
		name = "Miran Adil Khan I"
		adm = 4
		dip = 5
		mil = 3
	}
}

#Sultans of Khandesh
1441.1.1 = {
	monarch = {
		name = "Miran Mubarak Khan I"
		adm = 5
		dip = 5
		mil = 5
	}
}

1457.6.5 = {
	monarch = {
		name = "'Adil Khan II"
		adm = 5
		dip = 5
		mil = 5
	}
}

