government = constitutional_republic
aristocracy_plutocracy = 1
centralization_decentralization = 1
innovative_narrowminded = -1
mercantilism_freetrade = 0
offensive_defensive = 2
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = 5
technology_group = latin
primary_culture = american
religion = protestant
capital = 965	# New York

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_bill_of_rights }

1000.1.1 = { set_country_flag = mixed_country }

