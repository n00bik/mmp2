government = theocratic_government-lower

aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
technology_group = latin
religion = catholic
primary_culture = dutch
capital = 98	# Utrecht

1000.1.1 = { set_country_flag = idea_merchant_adventures }
1000.1.1 = { set_country_flag = idea_national_conscripts }
1000.1.1 = { set_country_flag = idea_superior_seamanship }

1000.1.1 = { set_country_flag = mixed_country }
1000.1.1 = { set_country_flag = ic_possible }
1000.1.1 = { set_country_flag = small_state }

#Prince Bishops of Utrecht
1393.1.1 = {
	monarch = {
		name = "Frederick III"
		adm = 6
		dip = 7
		mil = 4
	}
}

1423.1.1 = {
	monarch = {
		name = "Rudolf I"
		adm = 5
		dip = 5
		mil = 5
	}
}

1455.4.7 = {
	monarch = {
		name = "Gijsbrecht I"
		adm = 5
		dip = 5
		mil = 5
	}
}

1456.8.3 = {
	monarch = {
		name = "David I"
		adm = 8
		dip = 8
		mil = 5
	}
}

