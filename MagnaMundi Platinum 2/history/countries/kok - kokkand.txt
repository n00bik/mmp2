government = tribal_despotism-lower

aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
offensive_defensive = -1
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -3
technology_group = muslim
religion = sunni
primary_culture = uzbehk
capital = 458	# Kokkand

1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }
1000.1.1 = { set_country_flag = idea_engineer_corps }

1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = rejecting_change }
1000.1.1 = { set_country_flag = entrenched_past }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = cav_country }
1000.1.1 = { set_country_flag = horse_nomad }
1000.1.1 = { set_country_flag = horse_nomad_setup }

# (Khans)
