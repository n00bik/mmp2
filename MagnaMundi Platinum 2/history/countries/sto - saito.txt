government = feudal_monarchy-lowest
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = 0
land_naval = -2
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = yamato
religion = shinto
technology_group = chinese
capital = 1783	# Mino

1000.1.1 = { set_country_flag = closed_society }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = traditional_values }
1000.1.1 = { set_country_flag = limited_contact }
1000.1.1 = { set_country_flag = opening_society }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_smithian_economics }
1000.1.1 = { set_country_flag = idea_battlefield_commisions }

1000.1.1 = { set_country_flag = mixed_country }

1000.1.1 = { set_country_flag = tribute_prevention }

1336.1.1 = { #The Bakufu were "not in great force" in Mino
	set_country_flag = southern_court_supporter 
	set_country_flag = retainer
	} 

