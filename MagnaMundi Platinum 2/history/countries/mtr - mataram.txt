government = early_feudal_monarchy-lower

aristocracy_plutocracy = -3
centralization_decentralization = 2
innovative_narrowminded = 0
mercantilism_freetrade = 0
offensive_defensive = -2
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = javanese
religion = hinduism
technology_group = indian
capital = 626	# Karta

1000.1.1 = { set_country_flag = idea_battlefield_commisions }
1000.1.1 = { set_country_flag = idea_military_drill }
1000.1.1 = { set_country_flag = idea_bill_of_rights }

1000.1.1 = { set_country_flag = limited_social_mobility }
1000.1.1 = { set_country_flag = emerging_middle_class }
1000.1.1 = { set_country_flag = open_society }

1000.1.1 = { set_country_flag = inf_country }

1451.1.1 = {
	monarch = {
		name = "Rajasavardhana"
		adm = 4
		dip = 4
		mil = 3
	}
}

1453.1.1 = {
	monarch = {
		name = "Hyang Purvavisesa"
		adm = 4
		dip = 3
		mil = 3
	}
}

1466.1.1 = {
	monarch = {
		name = "Bhre Pandan Solar"
		adm = 3
		dip = 3
		mil = 3
	}
}

