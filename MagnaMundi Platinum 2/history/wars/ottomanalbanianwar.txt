name = "Ottoman-Albanian War"

1443.3.4 = {
	add_attacker = TUR
	add_defender = ALB
}

# Continued in the First Ottoman-Venetian War
1463.4.2 = {
	rem_attacker = TUR
	rem_defender = ALB
}

