name = "Hanseatic-English War" 

1470.1.1 = {
	add_attacker = ENG
	add_defender = HCL
	add_defender = LUN
	add_defender = MKL
	add_defender = POM
	add_defender = HAM
	add_defender = HCB
	add_defender = BRA
	add_defender = MAG
	add_defender = BRU
	add_defender = RIG
}

#Peace and end of war
1474.2.1 = {
	rem_attacker = ENG
	rem_defender = HCL
	rem_defender = LUN
	rem_defender = MKL
	rem_defender = POM
	rem_defender = HAM
	rem_defender = HCB
	rem_defender = BRA
	rem_defender = MAG
	rem_defender = BRU
	rem_defender = RIG
}

