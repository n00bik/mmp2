name = "Oirat-Chagatai War"

1440.1.1 = {
	add_attacker = OIR
	add_attacker = KHA
	add_defender = CHG
}

1445.1.1 = {
	rem_attacker = OIR
	rem_attacker = KHA
	rem_defender = CHG
}