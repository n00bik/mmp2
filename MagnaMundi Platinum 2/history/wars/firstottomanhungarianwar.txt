name = "First Ottoman-Hungarian War"

1453.1.1 = {
	add_attacker = TUR
	add_attacker = WAL
	add_defender = HUN
	add_defender = SER
}

1456.7.14 = {
	battle = {
		name = "Belgrade"
		location = 141
		attacker = {
#			leader =	# Mehmet II
			infantry = 50000
			cavalry = 10000
			losses = 50	# percent
		}
		defender = {
#			leader =	# Hunyadi J�nos
			infantry = 40000
			cavalry = 10000
			losses = 10	# percent
		}
		result = loss
	}
}

1456.7.23 = {
	rem_attacker = TUR
	rem_attacker = WAL
	rem_defender = HUN
	rem_defender = SER
}

