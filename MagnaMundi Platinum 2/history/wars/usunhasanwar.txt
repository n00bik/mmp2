name = "War of the Black and White Sheep"

1467.5.16 = {
	add_attacker = AKK
	add_defender = QAR
}

1467.11.11 = {
	battle = {
		name = "Chapakchur"
		location = 331
		attacker = {
#			leader =	# Uzun Hasan
			infantry = 7000
			cavalry = 10000
			losses = 10	# percent
		}
		defender = {
#			leader =	# Jah�n Sh�h
			infantry = 4000
			cavalry = 8000
			losses = 45	# percent
		}
		result = win	#Qara Koyunlu routed, Jah�n Sh�h killed
	}
}

1469.8.27 = { #Timurids defeated and Abu Said and Hasan Ali Captured and later executed. Qara Koyunlu Crumbles
	rem_attacker = AKK
	rem_defender = QAR
}