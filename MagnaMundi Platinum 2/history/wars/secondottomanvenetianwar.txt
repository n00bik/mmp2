name = "Second Ottoman-Venetian War"

1463.4.3 = {
	add_attacker = TUR
	add_attacker = WAL
	add_defender = VEN
	add_defender = ALB
}

# Ivan Crnojevic allies with Venice
1470.1.1 = { 
	add_defender = MON
}

# Ak Koyunlu declares war
1472.8.1 = {
	add_defender = AKK
}

# Battle of Otluk Beli
1473.8.11 = { 
	battle = { 
		name = "Otluk Beli" 
		location = 329
		attacker = { 
#			leader = 			# Mehmet II
			infantry = 65000
			cavalry = 35000
			losses = 10			# percent 
		} 
		defender = { 
#			leader =			# Uzun Hasan
			infantry = 55000
			cavalry = 25000
			losses = 25			# percent 
		} 
		result = win
	} 
}

# Death of Uzun Hasan
1478.1.6 = {
	rem_defender = AKK
}

# Conquest of Kruje
1478.6.16 = { 
	rem_defender = ALB
}

#Peace
1479.1.25 = {
	rem_attacker = TUR
	rem_attacker = WAL
	rem_defender = VEN
	rem_defender = MON
}

