name = "Onin War"

1467.5.1 = { #May
	add_attacker = HKW
	add_attacker = OTM
	add_attacker = HTK
	add_defender = YMN
	add_defender = TKI
}

1467.9.20 = { #Late September
	add_defender = OUC
	add_attacker = AKM
	#add_attacker = MIY
	}

1469.1.1 = { #Ashikaga Yoshimi, the shogun's brother and possibly heir, becomes a general for Yamana Sozen and is branded a rebel
	add_attacker = AKG #The war now becomes a succession war
	}

1469.8.1 = {
	add_attacker = SHN
	}

1471.1.1 = {
	add_attacker = ASK
}

1474.4.19 = { #Yamana Masatoyo and Hosokawa Masamoto agree to a truce
	rem_attacker = HKW
	rem_defender = YMN
	#rem_attacker = MIY
}

1475.1.1 = { #Ashikaga Yoshimi makes peace with Yoshimasa
	rem_attacker = AKG
}

1477.12.16 = {
	rem_defender = OUC 
	rem_defender = TKI
	rem_attacker = OTM
	rem_attacker = HTK
	rem_attacker = AKM
	rem_attacker = ASK
	rem_attacker = SHN
	} 

