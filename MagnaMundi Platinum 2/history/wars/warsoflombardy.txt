name = "War of Lombardy"

1425.5.4 = {
	add_attacker = MLO
	add_defender = VEN
}

# Peace of Cremona
1441.11.20 = {
	rem_attacker = MLO
	rem_defender = VEN
}

1450.6.1 = {
	add_attacker = MLO
	add_defender = VEN
}

# Treaty of Lodi
1454.4.9 = {
	rem_attacker = MLO
	rem_defender = VEN
}

