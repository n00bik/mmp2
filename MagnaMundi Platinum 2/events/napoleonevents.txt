###################COLONY EVENTS###########################


province_event = {

	id = 6400

	trigger = {
		has_empty_adjacent_province = yes
		is_overseas = yes
		is_colony = no
		has_owner_culture = yes
		any_neighbor_province = {	#require an existing cluster of provinces
			port = yes
			owned_by = THIS
			is_colony = no
		}
		NOT = { has_province_flag = colonial_expansion_random }
		NOT = { has_province_flag = colonial_expansion_random_target }
		OR = {
			AND = {
				region = south_american_region
				south_american_region = {
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = south_american_region
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
			}
			AND = {
				region = central_american_region
				central_american_region = {
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = central_american_region
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
			}
			AND = {
				region = north_american_region
				north_american_region = {
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = north_american_region
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
			}
			AND = {
				region = central_africa
				central_africa = {
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = central_africa
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
			}
			AND = {
				region = south_africa
				south_africa = {
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = south_africa
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		
		modifier = {
			factor = 0.5
			region = amazonas
			owner = {
				ai = yes
				tag = POR
			}
		}
		modifier = {
			factor = 0.5
			region = amazonas
			owner = {
				ai = yes
				tag = POR
			}
		}
		modifier = {
			factor = 0.5
			region = the_spanish_main
			owner = {
				ai = yes
				OR = {
					tag = CAS
					tag = SPA
				}
			}
		}
		modifier = {
			factor = 0.5
			region = central_american_region
			owner = {
				ai = yes
				OR = {
					tag = CAS
					tag = SPA
				}
			}
		}
		modifier = {
			factor = 0.75
			region = la_plata_region
			owner = {
				ai = yes
				OR = {
					tag = CAS
					tag = SPA
				}
			}
		}
		modifier = {
			factor = 0.6
			region = the_andes
			owner = {
				ai = yes
				OR = {
					tag = CAS
					tag = SPA
				}
			}
		}
		modifier = {
			factor = 0.5
			region = the_thirteen_colonies
			owner = {
				ai = yes
				OR = {
					tag = ENG
					tag = GBR
				}
			}
		}
		modifier = {
			factor = 0.75
			region = northern_america
			owner = {
				ai = yes
				tag = FRA
			}
		}
		modifier = {
			factor = 0.9
			region = the_mississippi_region
			owner = {
				ai = yes
				tag = FRA
			}
		}
		modifier = {
			factor = 0.75
			region = indonesian_region
			owner = {
				ai = yes
				tag = NED
			}
		}
		modifier = {
			factor = 0.95
			owner = { war = no }
		}
		modifier = {
			factor = 0.95
			owner = { idea =  colonial_ventures }
		}
		modifier = {
			factor = 0.95
			owner = { idea =  quest_for_the_new_world }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { innovative_narrowminded = -1 } }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { innovative_narrowminded = -2 } }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { innovative_narrowminded = -3 } }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { innovative_narrowminded = -4 } }
		}
		modifier = {
			factor = 1.05
			owner = { innovative_narrowminded = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { innovative_narrowminded = 3 }
		}
		modifier = {
			factor = 1.05
			owner = { innovative_narrowminded = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { innovative_narrowminded = 5 }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { centralization_decentralization = -1 } }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { centralization_decentralization = -2 } }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { centralization_decentralization = -3 } }
		}
		modifier = {
			factor = 0.95
			owner = { NOT = { centralization_decentralization = -4 } }
		}
		modifier = {
			factor = 1.05
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 1.05
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { centralization_decentralization = 5 }
		}
	}

	
	title = "EVTNAME6400"
	desc = "EVTDESC6400"

	immediate = {
		set_province_flag = regions_invisible
		random_country = {
			limit = { THIS = { has_province_flag = regions_invisible } }
			south_american_region = {
				limit = {
					THIS = { region = south_american_region }
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = south_american_region
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
				set_province_flag = targeted_for_ventures
			}
			central_american_region = {
				limit = {
					THIS = { region = central_american_region }
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = central_american_region
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
				set_province_flag = targeted_for_ventures
			}
			north_american_region = {
				limit = {
					THIS = { region = north_american_region }
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = north_american_region
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
				set_province_flag = targeted_for_ventures
			}
			central_africa = {
				limit = {
					THIS = { region = central_africa }
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = central_africa
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
				set_province_flag = targeted_for_ventures
			}
			south_africa = {
				limit = {
					THIS = { region = south_africa }
					empty = yes
					any_neighbor_province = {
						owned_by = THIS
						is_overseas = yes
						is_colony = no
						has_owner_culture = yes
						region = south_africa
						NOT = { has_province_flag = colonial_expansion_random }
						NOT = { has_province_flag = colonial_expansion_random_target }
						any_neighbor_province = {
							port = yes
							owned_by = THIS
							is_colony = no
						}
					}
				}
				set_province_flag = targeted_for_ventures
			}
		}
		clr_province_flag = regions_invisible
	}
	option = {
		name = "EVTOPTA6400"
		random_empty_neighbor_province = {
			limit = { has_province_flag = targeted_for_ventures }
			create_colony = 2
			set_province_flag = colonial_expansion_random_target
			set_province_flag = colonial_province
		}
		set_province_flag = colonial_expansion_random
		random_country = {
			limit = { THIS = { has_province_flag = colonial_expansion_random } }
			south_american_region = {
				limit = { has_province_flag = targeted_for_ventures }
				clr_province_flag = targeted_for_ventures
			}
			central_american_region = {
				limit = { has_province_flag = targeted_for_ventures }
				clr_province_flag = targeted_for_ventures
			}
			north_american_region = {
				limit = { has_province_flag = targeted_for_ventures }
				clr_province_flag = targeted_for_ventures
			}
			central_africa = {
				limit = { has_province_flag = targeted_for_ventures }
				clr_province_flag = targeted_for_ventures
			}
			south_africa = {
				limit = { has_province_flag = targeted_for_ventures }
				clr_province_flag = targeted_for_ventures
			}
		}
	}
}

province_event = {

	id =  6401
	trigger = {
		citysize = 201
		is_colony = yes
		owner = { number_of_colonies = 4 }
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6401"
	desc = "EVTDESC6401"
		
	option = {
		name = "EVTOPTA6401"
		citysize = -200
		owner = {
			random_owned = {
				limit = { is_colony = yes }
				citysize = 200
			}
		}	
	}
}

#######Tribal to Monarchy###########


#######Combat Event#######

province_event = {

	id = 6403
	trigger = {
		has_siege = no
		has_owner_religion = yes
		units_in_province = THIS
		local_enemy = {
			infantry_in_province = 1
                	NOT = { tag = NAT }
                	NOT = { tag = REB }
                       	NOT = { religion = THIS }
		}
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6403"
	desc = "EVTDESC6403"			

	option = {
		name = "EVTOPTA6403"
        	local_enemy = { country_event = 6404 }
	}
        option = {
		name = "EVTOPTB6403"
		owner = { years_of_income = -0.10 }
	}
}

country_event = {
	
	id = 6404
	is_triggered_only = yes

	title = "EVTNAME6404"
	desc = "EVTDESC6404"

	option = {
		name = "EVTOPTA6404"	
		FROM = { infantry = THIS }
	}
}

#######Combat Events#######

country_event = {

	id = 6405
	
	trigger = {
		OR = {
			religion = animism
			religion = shamanism
		}
		has_country_modifier = western_influences
		any_neighbor_province = {
			AND = {
				owner = { technology_group = western_europe }
				religion = protestant
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 2
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 3
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 4
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 5
		}
	}

	title = "EVTNAME6405"
	desc = "EVTDESC6405"

	option = {
		name = "EVTOPTA6405"
        	religion = protestant
	}
        option = {
		name = "EVTOPTB6405"
            	innovative_narrowminded = 1
	}

}


country_event = {

	id = 6406
	
	trigger = {
		OR = {
			religion = animism
			religion = shamanism
		}
		has_country_modifier = western_influences
		any_neighbor_province = {
			AND = {
				owner = { technology_group = western_europe }
				religion = reformed
			}
		}

	}
	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 2
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 3
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 4
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 5
		}
	}

	title = "EVTNAME6406"
	desc = "EVTDESC6406"

	option = {
		name = "EVTOPTA6406"
        	religion = reformed
	}
        option = {
		name = "EVTOPTB6406"
            	innovative_narrowminded = 1
	}

}


country_event = {

	id = 6407
	
	trigger = {
		OR = {
			religion = animism
			religion = shamanism
		}
		has_country_modifier = western_influences
		any_neighbor_province = {
			AND = {
				owner = { technology_group = western_europe }
				religion = catholic
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 2
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 3
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 4
		}
		modifier = {
			factor = 1.05
			innovative_narrowminded = 5
		}
	}

	title = "EVTNAME6407"
	desc = "EVTDESC6407"

	option = {
		name = "EVTOPTA6407"
        	religion = catholic
	}
        option = {
		name = "EVTOPTB6407"
            	innovative_narrowminded = 1
	}

}


#####Building Events#######

province_event = {

	id = 6408
	
	trigger = {
		NOT = { has_building = temple }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			government_tech = 9
			stability = 3
			OR = {
				AND = { 	
					idea = divine_supremacy
					idea = church_attendance_duty
				}		
				AND = {
					ai = yes
					government_tech = 12
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6408"
	desc = "EVTDESC6408"
	
	option = {
		name = "EVTOPTA6408"
		add_building = temple
	}
}


province_event = {

	id = 6409
	
	trigger = {
		NOT = { has_building = workshop }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			production_tech = 14
			stability = 3
			OR = {
				idea = bureaucracy
				AND = {
					ai = yes
					production_tech = 17
				}
			}
		}			
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6409"
	desc = "EVTDESC6409"
	
	option = {
		name = "EVTOPTA6409"
		add_building = workshop
	}
}


province_event = {

	id = 6410
	
	trigger = {
		NOT = { has_building = regimental_camp }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			land_tech = 22
			stability = 3
			OR = {
				AND = { 	
					idea = regimental_system
					idea = national_conscripts
				}		
				AND = {
					ai = yes
					land_tech = 25
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6410"
	desc = "EVTDESC6410"
	
	option = {
		name = "EVTOPTA6410"
		add_building = regimental_camp
	}
}


province_event = {

	id = 6411
	
	trigger = {
		NOT = { has_building = shipyard }
		port = yes
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			naval_tech = 29
			stability = 3
			OR = {
				AND = { 	
					idea = excellent_shipwrights
					idea = press_gangs
				}		
				AND = {
					ai = yes
					naval_tech = 32
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6411"
	desc = "EVTDESC6411"
	
	option = {
		name = "EVTOPTA6411"
		add_building = shipyard
	}
}


province_event = {

	id = 6412
	
	trigger = {
		NOT = { has_building = constable }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			government_tech = 25
			stability = 3
			OR = {
				AND = { 	
					idea = bureaucracy
					idea = national_bank
				}		
				AND = {
					ai = yes
					government_tech = 28
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6412"
	desc = "EVTDESC6412"
	
	option = {
		name = "EVTOPTA6412"
		add_building = constable
	}
}


province_event = {

	id = 6413
	
	trigger = {
		NOT = { has_building = marketplace }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			trade_tech = 26
			stability = 3
			OR = {
				AND = { 	
					idea = national_trade_policy
					idea = merchant_adventures
				}		
				AND = {
					ai = yes
					trade_tech = 29
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6413"
	desc = "EVTDESC6413"
	
	option = {
		name = "EVTOPTA6413"
		add_building = marketplace
	}
}


province_event = {

	id = 6414
	
	trigger = {
		NOT = { has_building = courthouse }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			government_tech = 20
			stability = 3
			OR = {
				AND = { 	
					idea = bill_of_rights
					idea = humanist_tolerance
				}		
				AND = {
					ai = yes
					government_tech = 23
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6414"
	desc = "EVTDESC6414"
	
	option = {
		name = "EVTOPTA6414"
		add_building = courthouse
	}
}


province_event = {

	id = 6415
	
	trigger = {
		NOT = { has_building = tax_assessor }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			government_tech = 36
			stability = 3
			OR = {
				AND = { 	
					idea = bill_of_rights
					idea = bureaucracy
				}		
				AND = {
					ai = yes
					government_tech = 39
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6415"
	desc = "EVTDESC6415"
	
	option = {
		name = "EVTOPTA6415"
		add_building = tax_assessor
	}
}


province_event = {

	id = 6416
	
	trigger = {
		NOT = { has_building = customs_house }
		is_colony = no
		is_core = THIS
		NOT = { revolt_risk = 1 }
		owner = {
			government_tech = 29
			stability = 3
			OR = {
				AND = { 	
					idea = smithian_economics
					idea = bureaucracy
				}		
				AND = {
					ai = yes
					government_tech = 32
				}
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 3 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { centralization_decentralization = 5 }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 1.05
			owner = { NOT = { stability = 3 } }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 2 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 4 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 6 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 8 }
		}
		modifier = {
			factor = 1.05
			owner = { war_exhaustion = 10 }
		}
	}

	title = "EVTNAME6416"
	desc = "EVTDESC6416"
	
	option = {
		name = "EVTOPTA6416"
		add_building = customs_house
	}
}

######### Product Events #######


province_event = {

	id = 6418

	trigger = {
		trade_goods = grain 
		NOT = { max_manpower = yes }
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6418"
	desc = "EVTDESC6418"

	option = {
		name = "EVTOPTA6418"
		owner = {
			manpower = 0.1
		}
	}
}

province_event = {
		
	id = 6419 

	trigger = {
		trade_goods = wine 
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6419"
	desc = "EVTDESC6419"

	option = {
		name = "EVTOPTA6419"
		add_province_modifier = {
			name = "excellent_vintage"
			duration = 360
		}
	}
}

province_event = {
		
	id = 6420 

	trigger = {
		trade_goods = wine 
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6420"
	desc = "EVTDESC6420"

	option = {
		name = "EVTOPTA6420"
		owner = { prestige = -0.01 }
	}
}

province_event = {
		
	id = 6421 

	trigger = {
		trade_goods = wool 
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6421"
	desc = "EVTDESC6421"

	option = {
		name = "EVTOPTA6421"
		owner = {
			treasury = 25
		}
	}
}


province_event = {
		
	id = 6422 

	trigger = {
		trade_goods = cloth 
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6422"
	desc = "EVTDESC6422"

	option = {
		name = "EVTOPTA6422"
		owner = {
			add_country_modifier = {
				name = "poor_uniforms"
				duration = 360
			}
		}
	}
}


province_event = {
		
	id = 6423 

	trigger = {
		trade_goods = fish 
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6423"
	desc = "EVTDESC6423"

	option = {
		name = "EVTOPTA6423"
		owner = {
			navy_tradition = 0.02
		}
	}
}


province_event = {
		
	id = 6424

	trigger = {
		trade_goods = fur 
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6424"
	desc = "EVTDESC6424"

	option = {
		name = "EVTOPTA6424"
		owner = {
			treasury = -25
		}
	}
}

province_event = {
		
	id = 6425

	trigger = {
		trade_goods = salt
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6425"
	desc = "EVTDESC6425"

	option = {
		name = "EVTOPTA6425"
		owner = {
			add_country_modifier = {
				name = "salt_crisis"
				duration = 360
			}
		}
	}
}

province_event = {
		
	id = 6426

	trigger = {
		trade_goods = naval_supplies
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6426"
	desc = "EVTDESC6426"

	option = {
		name = "EVTOPTA6426"
		owner = {
			add_country_modifier = {
				name = "good_supplies"
				duration = 360
			}
		}
	}
}


province_event = {
		
	id = 6427

	trigger = {
		trade_goods = copper
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6427"
	desc = "EVTDESC6427"

	option = {
		name = "EVTOPTA6427"
		owner = {
			add_country_modifier = {
				name = "poor_copper"
				duration = 360
			}
		}
	}
}
province_event = {
		
	id = 6428

	trigger = {
		trade_goods = gold
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6428"
	desc = "EVTDESC6428"

	option = {
		name = "EVTOPTA6428"
		owner = {
			treasury = 400
			inflation = 0.01
		}
	}
}


province_event = {
		
	id = 6429

	trigger = {
		trade_goods = iron
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6429"
	desc = "EVTDESC6429"

	option = {
		name = "EVTOPTA6429"
		owner = {
			add_country_modifier = {
				name = "poor_iron"
				duration = 360
			}
		}
	}
}


province_event = {
		
	id = 6430

	trigger = {
		trade_goods = slaves
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6430"
	desc = "EVTDESC6430"

	option = {
		name = "EVTOPTA6430"
		add_province_modifier = {
			name = "epidemic"
			duration = 360
		}
	}
}

province_event = {
		
	id = 6431

	trigger = {
		trade_goods = ivory
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6431"
	desc = "EVTDESC6431"

	option = {
		name = "EVTOPTA6431"
		owner = {
			prestige = 0.01
		}
	}
}

province_event = {
		
	id = 6432

	trigger = {
		trade_goods = tea
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6432"
	desc = "EVTDESC6432"

	option = {
		name = "EVTOPTA6432"
		revolt_risk = 1
	}
}

province_event = {
		
	id = 6433

	trigger = {
		trade_goods = chinaware
		owner = {
			capital_scope = {
				continent = europe
			}
		}
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6433"
	desc = "EVTDESC6433"

	option = {
		name = "EVTOPTA6433"
		owner = {
			prestige = -0.01
		}
	}
	option = {
		name = "EVTOPTB6433"
		add_province_modifier = {
			name = "storms"
			duration = 360
		}
	}
}

province_event = {
		
	id = 6434

	trigger = {
		trade_goods = spices
		
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6434"
	desc = "EVTDESC6434"

	option = {
		name = "EVTNAME6434"
		add_province_modifier = {
			name = "harvest_fails"
			duration = 360
		}
	}
}

province_event = {
		
	id = 6435

	trigger = {
		owner = { not = { has_country_modifier = the_abolish_slavery_act } }
		OR = {
			trade_goods = coffee
			trade_goods = cotton
			trade_goods = sugar
			trade_goods = tobacco
		}
		NOT = {
			religion = animism
			religion = shamanism
		}
	}

	mean_time_to_happen = {
		months = 5000
	}

	title = "EVTNAME6435"
	desc = "EVTDESC6435"

	option = {
		name = "EVTOPTA6435"
		revolt_risk = 1
	}
}