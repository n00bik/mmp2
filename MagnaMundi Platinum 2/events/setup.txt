#Carlos



#########################################################
#		AI Events			        #
#########################################################

country_event = {

	id = 905500
	
	trigger = {
		NOT = { has_country_flag = ai_controlled }
		NOT = { has_country_flag = player_controlled }
		OR = {
			luck = yes
			NOT = { ai = yes }
		}
  	}

	mean_time_to_happen = {
		days = 1
	}

	title = "EVTNAME905500"
	desc = "EVTDESC905500"

	immediate = {	# should be in immediate scope, to avoid multiple triggerings on May 31st
		random_owned = {	# Helius:  AI_Mod Setup and HRE Setup
			limit = { NOT = { has_global_flag = magna_mundi } }
			set_global_flag = magna_mundi
			EMP = {
				country_event = 295099		# AI_Mod
			}
			emperor = { country_event = 117000 }	# HRE
		} 	# will also trigger setup for AE, Jews, Trade Acumen, etc. 
	}
	option = {
	name = "EVTOPTA905500" #	OK
		ai_chance = { factor = 100 }

		random_owned = {
			limit = {
				owner = {
					NOT = { ai = yes }
				}
			}
			owner = {
				set_country_flag = player_controlled
	    			set_country_flag = loans_normal
			}
		}
		random_owned = {
			limit = {
				owner = {
					ai = yes
				}
			}
			owner = {
				set_country_flag = ai_controlled
	     			set_country_flag = loans_normal
				any_country = {
					limit = {
						ai = yes
					}
					set_country_flag = ai_controlled
	     				set_country_flag = loans_normal
				}
			}
		}
		
	
		any_country = {
			limit = {
				num_of_ports = 9
			}
				add_country_modifier = {
					name = "national_defence_plan"
					duration = -1
				}

		}
		
		any_country = {
			limit = {
				num_of_ports = 1
			}

			set_variable = { which = "numero_de_portos" value = 6 }

		}
				

	}

}



country_event = {

	id = 905503

	trigger = {
		has_country_flag = ai_controlled
		NOT = { ai = yes } #NA-only trigger
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME905503" #	Regime Change
	desc = "EVTDESC905503" #	In $YEAR$ the government of $MONARCH$ showed dramatic changes in policy, almost as if a different ruler were at the helm of $COUNTRY$.

	option = {
	name = "EVTOPTA905503" #	Setting player status properly
		ai_chance = { factor = 100 }
		clr_country_flag = ai_controlled
		set_country_flag = player_controlled
	}
}

country_event = {

	id = 905504

	trigger = {
		has_country_flag = player_controlled
		ai = yes #NA-only trigger
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME905504" #	Regime Change
	desc = "EVTDESC905504" #	In $YEAR$ the government of $MONARCH$ showed dramatic changes in policy, almost as if a different ruler were at the helm of $COUNTRY$.

	option = {
	name = "EVTOPTA905504" #	Setting player status properly
		ai_chance = { factor = 100 }
		set_country_flag = ai_controlled
		clr_country_flag = player_controlled
	}
}

