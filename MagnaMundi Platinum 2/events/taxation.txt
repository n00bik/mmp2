province_event = { 

	id = 774001

	trigger = {
		has_province_modifier = provincial_tax_h
		
		any_neighbor_province = {
	
			owned_by = THIS
			is_colony = no
			NOT = { has_province_modifier = provincial_tax_h }	
			NOT = { has_province_modifier = provincial_tax_vh }

		}

	}
	
	mean_time_to_happen = {
		years = 10 


		modifier = {

			factor = 1.5
			owner = {
				stability = 3
			}
		}

		modifier = {

			factor = 1.25
			owner = {
				stability = 2
			}
		}

		modifier = {

			factor = 1.25
			owner = {
				stability = 1
			}
		}

		modifier = {

			factor = 0.8
			owner = {
				NOT = { stability = 0 }
			}
		}

		modifier = {

			factor = 0.75
			owner = {
				NOT = { stability = -1 }
			}
		}

		modifier = {

			factor = 0.7
			owner = {
				NOT = { stability = -2 }
			}
		}

	}

	title = "EVTNAME774001" #	Taxations Problems
	desc = "EVTDESC774001" #	Province dwellers started to note a neighbor province has special lower taxes than their own. They don't understand why are they targeted with so harsh measures...

	option = { 
	name = "EVTOPTA774001" #	But we need their gold!
		ai_chance = { factor = 50 } 

		random_list = {
			25 = { 
				revolt_risk = 3 
			}

			15 = { 
				revolt_risk = 6 
			}

			25 = { 
				anti_tax_rebels = 1
			}

			15 = { 
				anti_tax_rebels = 2
			}

			20 = { 

			}

		}



	}

	option = {
	name = "EVTOPTB774001" #	Remove special taxes
		ai_chance = { factor = 50 } 

		owner = { prestige = -0.1 }
		remove_province_modifier = provincial_tax_h


	}

}


province_event = { 

	id = 774002

	trigger = { 
		has_province_modifier = provincial_tax_vh

		any_neighbor_province = {
	
			owned_by = THIS
			is_colony = no	
			NOT = { 
				has_province_modifier = provincial_tax_vh
			}

		}

	}
	
	mean_time_to_happen = {
		years = 10 


		modifier = {

			factor = 1.5
			owner = {
				stability = 3
			}
		}

		modifier = {

			factor = 1.25
			owner = {
				stability = 2
			}
		}

		modifier = {

			factor = 1.25
			owner = {
				stability = 1
			}
		}

		modifier = {

			factor = 0.8
			owner = {
				NOT = { stability = 0 }
			}
		}

		modifier = {

			factor = 0.75
			owner = {
				NOT = { stability = -1 }
			}
		}

		modifier = {

			factor = 0.7
			owner = {
				NOT = { stability = -2 }
			}
		}


	}

	title = "EVTNAME774002" #	Serious Problems in %PROVINCENAME%
	desc = "EVTDESC774002" #	Province dwellers started to note a neighbor province has lower taxes than their own. They don't understand why are they targeted with so harsh measures...

	option = { 
	name = "EVTOPTA774002" #	But we need their gold!
		ai_chance = { factor = 50 } 

		random_list = {
			25 = { 
				revolt_risk = 6 
			}

			20 = { 
				revolt_risk = 12 
			}

			25 = { 
				anti_tax_rebels = 1
			}

			20 = { 
				anti_tax_rebels = 2
			}

			5 = { 
				anti_tax_rebels = 3
				owner = {
					stability = -1
				}

			}

			5 = { 

			}

		}



	}

	option = {
	name = "EVTOPTB774002" #	Remove special taxes
		ai_chance = { factor = 50 } 

		owner = { prestige = -0.05 }
		remove_province_modifier = provincial_tax_vh


	}

}
