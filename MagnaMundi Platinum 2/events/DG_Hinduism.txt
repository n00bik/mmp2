# DEI GRATIA (DAVID)
# COMPATIBLE WITH V2

############
# Hinduism #
############

# ID RANGE 57 Dharmic Religions + 30 Hinduism + xx

################
# Hindu Events #
################

#01 A Favored School
#02 Bhakti Saint in $PROVINCENAME$
#03 Matha founded in $PROVINCENAME$
#04 Tantrism
#05 Tirtha pilgrimage
#06 REMOVED
#07 Self-Immolation
#08 The Caste System

#######################
# Reactions to Hindus #
#######################

#21 Children of the Book (muslim countries)
#22 Muslim castes

country_event = {

id = 573001

trigger = {
	religion = hinduism
	NOT = { has_country_modifier = vaishnavism }
	NOT = { has_country_modifier = shaivism }
	NOT = { has_country_modifier = shaktism }
	NOT = { has_country_modifier = smartism }
	check_variable = { which = "religious_fervor" value = -3 }
	NOT = { check_variable = { which = "religious_fervor" value = 5 } } #May use decisions instead
	}

mean_time_to_happen = { 

	months = 240
	
	modifier = { 
		NOT = { check_variable = { which = "religious_fervor" value = 1 } }
		factor = 2
		}
	}

title = "EVTNAME573001"
desc = "EVTDESC573001"

option = {
	name = "EVTOPTA573001" #The devotion of Vishnu, the only god
	ai_chance = { 
		factor = 25
		modifier = {
			any_neighbor_country = { religion_group = muslim }
			factor = 1.4
			}
		modifier = {
			culture_group = western_aryan
			factor = 4
			}
		}
	add_country_modifier = { name = "vaishnavism" duration = 9125 }
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			has_country_modifier = vaishnavism
			}
		relation = { who = THIS value = 25 }
		}
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			OR = {
				has_country_modifier = shaktism
				has_country_modifier = smartism
				has_country_modifier = shaivism
				}
			}
		relation = { who = THIS value = -25 }
		}
	}
option = {
	name = "EVTOPTB573001" #The mysticism of Shiva above all
	ai_chance = { 
		factor = 25
		modifier = {
			culture_group = hindusthani
			factor = 2
			}
		modifier = {
			culture_group = dravidian
			factor = 4
			}
		}
	add_country_modifier = { name = "shaivism" duration = 9125 }
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			has_country_modifier = shaivism
			}
		relation = { who = THIS value = 25 }
		}
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			OR = {
				has_country_modifier = shaktism
				has_country_modifier = smartism
				has_country_modifier = vaishnavism
				}
			}
		relation = { who = THIS value = -25 }
		}
	}
option = {
	name = "EVTOPTC573001" #The magic and power of Shakti is alluring
	ai_chance = { 
		factor = 25
		modifier = {
			culture_group = eastern_aryan
			factor = 4
			}
		modifier = {
			culture_group = dravidian
			factor = 2
			}
		}
	add_country_modifier = { name = "shaktism" duration = 9125 }
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			has_country_modifier = shaktism
			}
		relation = { who = THIS value = 25 }
		}
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			OR = {
				has_country_modifier = shaktism
				has_country_modifier = smartism
				has_country_modifier = vaishnavism
				}
			}
		relation = { who = THIS value = -25 }
		}
	}
option = {
	name = "EVTOPTD573001" #All gods are manifestations of Brahman
	ai_chance = { 
		factor = 25
		modifier = {
			culture_group = hindusthani
			factor = 2
			}
		modifier = {
			culture_group = dravidian
			factor = 2
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = 1 } }
			factor = 4
			}
		}
	add_country_modifier = { name = "smartism" duration = 9125 }
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			has_country_modifier = smartism
			}
		relation = { who = THIS value = 25 }
		}
	any_neighbor_country = {
		limit = { 
			religion = hinduism
			OR = {
				has_country_modifier = shaktism
				has_country_modifier = vaishnavism
				has_country_modifier = shaivism
				}
			}
		relation = { who = THIS value = -25 }
		}
	}

}

province_event = { #Bhakti Saint

id = 573002

trigger = {
	religion = hinduism
	NOT = { has_siege = yes }
	controller = { capital_scope = { owned_by = THIS } }
	owner = {
		religion = hinduism
		NOT = { has_country_modifier = smartism }
		NOT = { advisor = swami }
		check_variable = { which = "religious_fervor" value = -3 }
		}
	}

mean_time_to_happen = {

	months = 2400

	modifier = {
		owner = { has_country_modifier = vaishnavism }
		factor = 0.5
		}			
	modifier = {
		NOT = { year = 1500 }
		factor = 2
		}
	modifier = {
		any_neighbor_province = { 
			religion = hinduism
			owner = { religion_group = muslim } 
			}
		factor = 0.5
		}
	modifier = { 
		NOT = { owner = { check_variable = { which = "religious_fervor" value = 1 } } }
		factor = 2
		}
	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 5 } }
		factor = 0.7
		}
	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 9 } }
		factor = 0.7
		}
	modifier = {
		owner = { NOT = { num_of_cities = 3 } }
		factor = 0.5
		}
	modifier = { 
		owner = { num_of_cities = 5 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 10 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 20 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 40 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 80 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 160 }
		factor = 1.6
		}
	}

title = "EVTNAME573002"
desc = "EVTDESC573002"

option = {
	name = "EVTOPTA573002" #Support the movement
	ai_chance = { factor = 50 }
	owner = {
		change_variable = { which = "clergy_mood" value = -5 }
		manpower = -1
		create_advisor = swami
		random_owned = {
			limit = { owner = { innovative_narrowminded = 1 } }
			owner = { innovative_narrowminded = -1 }
			}
		}
	}
option = {
	name = "EVTOPTB573002" #Support the brahmin
	ai_chance = { factor = 50 }
	owner = {
		change_variable = { which = "clergy_mood" value = 5 }
		random_owned = { 
			limit = { owner = { stability = 3 } }
			owner = { prestige = 0.04 }
			}
		random_owned = { 
			limit = { owner = { NOT = { stability = 3 } } }
			owner = { stability = 1 prestige = -0.01 }
			}
		random_owned = {
			limit = { owner = { NOT = { innovative_narrowminded = 3 } } }
			owner = { innovative_narrowminded = 1 }
			}
		}
	}

}

province_event = { #Matha founded

id = 573003

trigger = {
	religion = hinduism
	owner = { 
		religion = hinduism 
		check_variable = { which = "religious_fervor" value = -3 }
		}
	NOT = { has_province_modifier = matha }
	NOT = { has_siege = yes }
	controller = { capital_scope = { owned_by = THIS } }
	}

mean_time_to_happen = {

	months = 2400
	
	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 5 } }
		factor = 0.7
		}
	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 9 } }
		factor = 0.7
		}
	modifier = { 
		NOT = { owner = { check_variable = { which = "religious_fervor" value = 1 } } }
		factor = 2
		}
	modifier = {
		has_province_flag = ashram
		factor = 5
		}
	modifier = {
		has_province_flag = matha
		factor = 3
		}
	modifier = {
		OR = {
			owner = { has_country_modifier = shaivism }
			owner = { has_country_modifier = smartism }
			}
		factor = 0.5
		}
	modifier = { 
		NOT = { citysize = 10000 }
		factor = 0.7
		}
	modifier = {
		citysize = 50000
		factor = 1.5
		}
	modifier = {
		citysize = 250000
		factor = 1.5
		}
	modifier = {
		owner = { NOT = { num_of_cities = 3 } }
		factor = 0.5
		}
	modifier = { 
		owner = { num_of_cities = 5 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 10 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 20 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 40 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 80 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 160 }
		factor = 1.6
		}
	}

title = "EVTNAME573003"
desc = "EVTDESC573003"

option = {
	name = "EVTOPTA573003" #Allow an ashram to gather
	owner = {	production_tech = +25 }
	set_province_flag = ashram
	}
option = {
	name = "EVTOPTB573003" #Encourage a matha to be built
	set_province_flag = matha
	owner = {
		prestige = 0.01
		treasury = -5
		production_tech = +25
		random = { chance = 10 create_advisor = ascetic_monk }
		}
	add_province_modifier = { name = "matha" duration = 7300 }
	}

}

country_event = { #Tantrism

id = 573004

trigger = {
	religion = hinduism
	check_variable = { which = "religious_fervor" value = -3 }
	NOT = { has_country_modifier = vaishnavism }
	NOT = { has_country_modifier = shaivism }
	NOT = { has_country_modifier = smartism }
	NOT = { has_country_modifier = religious_scandal_country }
	}

mean_time_to_happen = {

	months = 1200

	modifier = {
		has_country_modifier = shaktism
		factor = 0.5
		}
	modifier = { 
		check_variable = { which = "religious_fervor" value = 5 }
		factor = 0.7
		}
	modifier = { 
		check_variable = { which = "religious_fervor" value = 9 }
		factor = 0.7
		}
	modifier = { 
		NOT = { check_variable = { which = "religious_fervor" value = 1 } }
		factor = 2
		}

	}

title = "EVTNAME573004"
desc = "EVTDESC573004"

option = {
	name = "EVTOPTA573004" #Openly display our support
	ai_chance = { 
		factor = 50
		modifier = {
			NOT = { has_country_modifier = shaktism }
			factor = 0.5
			}
		modifier = { 
			check_variable = { which = "religious_fervor" value = 5 }
			factor = 1.4
			}
		modifier = { 
			check_variable = { which = "religious_fervor" value = 9 }
			factor = 1.4
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = 1 } }
			factor = 0.5
			}
		}
	change_variable = { which = "clergy_mood" value = 5 }
	random_owned = {
		limit = { owner = { innovative_narrowminded = -2 } }
		owner = { innovative_narrowminded = -1 }
		}
	prestige = 0.01
	any_country = {
		limit = {
			neighbour = THIS
			NOT = { capital_scope = { owned_by = THIS } }
			NOT = { has_country_modifier = shaktism }
			religion = hinduism
			check_variable = { which = "religious_fervor" value = 9 }
			}
		relation = { who = THIS value = -50 }
		casus_belli = THIS
		}
	any_country = {
		limit = {
			neighbour = THIS
			NOT = { capital_scope = { owned_by = THIS } }
			NOT = { has_country_modifier = shaktism }
			religion = hinduism
			check_variable = { which = "religious_fervor" value = 5 }
			NOT = { check_variable = { which = "religious_fervor" value = 9 } }
			}
		relation = { who = THIS value = -25 }
		}
	any_country = {
		limit = {
			neighbour = THIS
			NOT = { capital_scope = { owned_by = THIS } }
			NOT = { has_country_modifier = shaktism }
			religion = hinduism
			check_variable = { which = "religious_fervor" value = 1 }
			NOT = { check_variable = { which = "religious_fervor" value = 5 } }
			}
		relation = { who = THIS value = -10 }
		}
	any_country = {
		limit = {
			neighbour = THIS
			NOT = { capital_scope = { owned_by = THIS } }
			NOT = { has_country_modifier = shaktism }
			NOT = { religion = hinduism }
			check_variable = { which = "religious_fervor" value = 9 }
			}
		relation = { who = THIS value = -100 }
		casus_belli = THIS
		}
	any_country = {
		limit = {
			neighbour = THIS
			NOT = { capital_scope = { owned_by = THIS } }
			NOT = { has_country_modifier = shaktism }
			NOT = { religion = hinduism }
			NOT = { check_variable = { which = "religious_fervor" value = 9 } }
			check_variable = { which = "religious_fervor" value = 5 }
			}
		relation = { who = THIS value = -50 }
		casus_belli = THIS
		}
	any_country = {
		limit = {
			neighbour = THIS
			NOT = { capital_scope = { owned_by = THIS } }
			NOT = { has_country_modifier = shaktism }
			NOT = { religion = hinduism }
			NOT = { check_variable = { which = "religious_fervor" value = 5 } }
			check_variable = { which = "religious_fervor" value = 1 }
			}
		relation = { who = THIS value = -25 }
		}
	}
option = {
	name = "EVTOPTB573004" #Rein in the movement
	ai_chance = { 
		factor = 50 
		modifier = {
			NOT = { has_country_modifier = shaktism }
			factor = 2
			}
		}
	change_variable = { which = "clergy_mood" value = -5 }
	random_owned = {
		limit = { owner = { NOT = { innovative_narrowminded = 5 } } }
		owner = { innovative_narrowminded = 1 }
		}
	random_owned = { 
		limit = { 
			owner = { 
				check_variable = { which = "religious_fervor" value = 9 }
				}
			}
		owner = { add_country_modifier = { name = "religious_scandal_country" duration = 2920 } }
		}
	random_owned = { 
		limit = { 
			owner = { 
				check_variable = { which = "religious_fervor" value = 5 }
				NOT = { check_variable = { which = "religious_fervor" value = 9 } }
				}
			}
		owner = { add_country_modifier = { name = "religious_scandal_country" duration = 2190 } }
		}
	random_owned = { 
		limit = { 
			owner = { 
				check_variable = { which = "religious_fervor" value = 1 }
				NOT = { check_variable = { which = "religious_fervor" value = 5 } }
				}
			}
		owner = { add_country_modifier = { name = "religious_scandal_country" duration = 1460 } }
		}
	random_owned = { 
		limit = { 
			owner = { 
				NOT = { check_variable = { which = "religious_fervor" value = 1 } }
				}
			}
		owner = { add_country_modifier = { name = "religious_scandal_country" duration = 730 } }
		}
	}
}

province_event = { #$PROVINCENAME$ becomes a center of pilgrimage (tirtha)

id = 573005

trigger = {
	religion = hinduism
	owner = { 
		religion = hinduism 
		check_variable = { which = "religious_fervor" value = -3 }
		}
	NOT = { has_siege = yes }
	controller = { capital_scope = { owned_by = THIS } }
	NOT = { has_province_flag = pilgrimage }
	NOT = { any_neighbor_province = { has_province_flag = pilgrimage } }
	}

mean_time_to_happen = { 

	months = 3600 

	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 5 } }
		factor = 0.7
		}
	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 9 } }
		factor = 0.7
		}
	modifier = { 
		NOT = { owner = { check_variable = { which = "religious_fervor" value = 1 } } }
		factor = 2
		}
	modifier = {
		owner = { NOT = { num_of_cities = 3 } }
		factor = 0.5
		}
	modifier = { 
		owner = { num_of_cities = 5 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 10 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 20 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 40 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 80 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 160 }
		factor = 1.6
		}
	}

title = "EVTNAME573005"
desc = "EVTDESC573005"

option = {
	name = "EVTOPTA573005" #Condemn this commercialization
	ai_chance = { 
		factor = 40
		modifier = { 
			owner = { missionaries = 5 }
			factor = 0.7
			}
		modifier = { 
			owner = { missionaries = 4 }
			factor = 0.7
			}
		modifier = { 
			owner = { NOT = { missionaries = 2 } }
			factor = 1.4
			}
		modifier = { 
			owner = { NOT = { missionaries = 1 } }
			factor = 1.4
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			factor = 1.4
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			factor = 1.4
			}
		modifier = { 
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
			factor = 0.5
			}
		}
	set_province_flag = pilgrimage
	owner = { 
		change_variable = { which = "clergy_mood" value = 5 }
		random_owned = { 
			limit = { 
				owner = { check_variable = { which = "religious_fervor" value = 9 } }
				}
			owner = { 
				missionaries = 3
				prestige = 0.04
				}
			}
		random_owned = { 
			limit = { 
				owner = { check_variable = { which = "religious_fervor" value = 5 } }
				owner = { NOT = { check_variable = { which = "religious_fervor" value = 9 } } }
				}
			owner = { 
				missionaries = 2
				prestige = 0.03
				}
			}
		random_owned = { 
			limit = { 
				owner = { check_variable = { which = "religious_fervor" value = 1 } }
				owner = { NOT = { check_variable = { which = "religious_fervor" value = 5 } } }
				}
			owner = { 
				missionaries = 1
				prestige = 0.02
				}
			}
		random_owned = { 
			limit = { 
				owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
				}
			owner = { 
				prestige = 0.01
				}
			}
		}
	}
option = {
	name = "EVTOPTB573005" #Take our share of the proceeds
	ai_chance = { factor = 60 }
	set_province_flag = pilgrimage
	base_tax = 1
	owner = { 
		random_owned = { 
			limit = { 
				owner = { check_variable = { which = "religious_fervor" value = 9 } }
				}
			owner = { 
				prestige = -0.02
				}
			}
		random_owned = { 
			limit = { 
				owner = { check_variable = { which = "religious_fervor" value = 5 } }
				owner = { NOT = { check_variable = { which = "religious_fervor" value = 9 } } }
				}
			owner = { 
				prestige = -0.015
				}
			}
		random_owned = { 
			limit = { 
				owner = { check_variable = { which = "religious_fervor" value = 1 } }
				owner = { NOT = { check_variable = { which = "religious_fervor" value = 5 } } }
				}
			owner = { 
				prestige = -0.01
				}
			}
		random_owned = { 
			limit = { 
				owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
				}
			owner = { 
				prestige = -0.005
				}
			}
		}
	}

}

country_event = { #Self-Immolation

id = 573007

trigger = {
	religion = hinduism
	check_variable = { which = "religious_fervor" value = -3 }
	}

mean_time_to_happen = { 

	months = 1200

	modifier = { 
		check_variable = { which = "religious_fervor" value = 5 }
		factor = 0.7
		}
	modifier = { 
		check_variable = { which = "religious_fervor" value = 9 }
		factor = 0.7
		}
	modifier = { 
		NOT = { check_variable = { which = "religious_fervor" value = 1 } }
		factor = 2
		}
	modifier = {
		serfdom_freesubjects = 4
		factor = 1.4
		}
	modifier = {
		serfdom_freesubjects = 2
		factor = 1.4
		}
	modifier = {
		NOT = { serfdom_freesubjects = -1 } 
		factor = 0.7
		}
	modifier = {
		NOT = { serfdom_freesubjects = -3 } 
		factor = 0.7
		}
	modifier = {
		has_country_flag = sati_banned
		factor = 3
		}
	modifier = {
		has_country_flag = symbolic_sati
		factor = 2
		}
	}

title = "EVTNAME573007"
desc = "EVTDESC573007"

option = {
	name = "EVTOPTA573007" #Leave the practice alone
	ai_chance = { 
		factor = 35 
		modifier = { 
			check_variable = { which = "religious_fervor" value = 5 }
			factor = 1.4
			}
		modifier = { 
			check_variable = { which = "religious_fervor" value = 9 }
			factor = 1.4
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = 1 } }
			factor = 0.5
			}
		modifier = { 
			has_country_flag = symbolic_sati
			factor = 0.7
			}
		modifier = { 
			has_country_flag = sati_banned
			factor = 0.7
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			}
		citysize = -1
		add_province_modifier = { name = "religious_scandal_province" duration = 120 }
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 9 } } }
			}
		citysize = -1
		add_province_modifier = { name = "religious_scandal_province" duration = 240 }
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 1 } }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 5 } } }
			}
		citysize = -1
		add_province_modifier = { name = "religious_scandal_province" duration = 360 }
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
			}
		citysize = -1
		add_province_modifier = { name = "religious_scandal_province" duration = 480 }
		revolt_risk = 1
		}
	}
option = {
	name = "EVTOPTB573007" #Encourage symbolic sati
	ai_chance = { 
		factor = 35 
		modifier = { 
			has_country_flag = symbolic_sati
			factor = 2
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			owner = { NOT = { has_country_flag = symbolic_sati } }
			}
		citysize = -1
		revolt_risk = 1
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 360 }
			stability = -1
			missionaries = -3
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 9 } } }
			owner = { NOT = { has_country_flag = symbolic_sati } }
			}
		citysize = -1
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 240 }
			missionaries = -3
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 1 } }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 5 } } }
			owner = { NOT = { has_country_flag = symbolic_sati } }
			}
		citysize = -1
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 120 }
			missionaries = -3
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
			owner = { NOT = { has_country_flag = symbolic_sati } }
			}
		citysize = -1
		owner = { missionaries = -3 }
		}
	random_owned = { 
		limit = { owner = { has_country_flag = symbolic_sati } }
		owner = { missionaries = -1 }
		}
	random_owned = {
		limit = { owner = { NOT = { has_country_flag = symbolic_sati } } }
		owner = { innovative_narrowminded = -1 }
		}
	change_variable = { which = "clergy_mood" value = -5 }
	set_country_flag = symbolic_sati
	}
option = {
	name = "EVTOPTC573007" #Ban the practice entirely
	ai_chance = { 
		factor = 30 
		modifier = { 
			check_variable = { which = "religious_fervor" value = 5 }
			factor = 0.7
			}
		modifier = { 
			check_variable = { which = "religious_fervor" value = 9 }
			factor = 0.7
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = 1 } }
			factor = 2
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = -3 } }
			factor = 2
			}
		modifier = { 
			has_country_flag = sati_banned
			factor = 2
			}
		}
	change_variable = { which = "clergy_mood" value = -10 }
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			owner = { NOT = { has_country_flag = sati_banned } }
			}
		citysize = -1
		revolt_risk = 3
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 480 }
			stability = -3
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 9 } } }
			owner = { NOT = { has_country_flag = sati_banned } }
			}
		citysize = -1
		revolt_risk = 2
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 360 }
			stability = -2
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { check_variable = { which = "religious_fervor" value = 1 } }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 5 } } }
			owner = { NOT = { has_country_flag = sati_banned } }
			}
		citysize = -1
		revolt_risk = 1
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 240 }
			stability = -1
			}
		}
	random_owned = {
		limit = { 
			religion = hinduism
			NOT = { has_province_modifier = religious_scandal_province }
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
			owner = { NOT = { has_country_flag = sati_banned } }
			}
		citysize = -1
		owner = { 
			add_country_modifier = { name = "religious_scandal_country" duration = 120 }
			}
		}
	random_owned = {
		limit = { owner = { NOT = { has_country_flag = sati_banned } } }
		owner = { serfdom_freesubjects = 1 }
		}
	set_country_flag = sati_banned
	}

}

country_event = { #The Caste System

id = 573008

trigger = {
	religion = hinduism
	serfdom_freesubjects = -2
	NOT = { serfdom_freesubjects = 3 }
	any_neighbor_country = { NOT = { religion = hinduism } }
	}

mean_time_to_happen = { 

	months = 1200

	modifier = {
		NOT = { innovative_narrowminded = -3 }
		factor = 0.7
		}
	modifier = {
		NOT = { innovative_narrowminded = -1 }
		factor = 0.7
		}
	modifier = {
		innovative_narrowminded = 2
		factor = 1.4
		}
	modifier = {
		innovative_narrowminded = 4
		factor = 1.4
		}
	modifier = {
		serfdom_freesubjects = 4
		factor = 1.4
		}
	modifier = {
		serfdom_freesubjects = 2
		factor = 1.4
		}
	modifier = {
		NOT = { serfdom_freesubjects = -1 } 
		factor = 0.7
		}
	modifier = {
		NOT = { serfdom_freesubjects = -3 } 
		factor = 0.7
		}
	}

title = "EVTNAME573008"
desc = "EVTDESC573008"

option = {
	name = "EVTOPTA573008" #Reinforce social structures
	ai_chance = { factor = 30 }
	random_owned = { 
		limit = { owner = { stability = 3 } }
		owner = { prestige = 0.05 }
		}
	random_owned = { 
		limit = { owner = { NOT = { stability = 3 } } }
		owner = { stability = 1 }
		}
	random = { chance = 50 serfdom_freesubjects = -1 }
	change_variable = { which = "clergy_mood" value = 5 }
	}
option = {
	name = "EVTOPTB573008" #Ignore these trends
	ai_chance = { factor = 40 }
	prestige = 0.001
	}
option = {
	name = "EVTOPTC573008" #Dismantle the caste system
	ai_chance = { 
		factor = 30 
		modifier = { 
			check_variable = { which = "religious_fervor" value = 5 }
			factor = 0.7
			}
		modifier = { 
			check_variable = { which = "religious_fervor" value = 9 }
			factor = 0.7
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = 1 } }
			factor = 1.4
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = -3 } }
			factor = 1.4
			}
		}
	stability = -1
	random = { chance = 50 serfdom_freesubjects = 1 }
	change_variable = { which = "clergy_mood" value = -5 }
	}

}

##############################
# Muslim Reactions to Hindus #
##############################

province_event = { #Children of the Book

id = 573021

trigger = {
	owner = { religion_group = muslim }
	religion = hinduism
	NOT = { tolerance_to_this = -1 }
	NOT = { owner = { has_country_modifier = religious_intolerance } }
	NOT = { owner = { has_country_modifier = religious_tolerance } }
	}

mean_time_to_happen = { 

	months = 1200 

	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 5 } }
		factor = 0.7
		}
	modifier = { 
		owner = { check_variable = { which = "religious_fervor" value = 9 } }
		factor = 0.7
		}
	modifier = { 
		owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
		factor = 1.4
		}
	modifier = { 
		owner = { NOT = { check_variable = { which = "religious_fervor" value = -3 } } }
		factor = 1.4
		}
	}

title = "EVTNAME573021"
desc = "EVTDESC573021"

option = {
	name = "EVTOPTA573021" #Hindus are also dhimmi
	ai_chance = { 
		factor = 30
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			factor = 0.7
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			factor = 0.7
			}
		modifier = { 
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
			factor = 1.4
			}
		modifier = { 
			owner = { NOT = { check_variable = { which = "religious_fervor" value = -3 } } }
			factor = 1.4
			}
		modifier = {
			owner = { MIL = 6 }
			factor = 0.8
			}
		modifier = {
			owner = { MIL = 8 }
			factor = 0.8
			}
		modifier = {
			owner = { ADM = 6 }
			factor = 1.25
			}
		modifier = {
			owner = { ADM = 8 }
			factor = 1.25
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.10 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.20 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.30 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.40 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.50 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.60 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.70 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.80 } }
			factor = 1.1
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.90 } }
			factor = 1.1
			}
		}
	owner = {
		add_country_modifier = {
			name = "religious_tolerance"
			duration = 3650
			}
		random_owned = {
			limit = { 
				is_capital = yes
				religion = hinduism
				}
			owner = { add_idea = humanist_tolerance }
			}
		}
	}
option = {
	name = "EVTOPTC573021" #It is a question for the ulemas
	ai_chance = { factor = 40 }
	owner = { 
		prestige = -0.01 
		change_variable = { which = "clergy_mood" value = 5 }
		}
	}
option = {
	name = "EVTOPTB573021" #They are idolators and must die
	ai_chance = { 
		factor = 30 
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.10 } }
			factor = 0.9
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.20 } }
			factor = 0.9
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.30 } }
			factor = 0.9
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.40 } }
			factor = 0.9
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.50 } }
			factor = 0.9
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.60 } }
			factor = 0.9
			}
		modifier = { 
			owner = { num_of_religion = { religion = hinduism value = 0.70 } }
			factor = 0
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			factor = 1.4
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			factor = 1.4
			}
		modifier = { 
			owner = { NOT = { check_variable = { which = "religious_fervor" value = 1 } } }
			factor = 0.7
			}
		modifier = { 
			owner = { NOT = { check_variable = { which = "religious_fervor" value = -3 } } }
			factor = 0.7
			}
		}
	religious_rebels = 1
	owner = {
		add_country_modifier = {
			name = "religious_intolerance"
			duration = 3650
			}
		any_neighbor_country = {
			limit = { religion = hinduism }
			relation = { who = THIS value = -50 }
			casus_belli = THIS
			}
		}
	}

}

country_event = { #Muslim Castes

id = 573022

trigger = {
	religion_group = muslim
	capital_scope = { religion = hinduism }
	serfdom_freesubjects = -2
	NOT = { serfdom_freesubjects = 3 }
	NOT = { has_country_modifier = religious_scandal_country }
	}

mean_time_to_happen = { 

	months = 1200

	modifier = {
		serfdom_freesubjects = 2
		factor = 1.4
		}
	modifier = {
		NOT = { serfdom_freesubjects = -1 }
		factor = 0.7
		}
	}

title = "EVTNAME573022"
desc = "EVTDESC573022"

option = {
	name = "EVTOPTA573022" #Discourage this practice
	ai_chance = { factor = 50 }
	stability = -1
	prestige = 0.01
	random = { chance = 50 serfdom_freesubjects = 1 }
	change_variable = { which = "clergy_mood" value = 5 }
	}
option = {
	name = "EVTOPTB573022" #Local customs are harmless
	ai_chance = { 
		factor = 50 
		modifier = { 
			check_variable = { which = "religious_fervor" value = 5 }
			factor = 0.7
			}
		modifier = { 
			check_variable = { which = "religious_fervor" value = 9 }
			factor = 0.7
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = 1 } }
			factor = 1.4
			}
		modifier = { 
			NOT = { check_variable = { which = "religious_fervor" value = -3 } }
			factor = 1.4
			}
		}
	change_variable = { which = "clergy_mood" value = -5 }
	random = { chance = 50 serfdom_freesubjects = -1 }
	random_owned = { 
		limit = { 
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			}
		owner = { 
			prestige = -0.02
			add_country_modifier = { name = "religious_scandal_country" duration = 2920 }
			}
		}
	random_owned = { 
		limit = { 
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			NOT = { owner = { check_variable = { which = "religious_fervor" value = 9 } } }
			}
		owner = { 
			prestige = -0.015
			add_country_modifier = { name = "religious_scandal_country" duration = 2190 }
			}
		}
	random_owned = { 
		limit = { 
			owner = { check_variable = { which = "religious_fervor" value = 1 } }
			NOT = { owner = { check_variable = { which = "religious_fervor" value = 5 } } }
			}
		owner = { 
			prestige = -0.01
			add_country_modifier = { name = "religious_scandal_country" duration = 1460 }
			}
		}
	random_owned = { 
		limit = { 
			NOT = { owner = { check_variable = { which = "religious_fervor" value = 1 } } }
			}
		owner = { 
			prestige = -0.005
			add_country_modifier = { name = "religious_scandal_country" duration = 730 }
			}
		}
	}

}

