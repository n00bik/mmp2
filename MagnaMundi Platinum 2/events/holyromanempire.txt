# Give the Emperor a chance to react to a province removed from the HRE

country_event = {

	id = 10522

	is_triggered_only = yes

	title = "EVTNAME10522"
	desc = "EVTDESC10522"

	option = {						#ok
		name = "EVTOPTA285731"
		ai_chance = {
			factor = 800
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					army = THIS
				}
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					capital_scope = { hre = yes }
				}
			}
			modifier = {
				factor = 2
				NOT = { diplomats = 3 }
			}
			modifier = {
				factor = 2
				war_exhaustion = 4
			}
			modifier = {
				factor = 2
				war_exhaustion = 8
			}
			modifier = {
				factor = 2
				war_exhaustion = 12
			}
			modifier = {
				factor = 2
				war_exhaustion = 16
			}
			modifier = {
				factor = 2
				war_exhaustion = 20
			}
			modifier = {
				factor = 0
				stability = 3
			}
			modifier = {
				factor = 2
				NOT = { manpower_percentage = 0.5 }
			}
			modifier = {
				factor = 4
				war = yes
			}
			modifier = {
				factor = 0.6
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 40 }
				}
			}
			modifier = {
				factor = 0.5
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 80 }
				}
			}
			modifier = {
				factor = 0.4
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 120 }
				}
			}
			modifier = {
				factor = 0.2
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 160 }
				}
			}
			modifier = {
				factor = 2
				EMP = { NOT = { prestige = 0 } }
			}
			modifier = {
				factor = 1.2
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 0.8
				EMP = { prestige = 0.3 }
			}
			modifier = {
				factor = 0.6
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 0.4
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 0.2
				has_global_flag = reichsexekution_ist_m�glich
			}
		}
		prestige = -0.03
		any_country = {
			limit = { has_country_flag = we_removed_a_province }
			relation = { who = THIS value = -25 }
			clr_country_flag = we_removed_a_province
			random_owned = {
				limit = {
					emperor = { ai = yes }
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
					}
				}
				owner = {
					set_country_flag = un_befriend
					set_country_flag = un_protect
					random_owned = {
						limit = { owner = { army = THIS } }
						owner = { set_country_flag = threat }
					}
				}
			}
			random_owned = {
				limit = {
					owner = {
						ai = yes
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
						NOT = { has_country_flag = we_removed_a_province }
					}
				}
				owner = {
					set_country_flag = un_befriend_from
					set_country_flag = un_protect_from
					random_owned = {
						limit = { owner = { NOT = { army = THIS } } }
						owner = { set_country_flag = threat_from }
					}
				}
			}
			random_owned = {
				limit = {
					OR = {
						owner = { ai = yes }
						emperor = { ai = yes }
					}
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
						NOT = { has_country_flag = we_removed_a_province }
					}
				}
				owner = { country_event = 296003 }
			}
			any_owned = {
				limit = { has_province_flag = removed_province_in_question }
				clr_province_flag = removed_province_in_question
				random = {
					chance = 50
					revolt_risk = -4
				}
			}
		}
		EMP = { prestige = -0.01 }
	}
	option = {						#ok
		name = "EVTOPTB285731"
		ai_chance = {
			factor = 400
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					army = THIS
				}
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					capital_scope = { hre = yes }
				}
			}
			modifier = {
				factor = 2
				diplomats = 3
			}
			modifier = {
				factor = 2
				war_exhaustion = 4
			}
			modifier = {
				factor = 2
				war_exhaustion = 8
			}
			modifier = {
				factor = 2
				war_exhaustion = 12
			}
			modifier = {
				factor = 2
				war_exhaustion = 16
			}
			modifier = {
				factor = 2
				war_exhaustion = 20
			}
			modifier = {
				factor = 0.5
				stability = 3
			}
			modifier = {
				factor = 2
				NOT = { manpower_percentage = 0.5 }
			}
			modifier = {
				factor = 2
				war = yes
			}
			modifier = {
				factor = 0.9
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 40 }
				}
			}
			modifier = {
				factor = 0.8
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 80 }
				}
			}
			modifier = {
				factor = 0.6
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 120 }
				}
			}
			modifier = {
				factor = 0.4
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 160 }
				}
			}
			modifier = {
				factor = 0.8
				EMP = { NOT = { prestige = 0 } }
			}
			modifier = {
				factor = 0.9
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 0.95
				EMP = { prestige = 0.3 }
			}
			modifier = {
				factor = 0.8
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 0.7
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 0.4
				has_global_flag = reichsexekution_ist_m�glich
			}
		}
		diplomats = -1
		prestige = -0.01
		any_country = {
			limit = { has_country_flag = we_removed_a_province }
			relation = { who = THIS value = -100 }
			clr_country_flag = we_removed_a_province
			any_owned = {
				limit = { has_province_flag = removed_province_in_question }
				clr_province_flag = removed_province_in_question
				random_list = {
					20 = { revolt_risk = 4 }
					20 = { create_revolt = 1 }
					60 = { }
				}
			}
			random_owned = {
				limit = {
					emperor = { ai = yes }
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
					}
				}
				owner = {
					set_country_flag = un_befriend
					set_country_flag = un_protect
					random_owned = {
						limit = { owner = { army = THIS } }
						owner = { set_country_flag = threat }
					}
					random_owned = {
						limit = { owner = { check_variable = { which = rogue value = 120 } } }
						owner = {
							set_country_flag = antagonize
							set_country_flag = targeted_by_emperor_antagonize
						}
					}
				}
			}
			random_owned = {
				limit = {
					owner = {
						ai = yes
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
						NOT = { has_country_flag = we_removed_a_province }
					}
				}
				owner = {
					set_country_flag = un_befriend_from
					set_country_flag = un_protect_from
					random_owned = {
						limit = { owner = { NOT = { army = THIS } } }
						owner = { set_country_flag = threat_from }
					}
				}
			}
			random_owned = {
				limit = {
					OR = {
						owner = { ai = yes }
						emperor = { ai = yes }
					}
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
						NOT = { has_country_flag = we_removed_a_province }
					}
				}
				owner = { country_event = 296003 }
			}
		}
	}
	option = {						#ok
		name = "EVTOPTC285731"
		ai_chance = {
			factor = 200
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					NOT = { army = THIS }
				}
			}
			modifier = {
				factor = 0.5
				any_known_country = {
					has_country_flag = we_removed_a_province
					capital_scope = { hre = yes }
				}
			}
			modifier = {
				factor = 0.5
				NOT = { diplomats = 3 }
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 4
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 8
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 12
			}
			modifier = {
				factor = 0
				war_exhaustion = 16
			}
			modifier = {
				factor = 2
				stability = 3
			}
			modifier = {
				factor = 0.75
				NOT = { manpower_percentage = 0.75 }
			}
			modifier = {
				factor = 0.5
				war = yes
			}
			modifier = {
				factor = 1.1
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 40 }
				}
			}
			modifier = {
				factor = 1.2
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 80 }
				}
			}
			modifier = {
				factor = 1.5
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 120 }
				}
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 160 }
				}
			}
			modifier = {
				factor = 0.4
				EMP = { NOT = { prestige = 0 } }
			}
			modifier = {
				factor = 0.75
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 1.25
				EMP = { prestige = 0.3 }
			}
			modifier = {
				factor = 2
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 4
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 4
				has_global_flag = reichsexekution_ist_m�glich
			}
			modifier = {
				factor = 0
				any_known_country = {
					has_country_flag = we_removed_a_province
					any_owned_province = {
						has_province_flag = removed_province_in_question
						region = lithuania
					}
				}
			}
		}
		diplomats = -3
		prestige = 0.01
		any_country = {
			limit = { has_country_flag = we_removed_a_province }
			relation = { who = THIS value = -200 }
			casus_belli = THIS
			clr_country_flag = we_removed_a_province
			random_owned = {
				limit = {
					emperor = { ai = yes }
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
					}
				}
				clr_global_flag = ai_may_strategize
				owner = {
					set_country_flag = un_befriend
					set_country_flag = un_protect
					random_owned = {
						limit = { owner = { army = THIS } }
						owner = { set_country_flag = threat }
					}
					random_owned = {
						limit = { owner = { check_variable = { which = rogue value = 80 } } }
						owner = {
							set_country_flag = antagonize
							set_country_flag = targeted_by_emperor_antagonize
						}
					}
					any_owned = {
						limit = {
							OR = {
								has_province_flag = takenmid
								has_province_flag = removed_province_in_question
							}
							NOT = { has_province_flag = targeted_by_emperor_conquer_prov }
						}
						set_province_flag = targeted_by_emperor_conquer_prov
						set_province_flag = conquer_prov
						set_global_flag = conquer_prov
					}
				}
				emperor = { country_event = 296000 }
			}
			random_owned = {
				limit = {
					owner = {
						ai = yes
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
						NOT = { has_country_flag = we_removed_a_province }
					}
				}
				owner = {
					set_country_flag = un_befriend_from
					set_country_flag = un_protect_from
					random_owned = {
						limit = { owner = { NOT = { army = THIS } } }
						owner = { set_country_flag = threat_from }
					}
				}
			}
			random_owned = {
				limit = {
					OR = {
						owner = { ai = yes }
						emperor = { ai = yes }
						NOT = { has_country_flag = we_removed_a_province }
					}
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
					}
				}
				owner = { country_event = 296003 }
			}
			any_owned = {
				limit = { has_province_flag = removed_province_in_question }
				clr_province_flag = removed_province_in_question
				random_list = {
					40 = { revolt_risk = 4 }
					40 = { create_revolt = 1 }
					20 = { }
				}
			}
		}
		EMP = { prestige = 0.01 }
	}
	option = {						#ok
		name = "EVTOPTD285731"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					NOT = { army = THIS }
				}
			}
			modifier = {
				factor = 0.5
				any_known_country = {
					has_country_flag = we_removed_a_province
					capital_scope = { hre = yes }
				}
			}
			modifier = {
				factor = 0
				AND = {
					NOT = { diplomats = 5 }
					NOT = { has_global_flag = reichsexekution_ist_m�glich }
				}
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 4
			}
			modifier = {
				factor = 0
				AND = {
					war_exhaustion = 8
					NOT = { has_global_flag = reichsexekution_ist_m�glich }
				}
			}
			modifier = {
				factor = 0
				AND = {
					NOT = { stability = 3 }
					NOT = { has_global_flag = reichsexekution_ist_m�glich }
				}
			}
			modifier = {
				factor = 0
				AND = {
					NOT = { manpower_percentage = 1 }
					NOT = { has_global_flag = reichsexekution_ist_m�glich }
				}
			}
			modifier = {
				factor = 0
				AND = {
					war = yes
					NOT = { has_global_flag = reichsexekution_ist_m�glich }
				}
			}
			modifier = {
				factor = 1.25
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 40 }
				}
			}
			modifier = {
				factor = 1.5
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 80 }
				}
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 120 }
				}
			}
			modifier = {
				factor = 4
				any_known_country = {
					has_country_flag = we_removed_a_province
					check_variable = { which = rogue value = 160 }
				}
			}
			modifier = {
				factor = 0
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 0.5
				EMP = { NOT = { prestige = 0.6 } }
			}
			modifier = {
				factor = 2
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 4
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 20
				has_global_flag = reichsexekution_ist_m�glich
			}
			modifier = {
				factor = 0
				any_known_country = {
					has_country_flag = we_removed_a_province
					any_owned_province = {
						has_province_flag = removed_province_in_question
						OR = {
							region = lithuania
							AND = {
								region = lowlands_region
								owner = { tag = NED }
							}
						}
					}
				}
			}
		}
		diplomats = -5
		stability = -1
		prestige = 0.03
		EMP = { prestige = 0.02 }
		any_country = {
			limit = { has_country_flag = we_removed_a_province }
			clr_country_flag = we_removed_a_province
			relation = { who = THIS value = -400 }
			random_owned = {
				limit = {
					owner = {
						ai = yes
						NOT = { has_country_flag = we_removed_a_province }
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
					}
				}
				owner = {
					set_country_flag = un_befriend_from
					set_country_flag = un_protect_from
					random_owned = {
						limit = { owner = { NOT = { army = THIS } } }
						owner = { set_country_flag = threat_from }
					}
				}
			}
			random_owned = {
				limit = {
					emperor = { ai = yes }
					owner = {
						NOT = { alliance_with = THIS }
						NOT = { junior_union_with = THIS }
						NOT = { vassal_of = THIS }
					}
				}
				clr_global_flag = ai_may_strategize
				owner = {
					set_country_flag = un_befriend
					set_country_flag = un_protect
					random_owned = {
						limit = { owner = { army = THIS } }
						owner = { set_country_flag = threat }
					}
					random_owned = {
						limit = { owner = { check_variable = { which = rogue value = 40 } } }
						owner = {
							set_country_flag = antagonize
							set_country_flag = targeted_by_emperor_antagonize
						}
					}
					any_owned = {
						limit = {	# target ANY owned non-core province
							continent = europe
							OR = {
								NOT = { any_core = { has_country_flag = un_befriend } }
								has_province_flag = removed_province_in_question
							}
						}
						set_province_flag = conquer_prov
						set_global_flag = conquer_prov
					}
					any_owned = {
						limit = {
							has_province_flag = takenmid
							has_province_flag = conquer_prov
						}
						set_province_flag = targeted_by_emperor_conquer_prov
					}
				}
				emperor = { country_event = 296000 }
			}
			random_owned = {
				limit = {
					OR = {
						owner = { ai = yes }
						emperor = { ai = yes }
					}
				}
				owner = { country_event = 296003 }
			}
			random_owned = {
				limit = {
					NOT = { has_global_flag = reichsexekution_ist_m�glich }
					owner = { NOT = { check_variable = { which = rogue value = 40 } } }
				}
				owner = {
					add_casus_belli = THIS
					casus_belli = THIS
				}
			}
			random_owned = {
				limit = {
					OR = {
						has_global_flag = reichsexekution_ist_m�glich
						owner = { check_variable = { which = rogue value = 40 } }
					}
					OR = {
						NOT = { has_global_flag = reichsexekution_ist_m�glich }
						owner = { NOT = { check_variable = { which = rogue value = 40 } } }
					}
				}
				owner = {
					casus_belli = THIS
					war = THIS
					set_country_flag = war_check
					set_country_flag = notify_hre
					set_country_flag = aggressive_party
					set_country_flag = at_war_with_emperor
					remove_country_modifier = at_peace
					random_owned = {
						limit = {
							is_capital = yes
							hre = yes
							owner = { ai = yes }
						}
						owner = {
							remove_country_modifier = hre_peace
							set_country_flag = at_war
						}
					}
					random_owned = {
						limit = {
							owner = { ai = no }
							EMP = { NOT = { has_country_modifier = at_war } }
						}
						EMP = {
							add_country_modifier = {
								name = "at_war"
								duration = 18250	#50 years
							}
						}
					}
				}
				THIS = {
					set_country_flag = defensive_party
					set_country_flag = war_check
					random_owned = {
						limit = {
							is_capital = yes
							hre = yes
							owner = { ai = yes }
						}
						owner = {
							remove_country_modifier = hre_peace
							set_country_flag = at_war
						}
					}
				}
			}
			random_owned = {
				limit = {
					is_capital = yes
					has_global_flag = reichsexekution_ist_m�glich
					owner = { check_variable = { which = rogue value = 40 } }
				}
				add_province_modifier = {
					name = "middle_class_resentment"
					duration = 360
				}
				add_province_modifier = {
					name = "reichsexekution_target"
					duration = -1
				}
				owner = {
					set_country_flag = reichsexekution_target
					random_owned = {
						limit = { owner = { ai = no } }
						THIS = { set_country_flag = caller }
						owner = { country_event = 285749 }
					}
				}
			}
			any_owned = {
				limit = { has_province_flag = removed_province_in_question }
				clr_province_flag = removed_province_in_question
			}
		}
	}
}

# Give the Emperor a chance to react to a province removed from the HRE
# Special case: whole country has left

country_event = {

	id = 10523

	is_triggered_only = yes

	title = "EVTNAME10523"
	desc = "EVTDESC10523"

	option = {						#ok
		name = "EVTOPTA285731"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_left_the_empire
					army = THIS
				}
			}
			modifier = {
				factor = 2
				war_exhaustion = 4
			}
			modifier = {
				factor = 2
				war_exhaustion = 8
			}
			modifier = {
				factor = 2
				war_exhaustion = 12
			}
			modifier = {
				factor = 2
				war_exhaustion = 16
			}
			modifier = {
				factor = 2
				war_exhaustion = 20
			}
			modifier = {
				factor = 0
				stability = 3
			}
			modifier = {
				factor = 2
				NOT = { manpower_percentage = 0.5 }
			}
			modifier = {
				factor = 4
				war = yes
			}
			modifier = {
				factor = 2
				EMP = { NOT = { prestige = 0 } }
			}
			modifier = {
				factor = 1.2
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 0.8
				EMP = { prestige = 0.3 }
			}
			modifier = {
				factor = 0.6
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 0.4
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 0.2
				has_global_flag = reichsexekution_ist_m�glich
			}
		}
		prestige = -0.03
		any_country = {
			limit = { has_country_flag = we_left_the_empire }
			relation = { who = THIS value = -25 }
			clr_country_flag = we_left_the_empire
			set_country_flag = and_we_bear_the_consequences
		}
		any_country = {	# must use country scope, since after a tag change, province ownership of the new tag is not yet recognized
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			set_country_flag = un_befriend
			set_country_flag = un_protect
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				army = THIS
			}
			set_country_flag = threat
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				ai = yes
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			set_country_flag = un_befriend_from
			set_country_flag = un_protect_from
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				ai = yes
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				NOT = { army = THIS }
			}
			set_country_flag = threat_from
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			country_event = 296003
		}
		any_country = {
			limit = { has_country_flag = and_we_bear_the_consequences }
			clr_country_flag = and_we_bear_the_consequences
			country_event = 113003
		}
		hre_region = {
			limit = { has_province_flag = removed_province_in_question }
			clr_province_flag = removed_province_in_question
			EMP = { prestige = -0.02 }
		}
	}
	option = {						#ok
		name = "EVTOPTB285731"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_left_the_empire
					army = THIS
				}
			}
			modifier = {
				factor = 2
				war_exhaustion = 4
			}
			modifier = {
				factor = 2
				war_exhaustion = 8
			}
			modifier = {
				factor = 2
				war_exhaustion = 12
			}
			modifier = {
				factor = 2
				war_exhaustion = 16
			}
			modifier = {
				factor = 2
				war_exhaustion = 20
			}
			modifier = {
				factor = 0.5
				stability = 3
			}
			modifier = {
				factor = 2
				NOT = { manpower_percentage = 0.5 }
			}
			modifier = {
				factor = 2
				war = yes
			}
			modifier = {
				factor = 0.8
				EMP = { NOT = { prestige = 0 } }
			}
			modifier = {
				factor = 0.9
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 0.95
				EMP = { prestige = 0.3 }
			}
			modifier = {
				factor = 0.8
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 0.7
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 0.4
				has_global_flag = reichsexekution_ist_m�glich
			}
		}
		diplomats = -1
		prestige = -0.01
		any_country = {
			limit = { has_country_flag = we_left_the_empire }
			relation = { who = THIS value = -100 }
			clr_country_flag = we_left_the_empire
			set_country_flag = and_we_bear_the_consequences
		}
		any_country = {	# must use country scope, since after a tag change, province ownership of the new tag is not yet recognized
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			set_country_flag = un_befriend
			set_country_flag = un_protect
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				army = THIS
			}
			set_country_flag = threat
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				OR = {
					check_variable = { which = rogue value = 120 }
					is_revolution_target = yes
				}
			}
			set_country_flag = antagonize
			set_country_flag = targeted_by_emperor_antagonize
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				ai = yes
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			set_country_flag = un_befriend_from
			set_country_flag = un_protect_from
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				ai = yes
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				NOT = { army = THIS }
			}
			set_country_flag = threat_from
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			country_event = 296003
		}
		any_country = {
			limit = { has_country_flag = and_we_bear_the_consequences }
			clr_country_flag = and_we_bear_the_consequences
			country_event = 113003
		}
		hre_region = {
			limit = { has_province_flag = removed_province_in_question }
			clr_province_flag = removed_province_in_question
			EMP = { prestige = -0.01 }
			random_list = {
				20 = { revolt_risk = 4 }
				20 = { create_revolt = 1 }
				60 = { }
			}
		}
	}
	option = {	# claim for Demesne
		name = "EVTOPTB11301121"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_left_the_empire
					NOT = { army = THIS }
				}
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 4
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 8
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 12
			}
			modifier = {
				factor = 0
				war_exhaustion = 16
			}
			modifier = {
				factor = 2
				stability = 3
			}
			modifier = {
				factor = 0.75
				NOT = { manpower_percentage = 0.75 }
			}
			modifier = {
				factor = 0.5
				war = yes
			}
			modifier = {
				factor = 0
				EMP = { NOT = { prestige = 0 } }
			}
			modifier = {
				factor = 0.75
				EMP = { NOT = { prestige = 0.3 } }
			}
			modifier = {
				factor = 1.25
				EMP = { prestige = 0.3 }
			}
			modifier = {
				factor = 2
				EMP = { prestige = 0.6 }
			}
			modifier = {
				factor = 4
				EMP = { prestige = 0.9 }
			}
			modifier = {
				factor = 4
				has_global_flag = reichsexekution_ist_m�glich
			}
			modifier = {
				factor = 0
				any_known_country = {
					has_country_flag = we_left_the_empire
					any_owned_province = { region = lithuania }
				}
			}
		}
		clr_global_flag = ai_may_strategize
		diplomats = -5
		prestige = 0.05
		any_country = {
			limit = { has_country_flag = we_left_the_empire }
			relation = { who = THIS value = -200 }
			casus_belli = THIS
			clr_country_flag = we_left_the_empire
			set_country_flag = and_we_bear_the_consequences
		}
		any_country = {	# must use country scope, since after a tag change, province ownership of the new tag is not yet recognized
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			set_country_flag = un_befriend
			set_country_flag = un_protect
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				army = THIS
			}
			set_country_flag = threat
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				emperor = { ai = yes }
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				OR = {
					check_variable = { which = rogue value = 120 }
					is_revolution_target = yes
				}
			}
			set_country_flag = antagonize
			set_country_flag = targeted_by_emperor_antagonize
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				ai = yes
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			set_country_flag = un_befriend_from
			set_country_flag = un_protect_from
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				ai = yes
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
				NOT = { army = THIS }
			}
			set_country_flag = threat_from
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				NOT = { alliance_with = THIS }
				NOT = { junior_union_with = THIS }
				NOT = { vassal_of = THIS }
			}
			country_event = 296003
		}
		any_country = {
			limit = {
				has_country_flag = and_we_bear_the_consequences
				has_global_flag = reichsexekution_ist_m�glich
				NOT = { is_revolution_target = yes }
			}
			EMP = { prestige = 0.05 }
			set_country_flag = reichsexekution_target
			relation = { who = THIS value = -400 }
			capital_scope = {
				add_province_modifier = {
					name = "middle_class_resentment"
					duration = 360
				}
				add_province_modifier = {
					name = "reichsexekution_target"
					duration = -1
				}
			}
			THIS = { set_country_flag = caller }
			country_event = 285749
		}
		any_country = {
			limit = { has_country_flag = and_we_bear_the_consequences }
			clr_country_flag = and_we_bear_the_consequences
			set_country_flag = make_RLD_beneficiary
			country_event = 113003
		}
		hre_region = {
			limit = { has_province_flag = removed_province_in_question }
			set_province_flag = original_hre
			EMP = { prestige = 0.02 }
		}
		hre_region = {
			limit = {
				has_province_flag = removed_province_in_question
				emperor = { ai = yes }
			}
			set_province_flag = targeted_by_emperor_conquer_prov
			set_province_flag = conquer_prov
			set_global_flag = conquer_prov
		}
		emperor = {
			random_owned = {
				limit = { owner = { ai = yes } }
				owner = { country_event = 296000 }
			}
		}
		RLD = {
			hre_region = {
				limit = { has_province_flag = removed_province_in_question }
				add_core = RLD
			}
		}
		hre_region = {
			limit = { has_province_flag = removed_province_in_question }
			clr_province_flag = removed_province_in_question
		}
	}
	option = {	# claim for us
		name = "EVTOPTC11301122"
		ai_chance = {
			factor = 100
			modifier = {
				factor = 0
				war_exhaustion = 12
			}
			modifier = {
				factor = 0
				NOT = { stability = 0 }
			}
			modifier = {
				factor = 0.5
				war_exhaustion = 8
			}
			modifier = {
				factor = 0.5
				NOT = { manpower_percentage = 0.8 }
			}
			modifier = {
				factor = 0.5
				war = yes
			}
			modifier = {
				factor = 0.5
				has_global_flag = reichsexekution_ist_m�glich
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_left_the_empire
					any_owned_province = { any_neighbor_province = { owned_by = THIS } }
				}
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_left_the_empire
					any_owned_province = { is_core = THIS }
				}
			}
			modifier = {
				factor = 2
				any_known_country = {
					has_country_flag = we_left_the_empire
					NOT = { army = THIS }
				}
			}
			modifier = {
				factor = 2
				NOT = { war_exhaustion = 4 }
			}
		}
		set_country_flag = defensive_party
		set_country_flag = war_check
		clr_global_flag = ai_may_strategize
		diplomats = -5
		stability = -1
		prestige = 0.05
		any_country = {
			limit = {
				has_country_flag = we_left_the_empire
				ai = yes
			}
			set_country_flag = un_befriend_from
			set_country_flag = un_protect_from
			set_country_flag = threat_from
		}
		any_country = {
			limit = {
				has_country_flag = we_left_the_empire
				emperor = { ai = yes }
			}
			set_country_flag = antagonize
			set_country_flag = un_befriend
			set_country_flag = un_protect
		}
		any_country = {
			limit = {
				has_country_flag = we_left_the_empire
				OR = {
					ai = yes
					emperor = { ai = yes }
				}
			}
			country_event = 296003
		}
		hre_region = {
			limit = {
				has_province_flag = removed_province_in_question
				THIS = { ai = yes }
			}
			set_province_flag = conquer_prov
			set_global_flag = conquer_prov
		}
		emperor = {
			random_owned = {
				limit = { owner = { ai = yes } }
				owner = { country_event = 296000 }
			}
		}
		hre_region = {
			limit = { has_province_flag = removed_province_in_question }
			add_core = THIS
			EMP = { prestige = -0.02 }
			THIS = { badboy = 1 }
			clr_province_flag = removed_province_in_question
		}
		any_country = {
			limit = { has_country_flag = we_left_the_empire }
			prestige = 0.25
			clr_country_flag = we_left_the_empire
			set_country_flag = make_emperor_beneficiary
			country_event = 113003	# beneficiary flag adjustment
			country_event = 10524	# DOW
			set_country_flag = war_check
			set_country_flag = notify_hre
			set_country_flag = aggressive_party
			set_country_flag = at_war_with_emperor
			remove_country_modifier = at_peace
			random_owned = {
				limit = {
					is_capital = yes
					hre = yes
					owner = { ai = yes }
				}
				owner = {
					remove_country_modifier = hre_peace
					set_country_flag = at_war
				}
			}
			random_owned = {
				limit = {
					owner = { ai = no }
					EMP = { NOT = { has_country_modifier = at_war } }
				}
				EMP = {
					add_country_modifier = {
						name = "at_war"
						duration = 18250	#50 years
					}
				}
			}
		}
		random_owned = {
			limit = {
				is_capital = yes
				hre = yes
				owner = { ai = yes }
			}
			owner = {
				remove_country_modifier = hre_peace
				set_country_flag = at_war
			}
		}
	}
}

country_event = {
	id = 10524
	is_triggered_only = yes
	title = "EVTNAME10524"
	desc = "EVTDESC10524"
	immediate = { FROM = { war = THIS } }
	option = {						#ok
		name = "OK1"
	}
}

country_event = {
	id = 10525
	is_triggered_only = yes
	title = "EVTNAME2857000"		#Good news
	desc = "EVTDESC10525"
	immediate = {
		FROM = {
			revolt_risk = -2
			owner = { relation = { who = THIS value = 5 } }
		}
	}
	option = {						#ok
		name = "GOOD1"
	}
}