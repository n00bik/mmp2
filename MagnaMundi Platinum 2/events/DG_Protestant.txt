#See Reformation_Events.txt for Dei Gratia equivalents to the Reformation events in vanilla.

# ID RANGE 55 Christian + 01 Protestant

province_event = { #Vacant Benefice

id = 550101

trigger = { 
	religion_group = christian
	citysize = 5000
	owner = { 
		OR = { 
			religion = protestant
			religion = reformed
			}
		missionaries = 1
		diplomats = 1
		}
	controlled_by = owner
	NOT = { has_province_modifier = greedy_bishop }
	NOT = { has_province_modifier = absentee_bishop }
	NOT = { has_province_modifier = effective_bishop }
	}

mean_time_to_happen = { 

	months = 2400

	modifier = { 
		owner = { religion = protestant }
		factor = 0.7
		}
	modifier = { 
		check_variable = { which = "religious_fervor" value = 12 }
		factor = 0.7
		}
	modifier = { 
		check_variable = { which = "religious_fervor" value = 9 }
		factor = 0.7
		}
	modifier = { 
		check_variable = { which = "religious_fervor" value = 6 }
		factor = 0.7
		}
	modifier = { 
		NOT = { check_variable = { which = "religious_fervor" value = 1 } }
		factor = 1.4
		}
	modifier = { 
		NOT = { check_variable = { which = "religious_fervor" value = -2 } }
		factor = 1.4
		}
	modifier = { 
		citysize = 25000
		factor = 0.9
		}
	modifier = { 
		citysize = 50000
		factor = 0.9
		}
	modifier = { 
		citysize = 100000
		factor = 0.9
		}
	modifier = { 
		citysize = 250000
		factor = 0.9
		}
	modifier = { 
		owner = { NOT = { num_of_cities = 3 } }
		factor = 0.7
		}
	modifier = { 
		owner = { num_of_cities = 5 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 10 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 20 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 40 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 80 }
		factor = 1.6
		}
	modifier = {
		owner = { num_of_cities = 160 }
		factor = 1.6
		}
	}

title = "EVTNAME550101"
desc = "EVTDESC550101"

option = { 
	name = "EVTOPTA551094" #Appoint a knowledgeable scholar
	ai_chance = { 
		factor = 25 
		modifier = { 
			owner = { idea = scientific_revolution }
			factor = 1.4
			}
		modifier = { 
			NOT = { PAP = { check_variable = { which = "papal_authority" value = 400 } } }
			factor = 1.4
			}
		}
	owner = { 
		random_owned = { 
			limit = { owner = { idea = vetting } }
			THIS = { 
				random_list = { 
					45 = { owner = { create_advisor = philosopher } }
					15 = { owner = { create_advisor = natural_scientist } }
					5 = { owner = { add_country_modifier = { name = "religious_scandal_country" duration = 365 } } }
					5 = { add_province_modifier = { name = "religious_scandal_province" duration = 1825 } }
					30 = { }
					}
				}
			}
		random_owned = { 
			limit = { owner = { NOT = { idea = vetting } } }
			THIS = { 
				random_list = { 
					30 = { owner = { create_advisor = philosopher } }
					10 = { owner = { create_advisor = natural_scientist } }
					10 = { owner = { add_country_modifier = { name = "religious_scandal_country" duration = 365 } } }
					10 = { add_province_modifier = { name = "religious_scandal_province" duration = 1825 } }
					40 = { }
					}
				}
			}

		}
	}
option = { 
	name = "EVTOPTB551094" #Appoint a pious theologian
	ai_chance = { 
		factor = 25 
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 3 } }
			factor = 1.4
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 5 } }
			factor = 1.4
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 7 } }
			factor = 1.4
			}
		modifier = { 
			owner = { check_variable = { which = "religious_fervor" value = 9 } }
			factor = 1.4
			}
		modifier = { 
			NOT = { owner = { check_variable = { which = "religious_fervor" value = 1 } } }
			factor = 0.7
			}
		modifier = { 
			NOT = { owner = { check_variable = { which = "religious_fervor" value = -1 } } }
			factor = 0.7
			}
		modifier = { 
			NOT = { owner = { check_variable = { which = "religious_fervor" value = -3 } } }
			factor = 0
			}
		}
	owner = { 
		missionaries = -1 
		random_owned = { 
			limit = { owner = { idea = vetting } }
			THIS = { 
				random_list = { 
					45 = { add_province_modifier = { name = "effective_bishop" duration = 3650 } clr_province_flag = absentee_bishop }
					15 = { owner = { create_advisor = theologian } }
					5 = { owner = { add_country_modifier = { name = "religious_intolerance" duration = 1825 } } }
					5 = { add_province_modifier = { name = "witch_hunt" duration = 1825 } }
					15 = { add_province_modifier = { name = "peasant_success_story" duration = 365 } } 
					15 = { }
					}
				}
			}
		random_owned = { 
			limit = { owner = { NOT = { idea = vetting } } }
			THIS = { 
				random_list = { 
					30 = { add_province_modifier = { name = "effective_bishop" duration = 3650 } clr_province_flag = absentee_bishop }
					10 = { owner = { create_advisor = theologian } }
					10 = { owner = { add_country_modifier = { name = "religious_intolerance" duration = 1825 } } }
					10 = { add_province_modifier = { name = "witch_hunt" duration = 1825 } }
					10 = { add_province_modifier = { name = "peasant_success_story" duration = 365 } } 
					30 = { }
					}
				}
			}
		}
	}
option = { 
	name = "EVTOPTC551094" #Appoint a court favorite
	ai_chance = { 
		factor = 25 
		modifier = { 
			owner = { government = theocracy }
			factor = 0
			}
		modifier = { 
			owner = { idea = patron_of_art }
			factor = 1.4
			}
		modifier = { 
			owner = { idea = cabinet }
			factor = 1.4
			}
		}
	owner = { 
		diplomats = -1 
		prestige = 0.01 
		random_owned = { 
			limit = { owner = { idea = vetting } }
			THIS = { 
				random_list = { 
					15 = { add_province_modifier = { name = "absentee_bishop" duration = 3650 } set_province_flag = absentee_bishop }
					15 = { owner = { prestige = 0.02 } }
					15 = { owner = { stability = 1 } }
					5 = { add_province_modifier = { name = "religious_scandal_province" duration = 1825 } }
					50 = { }
					}
				}
			}
		random_owned = { 
			limit = { owner = { NOT = { idea = vetting } } }
			THIS = { 
				random_list = { 
					30 = { add_province_modifier = { name = "absentee_bishop" duration = 3650 } set_province_flag = absentee_bishop }
					10 = { owner = { prestige = 0.02 } }
					10 = { owner = { stability = 1 } }
					10 = { add_province_modifier = { name = "religious_scandal_province" duration = 1825 } }
					40 = { }
					}
				}
			}
		}
	}
option = { 
	name = "EVTOPTD551094" #Turn the benefice into a sinecure
	ai_chance = { 
		factor = 25 
		modifier = { 
			owner = { government = theocracy }
			factor = 0
			}
		modifier = { 
			owner = { idea = shrewd_commerce_practise }
			factor = 1.2
			}
		modifier = { 
			owner = { number_of_loans = 1 }
			factor = 2
			}
		}
	owner = { 
		prestige = -0.01
		random_owned = { 
			limit = { 
				THIS = { base_tax = 1 }
				THIS = { NOT = { base_tax = 2 } }
				}
			owner = { treasury = 1 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 2 }
				THIS = { NOT = { base_tax = 3 } }
				}
			owner = { treasury = 2 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 3 }
				THIS = { NOT = { base_tax = 4 } }
				}
			owner = { treasury = 3 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 4 }
				THIS = { NOT = { base_tax = 5 } }
				}
			owner = { treasury = 4 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 5 }
				THIS = { NOT = { base_tax = 6 } }
				}
			owner = { treasury = 5 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 6 }
				THIS = { NOT = { base_tax = 7 } }
				}
			owner = { treasury = 6 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 7 }
				THIS = { NOT = { base_tax = 8 } }
				}
			owner = { treasury = 7 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 8 }
				THIS = { NOT = { base_tax = 9 } }
				}
			owner = { treasury = 8 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 9 }
				THIS = { NOT = { base_tax = 10 } }
				}
			owner = { treasury = 9 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 10 }
				THIS = { NOT = { base_tax = 11 } }
				}
			owner = { treasury = 10 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 11 }
				THIS = { NOT = { base_tax = 12 } }
				}
			owner = { treasury = 11 }
			}
		random_owned = { 
			limit = { 
				THIS = { base_tax = 12 } 
				}
			owner = { treasury = 12 }
			}
		random_owned = { 
			limit = { owner = { idea = vetting } }
			THIS = { 
				random_list = { 
					10 = { add_province_modifier = { name = "greedy_bishop" duration = 365 } set_province_flag = simony_province }
					10 = { add_province_modifier = { name = "absentee_bishop" duration = 3650 } set_province_flag = absentee_bishop }
					5 = { revolt_risk = 2 }
					5 = { add_province_modifier = { name = "religious_scandal_province" duration = 1825 } }
					70 = { }
					}
				}
			}
		random_owned = { 
			limit = { owner = { NOT = { idea = vetting } } }
			THIS = { 
				random_list = { 
					20 = { add_province_modifier = { name = "greedy_bishop" duration = 365 } set_province_flag = simony_province }
					20 = { add_province_modifier = { name = "absentee_bishop" duration = 3650 } set_province_flag = absentee_bishop }
					10 = { revolt_risk = 2 }
					10 = { add_province_modifier = { name = "religious_scandal_province" duration = 1825 } }
					40 = { }
					}
				}
			}
		}
	}

}
