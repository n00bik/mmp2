# Unprofitable Trade
country_event = {

	id = 4043

	trigger = {
		has_country_modifier = india_trade_co
		not = {
			eastasian_trade_ports = {
				owned_by =  this
			}
		}
	}

	mean_time_to_happen = {
		months = 24
		
		modifier = {
			factor = 0.9
			inflation = 10
		}
		modifier = {
			factor = 0.9
			inflation = 20
		}
		modifier = {
			factor = 0.9
			number_of_loans = 1
		}
		modifier = {
			factor = 0.9
			number_of_loans = 2
		}
		modifier = {
			factor = 1.1
			trader = 5
		}
		modifier = {
			factor = 1.1
			trader = 6
		}
		modifier = {
			factor = 1.1
			colonial_governor = 5
		}
		modifier = {
			factor = 1.1
			colonial_governor = 6
		}
	}

	title = "EVTNAME4043"
	desc = "EVTDESC4043"
	
	option = {
		name = "EVTOPTA4043"		# What a disaster!
		remove_country_modifier = india_trade_co
		prestige = -0.02
	}
}

country_event = {

	id = 4044

	trigger = {
		has_country_modifier = dutch_india_trade_co
		not = {
			eastasian_trade_ports = {
				owned_by =  this
			}
		}
	}

	mean_time_to_happen = {
		months = 24
		
		modifier = {
			factor = 0.9
			inflation = 10
		}
		modifier = {
			factor = 0.9
			inflation = 20
		}
		modifier = {
			factor = 0.9
			number_of_loans = 1
		}
		modifier = {
			factor = 0.9
			number_of_loans = 2
		}
		modifier = {
			factor = 1.1
			trader = 5
		}
		modifier = {
			factor = 1.1
			trader = 6
		}
		modifier = {
			factor = 1.1
			colonial_governor = 5
		}
		modifier = {
			factor = 1.1
			colonial_governor = 6
		}
	}

	title = "EVTNAME4043"
	desc = "EVTDESC4043"
	
	option = {
		name = "EVTOPTA4043"		# What a disaster!
		remove_country_modifier = dutch_india_trade_co
		prestige = -0.02
	}
}
