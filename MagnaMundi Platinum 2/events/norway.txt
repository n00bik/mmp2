province_event = { 

id = 882701

	trigger = {

	       owned_by = NOR


		NOT = 	{ has_province_flag = nor_moral_fibre }

		OR = {
			province_id = 10
			province_id = 16
			province_id = 17
			province_id = 20
			province_id = 21
			province_id = 22
			province_id = 23
			province_id = 24
			province_id = 315

		}

		owner = { 
			ai = yes
			war_with = RUS


			  NOT = {
				 AND = { 
					controls = 10
					controls = 16
					controls = 17
					controls = 20
					controls = 21
					controls = 22
					controls = 23
					controls = 24
					controls = 315

				}
			  }
				
		}
		

	}

	mean_time_to_happen = { months = 2 }

	title = "EVTNAME882701" #	 in $PROVINCENAME$!
	desc = "EVTDESC882701" #	Dummie

	option = {
	name = "EVTOPTA882701" #	Rally to the flag!
	  ai_chance = { factor = 40 }
   	  set_province_flag = nor_moral_fibre
	  owner = {
		  infantry = THIS
		  manpower = 2
	  }
	}	

	option = {
	name = "EVTOPTB882701" #	Rally to the flag!
	  ai_chance = { factor = 35 }
   	  set_province_flag = nor_moral_fibre
	  owner = {
		  infantry = THIS
		  cavalry = THIS
		  manpower = 3
	  }
	}

	option = {
	name = "EVTOPTC882701" #	Rally to the flag!
	  ai_chance = { factor = 15 }
   	  set_province_flag = nor_moral_fibre
	  owner = {
		  infantry = THIS
		  infantry = THIS
		  cavalry = THIS
		  manpower = 3
	  }
	}

	option = {
	name = "EVTOPTD882701" #	Rally to the flag!
	ai_chance = { factor = 10 }
	   	set_province_flag = nor_moral_fibre

		owner = {
			infantry = THIS
			infantry = THIS
			infantry = THIS
 		  	cavalry = THIS
			manpower = 4
		}

	}

}


province_event = { 

id = 882702

	trigger = {

		has_province_flag = nor_moral_fibre

		OR = {
			province_id = 10
			province_id = 16
			province_id = 17
			province_id = 20
			province_id = 21
			province_id = 22
			province_id = 23
			province_id = 24
			province_id = 315
		}
		

		owner = { 
			  war = no
			  ai = yes
		}

	}

	mean_time_to_happen = { years = 5 }

	title = "EVTNAME882702" #	A Beautiful Ball
	desc = "EVTDESC882702" #	Dummy

	option = {
	name = "EVTOPTA882702" #	I'll choose my best outfit!
	  ai_chance = { factor = 100 }
 	  clr_province_flag = nor_moral_fibre
	}	
}
