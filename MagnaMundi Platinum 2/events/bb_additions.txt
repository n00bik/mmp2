#File Contents:
#
#I. When a nation owns a province without a core, determine whether there may arise negative consequences.
#II. Determine if any other nation feels strongly enough about I. to do something about it.
#
# Flags used:
# conquered #
# --> nation takes a non-core, non-primary culture, non-accepted culture, province
# legitimized #
# --> nation gets to have the province, if it holds it for five years without any challenge,
# or if it is too small or peaceful for others to care
# contested #
# --> marks that a neighbor takes offense

#
# mark province as conquered - non-core
#  (now in Dynamic_cores.txt)

#
# legitimize the acquisition - 
# owner has less than 10% BB
#

#
# determine if any neighbor takes offense - 
# conquerer has same religion as province
# neighbor has culture of the province as primary or accepted, or has a core on the province
# rationale: intruding into territory claimed by another nation

province_event = {

  	id = 500110

	trigger = {
		has_province_flag = conquered
		owner = { 
			religion = THIS
			badboy = 0.1
		}
		any_neighbor_province = {
			NOT = { owned_by = THIS }
			has_owner_culture = yes
			culture = THIS
			owner = { NOT = { relation = { who = THIS value = 100 } } }
		}
		NOT = { has_province_flag = contested }
		NOT = { has_province_flag = legitimized }
 	}

	mean_time_to_happen = {
			months = 72
			modifier = {
				factor = 0.9
				owner = { badboy = 0.2 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.3 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.4 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.5 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.6 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.7 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.8 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.9 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 1 }
			}
			modifier = {
				factor = 0.5
				cot = yes
			}
			modifier = {
				factor = 0.9
				port = yes
			}
			modifier = {
				factor = 0.9
				citysize = 200000
			}
			modifier = {
				factor = 0.9
				citysize = 500000
			}
			modifier = {
				factor = 0.9
				OR = {
					trade_goods = wine
					trade_goods = cloth
					trade_goods = fur
					trade_goods = salt
					trade_goods = copper
					trade_goods = gold
					trade_goods = iron
					trade_goods = chinaware
					trade_goods = spices
					trade_goods = sugar
				}
			}
			modifier = {
				factor = 1.1
				NOT = { citysize = 50000 }
			}
			modifier = {
				factor = 1.1
				NOT = { citysize = 100000 }
			}
			modifier = {
				factor = 1.1
				OR = {
					trade_goods = grain
					trade_goods = wool
					trade_goods = slaves
				}
			}
	}				

	title = "EVTNAME500110"
	desc = "EVTDESC500110"

	option = {
		name = "EVTOPTA500110"
		ai_chance = { factor = 100 }
		owner = { badboy = 1 }
		clr_province_flag = conquered
		set_province_flag = contested
		revolt_risk = 4
		random_neighbor_province = {	#escalation 0: just adjust relations
			limit = {
				NOT = { owned_by = THIS }
				has_owner_culture = yes
				culture = THIS
				owner = { NOT = { relation = { who = THIS value = 100 } } }
			}
			owner = { set_country_flag = threat_from }
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 1: relations hit
			limit = {
				NOT = { owned_by = THIS }
				has_owner_culture = yes
				culture = THIS
				owner = { 
					NOT = { relation = { who = THIS value = 100 } }
					NOT = { badboy = 0.5 }
					stability = 1
					manpower = 4
					manpower_percentage = 0.75
					num_of_cities = 5
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 4
					NOT = { war_exhaustion = 4 }
				}
			}
			owner = {
				set_country_flag = un_befriend_from
				relation = { who = THIS value = -100 }
			}
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 2: cb
			limit = {
				NOT = { owned_by = THIS }
				has_owner_culture = yes
				culture = THIS
				owner = { 
					NOT = { relation = { who = THIS value = 100 } }
					NOT = { badboy = 0.4 }
					stability = 2
					manpower = 6
					manpower_percentage = 0.8
					num_of_cities = 10
					num_of_allies = 1
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 5
					NOT = { war_exhaustion = 2 }
				}
			}
			owner = {
				set_country_flag = antagonize_from
				casus_belli = THIS
			}
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 3: war
			limit = {
				NOT = { owned_by = THIS }
				has_owner_culture = yes
				culture = THIS
				owner = { 
					NOT = { relation = { who = THIS value = 100 } }
					NOT = { badboy = 0.33 }
					stability = 3
					manpower = 8
					manpower_percentage = 0.8
					num_of_cities = 15
					num_of_allies = 2
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 6
					NOT = { war_exhaustion = 1 }
					war = no
					ai = yes
				}
			}
			owner = { war = THIS }
			THIS = {
				set_province_flag = trigger_296004
				owner = { set_country_flag = threat }
			}
		}
		owner = {
			random_owned = {
				limit = { has_province_flag = trigger_296004 }
				owner = { country_event = 296004 }
				clr_province_flag = trigger_296004
			}
		}
	}
}

#
# determine if any neighbor takes offense
# conquerer has different religion from province
# neighbor has same religion as province
# rationale: protecting co-religionists

province_event = {

  	id = 500111

	trigger = {
		has_province_flag = conquered
		owner = { 
			NOT = { religion = THIS }
			badboy = 0.1
		}
		any_neighbor_province = {
			NOT = { owned_by = THIS }
			has_owner_religion = yes
			religion = THIS
			owner = { NOT = { relation = { who = THIS value = 100 } } }
		}
		NOT = { has_province_flag = contested }
		NOT = { has_province_flag = legitimized }
 	}

	mean_time_to_happen = {
			months = 72
			modifier = {
				factor = 0.9
				owner = { badboy = 0.2 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.3 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.4 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.5 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.6 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.7 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.8 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.9 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 1 }
			}
			modifier = {
				factor = 0.5
				cot = yes
			}
			modifier = {
				factor = 0.9
				port = yes
			}
			modifier = {
				factor = 0.9
				citysize = 200000
			}
			modifier = {
				factor = 0.9
				citysize = 500000
			}
			modifier = {
				factor = 0.9
				OR = {
					trade_goods = wine
					trade_goods = cloth
					trade_goods = fur
					trade_goods = salt
					trade_goods = copper
					trade_goods = gold
					trade_goods = iron
					trade_goods = chinaware
					trade_goods = spices
					trade_goods = sugar
				}
			}
			modifier = {
				factor = 1.1
				NOT = { citysize = 50000 }
			}
			modifier = {
				factor = 1.1
				NOT = { citysize = 100000 }
			}
			modifier = {
				factor = 1.1
				OR = {
					trade_goods = grain
					trade_goods = wool
					trade_goods = slaves
				}
			}
	}				

	title = "EVTNAME500111"
	desc = "EVTDESC500111"

	option = {
		name = "EVTOPTA500111"
		ai_chance = { factor = 100 }
		owner = { badboy = 1 }
		clr_province_flag = conquered
		set_province_flag = contested
		revolt_risk = 4
		random_neighbor_province = {	#escalation 0: just adjust relations
			limit = {
				NOT = { owned_by = THIS }
				has_owner_religion = yes
				religion = THIS
				owner = { NOT = { relation = { who = THIS value = 100 } } }
			}
			owner = { set_country_flag = threat_from }
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 1: relations hit
			limit = {
				NOT = { owned_by = THIS }
				has_owner_religion = yes
				religion = THIS
				owner = {
					NOT = { relation = { who = THIS value = 100 } }
					NOT = { badboy = 0.5 }
					stability = 1
					manpower = 4
					manpower_percentage = 0.75
					num_of_cities = 5
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 4
					NOT = { war_exhaustion = 4 }
				}
			}
			owner = {
				set_country_flag = un_befriend_from
				relation = { who = THIS value = -100 }
			}
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 2: cb
			limit = {
				NOT = { owned_by = THIS }
				has_owner_religion = yes
				religion = THIS
				owner = {
					NOT = { relation = { who = THIS value = 100 } }
					NOT = { badboy = 0.4 }
					stability = 2
					manpower = 6
					manpower_percentage = 0.8
					num_of_cities = 10
					num_of_allies = 1
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 5
					NOT = { war_exhaustion = 2 }
				}
			}
			owner = {
				set_country_flag = antagonize_from
				casus_belli = THIS
			}
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 3: war
			limit = {
				NOT = { owned_by = THIS }
				has_owner_religion = yes
				religion = THIS
				owner = {
					NOT = { relation = { who = THIS value = 100 } }
					NOT = { badboy = 0.33 }
					stability = 3
					manpower = 8
					manpower_percentage = 0.8
					num_of_cities = 15
					num_of_allies = 2
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 6
					NOT = { war_exhaustion = 1 }
					war = no
					ai = yes
				}
			}
			owner = { war = THIS }
			THIS = {
				set_province_flag = trigger_296004
				owner = { set_country_flag = threat }
			}
		}
		owner = {
			random_owned = {
				limit = { has_province_flag = trigger_296004 }
				owner = { country_event = 296004 }
				clr_province_flag = trigger_296004
			}
		}
	}
}

#
# determine if any neighbor takes offense
# conquerer has different culture_group from province
# neighbor has same culture_group as province
# rationale: protecting countrymen

province_event = {

  	id = 500112

	trigger = {
		has_province_flag = conquered
		owner = { 
			NOT = { culture_group = THIS }
			badboy = 0.1
		}
		any_neighbor_province = {
			NOT = { owned_by = THIS }
			owner = { 
				culture_group = THIS
				NOT = { relation = { who = THIS value = 100 } }
			}
		}
		NOT = { has_province_flag = contested }
		NOT = { has_province_flag = legitimized }
 	}

	mean_time_to_happen = {
			months = 72
			modifier = {
				factor = 0.9
				owner = { badboy = 0.2 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.3 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.4 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.5 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.6 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.7 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.8 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 0.9 }
			}
			modifier = {
				factor = 0.9
				owner = { badboy = 1 }
			}
			modifier = {
				factor = 0.5
				cot = yes
			}
			modifier = {
				factor = 0.9
				port = yes
			}
			modifier = {
				factor = 0.9
				citysize = 200000
			}
			modifier = {
				factor = 0.9
				citysize = 500000
			}
			modifier = {
				factor = 0.9
				OR = {
					trade_goods = wine
					trade_goods = cloth
					trade_goods = fur
					trade_goods = salt
					trade_goods = copper
					trade_goods = gold
					trade_goods = iron
					trade_goods = chinaware
					trade_goods = spices
					trade_goods = sugar
				}
			}
			modifier = {
				factor = 1.1
				NOT = { citysize = 50000 }
			}
			modifier = {
				factor = 1.1
				NOT = { citysize = 100000 }
			}
			modifier = {
				factor = 1.1
				OR = {
					trade_goods = grain
					trade_goods = wool
					trade_goods = slaves
				}
			}
	}				

	title = "EVTNAME500112"
	desc = "EVTDESC500112"

	option = {
		name = "EVTOPTA500112"
		ai_chance = { factor = 100 }
		owner = { badboy = 1 }
		clr_province_flag = conquered
		set_province_flag = contested
		revolt_risk = 4
		random_neighbor_province = {	#escalation 0: just adjust relations
			limit = {
				NOT = { owned_by = THIS }
				owner = { 
					culture_group = THIS
					NOT = { relation = { who = THIS value = 100 } }
				}
			}
			owner = { set_country_flag = threat_from }
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 1: relations hit
			limit = {
				NOT = { owned_by = THIS }
				owner = {
					NOT = { relation = { who = THIS value = 100 } }
					culture_group = THIS
					NOT = { badboy = 0.5 }
					stability = 1
					manpower = 4
					manpower_percentage = 0.75
					num_of_cities = 5
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 4
					NOT = { war_exhaustion = 4 }
				}
			}
			owner = {
				set_country_flag = un_befriend_from
				relation = { who = THIS value = -100 }
			}
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 2: cb
			limit = {
				NOT = { owned_by = THIS }
				owner = {
					NOT = { relation = { who = THIS value = 100 } }
					culture_group = THIS
					NOT = { badboy = 0.4 }
					stability = 2
					manpower = 6
					manpower_percentage = 0.8
					num_of_cities = 10
					num_of_allies = 1
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 5
					NOT = { war_exhaustion = 2 }
				}
			}
			owner = {
				set_country_flag = antagonize_from
				casus_belli = THIS
			}
			THIS = { set_province_flag = trigger_296004 }
		}
		random_neighbor_province = {	#escalation 3: war
			limit = {
				NOT = { owned_by = THIS }
				owner = {
					ai = yes
					NOT = { relation = { who = THIS value = 100 } }
					culture_group = THIS
					NOT = { badboy = 0.33 }
					stability = 3
					manpower = 8
					manpower_percentage = 0.8
					num_of_cities = 15
					num_of_allies = 2
					NOT = { number_of_loans = 1 }
					NOT = { is_bankrupt = yes }
					MIL = 6
					NOT = { war_exhaustion = 1 }
					war = no
				}
			}
			owner = { war = THIS }
			THIS = {
				set_province_flag = trigger_296004
				owner = { set_country_flag = threat }
			}
		}
		owner = {
			random_owned = {
				limit = { has_province_flag = trigger_296004 }
				owner = { country_event = 296004 }
				clr_province_flag = trigger_296004
			}
		}
	}
}

# legitimize the acquisition - no contest by neighbors

province_event = {

  	id = 500120

	trigger = {
		has_province_flag = conquered
		NOT = { has_province_flag = contested }
		NOT = { has_province_flag = legitimized }
 	}

	mean_time_to_happen = {
			months = 60

			modifier = {
				factor = 0.9
				owner = { NOT = { badboy = 0.1 } }
			}
			modifier = {
				factor = 0.9
				owner = { NOT = { badboy = 0.2 } }
			}
			modifier = {
				factor = 0.9
				owner = { NOT = { badboy = 0.3 } }
			}
			modifier = {
				factor = 1.05
				owner = { badboy = 0.3 }
			}
			modifier = {
				factor = 1.05
				owner = { badboy = 0.4 }
			}
			modifier = {
				factor = 1.05
				owner = { badboy = 0.5 }
			}
			modifier = {
				factor = 1.1
				owner = { badboy = 0.6 }
			}
			modifier = {
				factor = 1.1
				owner = { badboy = 0.7 }
			}
			modifier = {
				factor = 1.1
				owner = { badboy = 0.8 }
			}
			modifier = {
				factor = 1.2
				owner = { badboy = 0.9 }
			}
			modifier = {
				factor = 1.5
				owner = { badboy = 1 }
			}
			modifier = {
				factor = 0.9
				NOT = { citysize = 50000 }
			}
			modifier = {
				factor = 0.9
				NOT = { citysize = 100000 }
			}
			modifier = {
				factor = 0.9
				OR = {
					trade_goods = grain
					trade_goods = wool
					trade_goods = slaves
				}
			}
			modifier = {
				factor = 1.1
				port = yes
			}
			modifier = {
				factor = 1.1
				citysize = 200000
			}
			modifier = {
				factor = 1.1
				citysize = 500000
			}
			modifier = {
				factor = 1.1
				OR = {
					trade_goods = wine
					trade_goods = cloth
					trade_goods = fur
					trade_goods = salt
					trade_goods = copper
					trade_goods = gold
					trade_goods = iron
					trade_goods = chinaware
					trade_goods = spices
					trade_goods = sugar
				}
			}
			modifier = {
				factor = 1.25
				cot = yes
			}
	}				

	title = "EVTNAME500120"
	desc = "EVTDESC500120"

	option = {
		name = "EVTOPTA500120"
		ai_chance = { factor = 100 }
		clr_province_flag = conquered
		set_province_flag = legitimized		
	}
}

# From contested to legitimized

province_event = {

  	id = 500121

	trigger = {
		has_province_flag = contested
		owner = { 
			DIP = 6
			NOT = { badboy = 0.1 }
		}
		NOT = { has_province_flag = conquered }
		NOT = { has_province_flag = legitimized }
 	}

	mean_time_to_happen = {
			years = 50
			modifier = {
				factor = 0.75
				owner = { DIP = 7 }
			}
			modifier = {
				factor = 0.5
				owner = { DIP = 8 }
			}
	}				

	title = "EVTNAME500120"
	desc = "EVTDESC500120"

	option = {
		name = "EVTOPTA500120"
		ai_chance = { factor = 100 }
		clr_province_flag = contested
		set_province_flag = legitimized		
	}
}



# nationalist revolt

province_event = {

  	id = 500130

	trigger = {
		NOT = { is_core = THIS }
		owner = { 
			OR = {
				badboy = 0.5
				NOT = { stability = 3 }
				war = yes
			}
		}
		OR = {
			has_province_flag = conquered	# between conquest and contest or legitimacy
			has_province_flag = legitimized
			has_province_flag = contested
		} 
		OR = {
			has_owner_culture = no
			has_owner_religion = no
		}
		OR = {
			can_spawn_rebels = patriot_rebels
			can_spawn_rebels = nationalist_rebels
			can_spawn_rebels = religious_rebels
		}
		NOT = { has_province_modifier = our_lien }
		NOT = { has_province_modifier = legit_demesne }
	}

	mean_time_to_happen = {
		months = 3600
		modifier = {
			factor = 2
			local_enemy = { tag = REB }
		}
		modifier = {
			factor = 2
			owner = { accepted_culture = THIS }
		}
		modifier = {
			factor = 1.4
			has_province_flag = legitimized
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = 2 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = 1 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -1 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -2 } }
		}
		modifier = {
			factor = 0.9
			has_owner_culture = no
			any_core = { primary_culture = THIS }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { culture_group = THIS } }
			any_core = { culture_group = THIS }
		}
		modifier = {
			factor = 0.9
			has_owner_religion = no
			any_core = { religion = THIS }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { religion_group = THIS } }
			any_core = { religion_group = THIS }
		}
		modifier = {
			factor = 0.9
			any_core = {
				religion_group = THIS
				culture_group = THIS
			}
		}
		modifier = {
			factor = 0.75
			can_spawn_rebels = religious_rebels
			any_neighbor_province = {
				NOT = { owned_by = THIS }
				religion = THIS
				has_owner_religion = yes
			}
		}
		modifier = {
			factor = 0.75
			can_spawn_rebels = religious_rebels
			any_neighbor_province = {
				NOT = { owned_by = THIS }
				religion = THIS
				has_owner_religion = yes
				owner = { is_core = current }
			}
		}
		modifier = {
			factor = 0.75
			can_spawn_rebels = nationalist_rebels
			any_core = { primary_culture = THIS }
		}
		modifier = {
			factor = 0.75
			can_spawn_rebels = patriot_rebels
			any_neighbor_province = {
				NOT = { owned_by = THIS }
				culture = THIS
				has_owner_culture = yes
			}
		}
		modifier = {
			factor = 0.75
			can_spawn_rebels = patriot_rebels
			any_neighbor_province = {
				NOT = { owned_by = THIS }
				has_owner_culture = yes
				culture = THIS
				owner = { is_core = current }
			}
		}
		modifier = {
			factor = 0.5
			has_province_flag = contested
		}
		modifier = {
			factor = 0.5
			owner = { badboy = 0.75 }
		}
		modifier = {
			factor = 0.5
			owner = { badboy = 1 }
		}
		modifier = {
			factor = 0.5
			NOT = { controlled_by = owner }
		}
	}				

	title = "EVTNAME500130"
	desc = "EVTDESC500130"

	immediate = {
		random_country = {
			limit = { THIS = { can_spawn_rebels = patriot_rebels } }
			THIS = {
				spawn_rebels = {
					type = patriot_rebels
					size = 2
				}
			}
		}
		random_country = {
			limit = {
				THIS = {
					NOT = { can_spawn_rebels = patriot_rebels }
					can_spawn_rebels = nationalist_rebels
				}
			}
			THIS = {
				spawn_rebels = {
					type = nationalist_rebels
					size = 2
				}
			}
		}
		random_country = {
			limit = {
				THIS = {
					NOT = { can_spawn_rebels = patriot_rebels }
					NOT = { can_spawn_rebels = nationalist_rebels }
				}
			}
			THIS = {
				spawn_rebels = {
					type = religious_rebels
					size = 2
				}
			}
		}
	}
	option = {
		name = "EVTOPTA500130"
		ai_chance = { factor = 100 }
	}
}


#####################################################################
#
#
#		HEATHEN PROVINCE CONQUERED
#
# The flag is deleted after siege in on_action
######################################################################



