#Carlos

country_event = {

	id = 889002
	
	trigger = {
		tag = ARA
		exists = CAS
		has_country_flag = ai_controlled
		CAS = { has_country_flag = ai_controlled }
		NOT = { war_with = CAS }
	}

	mean_time_to_happen = {
		years = 5

	}

	title = "EVTNAME889002" #	Iberian Bonds
	desc = "EVTDESC889002" #	Dummy

	option = {		
	name = "EVTOPTA889002" #	Biemvenidos, amigos
		ai_chance = { factor = 100 }
		relation = { who = CAS value = +100 }
	}
}


country_event = {

	id = 889003
	
	trigger = {
		tag = ARA
		exists = POR
		has_country_flag = ai_controlled
		POR = { has_country_flag = ai_controlled }
		NOT = { war_with = POR }
	}

	mean_time_to_happen = {
		years = 5

	}

	title = "EVTNAME889003" #	Iberian Bonds
	desc = "EVTDESC889003" #	Dummy

	option = {		
	name = "EVTOPTA889003" #	Biemvenidos, amigos
		ai_chance = { factor = 100 }
		relation = { who = POR value = +25 }
	}
}
