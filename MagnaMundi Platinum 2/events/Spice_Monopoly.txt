country_event = {

	id = 252001

	trigger = {


		REB = { NOT = { has_country_flag = monopoly_spices_end } }
		NOT = { has_country_flag = spice1 }
		NOT = { has_country_flag = spice2 }
		NOT = { has_country_flag = spice3 }

		num_of_ports = 1

		has_country_flag = won_india_route

		NOT = { mercantilism_freetrade = 0 }

	}

	mean_time_to_happen = {
		years = 10
		
		modifier = {
			factor = 0.2
			OR = {
				advisor = trader
				advisor = jewish_advisor_5
			}

		}

		modifier = {
			factor = 0.85
			NOT = { mercantilism_freetrade = -1 }

		}

		modifier = {
			factor = 0.70
			NOT = { mercantilism_freetrade = -2 }

		}

		modifier = {
			factor = 0.55
			NOT = { mercantilism_freetrade = -3 }

		}

		modifier = {
			factor = 0.4
			NOT = { mercantilism_freetrade = -4 }

		}

	}

	title = "EVTNAME252001" #	Distribution Monopoly
	desc = "EVTDESC252001"
	
	option = {
	name = "EVTOPTA252001" #	Force the monopoly
		ai_chance = { factor = 95 }

		REB = { set_country_flag = monopoly_spices }

		set_country_flag = spice1
		badboy = +3
		merchants = +5

	}

	option = {
	name = "EVTOPTB252001" #	Open trade
		ai_chance = { factor = 5 }

		prestige = 0.25
		years_of_income = +1.00
		mercantilism_freetrade = +4
		REB = { set_country_flag = monopoly_spices_end }
	}


}




country_event = {

	id = 252002

	trigger = {

		has_country_flag = spice1

	}

	mean_time_to_happen = {
		years = 10
		
		modifier = {				
			factor = 2
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}

		modifier = {
			factor = 0.2
			has_country_flag = sink_traders1
		}


	}

	title = "EVTNAME252002" #	Foreign Threat
	desc = "EVTDESC252002"
	
	option = {
	name = "EVTOPTA252002" #	No way to stop it...
		ai_chance = { factor = 80 }

		clr_country_flag = spice1
		set_country_flag = spice2
		clr_country_flag = sink_traders1

		prestige = -0.01
	}

	option = {
	name = "EVTOPTB252002" #	Sink the traders!
		ai_chance = { factor = 20 }

		badboy = +5
		relation = { who = ENG value = -50 }
		relation = { who = GBR value = -50 }
		relation = { who = CAS value = -50 }
		relation = { who = FRA value = -50 }
		relation = { who = SPA value = -50 }
		relation = { who = NED value = -50 }
		relation = { who = POR value = -50 }

		set_country_flag = sink_traders1
		
	}


}


country_event = {

	id = 252003

	trigger = {

		has_country_flag = spice2

	}

	mean_time_to_happen = {
		years = 10
		
		modifier = {				
			factor = 1.5
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}

		modifier = {
			factor = 0.2
			has_country_flag = sink_traders2
		}


	}

	title = "EVTNAME252003" #	Foreign Threat
	desc = "EVTDESC252003"
	
	option = {
	name = "EVTOPTA252003" #	No way to stop it...
		ai_chance = { factor = 80 }

		clr_country_flag = spice2
		set_country_flag = spice3
		clr_country_flag = sink_traders2

		prestige = -0.02
	}

	option = {
	name = "EVTOPTB252003" #	Sink the traders!
		ai_chance = { factor = 20 }

		badboy = +7

		relation = { who = ENG value = -75 }
		relation = { who = GBR value = -75 }
		relation = { who = CAS value = -75 }
		relation = { who = FRA value = -75 }
		relation = { who = SPA value = -75 }
		relation = { who = NED value = -75 }
		relation = { who = POR value = -75 }

		set_country_flag = sink_traders2
		
	}


}


country_event = {

	id = 252004

	trigger = {

		has_country_flag = spice3
		
		OR = {
			has_country_flag = sink_traders1
			has_country_flag = sink_traders2
		}

	}

	mean_time_to_happen = {
		years = 20

		modifier = {				
			factor = 1.25
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}
		
		modifier = {
			factor = 0.5
			has_country_flag = spice3
		}

		modifier = {
			factor = 0.75
			has_country_flag = sink_traders1
		}


		modifier = {
			factor = 0.25
			has_country_flag = sink_traders2
		}
	
	}


	title = "EVTNAME252004" #	End of Monopoly
	desc = "EVTDESC252004"
	
	option = {
	name = "EVTOPTA252004" #	So be it...
		ai_chance = { factor = 100 }


		clr_country_flag = spice3
		clr_country_flag = sink_traders2
		clr_country_flag = sink_traders1
		
		REB = { set_country_flag = monopoly_spices_end }

		prestige = -0.05
	}


}
