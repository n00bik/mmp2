#DAVID

#Localisation in MagnaMundi1.csv
#Event IDs 9031-9050

##############################
# Exploration of the Yucatan #
##############################

country_event = { #Discovery of the Yucatan

id = 9031
	
trigger = {
	OR = {
		owns = 484 #Havana, Cuba
		owns = 485 #Moron, Cuba
		owns = 486 #Guantanamo, Cuba
		owns = 926 #Seminole, Florida
		owns = 927 #Timucua, Florida
		}
	NOT = { has_country_flag = explore_yucatan }
	NOT = { has_country_flag = isolated }
	NOT = { has_country_flag = closed_society }
	NOT = { has_country_flag = limited_contact }
	NOT = { year = 1600 }
	852 = { owner = { culture_group = mesoamerican } }
	NOT = { owns = 852 }
	}

mean_time_to_happen = { months = 192 }

title = "EVTNAME9031"
desc = "EVTDESC9031"

option = {		
	name = "EVTOPTA9031"
	ai_chance = { factor = 100 }
	relation = { who = MAY value = -50 }
	MAY = { badboy = 1 }
	1505 = { discover = yes } # Florida Channel (just in case)
	1518 = { discover = yes } # Northwestern caribbean
	1512 = { discover = yes } # Yucatan Channel
	1513 = { discover = yes } # Belize Bay
	846 = { discover = yes } # Yucatan
	set_country_flag = explore_yucatan
	1514 = { discover = yes } #Moscitos Coast
	1515 = { discover = yes } #Gulf of Darien
	835 = { discover = yes } #Panama
	}
}

country_event = { #Exploration of the Yucatan Coast

id = 9032

trigger = {
	has_country_flag = explore_yucatan
	NOT = { has_country_flag = explore_yucatan_coast }
	852 = { owner = { culture_group = mesoamerican } }
	NOT = { has_country_flag = isolated }
	NOT = { has_country_flag = closed_society }
	NOT = { has_country_flag = limited_contact }
	NOT = { owns = 852 }
	}

mean_time_to_happen = { months = 12 }

title = "EVTNAME9032"
desc = "EVTDESC9032"

option = {		
	name = "EVTOPTA9032"
	ai_chance = { factor = 100 }
	1511 = { discover = yes } 	# Gulf of Mexico
	1510 = { discover = yes } 	# Yucatan Sea
	845 = { discover = yes } 	# Campeche
	set_country_flag = explore_yucatan_coast
	random_owned = { 
		limit = { 835 = { owner = { NOT = { num_of_cities = 1 } } } }
		owner = { 835 = { create_colony = 1 } }
		}
	}
}

####################################
# Trade Expedition to Tenochtitlan #
####################################

country_event = { #Trade Expedition to Tenochtitlan

id = 9033

trigger = {
	has_country_flag = explore_yucatan_coast
	NOT = { has_country_flag = isolated }
	NOT = { has_country_flag = closed_society }
	NOT = { has_country_flag = limited_contact }
	NOT = { has_country_flag = opening_society }
	NOT = { REB = { has_country_flag = aztec_expedition } }
	852 = { owner = { culture_group = mesoamerican } }
	NOT = { owns = 852 }
	NOT = { has_country_flag = mesoamerican_expedition }
	NOT = { has_country_flag = mesoamerican_expedition_over }
	}

mean_time_to_happen = { months = 6 }

title = "EVTNAME9033"
desc = "EVTDESC9033"

option = {
	ai_chance = { factor = 79 }
	name = "EVTOPTA9033"
	set_country_flag = mesoamerican_expedition
	852 = { owner = { set_country_flag = aztec_expedition set_country_flag = cortez_in_charge } }
	REB = { set_country_flag = aztec_expedition }
	848 = { discover = yes } 	# Tohancapan
	1509 = { discover = yes } 	# Tampico Bay
	}
option = { 
	ai_chance = { factor = 20 }
	name = "EVTOPTB9033"
	set_country_flag = mesoamerican_expedition
	852 = { owner = { set_country_flag = aztec_expedition } }
	REB = { set_country_flag = aztec_expedition }
	treasury = -25
	848 = { discover = yes } 	# Tohancapan
	1509 = { discover = yes } 	# Tampico Bay
	random_owned = { 
		limit = { continent = colombian }
		add_province_modifier = { 
			name = "colonial_tensions"
			duration = 1000
			}
		}
	}
option = { 
	ai_chance = { factor = 1 }
	name = "EVTOPTC9033"
	centralization_decentralization = -1
	set_country_flag = mesoamerican_expedition
	add_country_modifier = { 
		name = "export_restrictions"
		duration = 1000
		}
	random_owned = { 
		limit = { continent = colombian }
		add_province_modifier = { 
			name = "colonial_tensions"
			duration = 1000
			}
		}
	}
}

country_event = { #Strangers arrive in Tenochtitlan - ends this event chain

id = 9034

trigger = { 
	has_country_flag = aztec_expedition
	owns = 852 #Mexico
	NOT = { has_country_flag = cortez_in_charge }
	NOT = { has_country_flag = aztec_chaos }
	culture_group = mesoamerican
	848 = { owner = { culture_group = mesoamerican } }
	}

mean_time_to_happen = { months = 2 }

title = "EVTNAME9034"
desc = "EVTDESC9034"

option = {
	name = "EVTOPTA9034"
	ai_chance = { factor = 40 }
	stability = -2
	clr_country_flag = aztec_expedition
	treasury = -100
	set_country_flag = aztec_modernization
	add_country_modifier = { 
		name = "trading_post"
		duration = 7300
		}
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition }
		set_country_flag = aztec_trade
		set_country_flag = given_veracruz
		relation = { who = THIS value = 100 }
		clr_country_flag = mesoamerican_expedition 
		set_country_flag = mesoamerican_expedition_over
		}
	848 = {
		any_neighbor_province = {
			limit = { owned_by = THIS }
			base_tax = +1
			}
		}
	random_owned = {
		limit = { owner = { has_country_flag = isolated } }
		owner = { capital_scope = { province_event = 999001 } }
		}
	any_country = { 
		limit = { has_country_flag = aztec_expedition }
		clr_country_flag = aztec_expedition 
		}
	}
option = {
	name = "EVTOPTB9034"
	ai_chance = { factor = 40 }
	random_owned = { 
		limit = { owner = { stability = 3 } }
		owner = { prestige = 0.05 }
		}
	random_owned = { 
		limit = { owner = { NOT = { stability = 3 } } }
		owner = { stability = 1 }
		}
	clr_country_flag = aztec_expedition
	add_country_modifier = { 
		name = "religious_intolerance"
		duration = 365
		}
	any_country = { 
		limit = {	has_country_flag = mesoamerican_expedition }
		relation = { who = THIS value = -50 }
		add_casus_belli = THIS
		clr_country_flag = mesoamerican_expedition 
		set_country_flag = mesoamerican_expedition_over
		}
	any_country = { 
		limit = { has_country_flag = aztec_expedition }
		clr_country_flag = aztec_expedition 
		}
	}
option = {
	name = "EVTOPTC9034"
	ai_chance = { factor = 20 }
	clr_country_flag = aztec_expedition
	set_country_flag = aztec_modernization
	stability = -1
	treasury = -50
	add_country_modifier = { 
		name = "trading_post"
		duration = 1000
		}
	random_owned = {
		limit = { owner = { has_country_flag = isolated } }
		owner = { capital_scope = { province_event = 999001 } }
		}
	random_owned = { 
		limit = {
			any_neighbor_province = {
				NOT = { owner = { culture_group = mesoamerican } }
				citysize = 1
				}
			}
		base_tax = +1
		}
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition }
		set_country_flag = aztec_trade
		relation = { who = THIS value = 25 }
		clr_country_flag = mesoamerican_expedition 
		set_country_flag = mesoamerican_expedition_over
		}
	any_country = { 
		limit = { has_country_flag = aztec_expedition }
		clr_country_flag = aztec_expedition 
		}
	}
}

country_event = { #Mexican Trade

id = 9035

trigger = { 
	has_country_flag = aztec_trade
	NOT = { has_country_flag = given_veracruz }
	}

mean_time_to_happen = { months = 12 }

title = "EVTNAME9035"
desc = "EVTDESC9035"

option = { 
	name = "EVTOPTA9035" #	GOOD1
	treasury = +100
	848 = { discover = yes } 
	848 = { any_neighbor_province = { discover = yes } } #Tohancapan
	clr_country_flag = aztec_trade
	random_owned = { 
		limit = { continent = caribbean }
		base_tax = +1
		}
	add_country_modifier = { 
		name = "monetary_benefits"
		duration = 3650
		}
	merchants = 2
	}
}

country_event = { #Mexican Trade

id = 9036

trigger = { 
	has_country_flag = aztec_trade
	has_country_flag = given_veracruz
	}

mean_time_to_happen = { months = 12 }

title = "EVTNAME9036" #Same
desc = "EVTDESC9036"

option = { 
	name = "EVTOPTA9036"
	ai_chance = { factor = 50 }
	treasury = +250
	add_country_modifier = { 
		name = "monetary_benefits"
		duration = 3650
		}
	random_country = {
		limit = { 848 = { owner = { num_of_cities = 2 technology_group = america } } }
		848 = { 
			secede_province = THIS add_core = THIS #Tohancapan
			citysize = +500
			add_building = marketplace
			cot = yes
			any_neighbor_province = { discover = yes }
			}
		}
	clr_country_flag = given_veracruz
	clr_country_flag = aztec_trade
	merchants = 3
	any_country = { 
		limit = { has_country_flag = aztec_modernization }
		relation = { who = THIS value = 50 }
		}
	}
option = { 
	name = "EVTOPTB9036"
	ai_chance = { factor = 50 }
	treasury = +200
	manpower = -1
	REB = { set_country_flag = aztec_chaos }
	random_country = {
		limit = { 848 = { owner = { num_of_cities = 2 technology_group = america } } }
		848 = { 
			secede_province = THIS add_core = THIS #Tohancapan
			citysize = +500
			add_building = marketplace
			any_neighbor_province = { discover = yes }
			}
		}
	clr_country_flag = given_veracruz
	clr_country_flag = aztec_trade
	clr_country_flag = mesoamerican_expedition_over
	set_country_flag = mesoamerican_expedition
	any_country = { 
		limit = { has_country_flag = aztec_modernization }
		relation = { who = THIS value = -50 }
		casus_belli = THIS
		}
	}
}

########################
# Adventures in Mexico #
########################

country_event = { #Mexican Betrayal

id = 9037
	
trigger = {
	has_country_flag = mesoamerican_expedition
	NOT = { has_country_flag = revoke_mesoamerican_expedition }
	852 = { 
		owner = { 
			culture_group = mesoamerican
			NOT = { has_country_flag = aztec_recovery }
			}
		}
	}

mean_time_to_happen = { months = 2 }

title = "EVTNAME9037"
desc = "EVTDESC9037"

option = {	
	name = "EVTOPTA9037"
	ai_chance = { factor = 75 }
	set_country_flag = revoke_mesoamerican_expedition
	852 = { 
		owner = { clr_country_flag = aztec_expedition } 
		discover = yes
		any_neighbor_province = { discover = yes }
		}
	prestige = -0.02
	REB = { set_country_flag = aztec_chaos }
	random_owned = {
		limit = { 848 = { owner = { num_of_cities = 2 technology_group = america } } }
		848 = { 
			secede_province = THIS add_core = THIS #Tohancapan
			citysize = +250
			change_controller = REB
			pretender_rebels = 1
			}
		}
	}
option = {	
	name = "EVTOPTB9037"
	ai_chance = { factor = 25 }
	set_country_flag = revoke_mesoamerican_expedition
	852 = { 
		owner = { clr_country_flag = aztec_expedition } 
		discover = yes
		any_neighbor_province = { discover = yes }
		}
	treasury = +25
	prestige = -0.05
	badboy = +1
	REB = { set_country_flag = aztec_chaos }
	random_owned = {
		limit = { 848 = { owner = { num_of_cities = 2 technology_group = america } } } 
		848 = { 
			secede_province = THIS add_core = THIS #Tohancapan
			citysize = +500
			}
		owner = { infantry = 848 }
		}
	random_owned = { 
		limit = { owner = { ai = yes } }
		owner = { cavalry = 848 infantry = 848 }
		}
	random_owned = {
		limit = { 852 = { owner = { tag = AZT } } }
		THIS = { war = AZT }
		THIS = { stability = -1 }
		}
	random_owned = {
		limit = { 852 = { owner = { tag = TLA } } }
		THIS = { war = TLA }
		THIS = { stability = -1 }
		}
	random_owned = {
		limit = { 852 = { owner = { tag = MAY } } }
		THIS = { war = MAY }
		THIS = { stability = -1 }
		}
	random_owned = {
		limit = { 852 = { owner = { tag = MHC } } }
		THIS = { war = MHC }
		THIS = { stability = -1 }
		}
	random_owned = {
		limit = { 852 = { owner = { tag = CHC } } }
		THIS = { war = CHC }
		THIS = { stability = -1 }
		}
	}
}

country_event = { #Chaos in $COUNTRY$

id = 9038

trigger = { 
	owns = 852
	REB = { has_country_flag = aztec_chaos }
	NOT = { has_country_flag = aztec_chaos }
	NOT = { has_country_flag = aztec_recovery }
	culture_group = mesoamerican
	OR = { #It was their government that brought them down
		government = tribal_despotism-upper
		government = tribal_democracy-upper
		government = tribal_federation-upper
		government = tribal_despotism
		government = tribal_democracy
		government = tribal_federation
		government = tribal_despotism-lower
		government = tribal_democracy-lower
		government = tribal_federation-lower
		government = tribal_despotism-lowest
		government = tribal_democracy-lowest
		government = tribal_federation-lowest
		}
	}

mean_time_to_happen = { months = 1 }

title = "EVTNAME9038"
desc = "EVTDESC9038"

option = { 
	name = "EVTOPTA9038"
	ai_chance = { factor = 100 }
	set_country_flag = civil_war
	set_country_flag = aztec_chaos
	stability = -2
	add_country_modifier = { 
		name = "disorder"
		duration = 1000
		}
	random_owned = { 
		limit = { any_neighbor_province = { NOT = { owned_by = THIS } citysize = 100 } }
		nationalist_rebels = 2
		}
	random_owned = {
		limit = { owner = { has_country_flag = isolated NOT = { has_country_flag = first_contact } } }
		owner = { capital_scope = { province_event = 999001 } }
		}
	}
}

province_event = { 

id = 9049

trigger = { 
	province_id = 852
	owner = { 
		has_country_flag = aztec_chaos
		NOT = { has_country_flag = cortez_attacks }
		}
	}

mean_time_to_happen = { months = 12 }

title = "EVTNAME9049"
desc = "EVTDESC9049"

option = { 
	name = "EVTOPTA9049"
	noble_rebels = 2
	owner = {
		set_country_flag = cortez_attacks
		random_owned = {
			limit = { THIS = { owner = { ai = yes } } }
			THIS = { change_controller = REB }
			}
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		852 = { noble_rebels = 1 }
		}
	}
}

province_event = { #Strange news in $PROVINCENAME$

id = 9039
	
trigger = {
	owner = { 
		culture_group = mesoamerican
		NOT = { owns = 852 }
		NOT = { has_country_flag = disloyal_aztec_allies }
		NOT = { has_country_flag = loyal_aztec_allies }
		}
	852 = { owner = { has_country_flag = aztec_chaos } }
	}

mean_time_to_happen = { 

	months = 24

	modifier = {
		any_neighbor_province = { owner = { NOT = { culture_group = mesoamerican } } }
		factor = 0.25
		}
	modifier = { 
		any_neighbor_province = { owner = { has_country_flag = aztec_chaos } }
		factor = 0.5
		}
	modifier = { 
		any_neighbor_province = { owner = { has_country_flag = disloyal_aztec_allies } }
		factor = 0.5
		}
	modifier = { 
		any_neighbor_province = { owner = { has_country_flag = loyal_aztec_allies } }
		factor = 2
		}
	}

title = "EVTNAME9039"
desc = "EVTDESC9039"

option = {		
	name = "EVTOPTA9039"
	ai_chance = { factor = 80 }
	owner = { set_country_flag = disloyal_aztec_allies }
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition }
		relation = { who = THIS value = 50 }
		}
	any_country = { 
		limit = { owns = 852 }
		relation = { who = THIS value = -100 }
		casus_belli = THIS
		war = THIS
		}
	}
option = {		
	name = "EVTOPTB9039"
	ai_chance = { factor = 20 }
	owner = { set_country_flag = loyal_aztec_allies }
	any_country = { 
		limit = { owns = 852 }
		relation = { who = THIS value = 100 }
		}
	}
}

country_event = { #Puppet Emperor

id = 9040

trigger = { 
	culture_group = mesoamerican
	has_country_flag = aztec_chaos
	NOT = { controls = 852 } #Mexico
	NOT = { has_country_flag = tenochtitlan_lost }
	852 = { 
		controller = { 
			OR = { 
				has_country_flag = mesoamerican_expedition
				tag = REB
				culture_group = mesoamerican
				}
			} 
		}
	}

mean_time_to_happen = { days = 20 }

title = "EVTNAME9040"
desc = "EVTDESC9040"

option = { 
	ai_chance = { factor = 95 }
	name = "EVTOPTA9040"
	set_country_flag = tenochtitlan_lost
	stability = -1
	prestige = -0.10
	treasury = -100
	852 = { base_tax = -1 change_manpower = -0.5 }
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 2000
			NOT = { citysize = 4000 }
			}
		852 = { citysize = -1000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 4000
			NOT = { citysize = 8000 }
			}
		852 = { citysize = -2000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 8000
			NOT = { citysize = 16000 }
			}
		852 = { citysize = -4000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 16000
			NOT = { citysize = 32000 }
			}
		852 = { citysize = -8000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 32000
			NOT = { citysize = 64000 }
			}
		852 = { citysize = -16000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 64000
			NOT = { citysize = 128000 }
			}
		852 = { citysize = -32000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 128000
			NOT = { citysize = 256000 }
			}
		852 = { citysize = -64000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 256000
			}
		852 = { citysize = -128000 }
		}
	add_country_modifier = { 
		name = "decreased_morale"
		duration = 1000
		}
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition } 
		add_casus_belli = THIS
		relation = { who = THIS value = -200 }
		set_country_flag = cortez_wins
		}
	any_country = {
		limit = { has_country_flag = mesoamerican_expedition }
		country_event = 9041 
		}
	}
option = { 
	ai_chance = { factor = 5 }
	name = "EVTOPTB9040"
	capital = 853 #Huastec
	set_country_flag = tenochtitlan_lost
	kill_ruler = THIS
	stability = -3
	prestige = -0.20
	treasury = -200
	852 = { base_tax = -2 change_manpower = -0.5 }
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 2000
			NOT = { citysize = 4000 }
			}
		852 = { citysize = -1500 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 4000
			NOT = { citysize = 8000 }
			}
		852 = { citysize = -3000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 8000
			NOT = { citysize = 16000 }
			}
		852 = { citysize = -6000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 16000
			NOT = { citysize = 32000 }
			}
		852 = { citysize = -12000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 32000
			NOT = { citysize = 64000 }
			}
		852 = { citysize = -16000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 64000
			NOT = { citysize = 128000 }
			}
		852 = { citysize = -48000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 128000
			NOT = { citysize = 256000 }
			}
		852 = { citysize = -96000 }
		}
	random_owned = {
		limit = { 
			province_id = 852
			citysize = 256000
			}
		852 = { citysize = -192000 }
		}
	add_country_modifier = { 
		name = "disorder"
		duration = 3650
		}
	add_country_modifier = { 
		name = "decreased_morale"
		duration = 3650
		}
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition } 
		add_casus_belli = THIS
		relation = { who = THIS value = -200 }
		}
	}
}

country_event = { #Adventures in Mexico

id = 9041

is_triggered_only = yes 

title = "EVTNAME9041"
desc = "EVTDESC9041"

option = { 
	ai_chance = { factor = 100 }
	name = "EVTOPTA9041" #	Success is the ultimate justification
	treasury = +100
	prestige = +0.05
	badboy = +2
	clr_country_flag = cortez_wins
	set_country_flag = mexico
	848 = { change_controller = THIS }
	random_owned = { 
		limit = { AZT = { has_country_flag = tenochtitlan_lost } }
		852 = {
			secede_province = THIS 
			any_neighbor_province = {
				limit = { owned_by = AZT }
				secede_province = THIS
				discover = yes
				}
			}
		}
	random_owned = { 
		limit = { TLA = { has_country_flag = tenochtitlan_lost } }
		852 = {
			secede_province = THIS 
			any_neighbor_province = {
				limit = { owned_by = TLA }
				secede_province = THIS
				discover = yes
				}
			}
		}
	random_owned = { 
		limit = { MAY = { has_country_flag = tenochtitlan_lost } }
		852 = {
			secede_province = THIS 
			any_neighbor_province = {
				limit = { owned_by = MAY }
				secede_province = THIS
				discover = yes
				}
			}
		}
	random_owned = { 
		limit = { CHC = { has_country_flag = tenochtitlan_lost } }
		852 = {
			secede_province = THIS 
			any_neighbor_province = {
				limit = { owned_by = CHC }
				secede_province = THIS
				discover = yes
				}
			}
		}
	random_owned = { 
		limit = { MHC = { has_country_flag = tenochtitlan_lost } }
		852 = {
			secede_province = THIS 
			any_neighbor_province = {
				limit = { owned_by = MHC }
				secede_province = THIS
				discover = yes
				}
			}
		}
	any_country = { 
		limit = { culture_group = mesoamerican }
		set_country_flag = tenochtitlan_lost
		}
	}
}

#############
# Aftermath #
#############

country_event = { #Chichimeca Wars

id = 9044

trigger = { 
	owns = 852
	has_country_flag = mexico
	}

mean_time_to_happen = { months = 12 } 

title = "EVTNAME9044"
desc = "EVTDESC9044"

option = { 
	name = "EVTOPTA9044"
	ai_chance = { factor = 90 }
	841 = { discover = yes } #Guatemala
	843 = { discover = yes } #Belize
	844 = { discover = yes } #Zapotec
	849 = { discover = yes } #Tlaplanec
	clr_country_flag = mexico
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -200 }
		}
	any_country = { 
		limit = { has_country_flag = loyal_aztec_allies num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -200 }
		}
	any_country = {
		limit = { has_country_flag = aztec_chaos num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -400 }
		}
	}
option = { 
	name = "EVTOPTB9044"
	ai_chance = { factor = 5 }
	clr_country_flag = mexico
	841 = { discover = yes } #Guatemala
	843 = { discover = yes } #Belize
	844 = { discover = yes } #Zapotec
	849 = { discover = yes } #Tlaplanec
	set_country_flag = mexican_nationalism
	MEX = {
		add_core = 844		# Zapotec
		add_core = 845		# Campeche
		add_core = 846		# Yucatan
		add_core = 847		# Mixtec
		add_core = 848		# Tohancapan
		add_core = 849		# Tlapanec
		add_core = 850		# Tlaxcala
		add_core = 851		# Tarasco
		add_core = 852		# Mexico
		add_core = 853		# Huastec
		add_core = 854		# Sayultecas
		add_core = 856		# Zacatecas
		add_core = 858		# Tamaulipas
		add_core = 859		# Sinaloa
		add_core = 860		# Tepehuan
		add_core = 861		# Coahuila
		add_core = 862		# Yaqui
		add_core = 863		# Suma
		add_core = 864		# Concho
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies num_of_cities = 1 }
		relation = { who = THIS value = 200 }
		set_country_flag = aztec_modernization
		}
	any_country = { 
		limit = { has_country_flag = loyal_aztec_allies num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -200 }
		}
	any_country = {
		limit = { has_country_flag = aztec_chaos num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -400 }
		}
	}
option = { 
	ai_chance = { factor = 5 }
	name = "EVTOPTC9044"
	clr_country_flag = mexico
	841 = { discover = yes } #Guatemala
	843 = { discover = yes } #Belize
	844 = { discover = yes } #Zapotec
	849 = { discover = yes } #Tlaplanec
	treasury = -100
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies num_of_cities = 1 }
		relation = { who = THIS value = 50 }
		treasury = +100
		set_country_flag = aztec_modernization
		}
	any_country = { 
		limit = { has_country_flag = loyal_aztec_allies num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -200 }
		}
	any_country = {
		limit = { has_country_flag = aztec_chaos num_of_cities = 1 }
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -400 }
		}
	}
}

country_event = { #The European Legacy in $COUNTRY$

id = 9045

trigger = { 
	culture_group = mesoamerican
	has_country_flag = aztec_modernization 
	}

mean_time_to_happen = { months = 36 }

title = "EVTNAME9045"
desc = "EVTDESC9045"

option = { 
	name = "EVTOPTA9045"
	ai_chance = { factor = 50 }
	clr_country_flag = aztec_modernization
	stability = -5
	random_owned = {
		limit = {
			owner = {
				OR = {
					government = tribal_despotism-upper
					government = tribal_democracy-upper
					government = tribal_federation-upper
					government = tribal_despotism
					government = tribal_democracy
					government = tribal_federation
					government = tribal_despotism-lower
					government = tribal_democracy-lower
					government = tribal_federation-lower
					government = tribal_despotism-lowest
					government = tribal_democracy-lowest
					government = tribal_federation-lowest
				}
			}
		}
		owner = { government = early_feudal_monarchy-upper }
		}
	innovative_narrowminded = -2
	centralization_decentralization = -2
	mercantilism_freetrade = 1
	add_idea = bureaucracy
	capital_scope = { anti_tax_rebels = 2 }
	government_tech = +250
	production_tech = +250
	add_country_modifier = { 
		name = "western_influences"
		duration = 7300
		}
	any_country = { 
		limit = { owns = 852 }
		relation = { who = THIS value = 50 }
		}
	}
option = { 
	ai_chance = { factor = 50 }
	name = "EVTOPTB9045"
	clr_country_flag = aztec_modernization
	stability = -3
	add_idea = deus_vult
	set_country_flag = event_only_DV
	innovative_narrowminded = 1
	centralization_decentralization = -1
	offensive_defensive = -2
	land_naval = 1
	capital_scope = { cavalry = THIS }
	land_tech = +1000
	add_country_modifier = { 
		name = "religious_intolerance"
		duration = 7300
		}
	add_country_modifier = { 
		name = "army_reform"
		duration = 7300
		}
	any_country = { 
		limit = { owns = 852 }
		relation = { who = THIS value = -50 }
		casus_belli = THIS
		}
	}
}

country_event = { #Recovery of the Empire

id = 9046

trigger = { 
	culture_group = mesoamerican
	owns = 852
	has_country_flag = aztec_chaos
	NOT = { war = yes }
	NOT = { has_country_flag = civil_war }
	}

mean_time_to_happen = { months = 3 }

title = "EVTNAME9046"
desc = "EVTDESC9046"

option = { 
	ai_chance = { factor = 50 }
	name = "EVTOPTA9046"
	any_country = { limit = { has_country_flag = aztec_chaos } clr_country_flag = aztec_chaos }
	clr_country_flag = aztec_chaos
	set_country_flag = aztec_recovery
	set_country_flag = harsh_demands
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition }
		set_country_flag = mesoamerican_expedition_over
		}
	any_country = { 
		limit = { has_country_flag = aztec_expedition }
		clr_country_flag = aztec_expedition 
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		casus_belli = THIS
		capital_scope = { add_core = THIS }
		relation = { who = THIS value = -50 }
		}
	}
option = { 
	ai_chance = { factor = 50 }
	name = "EVTOPTB9046"
	any_country = { limit = { has_country_flag = aztec_chaos } clr_country_flag = aztec_chaos }
	set_country_flag = aztec_recovery
	set_country_flag = lenient_demands
	clr_country_flag = aztec_chaos
	any_country = { 
		limit = { has_country_flag = mesoamerican_expedition }
		set_country_flag = mesoamerican_expedition_over
		}
	any_country = { 
		limit = { has_country_flag = aztec_expedition }
		clr_country_flag = aztec_expedition 
		}
	random_owned = { 
		limit = { owner = { stability = 3 } }
		owner = { prestige = 0.05 }
		}
	random_owned = { 
		limit = { owner = { NOT = { stability = 3 } } }
		owner = { stability = 1 }
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		relation = { who = THIS value = +50 }
		}
	}
}

country_event = { #The failed gambit

id = 9047

trigger = { 
	culture_group = mesoamerican
	has_country_flag = disloyal_aztec_allies
	NOT = { REB = { has_country_flag = aztec_chaos } }
	NOT = { has_country_flag = mesoamerican_expedition_over }
	}

mean_time_to_happen = { months = 12 }

title = "EVTNAME9047"
desc = "EVTDESC9047"

option = { 
	name = "EVTOPTA9047"
	ai_chance = { factor = 30 }
	prestige = -0.10
	war_exhaustion = -2
	treasury = -50
	aristocracy_plutocracy = 3
	serfdom_freesubjects = -3
	centralization_decentralization = -3
	set_country_flag = mesoamerican_expedition_over
	any_country = { 
		limit = { has_country_flag = aztec_recovery has_country_flag = harsh_demands }
		relation = { who = THIS value = +100 }
		inherit = THIS
		treasury = +50
		}
	any_country = { 
		limit = { has_country_flag = aztec_recovery has_country_flag = lenient_demands }
		relation = { who = THIS value = +50 }
		treasury = +50
		}
	}
option = { 
	name = "EVTOPTB9047"
	ai_chance = { factor = 40 }
	prestige = -0.05
	treasury = -25
	aristocracy_plutocracy = 1
	serfdom_freesubjects = -1
	centralization_decentralization = -1
	set_country_flag = mesoamerican_expedition_over
	any_country = { 
		limit = { has_country_flag = aztec_recovery has_country_flag = harsh_demands }
		relation = { who = THIS value = +50 }
		treasury = +25
		add_casus_belli = THIS
		}
	any_country = { 
		limit = { has_country_flag = aztec_recovery has_country_flag = lenient_demands }
		relation = { who = THIS value = +25 }
		treasury = +25
		}
	}
option = { 
	name = "EVTOPTC9047"
	ai_chance = { factor = 30 }
	stability = -1
	set_country_flag = mesoamerican_expedition_over
	any_country = { 
		limit = { has_country_flag = aztec_recovery has_country_flag = harsh_demands }
		relation = { who = THIS value = -100 }
		war = THIS
		}
	any_country = { 
		limit = { has_country_flag = aztec_recovery has_country_flag = lenient_demands }
		relation = { who = THIS value = -100 }
		add_casus_belli = THIS
		}
	}
}

country_event = { #Disaster in Mexico

id = 9048

trigger = {
	has_country_flag = mesoamerican_expedition_over
	has_country_flag = mesoamerican_expedition
	}

mean_time_to_happen = { months = 6 }

title = "EVTNAME9048"
desc = "EVTDESC9048"

option = { 
	name = "EVTOPTA9048"
	ai_chance = { factor = 50 }
	clr_country_flag = mesoamerican_expedition
	diplomats = -1
	any_country = { 
		limit = { has_country_flag = aztec_recovery }
		relation = { who = THIS value = -50 }
		set_country_flag = aztec_modernization
		casus_belli = THIS
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		relation = { who = THIS value = -100 }
		casus_belli = THIS
		}
	848 = { 
		add_province_modifier = { 
			name = "local_dissatisfaction"
			duration = 1000
			}
		}
	}
option = { 
	name = "EVTOPTB9048"
	ai_chance = { factor = 20 }
	clr_country_flag = mesoamerican_expedition
	prestige = -0.01
	any_country = { 
		limit = { has_country_flag = aztec_recovery }
		relation = { who = THIS value = -100 }
		casus_belli = THIS
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		relation = { who = THIS value = -200 }
		casus_belli = THIS
		}
	}
option = { 
	name = "EVTOPTC9048"
	ai_chance = { factor = 20 }
	clr_country_flag = mesoamerican_expedition
	badboy = +1
	any_country = { 
		limit = { has_country_flag = aztec_recovery }
		relation = { who = THIS value = -200 }
		casus_belli = THIS
		add_casus_belli = THIS
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		relation = { who = THIS value = -200 }
		casus_belli = THIS
		add_casus_belli = THIS
		}
	}
option = { 
	name = "EVTOPTD9048"
	ai_chance = { factor = 10 }
	clr_country_flag = mesoamerican_expedition
	prestige = -0.02
	stability = -1
	diplomats = -1
	any_country = { 
		limit = { has_country_flag = aztec_recovery }
		set_country_flag = aztec_modernization
		casus_belli = THIS
		}
	any_country = { 
		limit = { has_country_flag = disloyal_aztec_allies }
		relation = { who = THIS value = 50 }
		}
	848 = { 
		add_province_modifier = { 
			name = "local_dissatisfaction"
			duration = 1000
			}
		}
	}
}


