#Give it back
country_event = {

	id = 1130

	is_triggered_only = yes

	title = "EVTNAME1130"
	desc = "EVTTEXT295000"

	option = { 
		name = "EVTTEXT295000"
		any_owned = {
			limit = { 
				has_province_flag = targeted_by_emperor_conquer_prov
				emperor = { ai = yes }
			}
			clr_province_flag = targeted_by_emperor_conquer_prov
			set_province_flag = un_conquer_prov
		}
		random_owned = {
			limit = { has_province_flag = un_conquer_prov }
			set_global_flag = un_conquer_prov
			emperor = { country_event = 296000 }
		}
		any_owned = {	# general clean-up of flags and modifiers
			clr_province_flag = bargaining_chip
			clr_province_flag = buettel_on_the_way
			clr_province_flag = contested
			clr_province_flag = conquered
			clr_province_flag = full_amnesty_for_province
			clr_province_flag = hre_disputed
			clr_province_flag = hre_lien
			clr_province_flag = hre_lien_applied
			clr_province_flag = legitimized
			clr_province_flag = make_core_rld
			clr_province_flag = must_give_up
			clr_province_flag = no_amnesty
		 	clr_province_flag = non_core_base
		 	clr_province_flag = object_of_dispute
			clr_province_flag = small_amnesty_for_province
			clr_province_flag = small_amnesty_without_strings
			clr_province_flag = takenmid
			clr_province_flag = reset_takenmid
			clr_province_flag = unchristian_conquest
	 		remove_province_modifier = amnesty
	 		remove_province_modifier = court_case
	 		remove_province_modifier = crusader_bonus
	 		remove_province_modifier = custody
	 		remove_province_modifier = legit_demesne
	 		remove_province_modifier = non_core_timer
	 		remove_province_modifier = our_lien
		}
		any_owned = {		# Demesne always takes precedence
			limit = { has_province_flag = beneficiary_RLD RLD = { is_emperor = no } NOT = { has_global_flag = reichsverweser } }
			remove_province_modifier = ex_vassal_of_emperor
			set_province_flag = make_your_core
			RLD = { country_event = 1136 }	#Special event needed to add modifier and flags
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_ALS }
			set_province_flag = make_your_core
			secede_province = ALS
			ALS = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_ANH }
			set_province_flag = make_your_core
			secede_province = ANH
			ANH = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_ANS }
			set_province_flag = make_your_core
			secede_province = ANS
			ANS = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_AQU }
			set_province_flag = make_your_core
			secede_province = AQU
			AQU = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BAD }
			set_province_flag = make_your_core
			secede_province = BAD
			BAD = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BAV }
			set_province_flag = make_your_core
			secede_province = BAV
			BAV = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BOH }
			set_province_flag = make_your_core
			secede_province = BOH
			BOH = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BRA }
			set_province_flag = make_your_core
			secede_province = BRA
			BRA = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BRE }
			set_province_flag = make_your_core
			secede_province = BRE
			BRE = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BRU }
			set_province_flag = make_your_core
			secede_province = BRU
			BRU = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_BUR }
			set_province_flag = make_your_core
			secede_province = BUR
			BUR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_DAN }
			set_province_flag = make_your_core
			secede_province = DAN
			DAN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_FER }
			set_province_flag = make_your_core
			secede_province = FER
			FER = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_FRA }
			set_province_flag = make_your_core
			secede_province = FRA
			FRA = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_FRI }
			set_province_flag = make_your_core
			secede_province = FRI
			FRI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_GEL }
			set_province_flag = make_your_core
			secede_province = GEL
			GEL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_GEN }
			set_province_flag = make_your_core
			secede_province = GEN
			GEN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HAB }
			set_province_flag = make_your_core
			secede_province = HAB
			HAB = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HAI }
			set_province_flag = make_your_core
			secede_province = HAI
			HAI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HAM }
			set_province_flag = make_your_core
			secede_province = HAM
			HAM = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HAN }
			set_province_flag = make_your_core
			secede_province = HAN
			HAN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HCB }
			set_province_flag = make_your_core
			secede_province = HCB
			HCB = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HCD }
			set_province_flag = make_your_core
			secede_province = HCD
			HCD = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HCL }
			set_province_flag = make_your_core
			secede_province = HCL
			HCL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HES }
			set_province_flag = make_your_core
			secede_province = HES
			HES = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HOL }
			set_province_flag = make_your_core
			secede_province = HOL
			HOL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_HUN }
			set_province_flag = make_your_core
			secede_province = HUN
			HUN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_ICF }
			set_province_flag = make_your_core
			secede_province = ICF
			ICF = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_ICN }
			set_province_flag = make_your_core
			secede_province = ICN
			ICN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_KLE }
			set_province_flag = make_your_core
			secede_province = KLE
			KLE = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_KOL }
			set_province_flag = make_your_core
			secede_province = KOL
			KOL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_KUR }
			set_province_flag = make_your_core
			secede_province = KUR
			KUR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_LIE }
			set_province_flag = make_your_core
			secede_province = LIE
			LIE = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_LIT }
			set_province_flag = make_your_core
			secede_province = LIT
			LIT = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_LIV }
			set_province_flag = make_your_core
			secede_province = LIV
			LIV = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_LOR }
			set_province_flag = make_your_core
			secede_province = LOR
			LOR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_LUN }
			set_province_flag = make_your_core
			secede_province = LUN
			LUN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_LUX }
			set_province_flag = make_your_core
			secede_province = LUX
			LUX = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MAG }
			set_province_flag = make_your_core
			secede_province = MAG
			MAG = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MAI }
			set_province_flag = make_your_core
			secede_province = MAI
			MAI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MAN }
			set_province_flag = make_your_core
			secede_province = MAN
			MAN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MEI }
			set_province_flag = make_your_core
			secede_province = MEI
			MEI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MKL }
			set_province_flag = make_your_core
			secede_province = MKL
			MKL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MLO }
			set_province_flag = make_your_core
			secede_province = MLO
			MLO = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MOD }
			set_province_flag = make_your_core
			secede_province = MOD
			MOD = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_MUN }
			set_province_flag = make_your_core
			secede_province = MUN
			MUN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_NAP }
			set_province_flag = make_your_core
			secede_province = NAP
			NAP = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_NED }
			set_province_flag = make_your_core
			secede_province = NED
			NED = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_OLD }
			set_province_flag = make_your_core
			secede_province = OLD
			OLD = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_PAL }
			set_province_flag = make_your_core
			secede_province = PAL
			PAL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_PAR }
			set_province_flag = make_your_core
			secede_province = PAR
			PAR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_PIS }
			set_province_flag = make_your_core
			secede_province = PIS
			PIS = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_POL }
			set_province_flag = make_your_core
			secede_province = POL
			POL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_POM }
			set_province_flag = make_your_core
			secede_province = POM
			POM = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_PRO }
			set_province_flag = make_your_core
			secede_province = PRO
			PRO = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_PRU }
			set_province_flag = make_your_core
			secede_province = PRU
			PRU = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_RIG }
			set_province_flag = make_your_core
			secede_province = RIG
			RIG = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SAR }
			set_province_flag = make_your_core
			secede_province = SAR
			SAR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SAV }
			set_province_flag = make_your_core
			secede_province = SAV
			SAV = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SAX }
			set_province_flag = make_your_core
			secede_province = SAX
			SAX = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SCA }
			set_province_flag = make_your_core
			secede_province = SCA
			SCA = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SHL }
			set_province_flag = make_your_core
			secede_province = SHL
			SHL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SIE }
			set_province_flag = make_your_core
			secede_province = SIE
			SIE = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SIL }
			set_province_flag = make_your_core
			secede_province = SIL
			SIL = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SLZ }
			set_province_flag = make_your_core
			secede_province = SLZ
			SLZ = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SPI }
			set_province_flag = make_your_core
			secede_province = SPI
			SPI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_SWI }
			set_province_flag = make_your_core
			secede_province = SWI
			SWI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_TEU }
			set_province_flag = make_your_core
			secede_province = TEU
			TEU = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_THU }
			set_province_flag = make_your_core
			secede_province = THU
			THU = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_TIR }
			set_province_flag = make_your_core
			secede_province = TIR
			TIR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_TRI }
			set_province_flag = make_your_core
			secede_province = TRI
			TRI = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_TUS }
			set_province_flag = make_your_core
			secede_province = TUS
			TUS = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_UTR }
			set_province_flag = make_your_core
			secede_province = UTR
			UTR = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_VEN }
			set_province_flag = make_your_core
			secede_province = VEN
			VEN = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_WBG }
			set_province_flag = make_your_core
			secede_province = WBG
			WBG = { country_event = 1131 }
		}
		any_owned = {
			limit = { has_province_flag = beneficiary_WUR }
			set_province_flag = make_your_core
			secede_province = WUR
			WUR = { country_event = 1131 }
		}
		any_owned = {	#Residual, if beneficiary flag is absent / RLD is not RVW
			limit = { RLD = { is_emperor = no } NOT = { has_global_flag = reichsverweser } }
			remove_province_modifier = ex_vassal_of_emperor
			set_province_flag = make_your_core
			set_province_flag = beneficiary_RLD
			RLD = { country_event = 1136 }	#Special event needed to add modifier and flags
		}
		any_owned = {	#Residual, if beneficiary flag is absent / RLD is RVW
			limit = {
				OR = {
					RLD = { is_emperor = yes }
					has_global_flag = reichsverweser
				}
				NOT = { is_core = THIS }
			}
			remove_province_modifier = ex_vassal_of_emperor
			set_province_flag = make_your_core
			set_province_flag = beneficiary_RLD
			owner = { country_event = 1136 }	#Special event needed to add modifier and flags
		}
	}
}

#recipient of province gets money and core
country_event = {

	id = 1131

	is_triggered_only = yes

	title = "EVTNAME1130"
	desc = "EVTDESC112201"

	option = { 
		name = "FINALLY2"
		random_owned = {
			limit = { owner = { NOT = { num_of_cities = 2 } } }
			owner = { treasury = 100 }
		}
		random_owned = {
			limit = { owner = { ai = yes } }
			owner = { treasury = 100 }
		}
		random_owned = {
			limit = { owner = { capital_scope = { NOT = { has_building = guardia_real } } } }
			owner = { capital_scope = { add_building = guardia_real } }
		}
		random_owned = {
			limit = { has_province_flag = make_your_core }
			clr_province_flag = make_your_core
			add_core = THIS
			hre = yes
			owner = { badboy = -1 }
		}
		random_owned = {
			limit = {
				owner = { NOT = { num_of_cities = 3 } }
				has_building = guardia_real
			}
			any_neighbor_province = {
				limit = { hre = yes NOT = { owned_by = THIS } NOT = { owner = { relation = { who = THIS value = 100 } } } }
				owner = { relation = { who = THIS value = 200 } }
			}
		}
		any_country = {
			limit = {
				THIS = { ai = yes }
				any_owned_province = {
					continent = europe
					NOT = { has_discovered = THIS }
				}
			}
			any_owned = {
				limit = {
					continent = europe
					NOT = { has_discovered = THIS }
				}
				discover = yes
			}
		}
	}
}

#remove core of RLD before returning to rightful owner
country_event = {

	id = 1132

	is_triggered_only = yes

	title = "EVTNAME1132"
	desc = "EVTTEXT295000"

	option = { 
		name = "EVTTEXT295000"
		any_owned = { remove_core = THIS }
		RLD = { country_event = 1130 }
	}
}

# Special case: restitution to a country that may not be the offical beneficiary
country_event = {

	id = 1133

	is_triggered_only = yes

	title = "EVTNAME1133"
	desc = "EVTDESC1133"

	option = { 
		name = "FINALLY2"
		clr_country_flag = restitution_granted
		clr_country_flag = has_requested_restitution
		random_country = {
			limit = { has_country_flag = restitution_target }
			clr_country_flag = restitution_target
			relation = { who = THIS value = 10 }
			random_owned = {
				limit = { has_province_flag = to_be_restituted }
				clr_province_flag = to_be_restituted
			 	remove_province_modifier = non_core_timer
				clr_province_flag = takenmid
				clr_province_flag = reset_takenmid
				clr_province_flag = legitimized
				clr_province_flag = contested
				clr_province_flag = conquered
			 	clr_province_flag = non_core_base
				secede_province = THIS
				hre = yes
			}
		}
		clr_global_flag = restitution_in_progress
	}
}

# Special case: restitution to a country that may not be the offical beneficiary
country_event = {

	id = 1134

	is_triggered_only = yes

	title = "EVTNAME1134"
	desc = "EVTDESC1134"

	option = { 
		name = "FINALLY2"
		clr_country_flag = restitution_plaintiff
		random_country = {
			limit = { has_country_flag = restitution_origin }
			clr_country_flag = restitution_origin
			relation = { who = THIS value = 10 }
			random_owned = {
				limit = { has_province_flag = to_be_restituted }
				clr_province_flag = to_be_restituted
			 	remove_province_modifier = non_core_timer
				clr_province_flag = takenmid
				clr_province_flag = reset_takenmid
				clr_province_flag = legitimized
				clr_province_flag = contested
				clr_province_flag = conquered
			 	clr_province_flag = non_core_base
				secede_province = THIS
				hre = yes
			}
		}
		clr_global_flag = restitution_in_progress
	}
}

#add core and modifier for RLD, give to Emperor
country_event = {

	id = 1136

	is_triggered_only = yes

	title = "EVTNAME1136"
	desc = "EVTTEXT295000"

	option = { 
		name = "EVTTEXT295000"
		random_owned = {
			limit = { emperor = { ai = no } }
			set_province_flag = trigger_28640
		}
		any_owned = { remove_province_modifier = legit_demesne }
		any_owned = {	# Limit new HRE provs to those that neighbor former or existing HRE provs
			limit = {
				has_province_flag = make_your_core
				NOT = { hre = yes }
				NOT = { region = hre_region }
				any_neighbor_province = {
					OR = {
						hre = yes
						region = hre_region
					}
				}
			}
			hre = yes
		}
		any_owned = {	# Clear flags from restored HRE provs
			limit = {
				has_province_flag = make_your_core
				NOT = { hre = yes }
				region = hre_region
			}
			hre = yes
		}
		any_owned = {
			limit = {
				has_province_flag = make_your_core
				hre = yes
			}
			add_core = THIS
			clr_province_flag = make_your_core
			clr_province_flag = takenmid
			clr_province_flag = reset_takenmid
			clr_province_flag = legitimized
			clr_province_flag = contested
			clr_province_flag = conquered
		 	clr_province_flag = non_core_base
			secede_province = emperor
			emperor = { badboy = -1 }
			emperor = { prestige = 0.05 }
		 	add_province_modifier = { 
				name = "legit_demesne"
				duration = -1
			}
		}
		any_owned = {
			clr_province_flag = make_your_core
			clr_province_flag = takenmid
			clr_province_flag = reset_takenmid
			clr_province_flag = legitimized
			clr_province_flag = contested
			clr_province_flag = conquered
		 	clr_province_flag = non_core_base
			emperor = { prestige = 0.05 }
		}
		any_owned = {
			limit = { RLD = { is_emperor = no } NOT = { has_global_flag = reichsverweser } }
			secede_province = emperor
		}
		emperor = {
			random_owned = {
				limit = { has_province_flag = trigger_28640 }
				clr_province_flag = trigger_28640
				province_event = 28640
			}
		}
	}
}
