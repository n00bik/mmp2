# Revolution
country_event = {

	id = 3011

	major = yes
	
	trigger = {
		
		NOT = { has_country_flag = revolution }
		OR = {
			government = feudal_monarchy
			government = despotic_monarchy
			government = noble_republic
			government = absolute_monarchy
			government = administrative_monarchy
			government = enlightened_despotism
			government = bureaucratic_despotism
			government = republican_dictatorship
			government = electoral_government
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.6 }
			}
			has_country_flag = hereditary_emperor
			AND = {
				has_global_flag = atage
				OR = {
					government = feudal_monarchy-upper
					government = feudal_monarchy-lower
					government = feudal_monarchy-lowest
					government = despotic_monarchy-upper
					government = despotic_monarchy-lower
					government = despotic_monarchy-lowest
					government = noble_republic-upper
					government = noble_republic-lower
					government = noble_republic-lowest
					government = absolute_monarchy-upper
					government = absolute_monarchy-lower
					government = absolute_monarchy-lowest
					government = administrative_monarchy-upper
					government = administrative_monarchy-lower
					government = administrative_monarchy-lowest
					government = enlightened_despotism-upper
					government = enlightened_despotism-lower
					government = enlightened_despotism-lowest
					government = bureaucratic_despotism-upper
					government = bureaucratic_despotism-lower
					government = bureaucratic_despotism-lowest
					government = republican_dictatorship-upper
					government = republican_dictatorship-lower
					government = republican_dictatorship-lowest
				}
			}
		}
		OR = {
			war_exhaustion = 8
			inflation = 30
			NOT = { prestige = 0 }
			NOT = { check_variable = { which = AE_level value = 6 } }
			check_variable = { which = power_of_parliament value = 10 }
			capital_scope = { check_variable = { which = social_revolution value = 20 } }
			capital_scope = { controlled_by = REB }
		}
		OR = {
			war = no
			AND = {
				war_exhaustion = 12
				NOT = { war_score = -25 }
			}
		}
		government_tech = 48
		num_of_cities = 10
		revolution_target_exists = no 
		capital_scope = { continent = europe }
	}
	
	mean_time_to_happen = {
		months = 360
		
		modifier = {
			factor = 10
			has_global_flag = revolution_happened
		}
		modifier = {
			factor = 10
			has_country_flag = had_revolution
		}
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.8
			NOT = { manpower_percentage = 0.5 }
		}
		modifier = {
			factor = 0.8
			inflation = 30
		}
		modifier = {
			factor = 0.8
			inflation = 40
		}
		modifier = {
			factor = 0.8
			inflation = 50
		}
		modifier = {
			factor = 0.8
			wartax = yes
		}
		modifier = {
			factor = 0.8
			number_of_loans = 1
		}
		modifier = {
			factor = 0.8
			is_bankrupt = yes
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.9
			NOT = { ADM = 7 }
		}
		modifier = {
			factor = 0.8
			NOT = { ADM = 5 }
		}
		modifier = {
			factor = 0.9
			NOT = { DIP = 7 }
		}
		modifier = {
			factor = 0.8
			NOT = { DIP = 5 }
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 7 }
		}
		modifier = {
			factor = 0.8
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.9
			capital_scope = { check_variable = { which = social_revolution value = 20 } }
		}
		modifier = {
			factor = 0.9
			capital_scope = { check_variable = { which = social_revolution value = 10 } }
		}
		modifier = {
			factor = 0.9
			any_owned_province = { is_overseas = no is_core = THIS has_owner_culture = yes check_variable = { which = social_revolution value = 7 } }
		}
		modifier = {
			factor = 0.9
			any_owned_province = { is_overseas = no is_core = THIS has_owner_culture = yes check_variable = { which = social_revolution value = 14 } }
		}
		modifier = {
			factor = 0.9
			any_owned_province = { is_overseas = no is_core = THIS has_owner_culture = yes check_variable = { which = social_revolution value = 21 } }
		}
		modifier = {
			factor = 0.9
			any_owned_province = { is_overseas = no is_core = THIS has_owner_culture = yes check_variable = { which = social_revolution value = 28 } }
		}
		modifier = {
			factor = 0.9
			any_owned_province = { is_overseas = no is_core = THIS has_owner_culture = yes check_variable = { which = social_revolution value = 35 } }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 10 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 8 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 6 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 4 }
		}
		modifier = {
			factor = 0.75
			has_country_flag = asked_for_war_funds
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.95
			NOT = { aristocracy_plutocracy = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { aristocracy_plutocracy = -4 }
		}		
		modifier = {
			factor = 0.95
			centralization_decentralization = -3
		}
		modifier = {
			factor = 0.85
			centralization_decentralization = -4
		}
		modifier = {
			factor = 0.95
			NOT = { serfdom_freesubjects = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { serfdom_freesubjects = -4 }
		}
		modifier = {
			factor = 0.9
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 1.2
			idea = humanist_tolerance
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3011"
	desc = "EVTDESC3011"
	
	immediate = { stability = -6 }
	option = {
		name = "EVTOPTA3011"			# Dire times are ahead of us
		set_country_flag = revolution
		
	}
}


# Religious intolerance
country_event = {

	id = 3012
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = religious_revolution }
		OR = {
			NOT = { protestant = 1 }
			NOT = { reformed = 1 }
		}
	}
	
	mean_time_to_happen = {
		months = 30
		
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.75
			check_variable = { which = power_of_parliament value = 6 }
			OR = {
				has_country_flag = parliament_conservative
				has_country_flag = parliament_ultra_conservative
			}
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
			OR = {
				has_country_flag = parliament_conservative
				has_country_flag = parliament_ultra_conservative
			}
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.9
			NOT = { protestant = -2 }
		}		
		modifier = {
			factor = 0.8
			NOT = { protestant = -3 }
		}
		modifier = {
			factor = 0.7
			NOT = { protestant = -4 }
		}
		modifier = {
			factor = 0.9
			NOT = { reformed = -2 }
		}		
		modifier = {
			factor = 0.8
			NOT = { reformed = -3 }
		}
		modifier = {
			factor = 0.7
			NOT = { reformed = -4 }
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 100 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3012"
	desc = "EVTDESC3012"
	
	option = {
		name = "EVTOPTA3012"			# One nation, one belief
		ai_chance = { factor = 75 }
		set_country_flag = religious_revolution
		add_country_modifier = {
			name = "religious_intolerance"
			duration = 720
		}
		stability = -1
	}
	option = {
		name = "EVTOPTB3012"			# Tolerate all beliefs
		ai_chance = { factor = 30 }
		set_country_flag = religious_revolution
		relation = { who = PAP value = -100 }
		add_country_modifier = {
			name = "religious_tolerance"
			duration = 720
		}
		stability = -1
	}
}



# Serfdom
country_event = {

	id = 3013
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = serfdom }
		NOT = { serfdom_freesubjects = 0 }
	}
	
	mean_time_to_happen = {
		months = 30
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.75
			check_variable = { which = power_of_parliament value = 6 }
			NOT = { has_country_flag = parliament_ultra_conservative }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
			NOT = { has_country_flag = parliament_ultra_conservative }
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.90
			NOT = { serfdom_freesubjects = -4 }
		}
		modifier = {
			factor = 0.95
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.85
			revolt_percentage = 0.3
		}
		modifier = {
			factor = 0.75
			revolt_percentage = 0.5
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}	
	}
	
	title = "EVTNAME3013"
	desc = "EVTDESC3013"
	
	option = {
		name = "EVTOPTA3013"			# Restrict serfdom
		ai_chance = { factor = 30 }
		set_country_flag = serfdom
		serfdom_freesubjects = 1
		add_country_modifier = {
			name = "restrict_serfdom"
			duration = 720
		}
		stability = -1
	}
	option = {
		name = "EVTOPTB3013"			# Keep the masses in chains
		ai_chance = { factor = 70 }
		set_country_flag = serfdom
		serfdom_freesubjects = -1
		random_owned = { revolutionary_rebels = 1 }
		add_country_modifier = {
			name = "enforce_serfdom"
			duration = 720
		}
		stability = -1
	}
}


# Social reform
country_event = {

	id = 3014
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = social_reform }
		NOT = { stability = 3 }
		advisor = philosopher
	}
	
	mean_time_to_happen = {
		months = 40
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.75
			check_variable = { which = power_of_parliament value = 6 }
			NOT = { has_country_flag = parliament_ultra_conservative }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
			NOT = { has_country_flag = parliament_ultra_conservative }
		}
		modifier = {
			factor = 0.75
			has_country_flag = asked_for_war_funds
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.9
			philosopher = 5
		}
		modifier = {
			factor = 0.8
			philosopher = 6
		}
		modifier = {
			factor = 1.2
			NOT = { advisor = philosopher }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
	}
	
	title = "EVTNAME3014"
	desc = "EVTDESC3014"
	
	option = {
		name = "EVTOPTA3014"			# Suppress the free thinkers
		ai_chance = { factor = 60 }
		innovative_narrowminded = 1
		set_country_flag = social_reform
		add_country_modifier = {
			name = "suppress_free_thinkers"
			duration = 720
		}
		stability = -1
	}
	option = {
		name = "EVTOPTB3014"			# They hardly pose a threat
		ai_chance = { factor = 40 }
		set_country_flag = social_reform
		innovative_narrowminded = -1
		random_owned = {
			limit = {
				is_core = THIS
				has_owner_culture = yes
				NOT = { has_building = guardia_real }
			}
			revolutionary_rebels = 3
			any_neighbor_province = {
				limit = { owned_by = THIS }
				revolutionary_rebels = 1
			}
		}
		stability = -1
	}
}


# Royal Bureaucracy
country_event = {

	id = 3015
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = royal_bureaucracy }
		centralization_decentralization = -2
		idea = bureaucracy
	}
	
	mean_time_to_happen = {
		months = 24
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.75
			check_variable = { which = power_of_parliament value = 6 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
		}
		modifier = {
			factor = 0.75
			has_country_flag = asked_for_war_funds
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { centralization_decentralization = -4 }
		}
		modifier = {
			factor = 0.9
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.2
		}		
		modifier = {
			factor = 0.7
			revolt_percentage = 0.3
		}
		modifier = {
			factor = 0.8
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.2
			stability = 3
		}
	}
	
	title = "EVTNAME3015"
	desc = "EVTDESC3015"
	
	option = {
		name = "EVTOPTA3015"			# Reduce the royal bureaucracy
		ai_chance = { factor = 65 }
		set_country_flag = royal_bureaucracy
		centralization_decentralization = 1
		add_country_modifier = {
			name = "bureaucratic_reduction"
			duration = 720
		}		
		stability = -1
	}
	option = {
		name = "EVTOPTB3015"			# Expand the royal bureaucracy
		ai_chance = { factor = 35 }
		set_country_flag = royal_bureaucracy
		add_country_modifier = {
			name = "bureaucratic_expansion"
			duration = 720
		}
		stability = -1
	}
}



# Deplorable reign
country_event = {

	id = 3016
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = deplorable_reign }
		OR = {
			NOT = { ADM = 4 }
			NOT = { MIL = 4 }
			NOT = { DIP = 4 }
		}
	}
	
	mean_time_to_happen = {
		months = 35
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.75
			check_variable = { which = power_of_parliament value = 6 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
		}
		modifier = {
			factor = 0.75
			has_country_flag = asked_for_war_funds
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.2
			stability = 3
		}
		modifier = {
			factor = 1.1
			advisor = statesman
		}
		modifier = {
			factor = 1.1
			statesman = 5
		}
		modifier = {
			factor = 1.1
			statesman = 6
		}
		modifier = {
			factor = 0.9
			NOT = { advisor = statesman }
		}
	}
	
	title = "EVTNAME3016"
	desc = "EVTDESC3016"
	
	option = {
		name = "EVTOPTA3016"			# Support
		ai_chance = { factor = 60 }
		set_country_flag = deplorable_reign
		add_country_modifier = {
			name = "support_monarch"
			duration = 720
		}
		stability = -1
	}
	option = {
		name = "EVTOPTB3016"			# Oppose
		ai_chance = { factor = 40 }
		set_country_flag = deplorable_reign
		random_owned = { revolutionary_rebels = 1 }
		add_country_modifier = {
			name = "oppose_monarch"
			duration = 720
		}
		stability = -1
	}
}



# The road to bankruptcy
country_event = {

	id = 3017
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = road_to_bankruptcy }
		OR = {
			number_of_loans = 1
			inflation = 20
			has_country_modifier = trade_crisis
			war_exhaustion = 5
		}
	}
	
	mean_time_to_happen = {
		months = 40
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 6 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
		}
		modifier = {
			factor = 0.5
			has_country_flag = asked_for_war_funds
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.9
			inflation = 40
		}
		modifier = {
			factor = 0.8
			inflation = 50
		}
		modifier = {
			factor = 0.7
			inflation = 60
		}
		modifier = {
			factor = 0.9
			number_of_loans = 2
		}
		modifier = {
			factor = 0.8
			number_of_loans = 3
		}
		modifier = {
			factor = 0.9
			NOT = { advisor = treasurer }
		}		
		modifier = {
			factor = 1.1
			advisor = treasurer
		}
		modifier = {
			factor = 1.1
			treasurer = 5
		}
		modifier = {
			factor = 1.1
			treasurer = 6
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3017"
	desc = "EVTDESC3017"
	
	immediate = { stability = -2 }
	option = {
		name = "EVTOPTA3017"			# Mint more money
		ai_chance = { factor = 30 }
		set_country_flag = road_to_bankruptcy
		treasury = 40
		add_country_modifier = {
			name = "mint_money"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3017"			# We must strengthen our currency
		ai_chance = { factor = 70 }
		set_country_flag = road_to_bankruptcy
		add_country_modifier = {
			name = "fight_inflation"
			duration = 720
		}
	}
}


# Financial Crisis
country_event = {

	id = 3018
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = financial_crisis }
		OR = {
			is_bankrupt = yes
			AND = {
				inflation = 10
				has_country_modifier = trade_crisis
			}
			war_exhaustion = 5
		}
		war = yes
	}
	
	mean_time_to_happen = {
		months = 40
		modifier = {
			factor = 0.8
			num_of_revolts = 1
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 6 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = power_of_parliament value = 3 }
		}
		modifier = {
			factor = 0.5
			has_country_flag = asked_for_war_funds
		}
		modifier = {
			factor = 0.8
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 0.95
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 0.5
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 0.9
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 0.8
			has_country_flag = usurper
		}
		modifier = {
			factor = 0.8
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 0.9
			NOT = { production_efficiency = 0.5 }
		}
		modifier = {
			factor = 0.9
			NOT = { production_efficiency = 0.4 }
		}
		modifier = {
			factor = 0.9
			NOT = { trade_efficiency = 0.5 }
		}
		modifier = {
			factor = 0.9
			NOT = { trade_efficiency = 0.4 }
		}
		modifier = {
			factor = 0.9
			NOT = { advisor = treasurer }
		}
		modifier = {
			factor = 1.1
			advisor = treasurer
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3018"
	desc = "EVTDESC3018"
	
	immediate = { stability = -2 }
	option = {
		name = "EVTOPTA3018"			# Cut back on war expenditures
		ai_chance = { factor = 70 }
		set_country_flag = financial_crisis
		add_country_modifier = {
			name = "disarmament"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3018"			# The war has its purpose
		ai_chance = { factor = 30 }
		set_country_flag = financial_crisis
		treasury = -50
		add_country_modifier = {
			name = "financial_disaster"
			duration = 720
		}
	}

}


# Stability returns
country_event = {

	id = 3019

	major = yes	
	
	trigger = {
		has_country_flag = revolution
		stability = 1
		NOT = { num_of_revolts = 1 }
		war = no
		manpower_percentage = 1
		NOT = { number_of_loans = 1 }
		NOT = { is_bankrupt = yes }
		capital_scope = {
			NOT = { has_province_modifier = conflict_with_the_estates }
			NOT = { has_province_modifier = pending_poc_resolution }
			NOT = { has_province_modifier = parliament_dissolved }
			controlled_by = THIS
			has_siege = no
		}
	}
	
	mean_time_to_happen = {
		months = 42	#84 (added several x2 mtth modifiers)
		
		modifier = {
			factor = 0.1
			revolution_target = { truce_with = THIS }
		}
		modifier = {
			factor = 0.5
			revolution_target_exists = yes
		}
		modifier = {
			factor = 0.8
			has_country_flag = national_assembly
		}
		modifier = {
			factor = 0.9
			MIL = 5
		}
		modifier = {
			factor = 0.8
			MIL = 6
		}
		modifier = {
			factor = 1.1
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.9
			ADM = 5
		}	
		modifier = {
			factor = 0.8
			ADM = 6
		}
		modifier = {
			factor = 1.1
			NOT = { ADM = 4 }
		}	
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 1.1
			has_country_flag = hereditary_emperor
		}
		modifier = {
			factor = 0.9
			AND = {
				is_emperor = yes
				EMP = { prestige = 0.9 }
			}
		}
		modifier = {
			factor = 1.1
			has_country_flag = usurper
		}
		modifier = {
			factor = 1.1
			capital_scope = { has_province_modifier = pending_poc_resolution }
		}
		modifier = {
			factor = 1.5
			capital_scope = { has_province_modifier = conflict_with_the_estates }
		}
		modifier = {
			factor = 4
			capital_scope = { has_province_modifier = parliament_dissolved }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = AE_level value = 8 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = AE_level value = 7 }
		}
		modifier = {
			factor = 0.9
			check_variable = { which = AE_level value = 6 }
		}
		modifier = {
			factor = 1.2
			NOT = { check_variable = { which = AE_level value = 6 } }
		}
		modifier = {
			factor = 1.2
			NOT = { check_variable = { which = AE_level value = 5 } }
		}
		modifier = {
			factor = 1.2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 1.2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 1.2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 1.2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {
			factor = 2
			any_owned_province = {
				looted = yes
				is_core = THIS
				is_overseas = no
			}
		}
		modifier = {
			factor = 2
			any_owned_province = {
				looted = yes
				has_owner_culture = yes
				is_core = THIS
				is_overseas = no
			}
		}
		modifier = {
			factor = 2
			any_owned_province = {
				looted = yes
				is_capital = yes
			}
		}
		modifier = {
			factor = 2
			NOT = { manpower_percentage = 1 }
		}
		modifier = {
			factor = 2
			NOT = { manpower_percentage = 0.75 }
		}
		modifier = {
			factor = 2
			NOT = { manpower_percentage = 0.5 }
		}
		modifier = {
			factor = 2
			has_country_flag = religious_revolution
		}
		modifier = {
			factor = 2
			has_country_flag = serfdom
		}
		modifier = {
			factor = 2
			has_country_flag = social_reform
		}
		modifier = {
			factor = 2
			has_country_flag = royal_bureaucracy
		}
		modifier = {
			factor = 2
			has_country_flag = deplorable_reign
		}
		modifier = {
			factor = 2
			has_country_flag = road_to_bankruptcy
		}
		modifier = {
			factor = 2
			has_country_flag = financial_crisis
		}
	}
	
	title = "EVTNAME3019"
	desc = "EVTDESC3019"
	
	option = {
		name = "EVTOPTA3019"			# Restore order
		set_country_flag = had_revolution
		clr_country_flag = revolution
		clr_country_flag = religious_revolution
		clr_country_flag = serfdom
		clr_country_flag = social_reform
		clr_country_flag = royal_bureaucracy
		clr_country_flag = deplorable_reign
		clr_country_flag = road_to_bankruptcy
		clr_country_flag = financial_crisis		
		stability = 2
		random_owned = {
			limit = {
				is_capital = yes
				check_variable = { which = social_revolution value = 20 }
			}
			change_variable = { which = social_revolution value = -8 }
		}
	}
}

# National Assembly Meets
country_event = {
	id = 3020
	major = yes
	is_triggered_only = yes
	title = "EVTNAME3020"
	desc = "EVTDESC3020"
	option = {
		name = "EVTOPTA3020"			# Restore order
		random_list = {
			50 = {	# Revolution Radicalizes
				set_country_flag = had_revolution
				clr_country_flag = revolution
				clr_country_flag = religious_revolution
				clr_country_flag = serfdom
				clr_country_flag = social_reform
				clr_country_flag = royal_bureaucracy
				clr_country_flag = deplorable_reign
				clr_country_flag = road_to_bankruptcy
				clr_country_flag = financial_crisis
				random_owned = {
					limit = {
						revolution_target_exists = no 
						owner = {
							government = monarchy
							OR = {
								NOT = { tag = FRA }
								exists = RFR
							}
							OR = {
								NOT = { tag = GER }
								exists = DDR
							}
						}
					}
					owner = { country_event = 6601 }
				}
				random_owned = {
					limit = {
						revolution_target_exists = no 
						owner = {
							government = monarchy
							tag = FRA
							NOT = { exists = RFR }
						}
					}
					owner = { country_event = 6606 }
				}
				random_owned = {
					limit = {
						revolution_target_exists = yes
						owner = {
							is_emperor = yes
							NOT = { tag = FRA }
							NOT = { tag = GER }
							NOT = { exists = DDR }
							culture_group = germanic
						}
					}
					owner = { country_event = 660101 }
				}
				random_owned = {
					limit = {
						owner = {
							government = monarchy
							tag = GER
							NOT = { exists = DDR }
						}
					}
					owner = { country_event = 660102 }
				}
				random_owned = {	# residual
					limit = {
						revolution_target_exists = yes
						owner = {
							government = monarchy
							NOT = { tag = FRA }
							NOT = { tag = GER }
							OR = {
								is_emperor = no
								exists = DDR
								NOT = { culture_group = germanic }
							}
						}
					}
					owner = {
						kill_ruler = THIS
						stability = -2
					}
				}
				random_owned = {
					limit = { owner = { government = republic } }
					owner = { stability = -2 }
				}
				any_owned = {
					limit = { is_core = THIS }
					change_variable = { which = social_revolution value = 8 }
				}
			}
			20 = {	# Things remain in limbo
				stability = 1
			}
			30 = {	# Revolution is over
				set_country_flag = had_revolution
				clr_country_flag = revolution
				clr_country_flag = religious_revolution
				clr_country_flag = serfdom
				clr_country_flag = social_reform
				clr_country_flag = royal_bureaucracy
				clr_country_flag = deplorable_reign
				clr_country_flag = road_to_bankruptcy
				clr_country_flag = financial_crisis
				stability = 2
				any_owned = {
					limit = { check_variable = { which = social_revolution value = 20 } }
					change_variable = { which = social_revolution value = -12 }
				}
				any_owned = {
					limit = { check_variable = { which = social_revolution value = 10 } }
					change_variable = { which = social_revolution value = -6 }
				}
			}
		}
		random_owned = {
			limit = { owner = { government = republic } }
			owner = { country_event = 704 }	# appoint new ruler
		}
	}
}

