#Carlos


#################################################
###	     The Fall of Constantinople       ###
#################################################
country_event = {

	id = 12001
	
	trigger = {
		tag = TUR
		owns = 151
		
		OR = {
			has_country_flag = medium_power
			has_country_flag = major_power
			has_country_flag = great_power
		}
		
		NOT = { has_country_flag = Tur_1_1 }
	}

	mean_time_to_happen = {
		days = 7
	}

	title = "EVTNAME12001"
	desc = "EVTDESC12001"

    immediate  = {
        add_core = 145  # Morea
        add_core = 146  # Athens
        add_core = 151  # Thrace
        add_core = 163  # Crete
        add_core = 164  # Naxos
        add_core = 1795 # Morea
        add_core = 1796 # Euboea
        add_core = 320  # Rhodes
        add_core = 321  # Cyprus
    }
	
	option = {		
		name = "EVTOPTA12001"
		ai_chance = { factor = 95 }
		set_country_flag = Tur_1_1

   		prestige = 0.25
		capital = 151 
		treasury = +250
		manpower = +10
		war_exhaustion = -5
		random_owned = {
			limit = { owner = { ai = no } }
			owner = { badboy = +10 }
		}
		
		any_country = { 
			limit = { 
				num_of_cities = 1
				NOT = { capital_scope = { owned_by = THIS } }
				capital_scope = { 
					OR = { 
						region = greece
						region = anatolia
						region = eastern_balkans
						owner = { tag = MAM }
					}
				}
			}
			casus_belli = THIS
			relation = { who = THIS value = -300 }
		}
		
		151 = { change_variable = { which = "converted_heathens" value = 12 } }

		any_owned = { 
				limit = { region = turkey citysize = 2000 has_owner_religion = yes }
				multiply_citysize = 0.85
		}
		
		151 = { 
			base_tax = +2 
			multiply_citysize = 2.0 
		}
		
		random_owned = { 
			limit = { 
				owner = { primary_culture = turkish } 
				151 = { NOT = { culture = turkish } }
			}
			
			151 = { culture = turkish }
			
		}
			
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		cavalry = 151
		cavalry = 151
		cavalry = 151
		cavalry = 151

		}

	option = {		
	name = "EVTOPTB12001" #	Follow a more diplomatic route
		ai_chance = { factor = 5 }
		set_country_flag = Tur_1_1
		set_country_flag = diplomatic_route
		capital = 151 
		stability = +1
		treasury = +50
		badboy = -5

		any_country = { 
			limit = { 
				num_of_cities = 1
				NOT = { capital_scope = { owned_by = THIS } }
				capital_scope = { 
					OR = { 
						region = greece
						region = anatolia
						region = eastern_balkans
						owner = { tag = MAM }
					}
				}
				NOT = { government = theocracy }
				NOT = { idea = deus_vult }
			}
	
			relation = { who = THIS value = 150 }
		}
		any_country = { 
			limit = { 
				num_of_cities = 1
				NOT = { capital_scope = { owned_by = THIS } }
				capital_scope = { 
					OR = { 
						region = greece
						region = anatolia
						region = eastern_balkans
						owner = { tag = MAM }
					}
		
				}
			
				OR = { 
					government = theocracy 
					idea = deus_vult 
				}
			}

			relation = { who = THIS value = -300 }
			casus_belli = THIS

		}

	}

}


country_event = {

	id = 12002
	
	trigger = {
		tag = TUR
		ai = yes
		
		OR = {
			war_with = HAB
			war_with = HUN
			emperor = { war_with = THIS }
		}
		
		NOT = { has_country_flag = Tur_vs_Chris }
		
		year = 1460
		
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}
		owns = 151
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME12001"
	desc = "EVTDESC12001"

	option = {		
		name = "EVTOPTA12001"
		ai_chance = { factor = 75 }
		set_country_flag = Tur_vs_Chris
		
		stability = +1
		war_exhaustion = -2

		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		cavalry = 151
		cavalry = 151
		cavalry = 151
		cavalry = 151
		infantry = 151		
		infantry = 151
		infantry = 151
		artillery = 151
		artillery = 151
		artillery = 151

		}

	option = {		
		name = "EVTOPTB12001" #	Follow a more diplomatic route
		ai_chance = { factor = 25 }


	}

}


country_event = {

	id = 12003
	
	trigger = {
		tag = TUR
		ai = yes
		NOT = { war = yes }
		
		has_country_flag = Tur_vs_Chris
		
		
	}

	mean_time_to_happen = {
		years = 5
	}

	title = "EVTNAME12001"
	desc = "EVTDESC12001"

	option = {		
		name = "EVTOPTA12001"
		ai_chance = { factor = 100 }

		clr_country_flag = Tur_vs_Chris
		
	}


}


#################################################
###	   Ottoman AI Expansion Events        ###
#################################################
# These AI only events create a very aggressive Ottoman empire
# 
province_event = {

	id = 13000

	trigger = {
		owner = { 
			tag = TUR
			NOT = { has_country_flag = Ottoman_decline }
			ai = yes
			}
		any_neighbor_province = {
    			NOT = { owned_by = THIS }
	  		NOT = { is_core = THIS }
  			owner = {
  				OR = {
  					culture_group = south_slavic
  					culture_group = ugric
  					primary_culture = greek
	  			}
  				NOT = { vassal_of = THIS }
	  			}
			}
		}

	mean_time_to_happen = {
		years = 50

		modifier = {
			factor = 2
			owner = { war = yes }
		}

		modifier = {
			factor = 2
			NOT = { owner = { stability = 0 } }
		}

		modifier = {
			factor = 0.75
			owner = {
				DIP = 8
			}
		}

		modifier = {
			factor = 0.75
			owner = {
				DIP = 7
			}
		}

		modifier = {
			factor = 0.9
			owner = { stability = 2 }
		}

		modifier = {
			factor = 0.8
			owner = { stability = 3 }
		}


	}

	title = "EVTNAME13000" #	Ottoman Expansion
	desc = "EVTDESC13000" #	Ottoman AI Expansion Event

	option = {
	name = "EVTOPTA13000" #	Our Borders
		ai_chance = { factor = 100 }
		any_neighbor_province = {
			limit = {
				NOT = { owned_by = THIS }
				NOT = { is_core = THIS }
				owner = {
					OR = {
  						culture_group = south_slavic
  						culture_group = ugric
  						primary_culture = greek
	  				}
	  				NOT = { vassal_of = THIS }
				}
			}
			add_core = THIS
			set_province_flag = ottoman_aggr_claim			
			owner = {
				casus_belli = THIS
				relation = { who = THIS value = -300 }
			}
		}

		infantry = THIS
		cavalry = THIS
		artillery = THIS

	}
}


#Remove Core#

province_event = {

	id = 13001

	trigger = {
		owner = { 
			tag = TUR
		}
		has_province_flag = ottoman_aggr_claim
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13001" #	Ottoman Expansion
	desc = "EVTDESC13001" #	Ottoman AI Remove Core Event

	option = {
	name = "EVTOPTA13001" #	Remove Core
		ai_chance = { factor = 100 }
		remove_core = THIS
		clr_province_flag = ottoman_aggr_claim
	}
}

province_event = {

	id = 13005

	trigger = {
		owner = { 
			tag = TUR
			NOT = { has_country_flag = Ottoman_decline }
			ai = yes
			war = no
		}

		361 = {
			NOT = { owned_by = THIS }
			NOT = { is_core = THIS }
			owner = {
				ai = yes
  				NOT = { vassal_of = THIS }
			}
		}
	}

	mean_time_to_happen = {
		years = 25
	}

	title = "EVTNAME13005" #	Ottoman Expansion
	desc = "EVTDESC13005" #	Ottoman AI Expansion Event

	option = {
	name = "EVTOPTA13005" #	Our Borders
		ai_chance = { factor = 100 }
		random_country = { 
			limit = { 
				capital_scope = { owned_by = THIS }
				361 = {
					NOT = { owned_by = THIS }
					NOT = { is_core = THIS }
					owner = { ai = yes }
					}
				}
			361 = { 
				add_core = THIS
				set_province_flag = ottoman_aggr_claim			
				owner = {
					casus_belli = THIS
					add_casus_belli = THIS
					relation = { who = THIS value = -30 }
					war = TUR
					}
				}
			}
		infantry = THIS
		infantry = THIS
		cavalry = THIS
		owner = {
			badboy = -3
			war_exhaustion = -5
		}

	}
}

country_event = {

	id = 13006

	trigger = {
		controls = 361
		tag = TUR
		exists = MAM
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes
		361 = {
			NOT = { owned_by = THIS }
		}
		MAM = {
			ai = yes
		}
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13006" #	Ottoman's take Mamluke Capitol
	desc = "EVTDESC13006" #	Ottoman AI Expansion Event

	option = {
	name = "EVTOPTA13006" #	Our Borders
		ai_chance = { factor = 100 }
		prestige = 0.1
		
		MAM = {
			any_owned = {
				add_core = TUR
			}
		}
		
		inherit = MAM
		add_idea = bureaucracy
	}
}

country_event = {

	id = 13163

	trigger = {

		tag = TUR
		
		OR = {
			has_country_flag = great_power
			has_country_flag = major_power
			has_country_flag = medium_power
		}
		
		war = no
		exists = MAM
		ai = yes

		MAM = {
			ai = yes
			NOT = { vassal_of = THIS }
		}
        stability = 3
        NOT = { badboy = 0.01 }
        NOT = { war_exhaustion = 1 }

	}

	mean_time_to_happen = {
		years = 50

		modifier = {
			factor = 5
			has_country_flag = medium_power
		}
		
		modifier = {
			factor = 0.25
			any_neighbor_country = {
				tag = MAM
			} 
		}

		modifier = {
			factor = 0.75
			NOT = { relation = { who = MAM value = 0 } }
		}

		modifier = {
			factor = 0.75
			NOT = { relation = { who = MAM value = -49 } }
		}

		modifier = {
			factor = 0.75
			NOT = { relation = { who = MAM value = -99 } }
		}

		modifier = {
			factor = 0.75
			NOT = { relation = { who = MAM value = -149 } }
		}

		modifier = {
			factor = 0.75
			NOT = { relation = { who = MAM value = -199 } }
		}

	}

	title = "EVTNAME13163" #	Land of Pharaohs
	desc = "EVTDESC13163" #	It is ours by right!

	option = {
	name = "EVTOPTA13163" #	It is ours by right!
		ai_chance = { factor = 100 }

		MAM = { add_casus_belli = THIS }
		war = MAM
		relation = { who = MAM value = -400 }
		manpower = +8
		war_exhaustion = -5

		infantry = 332
		infantry = 332
		infantry = 332
		infantry = 332
		infantry = 332
		cavalry = 332
		cavalry = 332
		cavalry = 332
		cavalry = 332
		artillery = 332
		artillery = 332
		artillery = 332

		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151
		infantry = 151


		MAM = {	
			set_country_flag = mam_dec
		}
	}
}



######################################################
###	   Ottoman's Vrs The Knights Part 1        ###
######################################################

#1480 Assault on Rhodes

country_event = {

	id = 13002

	trigger = {
		tag = TUR
		war = no
		exists = KNI
		KNI = {
			owns = 320
			NOT = { vassal_of = THIS }
		}
        stability = 3
        NOT = { badboy = 0.01 }
        NOT = { war_exhaustion = 1 }

	}

	mean_time_to_happen = {
		months = 240

		modifier = {
			factor = 0.5
			casus_belli = KNI
		}

		modifier = {
			factor = 0.9
			KNI = {
				land_naval = 1
			}
		}

		modifier = {
			factor = 0.9
			KNI = {
				war = yes
			}
		}
	}

	title = "EVTNAME13002"
	desc = "EVTDESC13002"

	option = {
		name = "EVTOPTA13002"			#Deal with the Knights
		ai_chance = { factor = 80 }
		KNI = {
			casus_belli = THIS
			relation = { who = THIS value = -100 }
		}
		add_casus_belli = KNI
		war = KNI
	}

	option = {					#Leave them alone
		name = "EVTOPTB13002"
		ai_chance = { factor = 20 }
	}

}

#1522 Assault on Rhodes

country_event = {

	id = 13003

	trigger = {
		tag = TUR
		ai = yes
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}
		war = no
		exists = KNI
		KNI = {
			owns = 320
			NOT = { vassal_of = THIS }
			ai = yes
		}
        stability = 3
        NOT = { badboy = 0.01 }
        NOT = { war_exhaustion = 1 }
		
	}

	mean_time_to_happen = {
		months = 240

		modifier = {
			factor = 0.5
			casus_belli = KNI
		}

		modifier = {
			factor = 0.9
			KNI = {
				land_naval = 1
			}
		}

		modifier = {
			factor = 0.9
			KNI = {
				war = yes
			}
		}

		modifier = {
			factor = 2
			has_country_flag = rhodes_dismissed
		}


	}

	title = "EVTNAME13003"
	desc = "EVTDESC13003"

	option = {
		name = "EVTOPTA13003"			#Deal with the Knights
		ai_chance = { factor = 50 }
		KNI = {
			casus_belli = THIS
			relation = { who = THIS value = -100 }
		}

		
		add_casus_belli = KNI
		war = KNI
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		infantry = 320
		artillery = 320
		artillery = 320
		artillery = 320

	}

	option = {					#Leave them alone
		name = "EVTOPTB13003"
		ai_chance = { factor = 50 }

		set_country_flag = rhodes_dismissed

	}
}

country_event = {

	id = 13004

	trigger = {
		tag = TUR
		ai = yes
		war_with = KNI

		exists = KNI
		
		KNI = {
			owns = 320
			ai = yes
		}
		
		320 = {
			has_siege = yes
			units_in_province = THIS
		}
		NOT = { any_owned_province = { controlled_by = KNI } }
	}

	mean_time_to_happen = {
		months = 36
	}

	title = "EVTNAME13002"
	desc = "EVTDESC13002"

	option = {
		name = "EVTOPTA13002"			#Deal with the Knights
		ai_chance = { factor = 100 }
		inherit = KNI
		prestige = +0.20
		stability = +2

		
	}

}

############################################################
###	      Battle of Vienna / Ottoman Decline         ###
############################################################

#Battle of Vienna - 1683#

country_event = {

	id = 13010

	trigger = {
		tag = TUR
		OR = {
			owns = 135
			owns = 154
			owns = 132
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		NOT = { owns = 134 }
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes

	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13010"
	desc = "EVTDESC13010"

	option = {
		name = "EVTOPTA13010"
		ai_chance = { factor = 95 }
		set_country_flag = Ottoman_decline
		add_core = 134
		134 = {	set_province_flag = ottoman_aggr_claim }
		PAP = { set_country_flag = Holy_League }
		MAM = { clr_country_flag = mam_dec }
	}

	option = {
		name = "EVTOPTB13010"
		ai_chance = { factor = 5 }
		set_country_flag = Ottoman_decline
		MAM = { clr_country_flag = mam_dec }
	}

}


#Mazovia#
country_event = {

	id = 13011

	trigger = {
		tag = TUR
		OR = {
			owns = 258
			owns = 259
			owns = 260
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		NOT = { owns = 257 }
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes


	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13011"
	desc = "EVTDESC13011"

	option = {
		name = "EVTOPTA13011"
		ai_chance = { factor = 95 }
		set_country_flag = Ottoman_decline
		add_core = 257
		257 = {	set_province_flag = ottoman_aggr_claim }
		PAP = { set_country_flag = Holy_League }
	}

	option = {
		name = "EVTOPTB13011"
		ai_chance = { factor = 5 }
		set_country_flag = Ottoman_decline
	}

}


#Venezia#
country_event = {

	id = 13012

	trigger = {
		tag = TUR
		OR = {
			owns = 111
			owns = 113
			owns = 108
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		NOT = { owns = 112 }
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes

	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13012"
	desc = "EVTDESC13012"

	option = {
		name = "EVTOPTA13012"
		ai_chance = { factor = 95 }
		set_country_flag = Ottoman_decline
		add_core = 112
		112 = {	set_province_flag = ottoman_aggr_claim }
		PAP = { set_country_flag = Holy_League }
	}

	option = {
		name = "EVTOPTB13012"
		ai_chance = { factor = 5 }
		set_country_flag = Ottoman_decline
	}

}


#Roma#
country_event = {

	id = 13013

	trigger = {
		tag = TUR
		OR = {
			owns = 117
			owns = 119
			owns = 120
			owns = 121
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		NOT = { owns = 118 }
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes

	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13013"
	desc = "EVTDESC13013"

	option = {
		name = "EVTOPTA13013"
		ai_chance = { factor = 95 }
		set_country_flag = Ottoman_decline
		add_core = 118
		118 = {	set_province_flag = ottoman_aggr_claim }
		PAP = { set_country_flag = Holy_League }
	}

	option = {
		name = "EVTOPTB13013"
		ai_chance = { factor = 5 }
		set_country_flag = Ottoman_decline
	}

}


country_event = {    # First line of defence

	id = 13019

	trigger = {
		tag = TUR
		OR = {
			has_country_flag = medium_power
			has_country_flag = small_state
			has_country_flag = minor_holding
		}


		has_country_flag = Ottoman_decline
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13019" #	Second Life
	desc = "EVTDESC13019" #	Dummy

	option = {
	name = "EVTOPTA13019" #	We shall be great!
		ai_chance = { factor = 100 }
		clr_country_flag = Ottoman_decline
	}
}


country_event = {     

	id = 13020

	trigger = {
		tag = TUR


		OR = {
			owns = 462
			owns = 463
			owns = 425
		}


		
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}


		ai = yes
		NOT = { has_country_flag = Ottoman_decline }		

	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13020" #	Cooling down
	desc = "EVTDESC13020" #	Dummy

	option = {
	name = "EVTOPTA13020" #	Time to think
		ai_chance = { factor = 100 }
		set_country_flag = Ottoman_decline
	}
}



country_event = {     

	id = 9013021

	trigger = {
		tag = TUR


		OR = {
			owns = 384
			owns = 393
			owns = 408
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}


		ai = yes
		NOT = { has_country_flag = Ottoman_decline }		

	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME9013021" #	Cooling down
	desc = "EVTDESC9013021" #	Dummy

	option = {
	name = "EVTOPTA9013021" #	Time to think
		ai_chance = { factor = 100 }
		set_country_flag = Ottoman_decline
	}
}


country_event = {     

	id = 9013022

	trigger = {
		tag = TUR


		OR = {
			owns = 412
			owns = 428
			owns = 426
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		ai = yes
		NOT = { has_country_flag = Ottoman_decline }		

	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME9013022" #	Cooling down
	desc = "EVTDESC9013022" #	Dummy

	option = {
	name = "EVTOPTA9013022" #	Time to think
		ai_chance = { factor = 100 }
		set_country_flag = Ottoman_decline
	}
}

#Volhynia#
country_event = {

	id = 13021

	trigger = {
		tag = TUR
		OR = {
			controls = 260
			controls = 261
			controls = 281
			controls = 277
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		NOT = { controls = 279 }
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13021" #	A Step Against Volhynia
	desc = "EVTDESC13013"

	option = {
	name = "EVTOPTA13021" #	A step too far?
		ai_chance = { factor = 95 }
		set_country_flag = Ottoman_decline
		add_core = 279
		279 = {	set_province_flag = ottoman_aggr_claim }
		PAP = { set_country_flag = Holy_League }
	}

	option = {
	name = "EVTOPTB13021" #	A step too far?
		ai_chance = { factor = 5 }
		set_country_flag = Ottoman_decline
	}

}


#Zaporoznhie#
country_event = {

	id = 13022

	trigger = {
		tag = TUR
		OR = {
			owns = 282
			owns = 281
			owns = 290
		}

		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
		}

		NOT = { owns = 283 }
		NOT = { has_country_flag = Ottoman_decline }
		ai = yes
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME13022" #	A Step Against Zaporoznhie
	desc = "EVTDESC13013"

	option = {
	name = "EVTOPTA13022" #	A step too far?
		ai_chance = { factor = 95 }
		set_country_flag = Ottoman_decline
		add_core = 283
		283 = {	set_province_flag = ottoman_aggr_claim }
		PAP = { set_country_flag = Holy_League }
	}

	option = {
	name = "EVTOPTB13022" #	A step too far?
		ai_chance = { factor = 5 }
		set_country_flag = Ottoman_decline
	}

}


#################################################
###      OTTOMAN POLITICAL		      ###
#################################################


country_event = {

	id = 674576

	trigger = {
		tag = TUR
		year = 1455
		ai = yes
		stability = 0
		prestige = 0
		
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
			has_country_flag = diplomatic_route
		}

		exists = TRE
		TRE = { 
			ai = yes 
			war = no
			NOT = { has_country_flag = major_power }
			NOT = { has_country_flag = great_power }
			
			OR = {
				overlord = {
					tag = TUR
				}
				
				NOT = { is_subject = yes }
				
			}
			
		}
	}

	mean_time_to_happen = {
		years = 6
		
		modifier = {
			factor = 0.75
			stability = 1
		}
		
		modifier = {
			factor = 0.75
			stability = 2
		}
		
		modifier = {
			factor = 0.75
			stability = 3
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.25
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.5
		}

		modifier = {
			factor = 0.75
			prestige = 0.75
		}
		
	}

	title = "EVTNAME674576" #	Make us Great!
	desc = "EVTDESC674576" #	Dummy

	option = {
	name = "EVTOPTA674576" #	Gladly!
		ai_chance = { factor = 100 }

		inherit = TRE
	}


}



country_event = {

	id = 674577

	trigger = {
		tag = TUR
		year = 1455
		ai = yes
		stability = 0
		prestige = 0
		
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
			has_country_flag = diplomatic_route
		}

		exists = KAR
		KAR = { 
			ai = yes
			war = no
			NOT = { has_country_flag = major_power }
			NOT = { has_country_flag = great_power }
			
			OR = {
				overlord = {
					tag = TUR
				}
				
				NOT = { is_subject = yes }
				
			}
		}
	}

	mean_time_to_happen = {
		years = 6
		
		modifier = {
			factor = 0.75
			stability = 1
		}
		
		modifier = {
			factor = 0.75
			stability = 2
		}
		
		modifier = {
			factor = 0.75
			stability = 3
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.25
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.5
		}

		modifier = {
			factor = 0.75
			prestige = 0.75
		}
		
	}

	title = "EVTNAME674577" #	Make us Great!
	desc = "EVTDESC674577" #	Dummy

	option = {
	name = "EVTOPTA674577" #	Gladly!
		ai_chance = { factor = 100 }

		inherit = KAR
	}


}


country_event = {

	id = 674578

	trigger = {
		tag = TUR
		year = 1455
		ai = yes
		stability = 0
		prestige = 0
		
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
			has_country_flag = diplomatic_route
		}

		exists = CND
		CND = { 
			ai = yes 
			war = no
			NOT = { has_country_flag = major_power }
			NOT = { has_country_flag = great_power }
			
			OR = {
				overlord = {
					tag = TUR
				}
				
				NOT = { is_subject = yes }
				
			}
		}
	}

	mean_time_to_happen = {
		years = 6
		
		modifier = {
			factor = 0.75
			stability = 1
		}
		
		modifier = {
			factor = 0.75
			stability = 2
		}
		
		modifier = {
			factor = 0.75
			stability = 3
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.25
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.5
		}

		modifier = {
			factor = 0.75
			prestige = 0.75
		}
		
	}

	title = "EVTNAME674578" #	Make us Great!
	desc = "EVTDESC674578" #	Dummy

	option = {
	name = "EVTOPTA674578" #	Gladly!
		ai_chance = { factor = 100 }

		inherit = CND
	}


}



country_event = {

	id = 674579

	trigger = {
		tag = TUR
		year = 1455
		ai = yes
		stability = 0
		prestige = 0
		
		OR = {
			has_country_flag = major_power
			has_country_flag = great_power
			has_country_flag = diplomatic_route
		}

		exists = DUL
		DUL = { 
			ai = yes
			war = no
			NOT = { has_country_flag = major_power }
			NOT = { has_country_flag = great_power }
			
			OR = {
				overlord = {
					tag = TUR
				}
				
				NOT = { is_subject = yes }
				
			}
		}
	}

	mean_time_to_happen = {
		years = 6
		
		modifier = {
			factor = 0.75
			stability = 1
		}
		
		modifier = {
			factor = 0.75
			stability = 2
		}
		
		modifier = {
			factor = 0.75
			stability = 3
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.25
		}
		
		modifier = {
			factor = 0.75
			prestige = 0.5
		}

		modifier = {
			factor = 0.75
			prestige = 0.75
		}
		
	}

	title = "EVTNAME674579" #	Make us Great!
	desc = "EVTDESC674579" #	Dummy

	option = {
	name = "EVTOPTA674579" #	Gladly!
		ai_chance = { factor = 100 }

		inherit = DUL
	}


}

