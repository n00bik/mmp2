# Aspiration for liberty
#08/05/05 FB corrections
country_event = {

   id = 1061

   trigger = {
      NOT = { has_country_flag = liberalism }
      NOT = { serfdom_freesubjects = -3 }   #FB added NOT (serfdom makes event MORE likely)
      innovative_narrowminded = 3
      NOT = { aristocracy_plutocracy = -2 }   #FB added NOT (aristocracy makes event MORE likely)
      year = 1700
   }

   mean_time_to_happen = {

      months = 180

      modifier = {
         factor = 0.7
         OR = {
			government = monarchy
			government = noble_republic-upper
			government = noble_republic
			government = noble_republic-lower
			government = noble_republic-lowest
			government = bureaucratic_despotism-upper
			government = bureaucratic_despotism
			government = bureaucratic_despotism-lower
			government = bureaucratic_despotism-lowest
			government = republican_dictatorship-upper
			government = republican_dictatorship
			government = republican_dictatorship-lower
			government = republican_dictatorship-lowest
			}
		}
      modifier = {
         factor = 0.8
         is_bankrupt = yes
      }
      modifier = {
         factor = 0.9
         idea = bureaucracy
      }
      modifier = {
         factor = 0.9
         war = yes
      }
      modifier = {
         factor = 0.9
         NOT = { serfdom_freesubjects = -4 }
      }
      modifier = {
         factor = 0.9
         innovative_narrowminded = 4
      }
      modifier = {
         factor = 0.9
         innovative_narrowminded = 5
      }
      modifier = {
         factor = 0.9
         NOT = { stability = 0 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -1 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -2 }
      }
      modifier = {
         factor = 1.1
         stability = 1
      }
      modifier = {
         factor = 1.1
         stability = 2
      }
      modifier = {
         factor = 1.1
         stability = 3
      }
   }

   title = "EVTNAME1061"
   desc = "EVTDESC1061"

   option = {
      name = "EVTOPTA1061"
      set_country_flag = liberalism
      stability = -3
      treasury = -60
   }
}


# Governmental incompetence
country_event = {

   id = 1062

   trigger = {
      has_country_flag = liberalism
      badboy = 0.2
      NOT = { ADM = 5 }
   }

   mean_time_to_happen = {

      months = 12

      modifier = {
         factor = 0.9
         revolt_percentage = 0.05
      }
      modifier = {
         factor = 0.9
         revolt_percentage = 0.15
      }
      modifier = {
         factor = 0.9
         revolt_percentage = 0.25
      }
      modifier = {
         factor = 0.9
         NOT = { ADM = 4 }
      }
      modifier = {
         factor = 0.9
         NOT = { ADM = 2 }
      }
      modifier = {
         factor = 0.9
         badboy = 0.4
      }
      modifier = {
         factor = 0.7
         OR = {
			government = monarchy
			government = noble_republic-upper
			government = noble_republic
			government = noble_republic-lower
			government = noble_republic-lowest
			government = bureaucratic_despotism-upper
			government = bureaucratic_despotism
			government = bureaucratic_despotism-lower
			government = bureaucratic_despotism-lowest
			government = republican_dictatorship-upper
			government = republican_dictatorship
			government = republican_dictatorship-lower
			government = republican_dictatorship-lowest
         }   #FB missing OR added
      }
      modifier = {
         factor = 1.1
         statesman = 5
      }
      modifier = {
         factor = 1.1
         statesman = 6
      }
      modifier = {
         factor = 0.9
         NOT = { stability = 0 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -1 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -2 }
      }
      modifier = {
         factor = 1.1
         stability = 1
      }
      modifier = {
         factor = 1.1
         stability = 2
      }
      modifier = {
         factor = 1.1
         stability = 3
      }
   }

   title = "EVTNAME1062"
   desc = "EVTDESC1062"

   option = {
      name = "EVTOPTA1062"         # There is nothing to be concerned about.
      ai_chance = { factor = 40 }
      badboy = 2
      random_owned = {
         base_tax = -1
         modernist_rebels = 2
      }
   }
   option = {
      name = "EVTOPTB1062"         # Prevent them from advancing further.
      ai_chance = { factor = 60 }
      treasury = -150
      add_country_modifier = {
         name = "disorder"
         duration = 100
      }
   }
}


# Battles on foreign ground
country_event = {

   id = 1063

   trigger = {
      has_country_flag = liberalism
      war = yes
      NOT = { treasury = 50 }
      wartax = yes
      num_of_allies = 1
   }

   mean_time_to_happen = {

      months = 24

      modifier = {
         factor = 0.9
         NOT = { production_efficiency = 0.6 }
      }
      modifier = {
         factor = 0.9
         NOT = { production_efficiency = 0.5 }
      }
      modifier = {
         factor = 0.8
         war_exhaustion = 5
      }
      modifier = {
         factor = 0.8
         war_exhaustion = 8
      }
      modifier = {
         factor = 0.9
         NOT = { stability = 0 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -1 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -2 }
      }
      modifier = {
         factor = 1.1
         stability = 1
      }
      modifier = {
         factor = 1.1
         stability = 2
      }
      modifier = {
         factor = 1.1
         stability = 3
      }
      modifier = {
         factor = 1.2
         advisor = treasurer
      }
   }

   title = "EVTNAME1063"
   desc = "EVTDESC1063"

   option = {
      name = "EVTOPTA1063"         # The treasury isn't even close to empty.
      ai_chance = { factor = 65 }
      treasury = -50
      ally = { relation = { who = THIS value = 50 } }
      add_country_modifier = {
         name = "decreased_morale"
         duration = 100
      }
   }
   option = {
      name = "EVTOPTB1063"         # Cut down on war expenditures.
      ai_chance = { factor = 35 }
      diplomats = -1
      spies = -1
      ally = {
         treasury = -50
         relation = { who = THIS value = -80 }
      }
      add_country_modifier = {
         name = "disarmament"
         duration = 100
      }
   }
}


# Order is restored
country_event = {

   id = 1064

   trigger = {
      has_country_flag = liberalism
      NOT = { num_of_revolts = 1 }
      stability = 1
   }

   mean_time_to_happen = {

      months = 48

      modifier = {
         factor = 0.9
         advisor = statesman
      }
      modifier = {
         factor = 0.9
         ADM = 7
      }
      modifier = {
         factor = 0.9
         ADM = 8
      }
      modifier = {
         factor = 0.9
         ADM = 9
      }
      modifier = {
         factor = 0.9
         MIL = 9
      }
      modifier = {
         factor = 0.9
         MIL = 8
      }
      modifier = {
         factor = 0.9
         NOT = { stability = 0 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -1 }
      }
      modifier = {
         factor = 0.9
         NOT = { stability = -2 }
      }
      modifier = {
         factor = 1.1
         stability = 1
      }
      modifier = {
         factor = 1.1
         stability = 2
      }
      modifier = {
         factor = 1.1
         stability = 3
      }
   }

   title = "EVTNAME1064"
   desc = "EVTDESC1064"

   option = {
      name = "EVTOPTA1064"
      innovative_narrowminded = -2
      stability = 2
      random_owned = {
         base_tax = 1
      }
      clr_country_flag = liberalism
   }
}
