province_event = {

	id = 3051
	
	trigger = {
		owner = {
			NOT = { has_country_flag = rebel_negotiations }
			NOT = { accepted_culture = THIS }
			NOT = { primary_culture = THIS }
			NOT = { culture_group = THIS }
			NOT = { religion = THIS }
		}
		NOT = { has_province_modifier = autonomy_A }
		NOT = { has_province_modifier = autonomy_B }
		NOT = { has_province_modifier = autonomy_C }
		NOT = { has_province_modifier = self_rule }
	}
	
	mean_time_to_happen = {
		months = 420

		modifier = {
			factor = 0.5 
			NOT = { is_core = THIS }
		}

		modifier = {
			factor = 0.95 
			owner = { NOT = { ADM = 4 } }
		}					
		modifier = {
			factor = 0.95 
		 	owner = { NOT = { MIL = 4 } }
		}		
		modifier = {
			factor = 1.05 
			owner = { ADM = 4 }
		}			
		modifier = {
			factor = 1.05
		 	owner = { MIL = 4 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.1 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.3 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.5 }
		}
		modifier = {
			factor = 10
			owner = {
				has_country_flag = rebellion
			}
		}
		modifier = {
			factor = 1.6
			owner = {
				num_of_cities = 15
			}
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -1 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -2 } }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 1 }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 2 }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 3 }
		}
	}
	
	title = "EVTNAME3051"
	desc = "EVTDESC3051"
	
	option = {
		name = "EVTOPTA3051"
		ai_chance = { factor = 75 }
		patriot_rebels = 2
		add_province_modifier = {
			name = "suppress_minority"
			duration = 150
		}
		owner = {
			set_country_flag = rebellion
		}
	}
	option = {
		name = "EVTOPTB3051"
		ai_chance = { factor = 25 }
		owner = {
			add_country_modifier = {
				name = "rebel_negotiation"
					duration = 150
			}
			set_country_flag = rebel_negotiations	
			treasury = -30
		}
	}
}


province_event = {

	id = 3052
	
	trigger = {
		owner = {
			NOT = { has_country_flag = rebel_negotiations } 
			NOT = { accepted_culture = THIS }
			NOT = { primary_culture = THIS }
			NOT = { culture_group = THIS }
		}
		NOT = { has_province_modifier = autonomy_A }
		NOT = { has_province_modifier = autonomy_B }
		NOT = { has_province_modifier = autonomy_C }
		NOT = { has_province_modifier = self_rule }
	}
	
	mean_time_to_happen = {
		months = 420

		modifier = {
			factor = 0.5 
			NOT = { is_core = THIS }
		}
		modifier = {
			factor = 0.95 
			owner = { NOT = { ADM = 4 } }
		}				
		modifier = {
			factor = 0.95 
			owner = { NOT = { MIL = 4 } }
		}		
		modifier = {
			factor = 1.05 
			owner = { ADM = 4 }
		}				
		modifier = {
			factor = 1.05
			owner = { MIL = 4 }
		}		
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.1 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.3 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.5 }
		}
		modifier = {
			factor = 10
			owner = {
				has_country_flag = rebellion
			}
		}
		modifier = {
			factor = 1.6
			owner = {
				num_of_cities = 15
			}
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -1 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -2 } }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 1 }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 2 }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 3 }
		}
	}
	
	title = "EVTNAME3051"
	desc = "EVTDESC3051"
	
	option = {
		name = "EVTOPTA3051"
		ai_chance = { factor = 75 }
		patriot_rebels = 2
		add_province_modifier = {
			name = "suppress_minority"
			duration = 180
		}
		owner = {
			set_country_flag = rebellion
		}
	}
	option = {
		name = "EVTOPTB3051"
		ai_chance = { factor = 25 }
		owner = {
			add_country_modifier = {
				name = "rebel_negotiation"
				duration = 180
			}
			set_country_flag = rebel_negotiations
			treasury = -30
		}
	}
}

