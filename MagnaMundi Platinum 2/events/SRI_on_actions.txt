# mft left

country_event = {

	id = 112986
	
	is_triggered_only = yes
	
	title = "EVTNAME112986"
	desc = "EVTDESC112986"
	
	option = {
		name = "OK1"
		ai_chance = { factor = 100 }
		random_owned = {
			limit = { owner = { mercantilism_freetrade = 0 } }
			random_list = {
				33 = { owner = { random_center_of_trade = { limit = { placed_merchants = 1 } remove_merchant = THIS } } }
				33 = { owner = { create_advisor = alderman } }
				33 = { owner = { stability = 1 } }
			}
		}
		random_owned = {
			limit = { owner = { NOT = { mercantilism_freetrade = 0 } } }
			random_list = {
				33 = { owner = { random_center_of_trade = { limit = { placed_merchants = 1 } remove_merchant = THIS } } }
				33 = { owner = { production_tech = 200 } }
				33 = { owner = { stability = 1 } }
			}
		}
	}
}

# mft right

country_event = {

	id = 112987
	
	is_triggered_only = yes
	
	title = "EVTNAME112987"
	desc = "EVTDESC112987"
	
	option = {
		name = "OK1"
		ai_chance = { factor = 100 }
		random_owned = {
			limit = {
				is_overseas = no
				owner = { NOT = { mercantilism_freetrade = -3 } }
			}
			random_list = {
				33 = { owner = { random_center_of_trade = { limit = { has_discovered = THIS NOT = { placed_merchants = 5 } } send_merchant = THIS } } }
				33 = { noble_rebels = 1 }
				33 = { owner = { trade_tech = 0.1 } }
			}
		}
		random_owned = {
			limit = {
				is_overseas = no
				owner = { mercantilism_freetrade = -3 NOT = { mercantilism_freetrade = 1 } }
			}
			random_list = {
				33 = { owner = { random_center_of_trade = { limit = { has_discovered = THIS NOT = { placed_merchants = 5 } } send_merchant = THIS } } }
				33 = { noble_rebels = 1 }
				33 = { owner = { stability = -1 } }
			}
		}
		random_owned = {
			limit = { owner = { mercantilism_freetrade = 1 NOT = { mercantilism_freetrade = 4 } } }
			random_list = {
				33 = { owner = { merchants = 5 } }
				33 = { owner = { production_tech = -0.20 } }
				33 = { owner = { stability = -2 } }
			}
		}
		random_owned = {
			limit = {
				is_overseas = no
				owner = { mercantilism_freetrade = 4 }
			}
			random_list = {
				33 = { revolutionary_rebels = 2 }
				33 = { owner = { production_tech = -0.20 } }
				33 = { owner = { stability = -3 } }
			}
		}
	}
}

# siege won

country_event = {

	id = 112988
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { tag = REB }
		OR = {
			any_known_country = { # reichsverweser
				war_with = THIS
				has_country_flag = reichsverweser
				is_emperor = yes
				any_owned_province = {
					controlled_by = THIS
					is_capital = yes
				}
			}
			any_known_country = { # takenmid
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					has_province_flag = takenmid
				}
			}
			any_known_country = { # non_core_base
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					has_province_flag = non_core_base
				}
			}
			any_known_country = { # resettlement
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					has_province_flag = resettlement
				}
			}
			any_known_country = { # DG conversion system
				has_global_flag = dei_gratia
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					OR = {
						check_variable = { which = "converted_heathens" value = 1 }
						check_variable = { which = "converted_heretics" value = 1 }
					}
				}
			}
			AND = {		# Imperial Crusade
				TUR = { NOT = { has_country_flag = waiting_for_IMA } }
				IMA = { has_country_flag = imperial_crusade_go }
				OR = { # country criteria
					has_country_flag = crusader_emperor	# in case someone else took over
					tag = IMA
					ally = { has_country_flag = crusader_emperor }
					overlord = { has_country_flag = crusader_emperor }
					tag = TUR
					alliance_with = TUR
					vassal_of = TUR
				}
				OR = {
					AND = { # christian camp
						war_with = TUR
						OR = {
							has_country_flag = crusader_emperor
							tag = IMA
							ally = { has_country_flag = crusader_emperor }
							overlord = { has_country_flag = crusader_emperor }
						}
						any_known_country = {
							war_with = THIS
							OR = {
								tag = TUR
								alliance_with = TUR
								vassal_of = TUR
							}
							any_owned_province = {
								OR = {
									controlled_by = THIS
									AND = {
										THIS = { tag = IMA }
										controller = { is_emperor = yes }
									}
								}
								NOT = { has_province_flag = captured }
							}
						}
					}
					AND = { # muslim camp
						OR = {
							tag = TUR
							alliance_with = TUR
							vassal_of = TUR
						}
						any_known_country = {
							war_with = THIS
							OR = {
								has_country_flag = crusader_emperor
								tag = IMA
								ally = { has_country_flag = crusader_emperor }
								overlord = { has_country_flag = crusader_emperor }
							}
							any_owned_province = {
								OR = {
									controlled_by = THIS
									AND = {
										THIS = { vassal_of = TUR }
										controlled_by = TUR
									}
								}
								NOT = { has_province_flag = captured }
							}
						}
					}
				}
			}
			AND = {		# HRE_reformation_war
				has_global_flag = HRE_war_active
				has_global_flag = HRE_reformation_war
				religion_group = christian
				any_known_country = {
					war_with = THIS
					OR = {
						AND = {
							religion = catholic
							THIS = {
								OR = {
									NOT = { religion = catholic }
									has_country_flag = HRE_ally_of_protestants
								}
							}
						}
						AND = {
							NOT = { religion = catholic }
							THIS = {
								OR = {
									religion = catholic
									has_country_flag = HRE_ally_of_catholics
								}
							}
						}
					}
					any_owned_province = {
						controlled_by = THIS
						NOT = { has_province_flag = captured }
						OR = {
							hre = yes
							owner = { has_country_flag = HRE_ally_of_protestants }
							owner = { has_country_flag = HRE_ally_of_catholics }
						}
					}
				}
			}
			AND = {		# OPM release
				THIS = { NOT = { tag = REB } }
				NOT = { has_global_flag = HRE_reformation_war }
				NOT = { is_emperor = yes }
				NOT = { emperor = { alliance_with = THIS } }
				any_known_country = {
					war_with = THIS
					ai = yes
					NOT = { num_of_cities = 2 }
					capital_scope = {
						hre = yes
						controlled_by = THIS
						NOT = { has_province_flag = captured }
					}
					NOT = { tag = EMP }
					NOT = { tag = IMA }
					NOT = { tag = RLD }
				}
			}
		}
	}
	
	title = "EVTNAME112988"
	desc = "EVTDESC112988"
	
	immediate = {
# wotlop (must identify province in question)
		random_country = {
			limit = {
				has_global_flag = HRE_war_active
				has_global_flag = HRE_reformation_war
				NOT = { has_global_flag = 30YW_in_progress }
				war_with = THIS
				OR = {
					AND = {
						religion = catholic
						THIS = {
							OR = {
								NOT = { religion = catholic }
								has_country_flag = HRE_ally_of_protestants
							}
						}
					}
					AND = {
						NOT = { religion = catholic }
						THIS = {
							OR = {
								religion = catholic
								has_country_flag = HRE_ally_of_catholics
							}
						}
					}
				}
				any_owned_province = {
					controlled_by = THIS
					NOT = { has_province_flag = captured }
					OR = {
						hre = yes
						owner = { has_country_flag = HRE_ally_of_protestants }
						owner = { has_country_flag = HRE_ally_of_catholics }
					}
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					NOT = { has_province_flag = captured }
					OR = {
						hre = yes
						owner = { has_country_flag = HRE_ally_of_protestants }
						owner = { has_country_flag = HRE_ally_of_catholics }
					}
				}
				set_province_flag = captured
				set_province_flag = province_is_target
				change_variable = { which = social_revolution value = 1 }
				owner = { set_country_flag = province_is_target }
			}
		}
	}
	option = {
		name = "GOOD1"
		ai_chance = { factor = 100 }
		army_tradition = 0.005
# takenmid
		random_country = {
			limit = {
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					has_province_flag = takenmid
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					has_province_flag = takenmid
				}
				clr_province_flag = takenmid
				set_province_flag = reset_takenmid
			}
		}
# core flags
		random_country = {
			limit = {
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					has_province_flag = non_core_base
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					has_province_flag = non_core_base
				}
				clr_province_flag = non_core_base
			}
		}
# reichsverweser
		random_country = {
			limit = {
				war_with = THIS
				has_country_flag = reichsverweser
				is_emperor = yes
				any_owned_province = {
					controlled_by = THIS
					is_capital = yes
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					is_capital = yes
				}
				province_event = 112967	# change back control
			}
		}
# resettlement
		random_country = {
			limit = {
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					has_province_flag = resettlement
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					has_province_flag = resettlement
				}
				province_event = 284002
			}
		}
# DG conversion system
		random_country = {
			limit = {
				has_global_flag = dei_gratia
				war_with = THIS
				any_owned_province = {
					controlled_by = THIS
					OR = {
						check_variable = { which = "converted_heathens" value = 1 }
						check_variable = { which = "converted_heretics" value = 1 }
					}
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					OR = {
						check_variable = { which = "converted_heathens" value = 1 }
						check_variable = { which = "converted_heretics" value = 1 }
					}
				}
				province_event = 591025
			}
		}
# 30YW_in_progress
		random_country = {
			limit = {
				has_global_flag = HRE_war_active
				has_global_flag = 30YW_in_progress
				NOT = { has_global_flag = peace_conference }
				NOT = { has_global_flag = peace_conference_called }
				war_with = THIS
				religion = catholic
				THIS = {
					OR = {
						NOT = { religion = catholic }
						has_country_flag = HRE_ally_of_protestants
					}
				}
				any_owned_province = {
					controlled_by = THIS
					NOT = { has_province_flag = captured }
					hre = yes
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					NOT = { has_province_flag = captured }
					hre = yes
				}
				set_province_flag = captured
				EMP = { change_variable = { which = 30YW_outcome value = -1 } }
			}
			random_owned = {
				limit = { owner = { has_country_flag = HRE_catholic_leader } }
				EMP = { change_variable = { which = 30YW_outcome value = -1 } }
			}
		}
		random_country = {
			limit = {
				has_global_flag = HRE_war_active
				has_global_flag = 30YW_in_progress
				NOT = { has_global_flag = peace_conference }
				NOT = { has_global_flag = peace_conference_called }
				war_with = THIS
				NOT = { religion = catholic }
				THIS = {
					OR = {
						religion = catholic
						has_country_flag = HRE_ally_of_catholics
					}
				}
				any_owned_province = {
					controlled_by = THIS
					NOT = { has_province_flag = captured }
					hre = yes
				}
			}
			random_owned = {
				limit = {
					controlled_by = THIS
					NOT = { has_province_flag = captured }
					hre = yes
				}
				set_province_flag = captured
				EMP = { change_variable = { which = 30YW_outcome value = 1 } }
			}
			random_owned = {
				limit = { owner = { has_country_flag = HRE_protestant_leader } }
				EMP = { change_variable = { which = 30YW_outcome value = 1 } }
			}
		}
# Imperial Crusade
		random_country = {	# count pro christian camp
			limit = {
				TUR = { NOT = { has_country_flag = waiting_for_IMA } }
				IMA = { has_country_flag = imperial_crusade_go }
				any_owned_province = { controlled_by = THIS NOT = { has_province_flag = captured } }
				war_with = THIS
				OR = {
					tag = TUR
					alliance_with = TUR
					vassal_of = TUR
				}
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { tag = IMA }
							controller = { is_emperor = yes }
						}
					}
					NOT = { has_province_flag = captured }
					has_building = guardia_real
					owner = { tag = TUR }
				}
				IMA = { change_variable = { which = crusade_outcome value = 10 } }
				THIS = { prestige = 0.1 }
				EMP = { prestige = 0.1 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { tag = IMA }
							controller = { is_emperor = yes }
						}
					}
					NOT = { has_province_flag = captured }
					has_building = guardia_real
				}
				IMA = { change_variable = { which = crusade_outcome value = 2 } }
				EMP = { prestige = 0.02 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { tag = IMA }
							controller = { is_emperor = yes }
						}
					}
					NOT = { has_province_flag = captured }
					region = eastern_balkans
				}
				IMA = { change_variable = { which = crusade_outcome value = 2 } }
				EMP = { prestige = 0.02 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { tag = IMA }
							controller = { is_emperor = yes }
						}
					}
					NOT = { has_province_flag = captured }
					has_province_flag = imperial_crusade_objective
				}
				IMA = { change_variable = { which = crusade_outcome value = 2 } }
				EMP = { prestige = 0.02 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { tag = IMA }
							controller = { is_emperor = yes }
						}
					}
					NOT = { has_province_flag = captured }
				}
				set_province_flag = captured
				IMA = { change_variable = { which = crusade_outcome value = 1 } }
				EMP = { prestige = 0.01 }
			}
		}
		random_country = {	# count pro muslim camp
			limit = {
				TUR = { NOT = { has_country_flag = waiting_for_IMA } }
				IMA = { has_country_flag = imperial_crusade_go }
				any_owned_province = { controlled_by = THIS NOT = { has_province_flag = captured } }
				war_with = THIS
				OR = {
					has_country_flag = crusader_emperor
					tag = IMA
					ally = { has_country_flag = crusader_emperor }
					overlord = { has_country_flag = crusader_emperor }
				}
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { vassal_of = TUR }
							controlled_by = TUR
						}
					}
					NOT = { has_province_flag = captured }
					has_building = guardia_real
					owner = { has_country_flag = crusader_emperor }
				}
				IMA = { change_variable = { which = crusade_outcome value = -10 } }
				TUR = { prestige = 0.1 }
				EMP = { prestige = -0.1 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { vassal_of = TUR }
							controlled_by = TUR
						}
					}
					NOT = { has_province_flag = captured }
					has_building = guardia_real
					owner = { tag = IMA }
				}
				IMA = { change_variable = { which = crusade_outcome value = -2 } }
				TUR = { prestige = 0.05 }
				EMP = { prestige = -0.02 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { vassal_of = TUR }
							controlled_by = TUR
						}
					}
					NOT = { has_province_flag = captured }
					has_building = guardia_real
				}
				IMA = { change_variable = { which = crusade_outcome value = -2 } }
				EMP = { prestige = -0.02 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { vassal_of = TUR }
							controlled_by = TUR
						}
					}
					NOT = { has_province_flag = captured }
					region = hre_region
				}
				IMA = { change_variable = { which = crusade_outcome value = -2 } }
				EMP = { prestige = -0.02 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { vassal_of = TUR }
							controlled_by = TUR
						}
					}
					NOT = { has_province_flag = captured }
					has_province_flag = imperial_crusade_objective
				}
				IMA = { change_variable = { which = crusade_outcome value = -1 } }
				EMP = { prestige = -0.01 }
			}
			random_owned = {
				limit = {
					OR = {
						controlled_by = THIS
						AND = {
							THIS = { vassal_of = TUR }
							controlled_by = TUR
						}
					}
					NOT = { has_province_flag = captured }
				}
				set_province_flag = captured
				IMA = { change_variable = { which = crusade_outcome value = -1 } }
				EMP = { prestige = -0.01 }
			}
		}
# OPM release
		random_country = {
			limit = {
				war_with = THIS
				ai = yes
				NOT = { num_of_cities = 2 }
				capital_scope = {
					controlled_by = THIS
					hre = yes
					NOT = { has_province_flag = captured }
				}
				THIS = { is_emperor = no }
				NOT = { emperor = { alliance_with = THIS } }
				NOT = { has_global_flag = HRE_reformation_war }
				NOT = { tag = EMP }
				NOT = { tag = IMA }
				NOT = { tag = RLD }
			}
			relation = { who = THIS value = -100 }
			prestige = 0.05
			capital_scope = {
				set_province_flag = captured
				set_province_flag = release_this_country
			}
		}
# wotlop
		random_country = {
			limit = {
				has_country_flag = province_is_target
				any_owned_province = {
					has_province_flag = province_is_target
					culture_group = germanic
					controller = { NOT = { capital_scope = { hre = yes } } }
				}
			}
			random_owned = {
				limit = {
					has_province_flag = province_is_target
				}
				change_variable = { which = social_revolution value = 3 }
			}
		}
		random_country = {
			limit = {
				has_country_flag = province_is_target
				religion = catholic
			}
			EMP = { change_variable = { which = league_war_result value = -1 } }
		}
		random_country = {
			limit = {
				has_country_flag = province_is_target
				NOT = { religion = catholic }
			}
			EMP = { change_variable = { which = league_war_result value = 1 } }
		}
		random_country = {
			limit = {
				has_country_flag = province_is_target
				religion = catholic
				any_owned_province = {
					has_province_flag = province_is_target
					is_capital = yes
				}
			}
			EMP = { change_variable = { which = league_war_result value = -1 } }
		}
		random_country = {
			limit = {
				has_country_flag = province_is_target
				NOT = { religion = catholic }
				any_owned_province = {
					has_province_flag = province_is_target
					is_capital = yes
				}
			}
			EMP = { change_variable = { which = league_war_result value = 1 } }
		}
		random_country = {
			limit = {
				has_country_flag = province_is_target
				has_country_flag = HRE_catholic_leader
				any_owned_province = {
					has_province_flag = province_is_target
					is_capital = yes
				}
			}
			EMP = { change_variable = { which = league_war_result value = -2 } }
		}
		random_country = {
			limit = {
				has_country_flag = province_is_target
				has_country_flag = HRE_protestant_leader
				any_owned_province = {
					has_province_flag = province_is_target
					is_capital = yes
				}
			}
			EMP = { change_variable = { which = league_war_result value = 2 } }
		}
# clean-up
		any_country = {
			limit = { has_country_flag = province_is_target }
			clr_country_flag = province_is_target
			any_owned = {
				limit = { has_province_flag = province_is_target }
				clr_province_flag = province_is_target
			}
		}
	}
}

# battle won

country_event = {

	id = 112989
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { tag = REB }
		OR = {
			has_country_flag = pbcslavelords
			AND = {		# Imperial Crusade
				TUR = { NOT = { has_country_flag = waiting_for_IMA } }
				IMA = { has_country_flag = imperial_crusade_go }
				OR = { # country criteria
					has_country_flag = crusader_emperor	# in case someone else took over
					tag = IMA
					ally = { has_country_flag = crusader_emperor }
					overlord = { has_country_flag = crusader_emperor }
					tag = TUR
					alliance_with = TUR
					vassal_of = TUR
				}
				OR = {
					AND = { # christian camp
						war_with = TUR
						OR = {
							has_country_flag = crusader_emperor
							tag = IMA
							ally = { has_country_flag = crusader_emperor }
							overlord = { has_country_flag = crusader_emperor }
						}
						any_known_country = {
							war_with = THIS
							OR = {
								tag = TUR
								alliance_with = TUR
								vassal_of = TUR
							}
						}
					}
					AND = { # muslim camp
						OR = {
							tag = TUR
							alliance_with = TUR
							vassal_of = TUR
						}
						any_known_country = {
							war_with = THIS
							OR = {
								has_country_flag = crusader_emperor
								tag = IMA
								ally = { has_country_flag = crusader_emperor }
								overlord = { has_country_flag = crusader_emperor }
							}
						}
					}
				}
				# add qualifications for what battles should count
				OR = {
					any_owned_province = {
						infantry_in_province = 12
						units_in_province = this
						units_in_province = 2
						local_enemy = { war_with = THIS NOT = { tag = REB } }
						controlled_by = THIS
					}
					any_known_country = {
						OR = {
							tag = TUR
							tag = IMA
							vassal_of = TUR
							alliance_with = TUR
							has_country_flag = crusader_emperor
							ally = { has_country_flag = crusader_emperor }
							overlord = { has_country_flag = crusader_emperor }
							AND = { is_emperor = yes war_with = TUR }
						}
						any_owned_province = {
							infantry_in_province = 12
							units_in_province = this
							units_in_province = 2
							local_enemy = { war_with = THIS NOT = { tag = REB } }
							NOT = { controlled_by = THIS }
						}
					}
				}
			}
			AND = {		# religious war in the Empire
				has_global_flag = HRE_war_active
				has_global_flag = HRE_reformation_war
				religion_group = christian
				any_known_country = {
					war_with = THIS
					religion_group = christian
					capital_scope = { hre = yes }
					OR = {
						AND = {
							religion = catholic
							THIS = {
								OR = {
									NOT = { religion = catholic }
									has_country_flag = HRE_ally_of_protestants
								}
							}
						}
						AND = {
							NOT = { religion = catholic }
							THIS = {
								OR = {
									religion = catholic
									has_country_flag = HRE_ally_of_catholics
								}
							}
						}
					}
				}
				# add qualifications for what battles should count
				hre_region = {
					hre = yes
					infantry_in_province = 12
					units_in_province = this
					units_in_province = 2
					local_enemy = { war_with = THIS NOT = { tag = REB } }
					OR = {
						NOT = { controlled_by = THIS }
						owned_by = THIS
					}
				}
			}
		}
	}
	
	title = "EVTNAME112989"
	desc = "EVTDESC112989"
	
	option = {
		name = "GOOD1"
		ai_chance = { factor = 100 }
		army_tradition = 0.005
		random_owned = {
			limit = { owner = { has_country_flag = pbcslavelords } }
			owner = { country_event = 510045 }
		}
		random_owned = {	# Crusade / christian win
			limit = {
				TUR = { NOT = { has_country_flag = waiting_for_IMA } }
				IMA = { has_country_flag = imperial_crusade_go }
				owner = {
					war_with = TUR
					NOT = { war_with = IMA }
				}
				TUR = { war_with = IMA }
				emperor = {
					war_with = TUR
					NOT = { war_with = THIS }
				}
			}
			IMA = { change_variable = { which = crusade_outcome value = 1 } }
			EMP = { prestige = 0.01 }
		}
		random_owned = {	# Crusade / Muslim win
			limit = {
				TUR = { NOT = { has_country_flag = waiting_for_IMA } }
				IMA = { has_country_flag = imperial_crusade_go }
				owner = {
					OR = {
						tag = TUR
						alliance_with = TUR
						vassal_of = TUR
					}
				}
				OR = {
					IMA = { war_with = THIS }
					emperor = { war_with = THIS }
				}
			}
			IMA = { change_variable = { which = crusade_outcome value = -1 } }
			EMP = { prestige = -0.01 }
		}
		random_owned = {	# WOTLOP
			limit = {
				has_global_flag = HRE_reformation_war
				NOT = { has_global_flag = 30YW_in_progress }
				owner = {
					NOT = { has_country_flag = pbcslavelords }
					NOT = { has_country_flag = HRE_ally_of_catholics }
					OR = {
						NOT = { religion = catholic }
						has_country_flag = HRE_ally_of_protestants
					}
				}
			}
			EMP = { change_variable = { which = league_war_result value = -1 } }
		}
		random_owned = {
			limit = {
				has_global_flag = HRE_war_active
				has_global_flag = HRE_reformation_war
				NOT = { has_global_flag = 30YW_in_progress }
				owner = {
					NOT = { has_country_flag = pbcslavelords }
					NOT = { has_country_flag = HRE_ally_of_protestants }
					OR = {
						religion = catholic
						has_country_flag = HRE_ally_of_catholics
					}
				}
			}
			EMP = { change_variable = { which = league_war_result value = 1 } }
		}
		random_owned = {	# 30YW
			limit = {
				has_global_flag = HRE_war_active
				has_global_flag = 30YW_in_progress
				owner = {
					NOT = { has_country_flag = pbcslavelords }
					NOT = { has_country_flag = HRE_ally_of_catholics }
					OR = {
						NOT = { religion = catholic }
						has_country_flag = HRE_ally_of_protestants
					}
				}
			}
			EMP = { change_variable = { which = 30YW_outcome value = -1 } }
		}
		random_owned = {
			limit = {
				has_global_flag = HRE_war_active
				has_global_flag = 30YW_in_progress
				owner = {
					NOT = { has_country_flag = pbcslavelords }
					NOT = { has_country_flag = HRE_ally_of_protestants }
					OR = {
						religion = catholic
						has_country_flag = HRE_ally_of_catholics
					}
				}
			}
			EMP = { change_variable = { which = 30YW_outcome value = 1 } }
		}
	}
}
