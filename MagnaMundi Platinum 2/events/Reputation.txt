#Carlos




##################################################

country_event = {

	id = 500001

	trigger = {
		NOT = { ai = yes }
		badboy = 0.25
		NOT = { has_country_flag = badboy1 }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME500001" #	A Friendly Warning
	desc = "EVTDESC500001"

	option = {
		name = "EVTOPTA500001"
		ai_chance = { factor = 100 }
		set_country_flag = badboy1
	}
}

country_event = {

	id = 500002

	trigger = {
		NOT = { ai = yes }
		badboy = 0.50
		has_country_flag = badboy1
		NOT = { has_country_flag = badboy2 }
	}

	mean_time_to_happen = {
		months = 9
	}

	title = "EVTNAME500002"
	desc = "EVTDESC500002"

	option = {
		name = "EVTOPTA500002"
		ai_chance = { factor = 100 }
		set_country_flag = badboy2
	}
}

country_event = {

	id = 500003

	trigger = {
		NOT = { ai = yes }
		badboy = 0.75
		has_country_flag = badboy2
		NOT = { has_country_flag = badboy3 }
	}

	mean_time_to_happen = {
		months = 12
	}

	title = "EVTNAME500003"
	desc = "EVTDESC500003"

	option = {
		name = "EVTOPTA500003"
		ai_chance = { factor = 100 }
		set_country_flag = badboy3
	}
}

country_event = {

	id = 500004

	trigger = {
		NOT = { ai = yes }
		badboy = 0.45
		NOT = { badboy = 0.65 }
		has_country_flag = badboy3
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME500004"
	desc = "EVTDESC500004"

	option = {
		name = "EVTOPTA500004"
		ai_chance = { factor = 100 }
		clr_country_flag = badboy3
		clr_country_flag = go_to_hell
		clr_country_flag = framed
		clr_country_flag = surrender_delayed1
		clr_country_flag = surrender_delayed2
		clr_country_flag = surrender_delayed3
	}
}

country_event = {

	id = 500005

	trigger = {
		NOT = { ai = yes }
		badboy = 0.25
		NOT = { badboy = 0.40 }
		has_country_flag = badboy2
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME500005"
	desc = "EVTDESC500005"

	option = {
		name = "EVTOPTA500005"
		ai_chance = { factor = 100 }
		clr_country_flag = badboy2
		clr_country_flag = badboy3
		clr_country_flag = go_to_hell
		clr_country_flag = framed
		clr_country_flag = surrender_delayed1
		clr_country_flag = surrender_delayed2
		clr_country_flag = surrender_delayed3
	}
}

country_event = {

	id = 500006

	trigger = {
		NOT = { ai = yes }
		NOT = { badboy = 0.25 }
		has_country_flag = badboy1
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME500006"
	desc = "EVTDESC500006"

	option = {
		name = "EVTOPTA500006"
		ai_chance = { factor = 100 }
		clr_country_flag = badboy1
		clr_country_flag = badboy2
		clr_country_flag = badboy3
		clr_country_flag = go_to_hell
		clr_country_flag = framed
		clr_country_flag = surrender_delayed1
		clr_country_flag = surrender_delayed2
		clr_country_flag = surrender_delayed3
	}
}

country_event = {

	id = 500007

	trigger = {
		NOT = { ai = yes }
		NOT = { badboy = 1.0 }
		NOT = { has_country_flag = framed }
		NOT = { has_country_flag = framed_again }
		NOT = { technology_group = new_world }
		NOT = { technology_group = america }
        OR = {
            AND = {
                has_global_flag = feudal_japan
                NOT = {
                    culture_group = japanese
                    OR = {
                        has_country_flag = state
                        has_country_flag = principality
                    }
                }
            }
            NOT = { has_global_flag = feudal_japan }
        }
	}

	mean_time_to_happen = {
		years = 100

		modifier = {
			factor = 10
			OR = {
				has_country_modifier = second_opportunity_small
				has_country_modifier = second_opportunity_medium
				has_country_modifier = second_opportunity_big
			}
		}	
		modifier = {
			factor = 1.25
			DIP = 7
		}	
		modifier = {
			factor = 1.5
			idea = vetting
		}
		
		modifier = {
			factor = 1.1
			idea = espionage
		}		
		
		modifier = {
			factor = 0.6
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 2	#518
			NOT = { badboy = 0.01 }
		}
		modifier = {
			factor = 1.5	#259
			NOT = { badboy = 0.10 }
		}
		modifier = {
			factor = 1.2	#173
			NOT = { badboy = 0.15 }
		}
		modifier = {
			factor = 1.2	#144
			NOT = { badboy = 0.20 }
		}
		modifier = {
			factor = 1.2	#120
			NOT = { badboy = 0.25 }
		}
		modifier = {
			factor = 0.9	#90
			badboy = 0.30
		}
		modifier = {
			factor = 0.9	#81
			badboy = 0.35
		}
		modifier = {
			factor = 0.9	#72.9
			badboy = 0.40
		}
		modifier = {
			factor = 0.9	#65.61
			badboy = 0.45
		}
		modifier = {
			factor = 0.8	# 52.48
			badboy = 0.50
		}
		modifier = {
			factor = 0.8	# 41.9
			badboy = 0.55
		}
		modifier = {
			factor = 0.7	# 29.3
			badboy = 0.60
		}
		modifier = {
			factor = 0.7	# 20.5
			badboy = 0.65
		}
		modifier = {
			factor = 0.7	# 14.4
			badboy = 0.70
		}
		modifier = {
			factor = 0.7	# 10.08
			badboy = 0.75
		}
	}

	title = "EVTNAME500007"
	desc = "EVTDESC500007"

	option = {
	name = "EVTOPTA500007" #	Finance an investigation
		ai_chance = { factor = 0 }
		badboy = +2
		treasury = -40
		set_country_flag = poirot
	}

	option = {
	name = "EVTOPTB500007" #	Pay compensation to the family
		ai_chance = { factor = 0 }
		treasury = -250
		badboy = +3
		set_country_flag = colorofmoney
	}

	option = {
		name = "EVTOPTC500007"
		ai_chance = { factor = 100 }
		badboy = +10
		set_country_flag = framed
		set_country_flag = framed_again
	}
}

country_event = {

	id = 500008

	trigger = {
		NOT = { ai = yes }
		badboy = 0.60
		idea = espionage
	}

	mean_time_to_happen = {
		months = 72
	}

	title = "EVTNAME500008"
	desc = "EVTDESC500008"

	option = {
		name = "EVTOPTA500008"
		ai_chance = { factor = 100 }
	}
}

country_event = {

	id = 500009

	trigger = {
		has_country_flag = player_controlled
		war = no
		badboy = 1.01
		NOT = { badboy = 1.25 }
	}

	mean_time_to_happen = {
		months = 1

		modifier = {
			factor = 24
			has_country_flag = surrender_delayed1
		}

	}

	title = "EVTNAME500009"
	desc = "EVTDESC500009"

	option = {
		name = "EVTOPTA500009"
		ai_chance = { factor = 90 }
		badboy = -22
		years_of_income = -1.00
		war_exhaustion = -5
		add_country_modifier = {
			name = "Baddyboy"
			duration = 5475
		}
		
		prestige = -1.25		
		
		release_vassal = random
		release_vassal = random
		release_vassal = random
		
		clr_country_flag = framed
		clr_country_flag = go_to_hell
		clr_country_flag = surrender_delayed1
		clr_country_flag = surrender_delayed2
		clr_country_flag = surrender_delayed3
	}

	option = {
		name = "EVTOPTB500009"
		ai_chance = { factor = 10 }

		set_country_flag = surrender_delayed1

	}
}


country_event = {

	id = 500010

	trigger = {
		has_country_flag = player_controlled
		war = no
		badboy = 1.26
		NOT = { badboy = 1.80 }
	}

	mean_time_to_happen = {
		months = 1

		modifier = {
			factor = 24
			has_country_flag = surrender_delayed2
		}

	}

	title = "EVTNAME500010"
	desc = "EVTDESC500010"

	option = {
		name = "EVTOPTA500010"
		ai_chance = { factor = 90 }
		badboy = -34
		years_of_income = -2.00
		war_exhaustion = -5
		add_country_modifier = {
			name = "Worseboy"
			duration = 7300
		}
		
		prestige = -1.50		
		
		release_vassal = random
		release_vassal = random
		release_vassal = random
		release_vassal = random
		
		clr_country_flag = framed
		clr_country_flag = go_to_hell
		clr_country_flag = surrender_delayed1
		clr_country_flag = surrender_delayed2
		clr_country_flag = surrender_delayed3
	}

	option = {
		name = "EVTOPTB500010"
		ai_chance = { factor = 10 }

		set_country_flag = surrender_delayed2

		
	}
}

country_event = {

	id = 500011

	trigger = {
		has_country_flag = player_controlled
		war = no
		badboy = 1.81
		NOT = { badboy = 2.99 }
	}

	mean_time_to_happen = {
		months = 1

		modifier = {
			factor = 24
			has_country_flag = surrender_delayed3
		}

	}

	title = "EVTNAME500011"
	desc = "EVTDESC500011"

	option = {
		name = "EVTOPTA500011"
		ai_chance = { factor = 90 }
		badboy = -45
		years_of_income = -3.00
		war_exhaustion = -5
		prestige = -2.00
		add_country_modifier = {
			name = "Evil_Incarnate"
			duration = 10950
		}
		
		release_vassal = random
		release_vassal = random
		release_vassal = random
		release_vassal = random
		release_vassal = random
		
		clr_country_flag = framed
		clr_country_flag = go_to_hell
		clr_country_flag = surrender_delayed1
		clr_country_flag = surrender_delayed2
		clr_country_flag = surrender_delayed3
	}

	option = {
		name = "EVTOPTB500011"
		ai_chance = { factor = 10 }

		set_country_flag = surrender_delayed3

	}
}



country_event = {

	id = 500012

	trigger = {
		NOT = { ai = yes }
		badboy = 3
		NOT = { has_country_flag = go_to_hell }
	}

	mean_time_to_happen = {
		months = 18
	}

	title = "EVTNAME500012"
	desc = "EVTDESC500012"

	option = {
		name = "EVTOPTA500012"
		ai_chance = { factor = 100 }
		badboy = +25
		set_country_flag = go_to_hell
	}
}

country_event = {

	id = 500013

	trigger = {
		NOT = { ai = yes }
		NOT = { has_country_flag = framed }
		has_country_flag = framed_again
	}

	mean_time_to_happen = {
		years = 25

		modifier = {				
			factor = 1.5
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}
		
		modifier = {				
			factor = 1.2
			NOT = { badboy = 0.45 }
		}
		modifier = {				
			factor = 1.5
			NOT = { badboy = 0.35 }
		}
		modifier = {				
			factor = 0.5
			badboy = 0.7
		}
		modifier = {				
			factor = 0.5
			badboy = 0.75
		}
	}

	title = "EVTNAME500013" #	Dangerous Schemes
	desc = "EVTDESC500013" #	Sir, other courts have finally been persuaded we were framed earlier. This has lent us some foreign sympathy. However, the simple passage of time or another such incident could lead others to start considering the same heinous act against us...

	option = {
	name = "EVTOPTA500013" #	We must strive for a good reputation
		ai_chance = { factor = 100 }
		clr_country_flag = framed_again
	}
}

country_event = {

	id = 500014

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500014" #	A Useful Donation
	desc = "EVTDESC500014" #	Sir, several of our neighbours are organizing an effort to alleviate the suffering of the worst victims of the latest plague outburst. Are you willing to contribute?

	option = {
	name = "EVTOPTA500014" #	Help victims of the plague
		ai_chance = { factor = 100 }		
		badboy = -2
		treasury = -100
	}

	option = {
	name = "EVTOPTB500014" #	There is nothing anyone can do for them now
		ai_chance = { factor = 0 }
	
	}
}

country_event = {

	id = 500015

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500015" #	Help to our Brethren
	desc = "EVTDESC500015" #	Sir, a sizeable number of nobles from other countries have lost family members and a significant sum of money fighting the enemies of our faith. A great donation is being organized to help alleviate their lot. Are you willing to contribute?

	option = {
	name = "EVTOPTA500015" #	Support their crusade
		ai_chance = { factor = 100 }		
		badboy = -1
		treasury = -65
	}

	option = {
	name = "EVTOPTB500015" #	A holy war is not in our best interests
		ai_chance = { factor = 0 }	
	}
}

country_event = {

	id = 500016

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		NOT = { has_country_modifier = gest_great }
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500016" #	A Gesture of Greatness
	desc = "EVTDESC500016" #	Sir, to improve our bad reputation we can inform other courts that we are going to reduce our military readiness and stand down...what do you think?

	option = {
	name = "EVTOPTA500016" #	Reduce our military unilaterally
		ai_chance = { factor = 100 }		
		badboy = -1
		manpower = -5
		add_country_modifier = {
			name = "gest_great"
			duration = 3650
		}
	}

	option = {
	name = "EVTOPTB500016" #	Our strength is in our army
		ai_chance = { factor = 0 }	
	}
}

country_event = {

	id = 500017

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		NOT = { has_country_modifier = gest_great2 }
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500017" #	Wolf in Sheep's Clothes
	desc = "EVTDESC500017" #	Sir, to reduce our bad reputation we can inform other courts that we are going to reduce our military readiness and stand down...what do you think?

	option = {
	name = "EVTOPTA500017" #	Reduce our military unilaterally
		ai_chance = { factor = 100 }		
		badboy = -2
		manpower = -15
		add_country_modifier = {
			name = "gest_great2"
			duration = 3650
		}
	}

	option = {
	name = "EVTOPTB500017" #	Our strength is in our army
		ai_chance = { factor = 0 }
	
	}
}

country_event = {

	id = 500018

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		NOT = { has_country_modifier = gest_great3 }
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500018" #	Improving our Diplomatic Standing
	desc = "EVTDESC500018" #	Sir, our reputation has been cause of concern for some time. We can give proof of our good intentions by significantly cutting our research capability...

	option = {
	name = "EVTOPTA500018" #	Maintaining the status quo is vital
		ai_chance = { factor = 100 }		
		badboy = -2
		add_country_modifier = {
			name = "gest_great3"
			duration = 3650
		}
	}

	option = {
	name = "EVTOPTB500018" #	We must progress at all costs
		ai_chance = { factor = 0 }	
	}
}

country_event = {

	id = 500019

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500019" #	A Good Deed
	desc = "EVTDESC500019" #	Sir, we can accept some refugees fleeing from the plague within our borders. The risk of plague spreading to us is not that high, but the internal politics are another matter...

	option = {
	name = "EVTOPTA500019" #	Provide refuge to these poor souls
		ai_chance = { factor = 100 }		
		badboy = -1
		stability = -2
		prestige = +0.01
	}

	option = {
	name = "EVTOPTB500019" #	Close our doors to these plague-carriers
		ai_chance = { factor = 0 }	
	}
}

country_event = {

	id = 500020

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {				
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {				
			factor = 0.75
			DIP = 8
		}
		modifier = {				
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {				
			factor = 0.75
			badboy = 0.85
		}
	}

	title = "EVTNAME500020" #	A Charming Tourney
	desc = "EVTDESC500020" #	Sir, given our reputation, perhaps we could host a great tourney like in the days of old, inviting the most important royal houses to impress them with our hospitality...

	option = {
	name = "EVTOPTA500020" #	Host a tourney in $CAPITAL$
		ai_chance = { factor = 100 }		
		badboy = -3
		treasury = -350
		prestige = +0.02
	}

	option = {
	name = "EVTOPTB500020" #	Such days are long past
		ai_chance = { factor = 0 }	
	}
}

country_event = {

	id = 500021

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {
			factor = 0.75
			DIP = 8
		}
		modifier = {
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {
			factor = 0.75
			badboy = 0.85
		}
	}

	title = "EVTNAME500021" #	The Exotic Zoo
	desc = "EVTDESC500021" #	Sir, given our reputation, perhaps we could purchase a number of exotic animals from colonial traders, organize a show, invite other royal houses and give away the animals to them as a token of goodwill...
	option = {
	name = "EVTOPTA500021" #	Host a royal zoo in $CAPITAL$
		ai_chance = { factor = 100 }
		badboy = -1
		treasury = -125
		prestige = +0.01
	}

	option = {
	name = "EVTOPTB500021" #	Our money can best be spent elsewhere
		ai_chance = { factor = 0 }
	}
}

country_event = {

	id = 500022

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		NOT = { has_country_modifier = gest_great4 }
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {
			factor = 0.75
			DIP = 8
		}
		modifier = {
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500022" #	Unilateral Disarmament
	desc = "EVTDESC500022" #	Sir, our reputation has been a cause for concern for some time. We can give proof of our good intentions by reducing the readiness of our army...

	option = {
	name = "EVTOPTA500022" #	Tell the army to stand down
		ai_chance = { factor = 100 }
		badboy = -2
		manpower = -25
		add_country_modifier = {
			name = "gest_great4"
			duration = 2555
		}
	}

	option = {
	name = "EVTOPTB500022" #	We dare not relax for an instant
		ai_chance = { factor = 0 }
	}
}

country_event = {

	id = 500023

	trigger = {
		NOT = { ai = yes }
		badboy = 0.40
		NOT = { badboy = 1 }
		DIP = 5
		NOT = { has_country_modifier = gest_great5 }
		war = no
	}

	mean_time_to_happen = {
		years = 100

		modifier = {
			factor = 1.5
			NOT = { DIP = 6 }
		}
		modifier = {
			factor = 0.75
			DIP = 8
		}
		modifier = {
			factor = 1.25
			NOT = { badboy = 0.51 }
		}
		modifier = {
			factor = 0.75
			badboy = 0.84
		}
	}

	title = "EVTNAME500023" #	Warm Smile, Icy Heart
	desc = "EVTDESC500023" #	Sir, to mend our reputation, we can help other countries who are suffering serious problems by sending them free goods and gold to...'alleviate their suffering'. Other courts will certainly start looking at us in a better light...

	option = {
	name = "EVTOPTA500023" #	Send relief at once!
		ai_chance = { factor = 100 }
		badboy = -1
		treasury = -60
		add_country_modifier = {
			name = "gest_great5"
			duration = 1825
		}
	}

	option = {
	name = "EVTOPTB500023" #	Our money is better spent elsewhere
		ai_chance = { factor = 0 }
	}
}

###################################################################
#
#              Answers to Framed! events
#
##############################################################

country_event = {

	id = 500024

	trigger = {
		NOT = { ai = yes }
		has_country_flag = poirot
	}

	mean_time_to_happen = {
		days = 240

		modifier = {
			factor = 0.5
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.6
			NOT = { ADM = 5 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.7
			NOT = { stability = -2 }
		}
	}

	immediate = {
		clr_country_flag = poirot
	}

	title = "EVTNAME500024" #	Investigation Failure
	desc = "EVTDESC500024" #	The investigation you ordered lead nowhere. It was poorly organized and horribly executed. We are now generally thought to have ordered the murder and of trying to deceive our neighbours. This is truly a disgrace!

	option = {
	name = "EVTOPTA500024" #	****!
		ai_chance = { factor = 100 }
		badboy = +10
		prestige = -0.25
		set_country_flag = framed
		set_country_flag = framed_again
		clr_country_flag = poirot
	}
}

country_event = {

	id = 500025

	trigger = {
		NOT = { ai = yes }
		has_country_flag = poirot
	}

	mean_time_to_happen = {
		days = 120
	}

	immediate = {
		clr_country_flag = poirot
	}

	title = "EVTNAME500025" #	Inconclusive Investigation
	desc = "EVTDESC500025" #	Some evidence was gathered that points to our innocence, but many pieces of the puzzle are missing. Our position stands at the mercy of other courts judgements.

	option = {
	name = "EVTOPTA500025" #	Someone will have to pay for this...
		ai_chance = { factor = 100 }
		badboy = +7
		set_country_flag = framed
		set_country_flag = framed_again
		clr_country_flag = poirot
	}
}

country_event = {

	id = 500026

	trigger = {
		NOT = { ai = yes }
		has_country_flag = poirot
	}

	mean_time_to_happen = {
		days = 360

		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 0.7
			ADM = 8
		}
		modifier = {
			factor = 0.8
			stability = 2
		}
		modifier = {
			factor = 0.7
			stability = 3
		}
		
		modifier = {				
			factor = 0.75
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}
	}

	immediate = {
		clr_country_flag = poirot
		set_country_flag = innocent_victim
	}

	title = "EVTNAME500026" #	A Sucessful Investigation!
	desc = "EVTDESC500026" #	The investigation you launched was a complete sucess! We cleared our name and even gained evidence about who ordered this nefarious deed!

	option = {
		name = "EVTOPTA500026" #	At last!
		ai_chance = { factor = 100 }
		prestige = +0.10
		badboy = -2
		random_country = {
			limit = { 
				any_known_country = { has_country_flag = innocent_victim }
				NOT = { war_with = THIS }

				OR = { 
					NOT = { is_subject = yes }
					overlord = { capital_scope = { owned_by = THIS } }
				}

				OR = { #Motive

					AND = { 
						any_known_country = { has_country_flag = innocent_victim capital_scope = { hre = yes } }
						elector = yes
					}

					truce_with = THIS
					is_rival = THIS
					trade_embargo_by = THIS
					vassal_of = THIS
					junior_union_with = THIS
					any_owned_province = { is_core = THIS }
					NOT = { relation = { who = THIS value = -100 } }
					badboy = 0.6
					neighbour = THIS
				}

				OR = { #Opportunity

					junior_union_with = THIS
					marriage_with = THIS
					casus_belli = THIS
					idea = espionage
					culture_group = THIS
					neighbour = THIS
				}
			}

			add_casus_belli = THIS
			relation = { who = THIS value = -100 }
			prestige = -0.15
		}

		clr_country_flag = innocent_victim
		clr_country_flag = poirot
	}
}

country_event = {

	id = 500027

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney
	}

	mean_time_to_happen = {
		days = 280

		modifier = {
			factor = 0.5
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.6
			NOT = { DIP = 5 }
		}
		modifier = {
			factor = 0.8
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 0.5
			NOT = { prestige = -0.25 }
		}
	}

	immediate = {
		clr_country_flag = colorofmoney
	}


	title = "EVTNAME500027" #	Offensive Compensation
	desc = "EVTDESC500027"

	option = {
	name = "EVTOPTA500027" #	Ungrateful Bastards!
		ai_chance = { factor = 100 }
		badboy = +6
		prestige = -0.25
		set_country_flag = framed
		set_country_flag = framed_again
		clr_country_flag = colorofmoney
	}
}


country_event = {

	id = 500028

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney
	}

	mean_time_to_happen = {
		days = 100

	}

	immediate = {
		clr_country_flag = colorofmoney
	}

	title = "EVTNAME500028" #	Ineffective Compensation
	desc = "EVTDESC500028"

	option = {
	name = "EVTOPTA500028" #	Pay the gold!
		ai_chance = { factor = 100 }
		prestige = -0.02
		badboy = +1
		treasury = -100
		set_country_flag = framed
		set_country_flag = framed_again
		clr_country_flag = colorofmoney
		set_country_flag = colorofmoney1
	}

	option = {
	name = "EVTOPTB500028" #	We have already paid more than he is worth
		ai_chance = { factor = 0 }
		prestige = -0.1
		badboy = +5
		set_country_flag = framed
		set_country_flag = framed_again
		clr_country_flag = colorofmoney
	}
}


country_event = {

	id = 500029

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney
	}

	mean_time_to_happen = {
		days = 250

		modifier = {
			factor = 0.8
			DIP = 7
		}
		modifier = {
			factor = 0.7
			DIP = 8
		}
		modifier = {
			factor = 0.8
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 0.5
			NOT = { prestige = -0.25 }
		}
		modifier = {
			factor = 0.8
			NOT = { prestige = 0.25 }
		}
		modifier = {
			factor = 0.5
			NOT = { prestige = 0.50 }
		}
		
		modifier = {				
			factor = 0.75
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}
	}

	immediate = {
		clr_country_flag = colorofmoney
	}

	title = "EVTNAME500029" #	Sucessful Compensation!
	desc = "EVTDESC500029"

	option = {
	name = "EVTOPTA500029" #	Everybody has his price!
		ai_chance = { factor = 100 }
		badboy = -2
		clr_country_flag = colorofmoney
	}
}

country_event = {

	id = 500030

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney1
	}

	mean_time_to_happen = {
		months = 4
	}

	immediate = {
		clr_country_flag = colorofmoney1
	}

	title = "EVTNAME500030" #	A Princely Sum
	desc = "EVTDESC500030"

	option = {
	name = "EVTOPTA500030" #	Pay the gold
		ai_chance = { factor = 100 }
		prestige = -0.01
		badboy = +1
		treasury = -50
		set_country_flag = colorofmoney1
		
	}

	option = {
	name = "EVTOPTB500030" #	This is extortion!
		ai_chance = { factor = 0 }
		prestige = -0.1
		badboy = +5
		clr_country_flag = colorofmoney1
	}
}

country_event = {

	id = 500031

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney1
	}

	mean_time_to_happen = {
		months = 4
	}

	immediate = {
		clr_country_flag = colorofmoney1
	}

	title = "EVTNAME500031" #	A Princely Sum
	desc = "EVTDESC500030"

	option = {
	name = "EVTOPTA500031" #	Pay the gold
		ai_chance = { factor = 100 }
		prestige = -0.01
		badboy = +1
		treasury = -100
		set_country_flag = colorofmoney1
	}

	option = {
	name = "EVTOPTB500031" #	This is extortion!
		ai_chance = { factor = 0 }
		prestige = -0.1
		badboy = +5
		clr_country_flag = colorofmoney1
	}
}

country_event = {

	id = 500032

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney1
	}

	immediate = {
		clr_country_flag = colorofmoney1
	}

	mean_time_to_happen = {
		months = 4
	}

	title = "EVTNAME500032" #	A Princely Sum
	desc = "EVTDESC500030"

	option = {
	name = "EVTOPTA500032" #	Pay the gold
		ai_chance = { factor = 100 }
		prestige = -0.01
		badboy = +1
		treasury = -150
		set_country_flag = colorofmoney1
	}

	option = {
	name = "EVTOPTB500032" #	This is extortion!
		ai_chance = { factor = 0 }
		prestige = -0.1
		badboy = +5
		clr_country_flag = colorofmoney1
	}
}

country_event = {

	id = 500033

	trigger = {
		NOT = { ai = yes }
		has_country_flag = colorofmoney1
	}

	mean_time_to_happen = {
		months = 4

		modifier = {
			factor = 0.8
			DIP = 6
		}
		modifier = {
			factor = 0.8
			idea = cabinet
		}
		
		modifier = {				
			factor = 0.75
			
			OR = {
				advisor = spymaster
				idea = espionage
				idea = vetting
			}
		}
	}

	immediate = {
		clr_country_flag = colorofmoney1
	}

	title = "EVTNAME500033" #	A Costly Ending...
	desc = "EVTDESC500031"

	option = {
	name = "EVTOPTA500033" #	It cost us a fortune!
		ai_chance = { factor = 100 }
		clr_country_flag = colorofmoney1
	}
}


