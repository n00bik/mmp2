# DEI GRATIA (DAVID)

country_event = { #Swiss Mercenaries

id = 55701

trigger = {
	knows_country = SWI
	OR = { 
		AND = { religion = catholic SWI = { religion = catholic } }
		AND = { religion = protestant SWI = { religion = protestant } }
		AND = { religion = reformed SWI = { religion = reformed } }
		}
  	SWI = { NOT = { war = yes } }
	treasury = 8 #Not meant to bankrupt countries
	OR = { 
		relation = { who = SWI value = 100 }
		tag = PAP 
		}
	NOT = { 
		tag = SWI
		has_country_flag = swiss_mercenaries
		war = yes
		}
	}
	
mean_time_to_happen = { months = 600 }

title = "EVTNAME55701"
desc = "EVTDESC55701"

option = { 
	name = "EVTOPTA55701" #Hire them to bolster our forces
	ai_chance = { factor = 50 } 
	set_country_flag = swiss_mercenaries 
	treasury = -8
	capital_scope = { province_event = 55703 } #Swiss Mercenaries
	SWI = { 
		manpower = -1 #1000 manpower
		treasury = 8
		relation = { who = THIS value = 5 } 
		}
	}
option = {
	name = "EVTOPTB55701" #We cannot rely on mercenaries
	ai_chance = { factor = 50 } 
	SWI = { relation = { who = THIS value = -5 } }
	}
}

country_event = { #Swiss Guards

id = 55702

trigger = { 
	knows_country = SWI
	OR = { 
		AND = { religion = catholic SWI = { religion = catholic } }
		AND = { religion = protestant SWI = { religion = protestant } }
		AND = { religion = reformed SWI = { religion = reformed } }
		}
	has_country_flag = swiss_mercenaries
	NOT = { has_country_flag = swiss_guards }
	treasury = 50
	OR = { 
	        SWI = { relation = { who = THIS value = 100 } }
	        tag = PAP #Crude, but it works
	        } 
   	}
	
mean_time_to_happen = { months = 600 }

title = "EVTNAME55702"
desc = "EVTDESC55702"

option = {
	name = "EVTOPTA55702" #Establish a permanent force
	ai_chance = { factor = 50 } 
	set_country_flag = swiss_guards
	treasury = -50
	prestige = +0.01
	random_owned = { 
		capital_scope = { 
			change_manpower = +0.25 #permanent manpower
			}
		}
	SWI = { 
		manpower = -1 #temporary loss
		treasury = 50
		relation = { who = THIS value = 25 } 
		}
	}
option = {
	name = "EVTOPTB55702" #We have no need for them
	ai_chance = { factor = 50 } 
	SWI = { relation = { who = THIS value = -5 } }
	}
}

province_event = { #Swiss Mercenaries (province)

id = 55703

is_triggered_only = yes

title = "EVTNAME55703"
desc = "EVTDESC55703"

option = {
	name = "EVTOPTA55703"
	owner = { 
		infantry = THIS 
		add_country_modifier = {
			name = "morale_boost"
			duration = 365
			}
		}
	}

}

