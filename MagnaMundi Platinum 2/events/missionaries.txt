province_event = {

	id = 2060

	trigger = {
		has_missionary = yes
		owner = { missionaries = 1 }
		not = { has_building = temple }
		not = { has_province_modifier = missionary_falls_ill }
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2060"
	desc = "EVTDESC2060"

	option = {
		name = "EVTOPTA2060"		# Send someone to assist him
		ai_chance = { factor = 55 }
		owner = {
			treasury = -20
			missionaries = -1
		}
	}
	option = {
		name = "EVTOPTB2060"		# Direct your efforts elsewhere
		ai_chance = { factor = 45 }
		add_province_modifier = {
			name = "missionary_falls_ill"
			duration = 365
		}
	}
}

province_event = {

	id = 2061

	trigger = {
		has_missionary = yes
		is_core = this
		any_neighbor_province = {
			owned_by = this
			has_owner_religion = no
		}
		not = { has_province_modifier = missionary_failure }
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2061"
	desc = "EVTDESC2061"

	option = {
		name = "EVTOPTA2061"		# Increase your efforts
		ai_chance = { factor = 55 }
		heretic_rebels = 1
	}
	option = {
		name = "EVTOPTB2061"		# It's a waste of time
		ai_chance = { factor = 45 }
		add_province_modifier = {
			name = "missionary_failure"
			duration = 365
		}
	}
}

province_event = {

	id = 2062

	trigger = {
		has_missionary = yes
		is_core = this
		any_neighbor_province = {
			native_size = 1
		}
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2062"
	desc = "EVTDESC2062"

	option = {
		name = "EVTOPTA2062"		# Send some troops and supplies just in case
		ai_chance = { factor = 55 }
		owner = { infantry = this }
		owner = { treasury = -25 }
	}
	option = {
		name = "EVTOPTB2062"		# They can probably handle it themselves
		ai_chance = { factor = 45 }
		random = {
			chance = 50
			heretic_rebels = 1
		}
	}
}

# Religious Uproar
province_event = {

	id = 2063

	trigger = {
		has_missionary = yes
		citysize = 2000
		is_core = this
		owner = { not = { advisor = theologian } }
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2063"
	desc = "EVTDESC2063"

	option = {
		name = "EVTOPTA2063"		# Send some troops
		ai_chance = { factor = 55 }
		owner = { infantry = this }
		revolt_risk = 10
	}
	option = {
		name = "EVTOPTB2063"		# It's not of much importance
		ai_chance = { factor = 45 }
		random = {
			chance = 40
			heretic_rebels = 1
		}
	}
}

# Wicked Rumours
province_event = {

	id = 2064

	trigger = {
		has_missionary = yes
		owner = { NOT = { ADM = 5 } }
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2064"
	desc = "EVTDESC2064"

	option = {
		name = "EVTOPTA2064"		# Find out if it's true
		ai_chance = { factor = 55 }
		random = {
			chance = 60
			revolt_risk = 15
		}
	}
	option = {
		name = "EVTOPTB2064"		# There's no reason to doubt a man of god
		ai_chance = { factor = 45 }
		owner = { treasury = -25 }
	}
}

province_event = {

	id = 2065

	trigger = {
		has_missionary = yes
		has_owner_culture = no
		not = { has_province_modifier = religious_tensions }
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2065"
	desc = "EVTDESC2065"

	option = {
		name = "EVTOPTA2065"		# Take your chances
		ai_chance = { factor = 45 }
		random = {
			chance = 50
			revolt_risk = 10
		}
	}
	option = {
		name = "EVTOPTB2065"		# Do what you can
		ai_chance = { factor = 55 }
		add_province_modifier = {
			name = "religious_tensions"
			duration = 365
		}
	}
}

# Neighbour Disrupting the Peace
province_event = {

	id = 2066

	trigger = {
		has_missionary = yes
		owner = { missionaries = 2 }
		any_neighbor_province = {
			owner = {
				war = yes
				not = { war_with = this }
			}
		}
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
	}

	title = "EVTNAME2066"
	desc = "EVTDESC2066"

	option = {
		name = "EVTOPTA2066"		# Offer him some more money
		ai_chance = { factor = 20 }
		random = {
			chance = 50
			revolt_risk = -3
		}
		owner = { treasury = -20 }
	}
	option = {
		name = "EVTOPTB2066"		# Offer him some help
		ai_chance = { factor = 50 }
		owner = { missionaries = -2 }
	}
	option = {
		name = "EVTOPTC2066"		# Don't worry about it
		ai_chance = { factor = 30 }
		random = {
			chance = 50
			revolt_risk = 5
		}		
	}
}

province_event = {

	id = 2067

	trigger = {
		has_missionary = yes
		owner = {
			advisor = theologian
			missionaries = 2
		}
	}

	mean_time_to_happen = {
		months = 4000

		modifier = {
			factor = 1.4
			owner = { num_of_cities = 20 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 40 }
		}
		modifier = {
			factor = 1.4
			owner = { num_of_cities = 60 }
		}
		modifier = {
			factor = 0.9
			owner = { theologian = 5 }
		}
		modifier = {
			factor = 0.9
			owner = { theologian = 6 }
		}
	}

	title = "EVTNAME2067"
	desc = "EVTDESC2067"

	option = {
		name = "EVTOPTA2067"		# Perhaps he's right
		ai_chance = { factor = 20 }
		owner = { missionaries = -1 }
	}
	option = {
		name = "EVTOPTB2067"		# It's more than enough
		ai_chance = { factor = 50 }
		random = {
			chance = 30
			revolt_risk = 3
		}		
	}
}
