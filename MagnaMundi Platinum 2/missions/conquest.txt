annex_minor_mission = {
	type = neighbor_provinces
	allow = {
		owner = {
			NOT = { tag = PAP }
			num_of_cities = 1
			NOT = { num_of_cities = 2 }
			NOT = { alliance_with = THIS }
			NOT = { relation = { who = THIS value = 0 } }
			OR = {
				is_subject = no
				overlord = { capital_scope = { owned_by = THIS } }
			}
		}
		has_discovered = THIS
		this = {
			is_subject = no
			war = no
		}
		hre = no
	}
	abort = {
		OR = {
			hre = yes
			owner = {
				is_subject = yes
				overlord = { capital_scope = { NOT = { owned_by = THIS } } }
			}
			THIS = { is_subject = yes }
			owner = { NOT = { num_of_cities = 1 } }	#IN patch 3.1 - NEW
		}
	}
	success = {
		owned_by = THIS
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			owner = { truce_with = THIS }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { MIL = 5 } }
		}		
	}
	effect = {
		add_core = THIS
		badboy = -2
	}
}

conquer_a_port = {
	type = neighbor_provinces
	allow = {
		owner = {
			NOT = { army = THIS }
			NOT = { alliance_with = THIS }
			NOT = { relation = { who = THIS value = 0 } }
			OR = {
				is_subject = no
				overlord = { capital_scope = { owned_by = THIS } }
			}
		}
		port = yes
		has_discovered = THIS
		THIS = {
			NOT = { num_of_ports = 1 } 
			is_subject = no
			war = no
		}
		OR = {
			NOT = { owned_by = PAP }
			is_capital = no
		}
		hre = no
	}
	abort = {
		OR = {
			AND = {
				this = { num_of_ports = 1 }
				NOT = { owned_by = THIS }
			}
			owner = {
				is_subject = yes
				overlord = { capital_scope = { NOT = { owned_by = THIS } } }
			}
			hre = yes
			THIS = { is_subject = yes }
		}
	}
	success = {
		owned_by = THIS
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { MIL = 5 } }
		}		
		modifier =  {
			factor = 0.5
			owner = { truce_with = THIS }
		}
	}
	effect = {
		navy_tradition = 0.05
	}
}

conquer_neighbour = {

	# Checked for all provinces bordering the country, with the country in the parent scope.
	type = neighbor_provinces 

	allow = {
		owner = {
			NOT = { army = THIS }
			NOT = { alliance_with = THIS }
			NOT = { relation = { who = THIS value = 0 } }
			OR = {
				is_subject = no
				overlord = { capital_scope = { owned_by = THIS } }
			}
		}
		culture_group = THIS
		has_discovered = THIS
		THIS = {
			is_subject = no
			war = no
		}
		OR = {
			NOT = { owned_by = PAP }
			is_capital = no
		}
		is_colony = no
		hre = no
	}
	
	abort = {
		OR = {
			hre = yes
			owner = {
				is_subject = yes
				overlord = { capital_scope = { NOT = { owned_by = THIS } } }
			}
			THIS = { is_subject = yes }
			owner = { not = { neighbour = this } }	#IN patch 3.1 - NEW
		}
	}

	success = {
		owned_by = THIS
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { MIL = 5 } }
		}
		modifier =  {
			factor = 0.5
			owner = { truce_with = THIS }
		}
	}

	# The effect always has country scope (for the country that gets the mission)
	effect = {
		add_core = THIS
	}	
}



conquer_core = {

	# Checked for all provinces bordering the country, with the country in the parent scope.
	type = neighbor_provinces 

	allow = {
		owner = {
			NOT = { army = THIS }
			NOT = { alliance_with = THIS }
			NOT = { relation = { who = THIS value = 0 } }
			OR = {
				is_subject = no
				overlord = { capital_scope = { owned_by = THIS } }
			}
		}
		is_core = THIS
		has_discovered = THIS
		THIS = {
			is_subject = no
			war = no
		}
		OR = {
			NOT = { owned_by = PAP }
			is_capital = no
		}
		is_colony = no
	}
	
	abort = {
		OR = {
			owner = {
				is_subject = yes
				overlord = { capital_scope = { NOT = { owned_by = THIS } } }
			}
			THIS = { is_subject = yes }
			not = { is_core = this }	#IN patch 3.1 - NEW
		}
	}

	success = {
		owned_by = THIS
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { MIL = 5 } }
		}
		modifier = { 
			factor = 0.5
			owner = { truce_with = THIS }
		}
	}

	# The effect always has country scope (for the country that gets the mission)
	effect = {
		prestige = 0.1
	}	
}


get_control_of_naval_supplies = {
	type = country
	allow = {
		NOT = { naval_supplies = 1 }
		num_of_ports = 1
		big_ship = 30
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			NOT = { big_ship = 10 }
		}
	}
	success = {
		naval_supplies = 1
	}
	chance = {
		factor = 1
		modifier = {
			factor = 2.0
			idea = press_gangs
		}
		modifier = {
			factor = 10.0
			idea = grand_navy
		}
		modifier = {
			factor = 2.0
			idea = sea_hawks
		}
		modifier = {
			factor = 2.0
			idea = superior_seamanship
		}	
		modifier = {
			factor = 2.0
			idea = naval_fighting_instruction
		}
		modifier = {
			factor = 2.0
			idea = excellent_shipwrights
		}
		modifier = {
			factor = 2.0
			idea = naval_glory
		}
		modifier = {
			factor = 2.0
			idea = naval_provisioning
		}
	}
	effect = {
		navy_tradition = 0.05
		badboy = -1
	}
}


protect_religious_minority = {
	type = neighbor_provinces
	allow = {
		this = { 
			war = no
			is_subject = no
		}
		OR = {
			hre = no
			has_global_flag = HRE_reformation_war	# 30YW chain
		}
		religion = THIS
		owner = { NOT = { religion = THIS } }
		NOT = { owner = { alliance_with = THIS } }
		has_discovered = THIS
		owner = { is_subject = no }
	}
	abort = {
		OR = {
			NOT = { religion = THIS }
			owner = { religion = THIS }
			owner = { alliance_with = THIS }
			owner = {
				is_subject = yes
				overlord = { capital_scope = { NOT = { owned_by = THIS } } }
			}
			THIS = { is_subject = yes }
		}
	}
	success = {
		owned_by = THIS
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { MIL = 5 } }
		}		
		modifier = { 
			factor = 0.5
			owner = { truce_with = THIS }
		}
	}
	effect = {
		add_core = THIS
	}
}

protect_cultural_minority = {
	type = neighbor_provinces
	allow = {
		this = { 
			war = no
			is_subject = no
		}
		hre = no
		culture = THIS
		owner = { NOT = { primary_culture = THIS } }
		NOT = { owner = { alliance_with = THIS } }
		has_discovered = THIS
		owner = { is_subject = no }
	}
	abort = {
		OR = {
			NOT = { culture = THIS }
			owner = { primary_culture = THIS }
			owner = { alliance_with = THIS }
			hre = yes
			owner = {
				is_subject = yes
				overlord = { capital_scope = { NOT = { owned_by = THIS } } }
			}
			THIS = { is_subject = yes }
		}
	}
	success = {
		owned_by = THIS
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { MIL = 5 } }
		}		
		modifier = {
			factor = 0.5
			owner = { truce_with = THIS }
		}
	}
	effect = {
		add_core = THIS
	}
}

keep_rival_out_of_italy = {

	type = rival_countries

	allow = {
	
		#we're strong enough.. and they are no small ones..
		THIS = { 
			NOT = { is_subject = yes } 
			num_of_cities = 10 
			war = no
		}
		num_of_cities = 5
		NOT = { culture_group = latin }

		#do we own any of italy.		
		italian_region = {
			owned_by = THIS
		}

		NOT = { relation = { who = THIS value = 100 } }

		any_owned_province = {	
			region = italian_region
		}
		
		NOT = { alliance_with = THIS }
		NOT = { is_subject = yes }
		OR = {
			capital_scope = { hre = no }
			THIS = { capital_scope = { hre = no } }
		}
	}
	abort = {
		NOT = {
			italian_region = {
				owned_by = THIS
			}
		}
	}

	success = {
		NOT = {
			any_owned_province = {	
				region = italian_region
			}
		}
	}
	
	chance = {
		factor = 2
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { DIP = 5 } }
		}

	}
	effect = {
		THIS = { prestige = 0.03 }
	}	
}


attack_prestigous_rival = {

	type = rival_countries

	allow = {
		prestige = THIS  #they got more prestige than us.
		neighbour = THIS	#neighbour with us.
		
		#we're strong enough.. and they are no small ones..
		THIS = { 
			num_of_cities = 5
			war = no
			is_lesser_in_union = no
			is_subject = no
		}
		num_of_cities = 5
		NOT = { alliance_with = THIS }
		NOT = { is_subject = yes }
		regency = no
		OR = {
			capital_scope = { hre = no }
			THIS = { capital_scope = { hre = no } }
		}
	}

	abort = {
		or = {
			THIS = {
				OR = {
					is_lesser_in_union = yes
					is_subject = yes
				}
			}
			NOT = { num_of_cities = 1 }
		}
	}

	success = {
		war_with = THIS
	}
	
	chance = {
		factor = 2
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { DIP = 5 } }
		}

	}
	effect = {
		random_owned = {
			limit = {
				any_neighbor_province = {
					owned_by = THIS
				}
				NOT = { is_core = THIS }
			}
			add_core = THIS
		}
		THIS = { prestige = 0.01 }
	}	
}


attack_weaker_rival = {

	type = rival_countries

	allow = {
	
		NOT = { army = THIS }
		neighbour = THIS	#neighbour with us.
		
		#we're strong enough.. and they are no small ones..
		THIS = {
			num_of_cities = 5
			war = no
			is_lesser_in_union = no
			is_subject = no
		}
		war_exhaustion = 4
		num_of_cities = 5
		
		NOT = { alliance_with = THIS }
		NOT = { is_subject = yes }
		regency = no
		OR = {
			capital_scope = { hre = no }
			THIS = { capital_scope = { hre = no } }
		}
	}

	abort = {
		or = {
			THIS = {
				OR = {
					is_lesser_in_union = yes
					is_subject = yes
				}
			}
			NOT = { war_exhaustion = 1 }
		}
	}

	success = {
		war_with = THIS
	}
	
	chance = {
		factor = 2
		modifier = {
			factor = 8
			war = yes
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			THIS = { NOT = { DIP = 5 } }
		}

	}
	effect = {
		random_owned = {
			limit = {
				any_neighbor_province = {
					owned_by = THIS
				}
				NOT = { is_core = THIS }
			}
			add_core = THIS
		}
		THIS = { prestige = 0.01 }
	}	
}




italian_ambition = {

	type = country

	allow = {
	
		num_of_cities = 10 
		NOT = { year = 1600 }
		NOT = { culture_group = latin }
		religion_group = christian
		is_lesser_in_union = no
		is_subject = no
		OR = {
			culture_group = french
			primary_culture = austrian
			primary_culture = castillian
			primary_culture = catalan
		}
		NOT = { italian_region = { owned_by = THIS } }
		
	}
	abort = {
		OR = {
			is_lesser_in_union = yes
			is_subject = yes
		}
	}

	success = {
		italian_region = { owned_by = THIS }
	}
	
	chance = {
		factor = 5
		modifier = {
			factor = 0.1
			ai = yes
			NOT = { has_country_flag = ai_competitive_01 }
		}
		modifier = {
			factor = 2
			ai = yes
			has_country_flag = ai_competitive_01
		}
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}
		modifier = {
			factor = 0.5
			NOT = { DIP = 5 }
		}

	}
	effect = {
		THIS = { prestige = 0.03 }
		italian_region = {
			limit = { 
				owned_by = THIS 
			}
			add_core = THIS
		}
	}	
}
