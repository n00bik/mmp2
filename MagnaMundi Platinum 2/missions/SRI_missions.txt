# General missions, economic

establish_trade_own = {

	type = all_cots 

	allow = {
		owned_by = THIS
		NOT = { placed_merchants = 4 }
	}
	abort = {
		OR = {
			NOT = { owned_by = THIS }
			cot = no
		}
	}
	
	success = {
		placed_merchants = 5
	}
	
	chance = {
		factor = 10
		modifier = {
			factor = 2
			THIS = { NOT = { total_merchants = 5 } }
		}
		modifier = {
			factor = 2
			THIS = { NOT = { total_merchants = 10 } }
		}
	}

	# The effect always has country scope (for the country that gets the mission)
	effect = {
		treasury = 50
		merchants = 5
	}	
}

break_another_agreement = {
	
	type = neighbor_provinces

	allow = {
		owner = {
			trade_agreement_with = THIS
			badboy = 0.5
		}
		THIS = {
			NOT = { badboy = 0.3 }
			NOT = { has_country_modifier = hanseatic_league }
		}
	}
	abort = {
		owner = { NOT = { badboy = 0.3 } }
	}
	success = {
		owner = { NOT = { trade_agreement_with = THIS } }
	}
	chance = {
		factor = 10
		modifier = {
			factor = 50
			THIS = { num_of_cots = 1 }
		}
	}
	effect = {
		THIS = { prestige = 0.02 }
		relation = { who = THIS value = -100 }
	}
}

get_a_cot = {
	
	type = country

	allow = {
		NOT = { merchants = 5 }
		total_merchants = 16
		NOT = { num_of_cots = 1 }
		NOT = { num_of_cities = 4 }
		NOT = { idea = shrewd_commerce_practise }
		NOT = { idea = merchant_adventures }
		NOT = { idea = national_trade_policy }
		total_ideas = 1
		trade_tech = 8
		NOT = { mercantilism_freetrade = 0 }
		NOT = { capital_scope = { any_neighbor_province = { cot = yes } } }
		NOT = {
			AND = {
				ai = yes
				capital_scope = { any_neighbor_province = { any_neighbor_province = { cot = yes } } }
			}
		}
	}
	abort = {
		OR = {
			NOT = { total_merchants = 6 }
			capital_scope = { cot = yes }
			capital_scope = { any_neighbor_province = { cot = yes } }
			num_of_cots = 1
			num_of_cities = 4
			AND = {
				ai = yes
				capital_scope = { any_neighbor_province = { any_neighbor_province = { cot = yes } } }
			}
		}
	}
	success = { total_merchants = 40 }
	chance = {
		factor = 10
		modifier = {
			factor = 2
			capital_scope = { hre = yes }
		}
		modifier = {
			factor = 5
			NOT = { mercantilism_freetrade = -2 }
		}
		modifier = {
			factor = 2
			total_merchants = 30
		}
	}
	effect = {
		capital_scope = {
			cot = yes
			send_merchant = THIS
			send_merchant = THIS
			send_merchant = THIS
			send_merchant = THIS
			send_merchant = THIS
		}
		merchants = -5
		random_center_of_trade = {
			limit = { placed_merchants = 5 THIS = { total_merchants = 40 } }
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 4 THIS = { total_merchants = 35 } }
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 3 THIS = { total_merchants = 31 } }
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 2 THIS = { total_merchants = 28 } }
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 1 THIS = { total_merchants = 26 } }
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 5 }
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 4 }
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 3 }
			remove_merchant = THIS
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 2 }
			remove_merchant = THIS
			remove_merchant = THIS
		}
		random_center_of_trade = {
			limit = { placed_merchants = 1 }
			remove_merchant = THIS
		}
	}
}

# Administrative Efficiency

increase_administrative_efficiency_1 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		idea = bill_of_rights
		aristocracy_plutocracy = 0
		serfdom_freesubjects = -2
		NOT = { serfdom_freesubjects = 0 }
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			NOT = { serfdom_freesubjects = -2 }
		}
	}
	success = {
		serfdom_freesubjects = 0
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			serfdom_freesubjects = -1
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_2 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		idea = bureaucracy
		centralization_decentralization = 0
		NOT = { centralization_decentralization = 2 }
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			centralization_decentralization = 2
		}
	}
	success = {
		NOT = { centralization_decentralization = 0 }
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			NOT = { centralization_decentralization = 1 }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_3 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		idea = smithian_economics
		mercantilism_freetrade = 0
		NOT = { mercantilism_freetrade = 2 }
		NOT = {
			AND = {
				idea = national_bank
				NOT = { aristocracy_plutocracy = 1 }
				mercantilism_freetrade = 1
			}
		}
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			mercantilism_freetrade = 2
			AND = {
				idea = national_bank
				NOT = { aristocracy_plutocracy = 1 }
				mercantilism_freetrade = 1
			}
		}
	}
	success = {
		NOT = { mercantilism_freetrade = 0 }
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			NOT = { mercantilism_freetrade = 1 }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_4 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		idea = national_bank
		NOT = { mercantilism_freetrade = 1 }
		NOT = { aristocracy_plutocracy = 1 }
		OR = {
			aristocracy_plutocracy = -1
			AND = {
				mercantilism_freetrade = -1
				NOT = {
					AND = {
						NOT = { mercantilism_freetrade = 0 }
						idea = smithian_economics
					}
				}
			}
		}
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			AND = {
				NOT = { aristocracy_plutocracy = -1 }
				NOT = { mercantilism_freetrade = -1 }
			}
			AND = {
				NOT = { aristocracy_plutocracy = -1 }
				mercantilism_freetrade = -1
				idea = smithian_economics
			}
		}
	}
	success = {
		OR = {
			aristocracy_plutocracy = 1
			mercantilism_freetrade = 1
		}
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			aristocracy_plutocracy = 0
		}
		modifier = {
			factor = 2
			mercantilism_freetrade = 0
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_5 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		OR = {
			idea = church_attendance_duty
			idea = divine_supremacy
			idea = deus_vult
		}
		NOT = { idea = ecumenism }
		NOT = { idea = humanist_tolerance }
		NOT = { idea = liberty_egalite_fraternity }
		innovative_narrowminded = -1
		NOT = { innovative_narrowminded = 1 }
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			NOT = { innovative_narrowminded = -1 }
			idea = ecumenism
			idea = humanist_tolerance
			idea = liberty_egalite_fraternity
		}
	}
	success = {
		innovative_narrowminded = 1
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			innovative_narrowminded = 0
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_6 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		OR = {
			idea = ecumenism
			idea = humanist_tolerance
			idea = liberty_egalite_fraternity
		}
		NOT = { idea = church_attendance_duty }
		NOT = { idea = divine_supremacy }
		NOT = { idea = deus_vult }
		innovative_narrowminded = 0
		NOT = { innovative_narrowminded = 2 }
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			innovative_narrowminded = 2
			idea = church_attendance_duty
			idea = divine_supremacy
			idea = deus_vult
		}
	}
	success = {
		NOT = { innovative_narrowminded = 0 }
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			NOT = { innovative_narrowminded = 1 }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_7 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		NOT = { serfdom_freesubjects = 2 }
		serfdom_freesubjects = 0
		NOT = { aristocracy_plutocracy = -1 }
		has_global_flag = hre_initialised
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			serfdom_freesubjects = 2
		}
	}
	success = {
		NOT = { serfdom_freesubjects = 0 }
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			NOT = { serfdom_freesubjects = 1 }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

increase_administrative_efficiency_8 = {
	
	type = country

	allow = {
		NOT = { check_variable = { which = AE_level value = 5 } }
		ADM = 6
		NOT = { aristocracy_plutocracy = 2 }
		aristocracy_plutocracy = 0
		NOT = { serfdom_freesubjects = -1 }
		has_global_flag = hre_initialised
		NOT = {
			AND = {
				aristocracy_plutocracy = 1
				idea = national_bank
				NOT = { mercantilism_freetrade = 1 }
			}
		}
	}
	abort = {
		OR = {
			check_variable = { which = AE_level value = 7 }
			aristocracy_plutocracy = 2
			AND = {
				idea = national_bank
				aristocracy_plutocracy = 1
				NOT = { mercantilism_freetrade = 1 }
			}
		}
	}
	success = {
		NOT = { aristocracy_plutocracy = 0 }
	}
	chance = {
		factor = 10
		modifier = {
			factor = 2
			ADM = 7
		}
		modifier = {
			factor = 2
			ADM = 8
		}
		modifier = {
			factor = 2
			ADM = 9
		}
		modifier = {
			factor = 2
			NOT = { aristocracy_plutocracy = 1 }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 4 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 3 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 2 } }
		}
		modifier = {
			factor = 2
			NOT = { check_variable = { which = AE_level value = 1 } }
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = {
		stability = 1
	}
}

# Historical Right and Balance of Power

return_our_cousin_to_power = {
	
	type = neighbor_countries

	allow = {
		NOT = {
			AND = {
				ADM = 8
				DIP = 8
			}
		}
		NOT = { alliance_with = THIS }
		NOT = { army = THIS }
		NOT = { num_of_cities = THIS }
		NOT = { num_of_cities = 4 }
		NOT = { relation = { who = THIS value = 60 } }
		THIS = {
			is_subject = no
			OR = {
				government = monarchy
				government = electoral_government
			}
		}
		has_country_flag = original_monarchy
		NOT = { government = monarchy }
		NOT = { government = electoral_government }
		is_subject = no
		religion_group = THIS
	}
	abort = {
		OR = {
			government = monarchy
			government = electoral_government
			NOT = { religion_group = THIS }
			alliance_with = THIS
			THIS = {
				NOT = { government = monarchy }
				NOT = { government = electoral_government }
			}
			AND = {
				is_subject = yes
				NOT = { vassal_of = THIS }
			}
			relation = { who = THIS value = 150 }
		}
	}
	success = {
		vassal_of = THIS
	}
	chance = {
		factor = 1
		modifier = {
			factor = 0
			ally = { alliance_with = THIS }
		}
		modifier = {
			factor = 2
			badboy = 0.2
		}
		modifier = {
			factor = 2
			NOT = { MIL = 7 }
		}
		modifier = {
			factor = 2
			THIS = { MIL = 8 }
		}
		modifier = {
			factor = 2
			NOT = { num_of_allies = 2 }
		}
		modifier = {
			factor = 2
			THIS = { num_of_allies = 2 }
		}
		modifier = {
			factor = 2
			THIS = { num_of_royal_marriages = 1 }
		}
		modifier = {
			factor = 2
			THIS = { num_of_royal_marriages = 2 }
		}
	}
	effect = {
		random_owned = {
			limit = {
				OR = {
					NOT = { has_global_flag = atage }
					owner = { has_country_flag = kingdom }
				}
			}
			owner = { government = feudal_monarchy }
		}
		random_owned = {
			limit = {
				has_global_flag = atage
				owner = { has_country_flag = empire }
			}
			owner = { government = feudal_monarchy-upper }
		}
		random_owned = {
			limit = {
				has_global_flag = atage
				owner = { has_country_flag = principality }
			}
			owner = { government = feudal_monarchy-lower }
		}
		random_owned = {
			limit = {
				has_global_flag = atage
				owner = { has_country_flag = state }
			}
			owner = { government = feudal_monarchy-lowest }
		}
		create_alliance = THIS
		create_marriage = THIS
		stability = 2
		THIS = {
			prestige = 0.1
			stability = 1
		}
		any_country = {
			limit = {
				religion_group = THIS
				culture_group = THIS
				government = monarchy
			}
			relation = { who = THIS value = 100 }
		}
		any_country = {
			limit = {
				religion_group = THIS
				culture_group = THIS
				government = republic
				NOT = { alliance_with = THIS }
			}
			relation = { who = THIS value = -200 }
		}
	}
}

# financial health of the AI

decommission_navy = {
	type = country
	allow = {
		navy_size_percentage = 1.01
		tariff_efficiency = 1
		ai = yes
	}
	abort = {
		OR = {
			NOT = { tariff_efficiency = 0.75 }
			ai = no
		}
	}
	success = { NOT = { navy_size_percentage = 0.9 } }
	chance = {
		factor = 1
		modifier = {
			factor = 10.0
			is_bankrupt = yes
		}
		modifier = {
			factor = 2.0
			war = no
		}
		modifier = {
			factor = 2.0
			number_of_loans = 1
		}
		modifier = {
			factor = 2.0
			number_of_loans = 2
		}
		modifier = {
			factor = 2.0
			number_of_loans = 3
		}
		modifier = {
			factor = 2.0
			number_of_loans = 4
		}
		modifier = {
			factor = 2.0
			number_of_loans = 5
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.1
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.2
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.3
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.4
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.5
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.6
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.7
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.8
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 1.9
		}
		modifier = {
			factor = 2.0
			navy_size_percentage = 2.0
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = { treasury = 100 }
}

decommission_army = {
	type = country
	allow = { ai = yes army_size_percentage = 1.5 }
	abort = { ai = no }
	success = { NOT = { army_size_percentage = 0.9 } }
	chance = {
		factor = 1
		modifier = {
			factor = 10.0
			is_bankrupt = yes
		}
		modifier = {
			factor = 2.0
			war = no
		}
		modifier = {
			factor = 2.0
			number_of_loans = 1
		}
		modifier = {
			factor = 2.0
			number_of_loans = 2
		}
		modifier = {
			factor = 2.0
			number_of_loans = 3
		}
		modifier = {
			factor = 2.0
			number_of_loans = 4
		}
		modifier = {
			factor = 2.0
			number_of_loans = 5
		}
		modifier = {
			factor = 2.0
			army_size_percentage = 1.6
		}
		modifier = {
			factor = 2.0
			army_size_percentage = 1.7
		}
		modifier = {
			factor = 2.0
			army_size_percentage = 1.8
		}
		modifier = {
			factor = 2.0
			army_size_percentage = 1.9
		}
		modifier = {
			factor = 2.0
			army_size_percentage = 2.0
		}
		modifier = {	# counteract "black hole" factor
			factor = 50000
			ai = yes
			has_global_flag = magna_mundi
		}
	}
	effect = { treasury = 100 }
}