
get_minor_cash_reserve = {

	type = country

	allow = {
		NOT = { ai = yes }
		NOT = { treasury = 50 }
		NOT = { number_of_loans = 1 }
	}
	
	abort = {
		number_of_loans = 1
	}
	
	success = {
		treasury = 100
	}
	
	chance = {
		factor = 0.25 # Base weight
	}

	effect = {
		treasury = 10
	}	
}

amass_wealth = {

	type = country

	allow = {
		NOT = { ai = yes }
		NOT = { treasury = 1000 }
		NOT = { number_of_loans = 1 }
		monthly_income = 100
		treasury = 300
	}
	
	abort = {
		OR = {
			NOT = { treasury = 200 }
			number_of_loans = 1
		}
	}
	
	success = {
		treasury = 2000
	}
	
	chance = {
		factor = 0.25 # Base weight
		modifier = {
			factor = 2.0
			NOT = { mercantilism_freetrade = -2 }
		}
		modifier = {
			factor = 0.5
			mercantilism_freetrade = 2
		}
		modifier = {
			factor = 1.5
			aristocracy_plutocracy = 2
		}
		modifier = {
			factor = 0.75
			NOT = { aristocracy_plutocracy = -2 }
		}
	}

	effect = {
		prestige = 0.1
		inflation = -1
	}	
}

recover_negative_stability = {

	type = country

	allow = {
		NOT = { ai = yes }
		NOT = { stability = 0 }
	}
	
	abort = {
	}
	
	success = {
		stability = 0
	}
	
	chance = {
		factor = 0.25 # Base weight
		modifier = {
			factor = 2.0
			ADM = 5
		}
	}

	effect = {
		prestige = 0.05
	}	
}


improve_economical_mismanagement = {

	type = country

	allow = {
		NOT = { ai = yes }
		number_of_loans = 1
	}
	
	abort = {
		is_bankrupt = yes
	}
	
	success = {
		NOT = { number_of_loans = 1 }
	}
	
	chance = {
		factor = 0.25 # Base weight
		modifier = {
			factor = 2.0
			ADM = 5
		}
	}

	effect = {
		inflation = -1
	}	
}

reduce_inflation = {

	type = country

	allow = {
		NOT = { ai = yes }
		inflation = 15
		idea = national_bank
		capital_scope = { has_building = tax_assessor }
		NOT = { inflation = 30 }                 #corrected vanilla bug
	}
	
	abort = {
		inflation = 30
	}
	
	success = {
		NOT = { inflation = 10 }
	}
	
	chance = {
		factor = 0.25 # Base weight
		modifier = {
			factor = 2.0
			ADM = 5
		}
	}

	effect = {
		prestige = 0.1
		inflation = -1
	}	
}

recover_from_warexhaustion = {

	type = country

	allow = {
		NOT = { ai = yes }
		war_exhaustion = 8
		war = no
	}
	
	abort = {
		war = yes
	}
	
	success = {
		NOT = { war_exhaustion = 1 }
		war = no
	}
	
	chance = {
		factor = 0.25 # Base weight
	}

	effect = {
		prestige = 0.1
	}	
}

buildup_manpower_reserves = {

	type = country

	allow = {
		NOT = { ai = yes }
		max_manpower = 5
		NOT = { manpower_percentage = 0.30 }
		war = no
	}
	
	abort = {
		war = yes
	}
	
	success = {
		manpower_percentage = 0.90
	}
	
	chance = {
		factor = 0.25 # Base weight
	}

	effect = {
		prestige = 0.05
		army_tradition = 0.1
	}	
}
