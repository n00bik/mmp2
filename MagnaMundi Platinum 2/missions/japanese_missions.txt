convert_ezochi_to_shintoism = {
	
	type = country

	allow = {
		NOT = { ai = yes }
		tag = JTG
		religion = shinto
		owns = 1031
		1031 = { not = { religion = shinto } }
		missionaries = 1
		not = { stability = 3 }
	}
	abort = {
		or = {
			not = { religion = shinto }
			not = { owns = 1031 }
			stability = 3
		}
	}
	success = {
		1031 = { religion = shinto }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 7
		}
	}
	effect = {
		stability = 1
	}
}
