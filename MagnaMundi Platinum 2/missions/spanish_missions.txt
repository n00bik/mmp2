####################################
# Reconquista = 4 Missions
####################################

finish_reconquista = {
	type = country

	allow = {
		NOT = { ai = yes }
		OR = {
			tag = CAS
			tag = SPA
		}
		war = no
		religion = catholic
		iberian_peninsula = {
			owner = { religion_group = muslim }
		}
	}
	abort = {
		OR = {
			NOT = { religion = catholic }
			AND = {
				NOT = { iberian_peninsula = { NOT = { owner = { religion = catholic } } } }
				NOT = { andalusia = { owned_by = THIS } }
			}
		}
	}
	success = {
		NOT = { iberian_peninsula = { NOT = { owner = { religion = catholic } } } }
		NOT = { andalusia = { NOT = { owned_by = THIS } } } 
	}

	chance = {
		factor = 1000
		modifier = {
			factor = 10
			exists = GRA
		}
	}
	effect = {
		army_tradition = 0.15
		prestige = +0.10
	}	


}


