# Timurid
conquer_delhi = {
	
	type = country

	allow = {
	    OR = {
	        tag = FGA
		    tag = TIM
	    }
		exists = DLH
		DLH = {
			neighbour = this
			owns = 522		# Delhi
		}
		is_subject = no
		is_lesser_in_union = no
	}
	abort = {
		OR = {	#IN patch 3.1 - NEW
			is_subject = yes
			is_lesser_in_union = yes
			and = {
				not = { exists = DLH }
				not = { owns = 506 }
				not = { owns = 507 }
				not = { owns = 510 }
				not = { owns = 511 }
				not = { owns = 521 }
				not = { owns = 522 }
				not = { owns = 523 }
				not = { owns = 524 }
				not = { owns = 578 }
				not = { owns = 740 }
			}
		}
	}
	success = {
		owns = 522 # Delhi
		owns = 578 # Kohistan
		owns = 507 # Lahore
		owns = 506 # Multan
		owns = 510 # Chandigarh
		owns = 511 # Sutlej
		owns = 521 # Panipat
		owns = 523 # Lucknow
		owns = 524 # Agra
		owns = 740 # Ladakh
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			year = 1526
		}
		modifier = {
			factor = 2
			TIM = { infantry = this cavalry = this }
		}
	}
	effect = {
		prestige = 0.25
		add_core = 522 # Delhi
		add_core = 578 # Kohistan
		add_core = 507 # Lahore
		add_core = 508 # Multan
		add_core = 510 # Chandigarh
		add_core = 511 # Sutlej
		add_core = 521 # Panipat
		add_core = 523 # Lucknow
		add_core = 524 # Agra
		add_core = 740 # Ladakh
	}
}

fund_the_taj_mahal = {
	
	type = country

	allow = {
		tag = MUG
		not = { year = 1648 }
		treasury = 50
		not = { number_of_loans = 1 }
		not = { has_country_flag = taj_mahal_exists } 	#IN patch 3.1 - NEW
	}
	abort = {
		number_of_loans = 1
	}
	success = {
		treasury = 200
		advisor = artist
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 7
		}
		modifier = {
			factor = 2
			adm = 9
		}
	}
	effect = {
		prestige = 0.1
		set_country_flag = taj_mahal_exists 	#IN patch 3.1 - NEW
	}
}

vassalize_golcanda = {
	
	type = country

	allow = {
		tag = MUG
		exists = GOC
		GOC = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			owns = 543
			is_subject = no
		}
		not = { has_country_flag = vassalize_golcanda_done } 	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = GOC }
			GOC = { is_lesser_in_union = yes }
		}
	}
	success = {
		GOC = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GOC value = 0 } }
		}
	}
	effect = {
		add_core = 543
		set_country_flag = vassalize_golcanda_done 	#IN patch 3.1 - NEW
	}
}

vassalize_gondwana = {
	
	type = country

	allow = {
		tag = MUG
		exists = GDW
		GDW = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
		not = { has_country_flag = vassalize_gondwana_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = GDW }
			GDW = { is_lesser_in_union = yes }
		}
	}
	success = {
		GDW = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GDW value = 0 } }
		}
	}
	effect = {
		stability = 3
		set_country_flag = vassalize_gondwana_done	#IN patch 3.1 - NEW
	}
}

defend_the_mughal_borders = {
	
	type = country

	allow = {
		year = 1649
		tag = MUG
		exists = PER
		war_with = PER
		PER = { neighbour = this }
		owns = 575
		owns = 576
		owns = 577
		owns = 448
		owns = 451
		owns = 450
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
		}
	}
	success = {
		not = { war_with = PER }
		owns = 575
		owns = 576
		owns = 577
		owns = 448
		owns = 451
		owns = 450
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	effect = {
		badboy = -5
		stability = 1
	}
}

mughal_break_the_persian_border = {
	
	type = country

	allow = {
		tag = MUG
		exists = PER
		war_with = PER
		PER = { neighbour = this }
		not = { persian_region = { owned_by = this } }
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
		}
	}
	success = {
		not = { war_with = PER }
		persian_region = { owned_by = this }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	effect = {
		army_tradition = 0.5
		war_exhaustion = -5
	}
}

vassalize_ahmadnagar = {
	
	type = country

	allow = {
		tag = MUG
		exists = BAS
		BAS = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
		not = { has_country_flag = vassalize_ahmadnagar_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = BAS }
			BAS = { is_lesser_in_union = yes }
		}
	}
	success = {
		BAS = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = BAS value = 0 } }
		}
	}
	effect = {
		prestige = 0.05
		set_country_flag = vassalize_ahmadnagar_done	#IN patch 3.1 - NEW
	}
}

conquer_khandesh = {

	type = country

	allow = {
		tag = MUG
		not = { owns = 527 }		# Burhanpur
	}
	abort = { }
	success = {
		owns = 527
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
	}
	effect = {
		stability = 1
	}
}

annex_berar = {
	
	type = country

	allow = {
		tag = MUG
		is_subject = no
		exists = BRR
		BRR = {
			vassal_of = MUG
			neighbour = this
			not = { num_of_cities = this }
			owns = 546		# Nagpur
		}
	}
	abort = {
		not = { exists = BRR }
	}
	success = {
		not = { exists = BRR }
		owns = 546
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = BRR value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = BRR value = 200 }
		}
	}
	effect = {
		add_core = 546
	}
}

vassalize_gujarat = {
	
	type = country

	allow = {
		tag = MUG
		exists = GUJ
		GUJ = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
		not = { has_country_flag = vassalize_gujarat_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = GUJ }
			GUJ = { is_lesser_in_union = yes }
		}
	}
	success = {
		GUJ = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GUJ value = 0 } }
		}
	}
	effect = {
		prestige = 0.05
		set_country_flag = vassalize_gujarat_done	#IN patch 3.1 - NEW
	}
}

vassalize_orissa = {
	
	type = country

	allow = {
		tag = MUG
		exists = ORI
		ORI = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
		not = { has_country_flag = vassalize_orissa_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = ORI }
			ORI = { is_lesser_in_union = yes }
		}
	}
	success = {
		ORI = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = ORI value = 0 } }
		}
	}
	effect = {
		prestige = 0.05
		set_country_flag = vassalize_orissa_done	#IN patch 3.1 - NEW
	}
}

defeat_england = {

	type = country
	
	allow = {
		tag = MUG
		exists = ENG
		indian_region = { owned_by = ENG }
		war_with = ENG
		ENG = { neighbour = this }
		is_subject = no		#IN patch 3.1 - NEW
		is_lesser_in_union = no	#IN patch 3.1 - NEW
		ENG = {
			is_subject = no	#IN patch 3.1 - NEW
			is_lesser_in_union = no	#IN patch 3.1 - NEW
		}
	}
	abort = {
		OR = {	#IN patch 3.1 - NEW
			is_subject = yes
			is_lesser_in_union = yes
			not = { neighbour = ENG }
			not = { war_with = ENG }
			ENG = {
				or = {
					is_subject = yes
					is_lesser_in_union = yes
				}
			}
		}
	}
	success = {
		not = { war_with = ENG }
		not = { indian_region = { owned_by = ENG } }	#IN patch 3.1 - NEW
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { dip = 4 }
		}
		modifier = {
			factor = 2
			not = { dip = 2 }
		}
	}
	effect = {
		army_tradition = 0.5
		stability = 2
	}
}

defend_timurid_lands = {
	
	type = country

	allow = {
	    OR = {
	        tag = TOX
    		tag = TIM
    	}
		exists = SHY
		war_with = SHY
		owns = 437	#IN patch 3.1 - NEW
		owns = 438	#IN patch 3.1 - NEW
		owns = 440	#IN patch 3.1 - NEW
		owns = 441	#IN patch 3.1 - NEW
		owns = 442	#IN patch 3.1 - NEW
		owns = 444	#IN patch 3.1 - NEW
		owns = 445	#IN patch 3.1 - NEW
		owns = 450
		owns = 451
		owns = 452
		owns = 453	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = SHY }
			not = { war_with = SHY }
		}
	}
	success = {
		not = { war_with = SHY }
		owns = 437	#IN patch 3.1 - NEW
		owns = 438	#IN patch 3.1 - NEW
		owns = 440	#IN patch 3.1 - NEW
		owns = 441	#IN patch 3.1 - NEW
		owns = 442	#IN patch 3.1 - NEW
		owns = 444	#IN patch 3.1 - NEW
		owns = 445	#IN patch 3.1 - NEW
		owns = 450
		owns = 451
		owns = 452
		owns = 453	#IN patch 3.1 - NEW
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			PER = { not = { mil = 5 } }
		}
	}
	effect = {
		army_tradition = 0.5
		stability = 1
	}
}

defend_timurid_lands_against_persia = {
	
	type = country

	allow = {
		OR = {	#IN patch 3.1 - NEW
			tag = KHO
			tag = TIM
		}
		exists = PER
		war_with = PER
		owns = 431	#IN patch 3.1 - NEW
		owns = 432	#IN patch 3.1 - NEW
		owns = 434	#IN patch 3.1 - NEW
		owns = 435	#IN patch 3.1 - NEW
		owns = 436	#IN patch 3.1 - NEW
		owns = 437	#IN patch 3.1 - NEW
		owns = 445	#IN patch 3.1 - NEW
		owns = 446	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
		}
	}
	success = {
		not = { war_with = PER }
		owns = 431	#IN patch 3.1 - NEW
		owns = 432	#IN patch 3.1 - NEW
		owns = 434	#IN patch 3.1 - NEW
		owns = 435	#IN patch 3.1 - NEW
		owns = 436	#IN patch 3.1 - NEW
		owns = 437	#IN patch 3.1 - NEW
		owns = 445	#IN patch 3.1 - NEW
		owns = 446	#IN patch 3.1 - NEW
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			PER = { not = { mil = 5 } }
		}
	}
	effect = {
		army_tradition = 0.5
		stability = 1
	}
}

timurid_break_the_persian_border = {
	
	type = country

	allow = {
		OR = {	#IN patch 3.1 - NEW
			tag = KHO
			tag = TIM
		}
		is_subject = no		#IN patch 3.1 - NEW
		is_lesser_in_union = no	#IN patch 3.1 - NEW
		exists = PER
		war_with = PER
		PER = { neighbour = this }
		not = { persian_region = { owned_by = this } }
	}
	abort = {
		or = {
			is_subject = yes	#IN patch 3.1 - NEW
			is_lesser_in_union = yes	#IN patch 3.1 - NEW
			not = { exists = PER }
			not = { war_with = PER }
		}
	}
	success = {
		not = { war_with = PER }
		persian_region = { owned_by = this }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	effect = {
		army_tradition = 0.5
		war_exhaustion = -5
	}
}

vassalize_the_akkoyunlu = {
	
	type = country

	allow = {
		tag = TIM
		exists = AKK
		not = { year = 1475 }
		AKK = {
			neighbour = this
			not = { vassal_of = TIM }
			not = { num_of_cities = this }
			is_subject = no
		}
		not = { has_country_flag = vassalize_the_akkoyunlu_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = AKK }
			AKK = { is_lesser_in_union = yes }
		}
	}
	success = {
		AKK = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = AKK value = 0 } }
		}
	}
	effect = {
		prestige = 0.05
		set_country_flag = vassalize_the_akkoyunlu_done	#IN patch 3.1 - NEW
	}
}

vassalize_the_qara_qoyunlu = {
	
	type = country

	allow = {
		tag = TIM
		exists = QAR
		not = { year = 1475 }
		QAR = {
			neighbour = this
			not = { vassal_of = QAR }
			not = { num_of_cities = this }
			is_subject = no
		}
		not = { has_country_flag = vassalize_the_qara_qoyunlu_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = QAR }
			QAR = { is_lesser_in_union = yes }
		}
	}
	success = {
		QAR = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = QAR value = 0 } }
		}
	}
	effect = {
		prestige = 0.05
		set_country_flag = vassalize_the_qara_qoyunlu_done	#IN patch 3.1 - NEW
	}
}

vassalize_georgia = {
	
	type = country

	allow = {
		tag = TIM
		exists = GEO
		not = { year = 1475 }
		GEO = {
			neighbour = this
			is_subject = no
			not = { vassal_of = TIM }
			not = { num_of_cities = this }
		}
		not = { has_country_flag = vassalize_georgia_done }	#IN patch 3.1 - NEW
	}
	abort = {
		or = {
			not = { exists = GEO }
			GEO = { is_lesser_in_union = yes }
		}
	}
	success = {
		GEO = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GEO value = 0 } }
		}
	}
	effect = {
		prestige = 0.05
		set_country_flag = vassalize_georgia_done	#IN patch 3.1 - NEW
	}
}
