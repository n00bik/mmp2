# THE KNIGHTS OF ST JOHN MISSIONS
# SHARPALIGNMENT
# 08_24_2008

# Defend Constantinople
KoSJ_defend_byz = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    KNI = {
      NOT = { war_with = TUR }
    }
    151 = {
      owner = {
        religion_group = christian
        war_with = TUR
      }
    }
  }
  abort = {
	  OR = {
      151 = {
        owner = {
          OR = {
            religion_group = muslim
            vassal_of = TUR
          }
        }
  	  }
      151 = {
        owner = {
          religion_group = christian
          NOT = { war_with = TUR }
        }
      }
  	}
  }
  success = {
    KNI = {
      war_with = TUR
    }
    151 = {
      owner = {
        religion_group = christian
        war_with = TUR
      }
    }
  }
	chance = {
		factor = 50
	}
	effect = {
	  prestige = 0.05
	  treasury = 10
	  manpower = 1
	}
}

# Blockade Berber Country
KoSJ_blockade_MOR = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { has_country_modifier = KoSJ_raid_ports }
    naval_tech = 7
    OR = {                                                # REQUIRES MINIMUM NAVAL FORCE VALUE TO BLOCKADE PORTS
        big_ship = 2
        light_ship = 4
        galley = 8
        AND = { galley = 4 big_ship = 1 }
        AND = { galley = 4 light_ship = 2 }
        AND = { galley = 6 light_ship = 1 }
        AND = { light_ship = 2 big_ship = 1 }
        AND = { galley = 2 light_ship = 1 big_ship = 1}
        AND = { galley = 4 big_ship = 1}
				AND = { big_ship = 2}
				AND = { light_ship = 3 galley = 2}
		}
	  exists = MOR
    MOR = {
      num_of_ports = 5
    }
  }
  success = {
      MOR = {
        war_with = KNI
        blockade = 0.8
      }
  }
	chance = {
		factor = 100
	}
	effect = {
	  treasury = 25
	  add_country_modifier = { 
      name = KoSJ_raid_ports
      duration = 3600
    }
	}
}
KoSJ_blockade_TRP = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { has_country_modifier = KoSJ_raid_ports }
    naval_tech = 7
    OR = {                                                # REQUIRES MINIMUM NAVAL FORCE VALUE TO BLOCKADE PORTS
        big_ship = 2
        light_ship = 4
        galley = 8
        AND = { galley = 4 big_ship = 1 }
        AND = { galley = 4 light_ship = 2 }
        AND = { galley = 6 light_ship = 1 }
        AND = { light_ship = 2 big_ship = 1 }
        AND = { galley = 2 light_ship = 1 big_ship = 1}
        AND = { galley = 4 big_ship = 1}
				AND = { big_ship = 2}
				AND = { light_ship = 3 galley = 2}
		}
	  exists = TRP
    TRP = {
      num_of_ports = 5
    }
  }
  success = {
      TRP = {
        war_with = KNI
        blockade = 0.8
      }
  }
	chance = {
		factor = 100
	}
	effect = {
	  treasury = 25
	  add_country_modifier = { 
      name = KoSJ_raid_ports
      duration = 3600
    }
	}
}
KoSJ_blockade_TUN = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { has_country_modifier = KoSJ_raid_ports }
    naval_tech = 7
    OR = {                                                # REQUIRES MINIMUM NAVAL FORCE VALUE TO BLOCKADE PORTS
        big_ship = 2
        light_ship = 4
        galley = 8
        AND = { galley = 4 big_ship = 1 }
        AND = { galley = 4 light_ship = 2 }
        AND = { galley = 6 light_ship = 1 }
        AND = { light_ship = 2 big_ship = 1 }
        AND = { galley = 2 light_ship = 1 big_ship = 1}
        AND = { galley = 4 big_ship = 1}
				AND = { big_ship = 2}
				AND = { light_ship = 3 galley = 2}
		}
	  exists = TUN
    TUN = {
      num_of_ports = 5
    }
  }
  success = {
      TUN = {
        war_with = KNI
        blockade = 0.8
      }
  }
	chance = {
		factor = 100
	}
	effect = {
	  treasury = 25
	  add_country_modifier = { 
      name = KoSJ_raid_ports
      duration = 3600
    }
	}
}
KoSJ_blockade_ALG = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { has_country_modifier = KoSJ_raid_ports }
    naval_tech = 7
    OR = {                                                # REQUIRES MINIMUM NAVAL FORCE VALUE TO BLOCKADE PORTS
        big_ship = 2
        light_ship = 4
        galley = 8
        AND = { galley = 4 big_ship = 1 }
        AND = { galley = 4 light_ship = 2 }
        AND = { galley = 6 light_ship = 1 }
        AND = { light_ship = 2 big_ship = 1 }
        AND = { galley = 2 light_ship = 1 big_ship = 1}
        AND = { galley = 4 big_ship = 1}
				AND = { big_ship = 2}
				AND = { light_ship = 3 galley = 2}
		}
	  exists = ALG
    ALG = {
      num_of_ports = 5
    }
  }
  success = {
      ALG = {
        war_with = KNI
        blockade = 0.8
      }
  }
	chance = {
		factor = 100
	}
	effect = {
	  treasury = 25
	  add_country_modifier = { 
      name = KoSJ_raid_ports
      duration = 3600
    }
	}
}
KoSJ_blockade_MAM = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { has_country_modifier = KoSJ_raid_ports }
    naval_tech = 7
    OR = {                                                # REQUIRES MINIMUM NAVAL FORCE VALUE TO BLOCKADE PORTS
        big_ship = 2
        light_ship = 4
        galley = 8
        AND = { galley = 4 big_ship = 1 }
        AND = { galley = 4 light_ship = 2 }
        AND = { galley = 6 light_ship = 1 }
        AND = { light_ship = 2 big_ship = 1 }
        AND = { galley = 2 light_ship = 1 big_ship = 1}
        AND = { galley = 4 big_ship = 1}
				AND = { big_ship = 2}
				AND = { light_ship = 3 galley = 2}
		}
	  exists = MAM
    MAM = {
      num_of_ports = 10
    }
  }
  success = {
      MAM = {
        war_with = KNI
        blockade = 0.8
      }
  }
	chance = {
		factor = 100
	}
	effect = {
	  treasury = 100
	  add_country_modifier = { 
      name = KoSJ_raid_ports
      duration = 3600
    }
	}
}
KoSJ_blockade_TUR = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { has_country_modifier = KoSJ_raid_ports }
    naval_tech = 7
    OR = {                                                # REQUIRES MINIMUM NAVAL FORCE VALUE TO BLOCKADE PORTS
        big_ship = 2
        light_ship = 4
        galley = 8
        AND = { galley = 4 big_ship = 1 }
        AND = { galley = 4 light_ship = 2 }
        AND = { galley = 6 light_ship = 1 }
        AND = { light_ship = 2 big_ship = 1 }
        AND = { galley = 2 light_ship = 1 big_ship = 1}
        AND = { galley = 4 big_ship = 1}
				AND = { big_ship = 2}
				AND = { light_ship = 3 galley = 2}
		}
	  exists = TUR
    TUR = {
      num_of_ports = 10
    }
  }
  success = {
      TUR = {
        war_with = KNI
        blockade = 0.8
      }
  }
	chance = {
		factor = 100
	}
	effect = {
	  treasury = 100
	  add_country_modifier = { 
      name = KoSJ_raid_ports
      duration = 3600
    }
	}
}
KoSJ_defender_faith = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    NOT = { defender_of_faith = yes }
    religion = catholic
    OR = {
      prestige = 0.5
      treasury = 1000
      check_variable = { which = KoSJ_priories value = 37 }
    }
    check_variable = { which = KoSJ_priories value = 20 }
  }
  success = {
    defender_of_faith = yes
  }
	chance = {
		factor = 100
	}
	effect = {
	  add_country_modifier = { 
      name = KoSJ_golden_age
      duration = 3650
    }
	}
}
KoSJ_holy_land = {
  type = country
  allow = {
    tag = KNI
    NOT = { ai = yes }
    or = {
      defender_of_faith = yes
      check_variable = { which = KoSJ_priories value = 42 }
    }
    check_variable = { which = KoSJ_priories value = 20 }
    NOT = { owns = 379 }
  }
  success = {
    owns = 379
  }
	chance = {
		factor = 100
	}
	effect = {
	  add_core = 379
	  capital = 379
	  379 = {
      add_province_modifier = {
  			name = "KoSJ_FORT5"
  			duration = -1
  		}
    }
	}
}
