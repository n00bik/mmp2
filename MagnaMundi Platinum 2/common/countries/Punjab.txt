#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 187  79  86 }

historical_ideas = {
	national_conscripts
	humanist_tolerance
	church_attendance_duty
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	reformed_mughal_musketeer
	punjabi_irregulars
	sikh_hit_and_run
	bhonsle_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Jassa Singh Ahuwalia #1" = 50
	"Lahina Singh #1" = 50
}

leader_names = {
	Bahadur
	Bhalla
	Das
	Dev
	Gahunia
	Gobind
	Granth
	Hoora
	Jhol
	Jhotti
	Krishan
	Lohaar
	Parmanand
	Rajsingh
	Shergill
	Singh
	Sodhi
	Sohal
	Rai
	Udasi
}

ship_names = {
	Beas Chenab Devi Gayatri Guru Indus
	Jalsena Jhelum Lakshmi Nausena
	"Nav ka Yudh" Parvati Radha Ratri
	Ravi Sagar Sutlej Sarasvati Sita
}
