#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 231  224  107 }

historical_ideas = {
	national_conscripts
	merchant_adventures
	superior_seamanship
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	reformed_mughal_musketeer
	punjabi_irregulars
	sikh_hit_and_run
	bhonsle_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Ahmad #1" = 20
	"Dawud Khan #0" = 10
	"Mahmud #0" = 20
	"Muzaffar #1" = 20
	"Bhadur #0" = 10
}

leader_names = {
	Desai
	Jain
	Gandhi
	Majmudar
	Munshi
	Muzzafar
	Parikh
	Pandya
	Patel
	Patidar
	Vanik
	Vyas
	Rushi
	Saraiya
	Shaw
	Shrivastav
	Tilak
}

ship_names = {
	Ambika Auranga Bhadar Damanganga
	Devki Jalsena "Kala Jahazi" Khari
	"Lal Jahazi" Luni Mahi Mithi
	Nausena Narmada "Nav ka Yudh"
	"Nila Jahazi" Purna Sabarmati
	Sagar Saraiya Saraswati Shetrunji
	Tapi Yasmin
}
