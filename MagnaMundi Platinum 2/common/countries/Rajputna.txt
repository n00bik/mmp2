#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 119  151  122 }

historical_ideas = {
	national_conscripts
	military_drill
	patron_of_art
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	reformed_mughal_musketeer
	punjabi_irregulars
	sikh_hit_and_run
	bhonsle_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Udha Rao #0" = 10
	"Chandrasena #0" = 10
	"Prithvi #0" = 10
	"Bhima #0" = 10
	"Ratan #0" = 10
	"Baharmalla #0" = 10
	"Bragwan Das #0" = 10
	"Man Singh #0" = 10
	"Jagat Singh #0" = 10
	"Bhao Singh #0" = 10
	"Jaya Singh #0" = 10
	"Rama Singh #0" = 10
	"Bishan Singh #0" = 10
	"Ishwari Singh #0" = 10
	"Mahdu Singh #0" = 10 
}

leader_names = {
	Rajput
	Babber
	Bargujars
	Bhoja
	Chandelas
	Chauhan
	Jadeja
	Kachwaha
	Karmavati
	Khumba
	Paramaras
	Pratap
	Rathore
	Singh
	Tomaras
	Mihirbhoj
	Mahipala
	Nagabhatta
	Padmini
	Vaghela
	Vatsraja
}

ship_names = {
	Chambal Devi Durga Gayatri Ghaggar
	Godwar Jalsena "Kala Jahazi" Lakshmi
	"Lal Jahazi" Luni Nausena "Marwar ka Nav"
	"Merwar ka Nav" "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Rani Ratri Sagar Sarasvati
	Sekhawati Sita
}
