#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 139  138  160}

historical_ideas = {
	national_conscripts
	divine_supremacy
	church_attendance_duty
}

historical_units = { #optimized by berto#
	ottoman_yaya
	ottoman_azab
	ottoman_sekban
	ottoman_janissary
	tofongchis_musketeer
	ottoman_reformed_janissary
	durrani_rifled_musketeer
	near_eastern_offensive_drill
	ottoman_new_model
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	muslim_cavalry_archers
	ottoman_musellem
	ottoman_timariot
	derbent_cavalry
	ottoman_spahi
	suvarileri_cavalry
	mongol_swarm
	ottoman_toprakli_dragoon
	durrani_swivel
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Kuchuk Muhammad #0" = 40
	"Mahmud #0" = 80
	"Ahmad #0" = 50
	"Sayid Ahmad #1" = 60
	"Baraq #0" = 30
	"Devlat Berdi #0" = 20
	"Ulug Muhammad #0" = 40
	"Jabbar Berdi #0" = 20
        "Shaik Ahmad #0" = 20
        "Murtada #0" = 20
}

leader_names = { 
	Balna Salmiianov Ravna Taiborey Vylka
	Kanin Maimbava Arinin Aven Tsoy
}

ship_names = {
	"Ghengis Khan" Orda Kochu Bayan Sasibuga
	Ilbasan "Mubarrak Kwaja" Chimtai Urus Tuqtaqiya
	"Timur Malik" Toqtamish "Timur Qutlugh" "Shadi Beg"
	Pulad "Jalal Ad-Din" "Karim Berdi" Kebek "Jabbar Berdi"
	"Ulugh Muhammad" "Devlat Berdi" Baraq "Sayyid Ahmad"
}
