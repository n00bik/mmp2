#Country Name: Please see filename.

graphical_culture = latingfx

color = { 101  102  163 }

historical_ideas = {
	superior_seamanship
	national_conscripts
	national_trade_policy
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

leader_names = {
	Alberti �lvarez "de Alvear" Artigas Azcuenaga Balcarce Baz�n Belgrano Brown Bustos
	Cabral Castro Chiclana Costa Derqui Dorrego Gonz�lez Goyeneche Guazurary "de G�emes"
	Heredia Jonte "Kaunitz de Holmberg" "de Laprida" Larrea "de las Heras" L�pez
	Mansilla Matheu Monteagudo Moreno Paso Paz Pe�a P�rez Planes Posadas "de Pueyrred�n"
	Ram�rez Riglos Robinson Rodr�guez Rondeau "de Rosas"
	Saavedra S�enz "de San Mart�n" Sarratea Urquiza Warnes
}

monarch_names = {
	"Jos� #0" = 20
	"Julio #0" = 20
	"Carlos #0" = 10
	"Mart�n #0" = 10
	"Vicente #0" = 5
	"Fernando #0" = 5
	"Agust�n #0 Pedro" = 0
        "Alejandro #0" = 0
	"Alfredo #0" = 0
	"Arturo #0" = 0
	"Baltasar #0" = 0
        "Bartolom� #0" = 0
	"Bernardino #0" = 0
        "Domingo #0" = 0
	"Edelmiro #0" = 0
	"Eduardo #0" = 0
	"H�ctor Jos� #0" = 0
	"Hip�lito #0" = 0
	"Jorge #0 Rafael" = 0
	"Jos� #0 Domingo" = 0
        "Jos� #0 Evaristo" = 0
	"Jos� #0 F�lix" = 0
	"Jos� #0 Mar�a" = 0
	"Juan Carlos #0" = 0
	"Juan #0 Domingo" = 0
        "Juan #0 Jos�" = 0
	"Justo #0 Jos�" = 0
	"Leopoldo #0" = 0
	"Luis #0" = 0
	"Marcelo #0" = 0
	"Miguel #0" = 0
	"Nicol�s #0" = 0
        "Pedro #0 Eugenio" = 0
	"Pedro #0 Pablo" = 0
	"Ram�n #0" = 0
	"Ra�l #0 Alberto" = 0
	"Roberto #0" = 0
	"Roque #0" = 0
	"Santiago #0" = 0
	"Victorino #0" = 0
}

ship_names = {
	"Buenos Aries"
	"Diego Armando Maradona"
	"El Plata"
	"General Belgrano"
	Independencia
	Libertad "Los Andes"
	Moreno 
	Patagonia
	Rivalda
}
	
