#Country Name: Please see filename.

graphical_culture = latingfx

color = { 152  85  92 }

historical_ideas = {
	battlefield_commisions
	humanist_tolerance
	patron_of_art
}

historical_units = { #optimized by berto#
	bardiche_infantry
	eastern_medieval_infantry
	hussite_draby
	tabor_infantry
	polish_musketeer
	polish_tercio
	saxon_infantry
	eastern_carabinier
	russian_mass
	eastern_european_columnar_infantry
	eastern_european_impulse_infantry
	eastern_european_breech_loaded_rifleman
	druzhina_cavalry
	eastern_knights
	strzelcy_cavalry
	hungarian_hussar
	polish_hussar
	polish_winged_hussar
	eastern_uhlan
	muscovite_cossack
	russian_cossack
	eastern_skirmisher
	eastern_european_light_hussar
	eastern_european_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
 "Ul�szl� #0" = 40
 "L�szlo #4" = 20
 "Lajos #1" = 20
 "Albert #0" = 20
 "J�nos #0" = 20
 "M�ty�s #0" = 20
 "Zsigmond #0" = 20
 "Andr�s #3" = 15
 "K�roly #2" = 15
 "Istv�n #5" = 10
 "Anna #0" = -10
 "B�la #4" = 5
 "Anaszt�z #0" = 0
 "Barnab�s #0" = 0
 "Betalan #0" = 0
 "Boldizs�r #0" = 0
 "D�vid #0" = 0
 "D�m�t�r #0" = 0
 "Edv�rd #0" = 0
 "Ern� #0" = 0
 "Ferenc #0" = 0
 "G�bor #0" = 0
 "Gy�rgy #0" = 0
 "Izs�k #0" = 0
 "Jen� #0" = 0
 "J�szef #0" = 0
 "K�lm�n #0" = 0
 "K�zm�r #0" = 0
 "Kelemen #0" = 0
 "L�rinc #0" = 0
 "Luk�cs #0" = 0
 "M�rk #0" = 0
 "Mikl�s #0" = 0
 "Oszk�r #0" = 0
 "Otto #0" = 0
 "Patrik #0" = 0
 "P�ter #0" = 0
 "Simon #0" = 0
 "Tam�s #0" = 0
 "Vencel #0" = 0
}

leader_names = {
 Altal Andr�s Andr�ssy Alvinczy Ap�ti Andornok Aracsi �rp�d
 Babos Bagdy Balassi Baranyi Barcza B�rczi B�rdos B�rdossy B�rk�ny Baross Bebek  B�g�nyi Benyhe Berecz Berger Bern�thfalvi Bern�th Bernt  B�r� Blaskovich Bogyay Bohuss  Bok� Borb�th Borbola csal�d Boros Bozs� B�r�cz Br�jer Bokor Bocskay Bethlen B�thory  Batthy�ny Benovsk� Beleznay
 Cs�k Cholnoky Cs�goly Cs�ky Csan�di Cs�ny Cseh Csehszombaty Csiba Csibra Csiffary  Csora Cz�kus 
 Dalotti Dencz Domj�n Domoszlai D�rnyei Draveczky Dessewffy
 Egyedi Einczinger Elekes Ercsey Erd�s Eszterh�zy 
 F�bry Fahidi Farag� Farkas Fazekas Fazi Fetter Forg� Frank Fr�ter Festetics Frangep�n
 Gabler Galaczy Galg�czy Garay G�sp�r Geiger G�kler Grell Gryllus 
 Halmaj Hausner Heller csal�d Hermann csal�d Hertelendy Hettyey Hevesi Hunyadi Hadik  Horthy
 J�kai 
 K�d�r K�lm�nczey Kanizsai Kar�tsonyi Kasnyik Kem�ny Kereszt�ry Keszler Kinsky  Kish�zy Kiss-Jankovics Klebercz Kolossvary Konkoly Kosztol�nyi Kovach K�rtv�lyessy  Kriza Kinizsi K�rolyi K�n Kov�cs
 Laczk� Laczkovics Lak Latinovics L�nyay Lackovic' Losonci
 Magony Manninger Massalsky Matarics Mattyasovszky Medny�nszky Medveczky  Mesterh�zi Mihalik M�sz�ros Milv�nyi
 N�dasdy Neh�z Nemes Ny�r�dy N�meth
 Ozoray �v�ri
 Pal�sthy P�llfy P�lmai P�l�czi-Horv�t P�chy Pecznyik Pet� Petrenyi Petrics Pongr�cz  
 Rajcs�nyi Rakovszky Reinisch Rekv�nyi Roczhlitz R�k�czi Rozgonyi
 Sasovits Schlezinger Schloss Siskovich Somfai Stiegler-G�sp�r Strilich Sulyok Szabari  Szab� Sz�sz Sz�chenyi Sz�kely Szelepcs�nyi  
 Szentp�ly Szigethy Szopory Sztr�nyay �ubic' 
 T�borosi Tak�ch Terray Tisza Toldy Tomassich Tomboly T�th Turcs�n 
 Th�k�ly Teleki
 Ujfalussy 
 V�rdai Varga V�csey Vir�gh
 Wessel�nyi
 Zacher Z�nyi Zerkovitz Zichy Zigler Zsemberi Zr�nyi Z�polya
}

ship_names = {
 Aranybulla 
 �rp�d
 Magyar
 Zsigmond
 "Szent Istv�n kir�ly"
 "Hunyadi M�ty�s"
 "K�roly R�bert"
 "Nagy Lajos"
 L�szl�
 K�lm�n
}
