# Wurttemberg

graphical_culture = latingfx

color = { 111  162  128 }

historical_ideas = {
	national_conscripts
	battlefield_commisions
	cabinet
}

monarch_names = {
	"Eberhard #0" = 60
	"Ulrich #0" = 40
	"Ludwig #2" = 20
	"Christoph #0" = 20
	"Eberhard #0 Ludwig" = 20
	"Friedrich #0" = 20
	"Johann Friedrich #0" = 20
	"Karl #0 Alexander" = 20
	"Karl #0 Eugen" = 20
	"Wilhelm Ludwig #0" = 20
	"Ludwig #2 Eugen" = 10
	"Ludwig #2 Friedrich" = 10
	"Friedrich Ludwig #0" = 10
	"Georg #0" = 10
	"Heinrich #0" = 10
	"August #0" = 5
	"Christian Eberhard #0" = 5
	"Eberhard #0 Friedrich" = 5
	"Friedrich #0 Eugen" = 5
	"Friedrich Achilles #0" = 5
	"Georg Friedrich #0" = 5
	"Joachim Friedrich #0" = 5
	"Julius Friedrich #0" = 5
	"Magnus #0" = 5
	"Maximilian #0" = 5
	"Philipp Friedrich #0" = 5
	"Adolf #0" = 0
	"Bruno #0" = 0
	"Dieter #0" = 0
	"Emanuel #0" = 0
	"Florian #0" = 0
	"Gregor #0" = 0
	"Hartmann #0" = 0
	"Joachim #0" = 0
	"Kreszenz #0" = 0
	"Manfred #0" = 0
	"Pascal #0" = 0
	"Raimund #0" = 0
	"Siegbert #0" = 0
	"Tobias #0" = 0
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

leader_names = {
	Breitmayer Danhauser Gohl Gute Helber Henke 
	Kircher Klein Knapp Kopp Meck Roether Schupp
	St�tz S�lzle Veit Vietz Vogel Weber Wieland Wurst
}

ship_names = {
	Wirtemberg "Konrad Wirtemberg" "Graf Ulrich I"
	"Gr�fin Mechthild" Stuttgart "Graf Ulrich III"
	"Graf Eberhard III" "Gr�fin Henriette"
	Marbach Neuffen Balingen M�mpelgard
}

army_names = {
	"Armee von $PROVINCE$" 
}
