#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 26  27  154 }

historical_ideas = {
	national_conscripts
	military_drill
	humanist_tolerance
}

historical_units = { #optimized by berto#
	persian_footsoldier
	mamluk_duel
	persian_shamshir
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	persian_rifle
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	persian_cavalry_charge
	muslim_cavalry_archers
	ottoman_timariot
	topchis_artillery
	qizilbash_cavalry
	ottoman_toprakli_hit_and_run
	mongol_swarm
	mamluk_musket_charge
	ali_bey_reformed_infantry
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Sayid Mahmud #0" = 20
	"Qasim #0" = 60
	"Abdul-Karim #0" = 40
	"Jani Beg #0" = 20
	"Husayn #0" = 30
	"Ak K�bek #0" = 40
	"Abdul-Rahman #0" = 40
	"Darwish Ali #0" = 30
	"Shayk Haidar #0" = 20
	"Yagmurchi #0" = 20
}

leader_names = { 
	"Qara Chinua" "Delger Qyugun" "Arigh Gal" "Koke Erdene"
	"Suren Unegen" "Tegus Enq" Qadandei Burilgigene Arighteni
	Yekecher Surenqai Qorchigge
}

ship_names = {
	Xacitarxan Volga �sterxan Khadjitarkhan
	"Chinggis Khan" Kuchuk Actarxan 
	"Sarai Batu" "Hadzhy-Tarkhan" 
	Gitorkhan Tsitrokhan
}
