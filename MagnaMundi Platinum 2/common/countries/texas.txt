#Country Name: Please see filename.

graphical_culture = latingfx

color = { 125  127  38 }

historical_ideas = {
	national_conscripts
	merchant_adventures
	military_drill
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_longbow
	gaelic_galloglaigh
	gaelic_free_shooter
	irish_charge
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	anglofrench_line
	british_square
	napoleonic_square
	french_impulse
	mixed_order_infantry
	western_medieval_knights
	chevauchee
	french_caracolle
	french_dragoon
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"George #0" = 20
	"James #0" = 20
	"John #0" = 20
	"Thomas #0" = 20
	"Charles #0" = 10
	"Daniel #0" = 10
	"Henry #0" = 10
	"Samuel #0" = 10
	"Arthur #0" = 5
	"Cyrus #0" = 5
	"David #0" = 5
	"Elias #0" = 5
	"Frederick Augustus #0" = 5
	"Nathaniel #0" = 5
	"Peyton #0" = 5
	"Richard #0" = 5
	"Aaron #0" = 0
	"Angus #0" = 0
	"August #0" = 0
	"Benjamin #0" = 0
	"Calvin #0" = 0
	"Darren #0" = 0
	"Elbridge #0" = 0
	"Erick #0" = 0
	"Francis #0" = 0
	"Frederick #0" = 0
	"Isaac #0" = 0
	"Jonathan #0" = 0
	"Joseph #0" = 0
	"Kyran #0" = 0
	"Luke #0" = 0
	"Milton #0" = 0
	"Patrick #0" = 0
	"Paul #0" = 0
	"Philip #0" = 0
	"Randolf #0" = 0
	"Silvester #0" = 0
	"Thimoty #0" = 0
	"Wilbur #0" = 0
	"William #0" = 0
}

leader_names = {
	Houston 
	Austin
	Clinton
	Jay Jefferson 
	Harrison
	King
	Langdon
	Madison Monroe 
	Pickney
	Rutledge
}

ship_names = {
	Adams Alfred Allaince America "Andrew Doria" Argo Argus
	"Bohomme Richard" Boston 
	Cabot Galveston Champion Columbus Confederacy Congress Constellation Constitution
	Dolphin Deane Houston Diligent 
	Effingham Enterprise Essex
	Fly
	"General Houston" "General Washington"
	Hampden Hancock Hornet
	Independence 
	"Sam houston"
	Lexington
	Montgomery Mosquito
	Nautilus
	Philadelphia Pigot President Providence
	"Queen of France"
	Raleigh Randolph Racehorse Ranger Reprisal Repulse Resistance Retaliation Revenge
	Sachem Saratoga Serapis Siren Surprise
	Trumbull
	"Texas"
	Viper Vixen
	Warren Washington Wasp 
}
