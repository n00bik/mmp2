#Norway 

graphical_culture = latingfx

color = { 117  165  188 }

historical_ideas = {
	superior_seamanship
	national_conscripts
	bill_of_rights
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	swedish_caroline
	prussian_drill
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_dragoon
	french_cuirassier
	swedish_arme_blanche
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Christian #0" = 60
	"H�kon #6" = 40
	"Magnus #7" = 20
	"Olav #4" = 20
	"Erik #3" = 20
	"Frederik #0" = 20
	"Hans #0" = 20
	"Harald #4" = 20
	"Inge #2" = 5
	"Sigurd #2" = 5
	"Aksel #0" = 0
	"Alf #0" = 0
	"Anders #0" = 0
	"Ansgard #0" = 0
	"Arvid #0" = 0
	"Bernt #0" = 0
	"Birger #0" = 0
	"Bj�rn #0" = 0
	"Christoffer #0" = 0
	"Daniel #0" = 0
	"Eilert #0" = 0
	"Einar #0" = 0
	"Elias #0" = 0
	"Esben #0" = 0
	"Folke #0" = 0
	"Kasper #0" = 0
	"Kennet #0" = 0
	"Knut #0" = 0
	"Ludvig #0" = 0
	"Mats #0" = 0
	"Mikael #0" = 0
	"Nj�ld #0" = 0
	"Njord #0" = 0
	"Peder #0" = 0
	"Rikard #0" = 0
	"Sigmund #0" = 0
	"Simon #0" = 0
	"S�ren #0" = 0
	"Tollak #0" = 0
	"Torhild #0" = 0
}

leader_names = {
	Arnesson Amundsen 
	Bjelke Bolt 
	Egilsson Eriksson 
	Haraldsson 
	Larsson Leifsson 
	Magnusson 
	Nansen Norheim 
	Olavsson 
	Sverresson Sigurdsson 
	Torgeirsson Torsson 
}

ship_names = {
	�lvik
	Bergen
	Engan
	Kristiansund
	Molde
	Oslo
	Roan
	Stavanger Sula
	Trondheim
}
