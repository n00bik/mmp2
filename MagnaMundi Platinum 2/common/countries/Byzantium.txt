#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 110  35 135 } 

historical_ideas = {
	national_conscripts
	espionage
	divine_supremacy
}

historical_units = { #optimized by berto#
	bardiche_infantry
	eastern_medieval_infantry
	eastern_militia
	strel'tsy_infantry
	muscovite_musketeer
	muscovite_soldaty
	russian_petrine
	russian_green_coat
	russian_mass
	eastern_european_columnar_infantry
	eastern_european_impulse_infantry
	eastern_european_breech_loaded_rifleman
	eastern_knights
	druzhina_cavalry
	strzelcy_cavalry
	hungarian_hussar
	tatar_cossack
	zaporoghian_cossack
	russian_cuirassier
	muscovite_cossack
	russian_cossack
	russian_lancer
	eastern_european_light_hussar
	eastern_european_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Konstantinos #11" = 20
	"Ioannes #8" = 20
	"Manuel #2" = 5
	"Andronikos #4" = 5
	"Michael #8" = 10
	"Alexeios #4" = 5
	"Zoe #1" = -5
	"Georgios #0" = 0
	"Aineias #0" = 0
	"Alexandros #0" = 0
	"Ambrosios #0" = 0
	"Aristoteles #0" = 0
	"Bartholomaios #0" = 0
	"Chrysanthos #0" = 0
	"Diogenes #0" = 0
	"Dionysos #0" = 0
	"Eirenaios #0" = 0
	"Elpidios #0" = 0
	"Gregorios #0" = 0
	"Hektor #0" = 0
	"Herakles #0" = 0
	"Hermes #0" = 0
	"Iason #0" = 0
	"Iosephos #0" = 0
	"Kastor #0" = 0
	"Kreon #0" = 0
	"Matthaios #0" = 0
	"Nestor #0" = 0
	"Nikolaos #0" = 0
	"Nikomedes #0" = 0
	"Orestes #0" = 0
	"Palaemon #0" = 0
	"Pavlos #0" = 0
	"Pelagios #0" = 0
	"Petros #0" = 0
	"Philemon #0" = 0
	"Philippos #0" = 0
	"Prokopios #0" = 0
	"Stefanos #0" = 0
	"Theodoros #0" = 0
	"Timotheos #0" = 0
}

leader_names = {
	Kamil
	Akyel
	Atan
	Yattara
	Cimsir
	Sungur
	Bulut
	Riza
	Zengin
	Said
}

ship_names = {
	Amisus Anna "Alexios I" "Andronikos I" 
	Basileios
	Chersonesos
	"Heraclea Pontica"
	Irene
	"Ioannes I" "Ioannes II"
	Kersh
	"Manouel I"
	Perateia Paphlagonia
	Sinope Sougdeia
	Trebizond "Theodora"
}
