#Desmond

graphical_culture = latingfx

color = { 104  137  140 }

historical_ideas = {
	excellent_shipwrights
	church_attendance_duty
	national_trade_policy
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_men_at_arms
	gaelic_galloglaigh
	gaelic_free_shooter
	irish_charge
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	scottish_highlander
	austrian_grenzer
	prussian_fredrickian
	napoleonic_square
	french_impulse
	mixed_order_infantry
	western_medieval_knights
	chevauchee
	swedish_galoop
	french_dragoon
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Tom�s #3" = 30
	"Gear�id #1" = 30
	"S�amas #0" = 40
	"Annraoi #0" = 20
	"Se�n #2" = 20
	"�amonn #0" = 10
	"Risteard #0" = 10
	"Seathr�n #0" = 10
	"S�om�n #0" = 10
	"St�of�n #0" = 10
	"Uilliam #0" = 0
	"Aed #0" = 0
	"Colum #0" = 0
	"Diarmaid #0" = 0
	"Domhnall #0" = 0
	"Eoghan #0" = 0
	"Flann #0" = 0
	"Niall #0" = 0
	"P�draig #0" = 0
	"Riain #0" = 0
	"Ruaidhr� #0" = 0
	"Tighearnach #0" = 0
	"Toirdhealbach #0" = 0
	"Tuathal #0" = 0
	"Uinseann #0" = 0
}

ship_names = {
	"Ailill Aulomm" Ao�fe "Ard R�"
	"Brian B�ruma"
	Caisel "Candida Casa"
	"D�re Cerbba" Dochar 
	"Eoghan M�r"
	Fiacha
	Lugaid
	"Rua Martra"
	S�ildub�n
	"T�naise R�g" Teamhair
}  

leader_names = {
	"Mac Gearailt" "Mac Gearailt" "�g Mac Gearailt" "M�r Mac Gearailt"
	"De Barra" "� Boill" "Breathnach" "� Coile�in" "� Cruadhlaoich" "� Dorchaidhe"
	"� Failbhe" "� Flannabhra" "Mac Giolla Mochuda" "� hAodha" "� hUallanch�in"
	"� Meachair" "Mac Tom�is" "Paor" "� Suilleabhain" "� Seasn�in" "� Tuama"
}
