#Country Name: Please see filename.

graphical_culture = latingfx

color = { 57  160  101 }

historical_ideas = {
	superior_seamanship
	national_trade_policy
	national_conscripts	
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_men_at_arms
	gaelic_galloglaigh
	gaelic_free_shooter
	irish_charge
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	scottish_highlander
	austrian_grenzer
	prussian_fredrickian
	napoleonic_square
	french_impulse
	mixed_order_infantry
	western_medieval_knights
	chevauchee
	swedish_galoop
	french_dragoon
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

leader_names = {
	Annesley Alexander
	Butler "de Burgh" Burke Boyle Browne Bourke Barry Balfour Blayney
	Cholmondeley Chichester Coote Caulfeild
	Dillon Dongan Duff Digby
	FitzGerald FitzWilliam FitzOliver Feilding FitzMaurice FitzPatrick Fortescue
	Guelph Gardiner Grimston
	Hamilton Hare Hervey
	Keating Keane Kavanagh
	Lambart
	Martyn McCarthy Moore Meade Macartney
	Nugent
	O'Neill O'Brien O'Connell O'Hara O'Malley O'Donnell O'Carroll 
	Parsons Pakenham Plunkett Preston
	Ridgeway
	Sterne
	Touchet Taaffe
	Vaughan Villiers
	Wentworth Wesley
}

monarch_names = {
	"Turlough #2" = 60
	"Donal #5" = 40
	"Muireadhach #2" = 40
	"Conchobhar #1" = 40
	"Feidlimidh #1" = 40
	"Cathaoir #0" = 40
	"Muchadh #0" = 40
	"�ed #6" = 20
	"Fonnchadh #3" = 20
	"Brian #2" = 20
	"Criomhthann #2" = 20
	"Art #1" = 20
	"Cathal #0" = 20
	"Gear�id #0" = 20
	"Muiris #0" = 20		
	"Tadhg #0" = 20
	"Se�n #0" = 20
	"Niall #4" = 10
	"Einr� #0" = 10
	"Conn #1" = 5
	"Eoghan #1" = 5
	"Aonghus #2" = 1
	"Comhghall #2" = 1
	"Maeleachlain #2" = 1
	"Diarmaid #1" = 1
	"Flann #1" = 1
	"Aerdghal #0" = 0
	"Bearach #0" = 0
	"Cao�mhin #0" = 0
	"Donnabhain #0" = 0
	"E�hmh�n #0" = 0
	"Fearghal #0" = 0
	"Iarfhlaith #0" = 0
	"Lorcc�n #0" = 0
	"Math�in #0" = 0
	"Naomh�n #0" = 0
	"Ois�n #0" = 0
	"P�draig #0" = 0
	"R�ag�in #0" = 0
	"Somhairle #0" = 0
}

ship_names = {
	Aoife
	Ciara Connacht
	Eithne Emer
	Leinster
	Munster
	Niamh
	Orla
	Roisin
	Ulster
} 
