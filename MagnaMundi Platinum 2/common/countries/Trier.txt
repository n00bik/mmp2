#Country Name: Please see filename.

graphical_culture = latingfx

color = { 208  104  44 }

historical_ideas = {
	church_attendance_duty
	cabinet
	smithian_economics
}

monarch_names = {
	"Johann #1" = 100
	"Jakob #0" = 60
	"Clemens Wenzel #0" = 20
	"Franz Georg #0" = 20
	"Franz Ludwig #0" = 20
	"Johann Hugo #0" = 20
	"Johann Ludwig #0" = 20
	"Johann Philipp #0" = 20
	"Karl Joseph #0" = 20
	"Karl Kaspar #0" = 20
	"Lothar #0" = 20
	"Philipp Christoph #0" = 20
	"Richard #0" = 20
	"Bohemond #2" = 10
	"Kuno #2" = 10
	"Otto #1" = 10
	"Werner #1" = 10
	"Heinrich #3" = 5
	"Arnold #2" = 5
	"Dieter #1" = 5
	"Alexander #0" = 0
	"Andreas #0" = 0
	"Baldur #0" = 0
	"Bruno #0" = 0
	"Clemens #0" = 0
	"Denis #0" = 0
	"Eckhard #0" = 0
	"Ernst #0" = 0
	"Florian #0" = 0
	"Gregor #0" = 0
	"Guntram #0" = 0
	"Helmfried #0" = 0
	"Joachim #0" = 0
	"J�rgen #0" = 0
	"Martin #0" = 0
	"Maximilian #0" = 0
	"Michael #0" = 0
	"Rainer #0" = 0
	"Siegert #0" = 0
	"Siegmund #0" = 0
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

leader_names = {
	Backes Corgell Dahnfels Feltes Haerig Hecker Herrich
	Kettler Lohmann Longnich Pinkernell Schmitt Tristant
	Wellner Zimmer	
}

ship_names = {
	Schweich Kenn Longuich Mertesdorf Kasel
	Waldrach Morscheid Korlingen Gusterath
	Hockweiler Igel Aach Newel Kordel Zemmer
}

army_names = {
	"Erzbischofliche Garde" "Armee von $PROVINCE$" 
}
