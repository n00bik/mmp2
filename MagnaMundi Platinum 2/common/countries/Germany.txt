graphical_culture = latingfx

color = { 150  177  161 }

historical_ideas = {
	battlefield_commisions
	colonial_ventures
	military_drill
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Albrecht #1" = 20
	"Friedrich #2" = 20
	"Maximilian #0" = 40
	"Karl #4" = 60
	"Ferdinand #0" = 60
	"Rudolf #1" = 20
        "Joseph #0" = 40
        "Franz #0" = 40
        "Ludwig #4" = 10
        "Heinrich #7" = 10
}

leader_names = { 
	Adam Angermann Appelt Arndt Beck Becker Bening Blasius
	Brilz Br�ser B�ttner Carl Dahm Deneke Eckstein Edelmann 
	Elsner Feil Fischer Franke Giewer Haase Hafner Hertel 
	Hirsch H�hn Kammel Keller Kessler Koeppe Kohn Kr�mer
	Kuhn Lindner Mertesdorf Meyer Mick M�ller Necker Noell 
	Oelgem�ller Raabe Rainer Scharfbillig Schave Scheler 
	Schmitt Schneider Schumann Stephan Thelen Trierweiler
	Uhlig "von Franken" "von Rothenburg" "von Schwaben"
	"von Vitzgau" "von Weimar" Wachsmuth Wacht "von Werl"
	Walther Wecker
}

ship_names = {
	Ariadne Habicht Adler Albatross Amazone
	Arcona Basilisk Beowulf Biene Wespe Wolf
	Nautilus Natter Otter Pelikan Pfeil Meteor
	M�we M�cke Luchs K�nig Kaiser Eber Drache
	Falke Frauenlob Freya Fuchs Gazelle Geier
	Greif Condor Cormoran Comet Wacht Tiger
	Seeadler Jaguar Hy�ne Blitz Bremse Brummer
	Bussard Vulkan
}

army_names = {
	"Kaiserliche Armee" "Armee von $PROVINCE$" 
}

fleet_names = {
	"Kaiserliche Flotte"
}
