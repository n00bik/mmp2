#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 175  122  10 }

historical_ideas = {
	divine_supremacy
	national_conscripts
	church_attendance_duty
}

historical_units = { #optimized by berto#
	bashi_bazouk
	mamluk_duel
	persian_shamshir
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	persian_rifle
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	persian_cavalry_charge
	muslim_cavalry_archers
	mongolian_bow
	topchis_artillery
	qizilbash_cavalry
	ottoman_toprakli_hit_and_run
	mongol_swarm
	mamluk_musket_charge
	ali_bey_reformed_infantry
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Muhammad #0" = 10
	"Abd al Aziz #0" = 10
	"'Abdullah #0" = 10
	"Turki #0" = 10
	"Faisal #0" = 10
	"Abd Al-Rahman #0" = 10
	"Khalid #0" = 10
	"Fahd #0" = 10
}

leader_names = {
	"Al Sabah"
	"Al Farran"
	"Al Nubi"
	"Akil"
	Surur
	Atef
	Madani
	"Al Jaber"
	Billah
	Kanasani
}

ship_names = {
	"Al-Armah" "Ad-Dahna" "Jabal Tuwayo"
	"Naf�d Qunayfidahah" "Naf�d Shuqayyiqah"
	"Al-Bayad" "Naf�d Ad-Dahy" "Naf�d As-Sirrah"
	"Ar-Riyad" "Ad-Dilam" Shaqra
}
