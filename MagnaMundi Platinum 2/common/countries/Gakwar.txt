# Country name see file name.

graphical_culture = indiangfx

color = { 245  139  127 }

historical_ideas = {
	national_conscripts
	battlefield_commisions
	military_drill
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	reformed_mughal_musketeer
	punjabi_irregulars
	sikh_hit_and_run
	bhonsle_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Pilaji Rao #0" = 10
	"Damajo Rao #0" = 10
	"Govind Rao #0" = 10
	"Sayaji Rao #0" = 10
}

leader_names = {
	Nisheeth
	Sukarman
	Yateen
	Sitipala
	Vyapari
	Vishwa
	Tjakur
	Ramdas
	Prajna
	Lakhani
}

ship_names = {
	Agni Bhim Brahma Devi Durga
	Ganesh Gayatri Hanumaun Indra
	Jalsena "Kala Jahazi" Kali Kubera
	Krishna Lakshmi "Lal Jahazi"
	Nausena "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Ratri Sagar Sarasvati
	Shiva Sita Soma Surya Varuna Vayu
	Vishnu Yama
}
