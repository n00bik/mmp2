#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 131  141  142}

historical_ideas = {
	national_conscripts
	national_trade_policy
	smithian_economics	
}

historical_units = { #optimized by berto#
	bardiche_infantry
	eastern_medieval_infantry
	eastern_militia
	strel'tsy_infantry
	muscovite_musketeer
	muscovite_soldaty
	russian_petrine
	russian_green_coat
	russian_mass
	eastern_european_columnar_infantry
	eastern_european_impulse_infantry
	eastern_european_breech_loaded_rifleman
	eastern_knights
	druzhina_cavalry
	strzelcy_cavalry
	hungarian_hussar
	tatar_cossack
	zaporoghian_cossack
	russian_cuirassier
	muscovite_cossack
	russian_cossack
	russian_lancer
	eastern_european_light_hussar
	eastern_european_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Aleksandr #2" = 20
	"Ivan #1" = 10
	"Dmitriy #2" = 10
	"Semen #0" = 10
	"Fyodor #2" = 5
	"Vasiliy #2" = 5
	"David #1" = 5
	"Adrian #0" = 0
	"Afanasiy #0" = 0
	"Anatoliy #0" = 0
	"Arseniy #0" = 0
	"Daniil #0" = 0
	"Dorofey #0" = 0
	"Eduard #0" = 0
	"Faddey #0" = 0
	"Feodosiy #0" = 0
	"Feofil #0" = 0
	"Ferapont #0" = 0
	"Foka #0" = 0
	"Foma #0" = 0
	"Gavriil #0" = 0
	"Grigoriy #0" = 0
	"Igor #0" = 0
	"Kliment #0" = 0
	"Mefodiy #0" = 0
	"Mikhail #0" = 0
	"Naum #0" = 0
	"Nikita #0" = 0
	"Nikolai #0" = 0
	"Pankrati #0" = 0
	"Prokhor #0" = 0
	"Radoslav #0" = 0
	"Ruslan #0" = 0
	"Serafim #0" = 0
	"Sevastian #0" = 0
	"Stanislav #0" = 0
	"Timofeiy #0" = 0
	"Varfolomeiy #0" = 0
	"Vikentiy #0" = 0
	"Viktor #0" = 0
}

leader_names = { 
	Alabyshe Alenkin Belsky Velikogagin Golygin Dulov Deyev 
	Zhirovy Zaozersky Zasekin Zubaty Kubensky Kurbsky 
	Mulozhsky Mortkin Okhlyabinin Pnekov Siseev Sitsky Sontsov
	Sudsky Temnosiny Troekurov Ukhorsky Ushaty Yukhotsky Shchetinin
}

ship_names = {
	Danilov Lyubim Myshkin Petrovskoye Rybinsk Uglich
	Borok Itolar Karabikha Borisoglebsky Berendeyevo
	Varegove Tunoshna Kurba Nekouz Pesochnoye Prechistoye
	Nekrasovskoye Semibratovo
}
