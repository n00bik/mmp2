#Country Name: Please see filename.

graphical_culture = latingfx

color = { 236  159  89 }

historical_ideas = {
	superior_seamanship
	colonial_ventures
	national_trade_policy
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Jos� #0 Santiago" = 20
	"Juan #0 Antonio" = 20
	"Francisco #0" = 10
	"Mart�n #0" = 10
	"Juan #0 Esteban" = 5
	"Pedro Jos� #0" = 5
	"Abraham #0" = 0
        "Agust�n #0" = 0
	"Alfredo #0" = 0
	"An�bal #0" = 0
	"Antonio #0 Jos�" = 0
        "Arturo #0" = 0
	"Bartolom� #0" = 0
        "Bernardo #0" = 0
	"Carlos #0" = 0
	"Diego #0 Jos�" = 0
	"Domingo #0" = 0
	"Eduardo #0" = 0
	"El�as #0" = 0
	"Emiliano #0" = 0
        "Emilio #0" = 0
	"Federico #0" = 0
	"Fernando #0" = 0
	"Gabriel #0" = 0
	"Germ�n #0" = 0
        "Jer�nimo #0" = 0
	"Jorge #0" = 0
	"Jos� #0 Manuel" = 0
	"Jos� #0 Miguel" = 0
	"Jos� #0 Tom�s" = 0
	"Jos� #0 Joaqu�n" = 0
	"Juan #0" = 0
        "Juan #0 Luis" = 0
	"Luis #0" = 0
	"Manuel #0" = 0
	"Mateo #0" = 0
	"Pedro #0" = 0
	"Ram�n #0" = 0
	"Salvador #0" = 0
	"Vicente #0" = 0
}

leader_names = {
	�lvarez Argomedo Balmaceda Barros Bello Bilbao Blanco Borgo�o Bulnes
	Carrera "de Carrera" Cochrane "de la Cruz" Echevarr�a Ega�a Err�zuriz Freire
	Henr�quez Herrera Infante "de Irisarri" "de Lasta" Lastarria "de la Lastra"
	Mackenna Mar�n "M�rquez de la Plata" "Mart�nez de Aldunate" "Mart�nez de Rozas" Montt
	O'Higgins Ovalle P�rez Pinto Portales Prieto "de la Reina" Rengifo Rodr�guez Rosales
	"de San Mart�n" Sarmiento Soler Tocornal "de Toro" Varas Vidaurre "de Villegas"
	Za�artu Zenteno
}

ship_names = {
	Abtao
	"Blanco Encalada"
	Chacubuco
	Esmeralda
	Huascar
	Latorre 
	Orella
	Riquelme
	Serrano
	Valparaiso "Virgilio Uribe"
}
	
