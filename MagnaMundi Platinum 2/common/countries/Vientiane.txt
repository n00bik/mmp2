#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 151  174  28}

historical_ideas = {
	bureaucracy
	national_trade_policy
	cabinet
}

historical_units = { #optimized by berto#
	east_asian_spearmen
	chinese_footsoldier
	chinese_gunner
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer
	asian_defensive_platoon_fire
	asian_irregular_skirmisher
	asian_rifled_musketeer
	asian_columnar_infantry
	asian_impulse_infantry
	asian_breech_loaded_rifleman
	chinese_lancer
	chinese_steppe
	asian_charge_cavalry
	chinese_dragoon
	reformed_asian_cavalry
	asian_cuirassier
	asian_arme_blanche
	asian_chasseur
	asian_light_hussar
	asian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Sai Thiakapat #0" = 10
	"Tene Kham #0" = 10
	"La Sene Thai #0" = 10
	"Som Phou #0" = 10
	"Visoum #0" = 10
        "Phothisarath #0" = 30
        "Setthathirota #0" = 10
        "Sene Soulintha #0" = 10
        "Maha Oupahat #0" = 10
        "Nakhone Noi #0" = 10
	"Nokeo Koumane #0" = 10
	"Thammikarath #0" = 10
	"Oupagnouvarat #0" = 10
	"Mone Keo #0" = 10
	"Oupagnaovarath #0" = 10
        "Tone Kham #0" = 10
        "Visai #0" = 10
        "Souligna Vongsa #0" = 10
        "Tan Thala #0" = 10
        "Nan Tharat #0"	= 10
        "Sai Ong Hue #0" = 10
        "Ong Long #0" = 10
        "Ong Boun #0" = 10
}

leader_names = {
	Vilavong
	Vong
	Vanh
	Ath
	Dang
	Thuy
	Thew
	Aut
	Bee
	Joy
}

ship_names = {
	Angkor Anousa
	Khamteam Kongkeo Khamtum
	Mekong Manora Manorom
	Phommathat
	Souphutta Sai
	Ubosot
	"Fa Ngum" "Lan Xang" "Muang Nan" "Nge-Anh"
	"Vinh" "Muang Sing" "Muang Huom" "Xieng Hung"
	"Pak Ou" "Pak Beng" "Un Heuan" "Keo Koumari"
	"Siva Yaka" "Ngo Fa" "Lan Kham Deng" "Leu Xay"
	"Fa Kheun" "Noy Onsa" "Chiang Kham" "Yu Khong"
}
