#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 237  28  36 }

historical_ideas = {
	patron_of_art
	cabinet
	grand_army
}

historical_units = { #optimized by berto#
	japanese_archer
	japanese_footsoldier
	teppi_ashigaru
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer
	asian_offensive_platoon_fire
	asian_offensive_charge_infantry
	asian_defensive_drill
	asian_columnar_infantry
	asian_impulse_infantry
	asian_breech_loaded_rifleman
	eastern_bow
	japanese_samurai
	asian_charge_cavalry
	reformed_japanese_samurai
	reformed_asian_cavalry
	asian_cuirassier
	asian_arme_blanche
	asian_chasseur
	asian_light_hussar
	asian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Ashikaga Yoshihisa" = 50
	"Ashikaga Yoshihisa" = 50
	"Ashikaga Yoshitane" = 50
	"Ashikaga Yoshizumi" = 50
	"Ashikaga Yoshitane" = 50
	"Ashikaga Yoshiharu" = 50
	"Ashikaga Yoshiteru" = 50
	"Ashikaga Yoshihide" = 50
	"Ashikaga Yasuuji" = 50
	"Ashikaga Yoshiaki" = 50
	"Ashikaga Yoshiakira" = 50
	"Ashikaga Yoshiari" = 50
	"Ashikaga Yoshiuji" = 50
	"Ashikaga Yoshikazu" = 50
	"Ashikaga Yoshikatsu" = 50
	"Ashikaga Yoshikane" = 50
	"Ashikaga Yoshiki" = 50
	"Ashikaga Yoshisuke" = 50
	"Ashikaga Yoshitaka" = 50
	"Ashikaga Yoshitatsu" = 50
	"Ashikaga Yoshitsugu" = 50
	"Ashikaga Yoshitsuna" = 50
	"Ashikaga Yoshinori" = 50
	"Ashikaga Yoshinao" = 50
	"Ashikaga Yoshimi" = 50
	"Akio #0" = 0
	"Akira #0" = 0
	"Daisuke #0" = 0
	"Genzaemon #0" = 0
	"Hanzo #0" = 0
	"Harufusa #0" = 0
	"Hideaki #0" = 0
	"Hidetada #0" = 0
	"Hironaga #0" = 0
	"Hirotsuna #0" = 0
	"Ieharu #0" = 0
	"Kagemochi #0" = 0
	"Katsuhisa #0" = 0
	"Katsumasa #0" = 0
	"Katsuyori #0" = 0
	"Katsuo #0" = 0
	"Kazuuji #0" = 0
	"Kiyomasa #0" = 0
	"Kiyomori #0" = 0
	"Kunichika #0" = 0
	"Masakatsu #0" = 0
	"Masanari #0" = 0
	"Masuyo #0" = 0
	"Mochitoyo #0" = 0
	"Morishige #0" = 0
	"Musashi #0" = 0
	"Nagaharu #0" = 0
	"Naoie #0" = 0
	"Naoyori #0" = 0
	"Nobuharu #0" = 0
	"Nobunari #0" = 0
	"Ryo #0" = 0
	"Ryoukan #0" = 0
	"Sadayoshi #0" = 0
	"Sojun #0" = 0
	"Sho #0" = 0
	"Sumitada #0" = 0
	"Susumu #0" = 0
	"Tadakazu #0" = 0
	"Tadatsune #0" = 0
	"Takashi #0" = 0
	"Takatora #0" = 0
	"Tanezane #0" = 0
	"Tatsuoki #0" = 0
	"Tenkai #0" = 0
	"Toshikatsu #0" = 0
	"Tsunenaga #0" = 0
	"Ujikane #0" = 0
	"Ujikazu #0" = 0
	"Yoritada #0" = 0
	"Yoshiaki #0" = 0
	"Yoshinobu #0" = 0
	"Yoshio #0" = 0
	"Yuki #0" = 0
	"Yukiyasu #0" = 0
}

leader_names = {
	Asai Abe Adachi Akamatsu Akechi Akita Akiyama Akizuki Amago
	Ando Anayama Asakura Ashikaga Asahina
	Chosokabe
	Date
	Hara Hatakeyama Hatano Hayashi Honda Hojo Hosokawa
	Idaten Ii Ikeda Imagawa Inoue Ishida Ishikawa Ishimaki Ito
	Kikkawa Kiso Kitabatake
	Maeda Matsuda Matsudaira Miura Mikumo Miyoshi Mogami M�ri
	Nanbu Nitta Niwa
	Oda �tomo Ouchi
	Rokkaku
	Sakai Sakuma Shimazu Shiba Sanada Sogo Suwa
	Takeda Takigawa Toda Toki Tokugawa Toyotomi Tsutsui
	Uesugi Ukita
	Yagyu Yamana
}

ship_names = {
	"Asai Maru" "Abe Maru" "Adachi Maru" "Akamatsu Maru" "Akechi Maru"
	"Akita Maru" "Akiyama Maru" "Akizuki Maru" "Amago Maru" "Ando Maru"
	"Anayama Maru" "Asakura Maru" "Ashikaga Maru" "Asano Maru" "Ashina Maru"
	"Atagi Maru" "Azai Maru"
	"Bito Maru" "Byakko Maru"
	"Chiba Maru" "Chousokabe Maru"
	"Date Maru" "Doi Maru"
	"Fujiwara Maru" "Fuji-san Maru"
	"Genbu maru"
	"Haga Maru" "Hatakeyama Maru" "Hatano Maru" "Honda Maru" "Hojo Maru"
	"Hosokawa Maru" "Hachisuka Maru" "Hayashi Maru" "Hiki Maru"
	"Idaten Maru" "Ikeda Maru" "Imagawa Maru" "Ishida Maru" "Ishikawa Maru"
	"Ishimaki Maru" "Ii Maru" "Inoue Maru" "Ito Maru"
	"Kikkawa Maru" "Kiso Maru" "Kisona Maru" "Kitabatake Maru" "Kyogoku Maru"
	"Maeda Maru" "Matsuda Maru" "Matsudaira Maru" "Miura Maru" "Mikumo Maru"
	"Miyoshi Maru" "Mogami Maru" "Mori Maru"
	"Nitta Maru" "Niwa Maru" "Nihon Maru" "Nanbu Maru"
	"Oda Maru" "Otomo Maru" "Ouchi Maru"
	"Rokkaku Maru"
	"Sakai Maru" "Sakuma Maru" "Satake Maru" "Shimazu Maru" "Shiba Maru"
	"Sanada Maru" "Sogo Maru" "Suwa Maru" "Seiryu Maru" "Suzaku Maru"
	"Takeda Maru" "Tokugawa Maru" "Taira Maru" "Toyotomi Maru" "Tada Maru"
	"Toki Maru" "Tsugaru Maru" "Tsutsui Maru" "Tenno Maru"
	"Uesugi Maru" "Ukita Maru" "Uchia Maru"
	"Yamana Maru" "Yagyu Maru"
}
