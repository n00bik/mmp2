#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 224  146  113 } 

historical_ideas = {
	sea_hawks
	divine_supremacy
	humanist_tolerance
}

historical_units = { #optimized by berto#
	mamluk_archer
	mamluk_duel
	taifat_al_rusa
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	persian_rifle
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	muslim_cavalry_archers
	mamluk_cavalry_charge
	ottoman_timariot
	voyniq_heavy_cavalry
	shaybani
	suvarileri_cavalry
	mongol_swarm
	mamluk_musket_charge
	ali_bey_reformed_infantry
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Muhammad #0" = 200
	"Ahmad #0" = 80
	"'Abdall�h #0" = 60
	"'Abd al-Malik #0" = 40
	"'Al� #0" = 40
	"Isma'�l #1" = 20
	"Yahy� #1" = 20
	"'al-Wal�d #0" = 20
	"al-Mustad� #0" = 20
	"al-Rash�d #0" = 20
	"Idr�sid #0" = 20
	"Zayd�n #0" = 20
	"Zayn #0" = 20
	"'Abd ar-Rahm�n #0" = 10
	"al-Yaz�d #0" = 10
	"Mansur #0" = 10
	"Muhraz #0" = 10
	"Nasir #0" = 10
	"Ab� Faris #0" = 5
	"Abbas #0" = 5
	"Ab� Merwan #0" = 5
	"al-Talib #0" = 5
	"Hammada #0" = 5
	"Hasam #0" = 5
	"Hashim #0" = 5
	"Hisham #0" = 5
	"Ibrahim #0" = 5
	"Ja'far #0" = 5
	"Maimun #0" = 5
	"Maslama #0" = 5
	"Said #0" = 5
	"Sulaiman #0" = 5
	"Umar #0" = 5
	"Y�suf #0" = 5
	"Ataullah #0" = 0
	"Habib #0" = 0
	"Jabril #0" = 0
	"Lutfi #0" = 0
	"Rasul #0" = 0
	"Zulfiqar #0" = 0
}

leader_names = {
	Sahiri
	Haida
	Sellami
	Arazi
	Berui
	Lahsini
	Skah
	Barek
	Alami
	Behar
}

ship_names = {
	Tawwab Muntaqim Afuww Fa'uf "Malik Mulk"
	"Jalal Ikram" Muqsit Jami Ghani Mughni
	Mani Darr Nafi Nur Hadi Badi Baqi Warith
	Rashid Sabur "Fazl Azim"
}
