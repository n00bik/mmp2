#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 41  89  174 }

historical_ideas = {
	merchant_adventures
	superior_seamanship
	national_trade_policy
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	jagir_infantry
	rajput_musketeer
	sikh_hit_and_run
	kalapieda_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Dik-Kha #0" = 20
	"Tsau-Lha #0" = 20
	"Ranoung #0" = 10
	"Radzathu #1" = 30
	"Thado #0" = 20
	"Tsandawimala #0" = 20
	"Thado #0" = 40
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

ship_names = {
	"Sungai Belait" "Sungai Tutong" "Sungai Tembureng"
	Batang Trusan "Sungai Pandaruan" "Lungai Limbang"
	"Batang Baram" "Sungai Bakong" "Sungai Tutoh"
	"Sungai Tinjar" "Kuala Belait" "Batu Danau"
	"Kuala Medamit" Bangar Penanjong Lumut Telingan
	Labi Sukang "Kuala Baram"
}
