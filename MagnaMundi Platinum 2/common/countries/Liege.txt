#Country Name: Please see filename.

graphical_culture = latingfx

color = { 149  186  37 }

historical_ideas = {
	humanist_tolerance
	national_conscripts
	military_drill
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	scottish_highlander
	anglofrench_line
	prussian_drill
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Jean #7" = 40
	"Robert #1" = 20
	"Corneille #0" = 20
	"Ferdinand #0" = 20
	"Fran�ois-Antoine #0" = 20
	"Fran�ois-Charles #0" = 20
	"G�rard #0" = 20
	"�rard #0" = 20
	"Ernest #0" = 20
	"Jean-Louis #0" = 20
	"Jean-Th�odore #0" = 20		
	"Georges #0" = 20
	"Georges-Louis #0" = 20
	"Joseph-Cl�ment #0" = 20
	"C�sar-Constantin #0" = 20
	"Charles #0" = 20
	"Louis #0" = 20
	"Maximilien-Henri #0" = 20
	"Guillaume #3" = 5
	"Alexandre #2" = 5
	"Th�oduin #1" = 5
	"Henri #1" = 5
	"Adriaan #0" = 0
	"Andries #0" = 0
	"Augustijn #0" = 0
	"Bartholomeus #0" = 0
	"Boudewijn #0" = 0
	"Christiaan #0" = 0
	"Constantijn #0" = 0
	"Dani�l #0" = 0
	"Dominicus #0" = 0
	"Fabian #0" = 0
	"Frans #0" = 0
	"Gillis #0" = 0
	"Godfried #0" = 0
	"Herman #0" = 0
	"Ignaas #0" = 0
	"Ivo #0" = 0
	"Jacob #0" = 0
	"Stefanus #0" = 0
}

leader_names = {
	Aubry Bouchet Chauchet Digneffe Fabry
	G�rard Hauzeur Henkart Josias Nannan
	Bastin Bauduinet Bitha Cloet Colson
	"da Franquinet" "de Donnea" "de Finesse"
	"de Haxhe" "de Lacu" "de Spirlet" Haeck
	Lambermont Paulet Philippens Reul Reynier
	Vanhove "la Vignette" 
}

ship_names = {
	"St. Lambert" "St. Hubert" "St. Martin"
	Meuse "Notger of Liege" "Place du Queer"
	"Saint Jaques" "Saint Bartholomew" Seraing
	"Sart-Tilman" Luik "Saint-Nicolas" Ans
}
