#Country Name: Seeee the Name of this File.

graphical_culture = latingfx

color = { 53  140  73 }

historical_ideas = {
	national_conscripts
	national_trade_policy
	cabinet
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	scottish_highlander
	anglofrench_line
	prussian_drill
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Franz #0" = 60
	"Ludwig #0" = 40
	"Karl Ludwig #0" = 20
	"Leonor #0" = 20
	"Ludwig Heinrich #0" = 20
	"Ludwig Joseph #0" = 20
	"Maria #0" = -20
	"Peter #0" = 20
	"Johann #2" = 10
	"Anton #1" = 10
	"Karl #1" = 10
	"Claudius #0" = 10
	"Ludwiga #0" = -10
	"Paul Andreas #0" = 10
	"Jakob #0" = 5
	"Heinrich #7" = 1
	"Konrad #2" = 1
	"Wenzel #2" = 1
	"Sigfried #1" = 1
	"Wilhelm #1" = 1
	"Albrecht #0" = 0
	"Bernhard #0" = 0
	"Cristoph #0" = 0
	"Diederik #0" = 0
	"Engelbert #0" = 0
	"Ferdinand #0" = 0
	"Georg #0" = 0
	"Hans #0" = 0
	"Isaak #0" = 0
	"Jochim #0" = 0
	"Klemens #0" = 0
	"Konstantin #0" = 0
	"Lothair #0" = 0
	"Mathias #0" = 0
	"Odo #0" = 0
	"Pascal #0" = 0
	"Philipp #0" = 0
	"Rudolf #0" = 0
	"Sigismund #0" = 0
	"Viktor #0" = 0
}

leader_names = {
	"von Bettborn" "von Bous" "von Capellen" "von Grevenmach"
	"von Grosbous" "von Pontpierre" "von Reisdorf" "von Redange"
	"von Diekirch" "von Schuttrange" Borchert B�ltel Dumont
	Genot Hoscheit Mersch Nissar Riener Schannen Pinkernell
	Petilliot Paguet "von Sommerfeld" Voigt Weiler Winen Biberen
	Clees Cemmerer Croye Delhaes "du Carme" Gonderinger Greff Herrig
	Maussen Bourscheidt 
}

ship_names = {
	Clervaux Diekirch Redange Vianden Wiltz
	Echternach Grevenmacher Remich Capellen
	Mersch Feulen "Esch-sur-Alzette"
}
