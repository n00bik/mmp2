#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 159  127  255 }

historical_ideas = {
	church_attendance_duty
	patron_of_art
	humanist_tolerance
}

historical_units = { #optimized by berto#
	east_asian_spearmen
	chinese_footsoldier
	chinese_gunner
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer
	asian_defensive_platoon_fire
	asian_irregular_skirmisher
	asian_rifled_musketeer
	asian_columnar_infantry
	asian_impulse_infantry
	asian_breech_loaded_rifleman
	chinese_lancer
	chinese_steppe
	asian_charge_cavalry
	chinese_dragoon
	reformed_asian_cavalry
	asian_cuirassier
	asian_arme_blanche
	asian_chasseur
	asian_light_hussar
	asian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Thammaracha #2" = 40
	"Loethai #1" = 15
	"Nguanamthom #1" = 15
	"Saisongkhram #1" = 15
	"Ban Muang #1" = 10
	"Sri Indraditya #1" = 10
	"Ramkhamhaeng #1" = 10
}

leader_names = {
	Akradej Ariyanuntaka 
	Boonmee 
	Chaipatana Chakrabonse Chuasiriporn 
	Diskul 
	Ekaluck 
	Hitapot 
	Inchareon 
	Kawrungruang Khuntilanont Klinpraneet Kriangsak 
	Monkoltham 
	Nitpattanasai Noppachorn Nut 
	Panomyaong Phatipatanawong 
	Sitdhirasdr Sivaraksa Srichure Supachai Supasawat 
	Tantasatityanon Thabchumpon Tomson 
	Wongrutiyan 
	Yongchaiyudh 
}

ship_names = {
	Ramithibodi Ramesuan Borommaracha "Thong Chan"
	Ramaratcha Intharatcha Boromtrailokanat Ratsada
	Chairacha Yotfa Worawongsa Chakkraphat Mahin
	Sanpet Naresuan Ekathotsarot "Si Saowaphak"
	Songtham Chehha "Somdet Phra" "Prasat Thong" Chai
	Suthammaracha Narai Petratcha S�a Phumintharacha
	Uthumpon Ekkathat
}
