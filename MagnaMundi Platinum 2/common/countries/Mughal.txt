#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 154  133  153 }

historical_ideas = {
	battlefield_commisions
	military_drill
	humanist_tolerance
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	jagir_infantry
	rajput_musketeer
	sikh_hit_and_run
	kalapieda_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Babur #0" = 10
	"Humayun #0" = 10
	"Akbar #0" = 10
	"Dawar Bakhsh #0" = 10
	"Jahan #0" = 30
	"Aurangaeb #0" = 10
	"Bahadur #0" = 10
	"'Azim-ush-Sha'n #0" = 10
	"Jahandar Sha #0" = 10
	"Farrukh-Siyar #0" = 10
	"Nikusiyar #0" = 10
	"Mohammed #0" = 10
	"'Alamgir #1" = 10
	"Alam #0" = 10
}

leader_names = {
	Ameerun
	Beg
	Bhutto
	Chughtai
	Jahan
	Kamaluddin
	Khan
	Mirza
	Musharif
	Nagar
	Nasseruddin
	Nazirun
	Radhidun
	Ra'ana
	Shahabuddin
	Sharif
	Sinan
	Singh
	Wazirun
	Zahiruddin
}

ship_names = {
	Alia Arusa Chambal Fatimah Jalsena
	Ganges "Kala Jahazi" Khan "Lal Jahazi"
	Nausena "Nav ka Yudh" Niknaz "Nila Jahazi"
	Niloufa Noor Sagar Salima Shameena Spiti
	Suhalia Yamuna Yasmine Zahira
}
