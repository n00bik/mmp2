#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 117  179  128 }

historical_ideas = {
	merchant_adventures   
	superior_seamanship
	national_trade_policy
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	jagir_infantry
	rajput_musketeer
	sikh_hit_and_run
	kalapieda_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

monarch_names = {
	"Tunyachoka-ri #0" = 10
	"TuMaoa'risi' #0" = 10
	"Karaeng Tunibatta #0" = 10
	"Tunijallo #0" = 10
	"Parabung #0" = 10
	"Tunipassulu #0" = 10
	"Aladdin Tumenanga #0" = 10
	"Muhammad Malik #0" = 10
	"Muhammad Bakr Hasan #0" = 10
	"Hamaza Tumammalianga #0" = 10
	"Muhammad 'Ali #0" = 10
	"Fakhr #0" = 10
	"Shahab #0" = 10
	"Siraj #0" = 10
	"Abdul Khair #0" = 10
	"Abdul Kudus #0" = 10
	"Usman Fakhr #0" = 10
	"Muhammad Imad #0" = 10
	"Zain #0" = 10	 
}

ship_names = {
	Balla Bulu Lompo Sallo Karaeng Bambang
	Cipuru Doe Iyo Tena Tabe Apa Lakeko Battu Keko
}
