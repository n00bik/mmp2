#Country Name: Seeee the Name of this File.

graphical_culture = latingfx

color = { 116  92  39}

historical_ideas = {
	patron_of_art
	merchant_adventures
	smithian_economics
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Heinrich #3" = 60
	"Wilhelm #0" = 40
	"August #1" = 20
	"Anton Ulrich #0" = 20
	"August Wilhelm #0" = 20
	"Ferdinand Albrecht #0" = 20
	"Friedrich Ulrich #0" = 20
	"Heinrich Julius #0" = 20
	"Julius #0" = 20
	"Karl #0" = 20
	"Karl #0 Wilhelm" = 20
	"Ludwig Rudolf #0" = 20
	"Rudolf August #0" = 20
	"Andreas #0" = 10
	"Christian #0" = 10
	"Franz #0" = 10
	"Karl #0 Georg" = 10
	"Philipp Siegmund #0" = 10
	"August #0" = 5
	"August Franz #0" = 5
	"August Heinrich #0" = 5
	"August Karl #0" = 5
	"Christian Franz #0" = 5
	"Erich #0" = 5
	"Ernst Ferdinand #0" = 5
	"Friedrich #0" = 5
	"Friedrich August #0" = 5
	"Friedrich Wilhelm #0" = 5
	"Georg #0" = 5
	"Georg Franz #0" = 5
	"Heinrich Ferdinand #0" = 5
	"Heinrich Karl #0" = 5
	"Joachim #0" = 5
	"Joachim Karl #0" = 5
	"Johann #0" = 5
	"Julius August #0" = 5
	"Julius Ernst #0" = 5
	"Otto #0" = 5
	"Philipp Magnus #0" = 5
	"Rudolf #0" = 5
}

leader_names = {
	Brach Eickenroth Gruebel Wassman Hattendorf Opp
	"von Rhein" Sch�tze Schauseil Talken Lindemann 
	Ickelrath Hein Hehn Jaxtheim
}

ship_names = {
	Bruno Darkward "Herzog Ludolf" "Gr�fin Gertrud"
	"Bischof Godehard" "Bischof Branthag" "Lothar III"
	"Heinrich I." "Herzog Otto" "Albert I" "Magnus II"
	Wolfenbuttel Gottingen Grubenhagen
}

army_names = {
	"Armee von $PROVINCE$" 
}
