#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 79  89  182 }

historical_ideas = {
	grand_army #Mongol Horde
	military_drill #Mongol Horde
	improved_foraging #Mongol Horde
}

historical_units = { #optimized by berto#
	bashi_bazouk
	mongol_bow
	persian_shamshir
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	durrani_rifled_musketeer
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	muslim_cavalry_archers
	mongol_steppe
	mongolian_bow
	east_mongolian_steppe
	shaybani
	reformed_manchu_rifle
	manchu_banner
	mamluk_musket_charge
	durrani_swivel
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Husain #1" = 20
	"Uwais #1" = 20
	"Ahmad #0" = 20
	"Mahmud #0" = 20
	"Muhammad #0" = 20
	"Shah Walad #0" = 20
	"Hasan #1" = 15
	"Rash�d #0" = 15
	"Isma'il #0" = 10
	"Qasim #0" = 10
	"'Abbas #0" = 0
        "'Abd al-Karim #0" = 0
	"'Abd al-Rahman #0" = 0
	"Anwar #0" = 0
	"Atuf #0" = 0
	"Bakr #0" = 0
	"Fahd #0" = 0
	"Faruq #0" = 0
	"Fouad #0" = 0
	"Hakim #0" = 0
        "Harun #0" = 0
	"Hikmat #0" = 0
	"Hisham #0" = 0
	"Iqbal #0" = 0
	"Jafar #0" = 0
        "Jamaal #0" = 0
	"Karim #0" = 0
	"Khalil #0" = 0
        "Mal�k #0" = 0
	"Mirza #0" = 0
	"Najib #0" = 0
	"Rusul #0" = 0
	"Shimun #0" = 0
	"Tar�q #0" = 0
	"Usama #0" = 0
	"Yasir #0" = 0
	"Yusuf #0" = 0
	"Zaid #0" = 0
	"Z�yad #0" = 0
	"Zulqifar #0" = 0
}

leader_names = {
	Rukh
	Jahagir
	Sa'id
	Uways
	Hussain
	Ahmad
	Mahmud
	Mirza
	Mohsin
	Zaman
}

ship_names = {
	Ardabil
	Bakhura Baluches Balkh
	Herat
	Ispahan
	Kirman Kish Kharesm Kandahar
	Lahore
	Merv
	Nishupur
	Ormuz
	Rustam
	Sikandar Sebzewar Slurax Samarqand
	Tamerlane Tahriz
	"Shah Rukh"
}
