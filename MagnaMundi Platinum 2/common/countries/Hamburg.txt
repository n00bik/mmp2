#Country Name: Seeee the Name of this File.

graphical_culture = latingfx

color = { 104  148  71}

ai_hard_strategy = { personality = capital }

historical_ideas = {
	merchant_adventures
	smithian_economics
	superior_seamanship
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Ernst #0" = 20
	"Friedrich #0" = 20
	"Konrad #0" = 10
	"Otto #0" = 10
	"Hermann #0" = 5
	"Philipp #0" = 5
	"Adalbert #0" = 0
	"Albrecht #0" = 0
	"Alexander #0" = 0
	"Baldur #0" = 0
	"Barnabas #0" = 0
	"Burkhard #0" = 0
	"Diederick #0" = 0
	"Dieter #0" = 0
	"Eckbert #0" = 0
	"Erwin #0" = 0
	"Fabian #0" = 0
	"Florian #0" = 0
	"Gottschalk #0" = 0
	"Gregor #0" = 0
	"Hagan #0" = 0
	"Hartwin #0" = 0
	"Herbert #0" = 0
	"Jeremias #0" = 0
	"Joseph #0" = 0
	"Julius #0" = 0
	"J�rgen #0" = 0
	"Kuno #0" = 0
	"Kurt #0" = 0
	"Markus #0" = 0
	"Martin #0" = 0
	"Niklas #0" = 0
	"Oskar #0" = 0
	"Reinhard #0" = 0
	"Siegfried #0" = 0
	"Thomas #0" = 0
	"Valentin #0" = 0
	"Viktor #0" = 0
	"Wenzel #0" = 0
	"Wilfried #0" = 0
}

leader_names = {
	Abendroth Ackermann Adler Ahlers Alpers Alt 
	Amsinck Baasch Backer Bagebuhr Bastian Billing 
	Boetefeur Dabelstein "de Greve" Dreyer Eggers 
	Engel "von der Fechte" Framhein Gevert Gossler 
	Gr�newaldt Hansen Heinsohn Henckell Heuer Horwege
	Imbeck Jarchow Kehrhahn Kellinghusen Kindt 
	"von Lengerke" Lieb Merck Meyer Muhl Nanne 
	Olbers Pape Pollnau Rosenbaum Rosendahl Schmerse
	Schmidt Schuberth Schuemann Soltau Steetz Thelemann 
	Tiedemann Vick Vilter Vollmer "von D�ten" 
}

ship_names = {
	"Hauke Haien"
	"G�decke Michels"
	Heiligenbeil
	Bartenstein
	"Furcht un Schreck"
	"Fliegender Geist"
	"Frech Haas"
	Hechtenvisch
	Igzorn
	Seewolff
	St�rtebecker
	Fenrizwulf
                  "Wappen von Hamburg"
                  "Leopoldus Primus"
                  "Admiralit�t von Hamburg"
                  Hoffning
                  "Prophet Daniel"
}

army_names = {
	"Stadtwache"
	"B�rgerwehr"
	"Miliz von $PROVINCE$"
	"Armee von $PROVINCE$" 
}

fleet_names = {
	"Elbflotte"
	"Hamburgische Flotte"
	"Hanseflotte"
	"Nordseegeschwader"
	"Bergengeschwader"
}
