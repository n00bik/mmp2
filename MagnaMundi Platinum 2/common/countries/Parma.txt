#Country Name: Please see filename.

graphical_culture = latingfx

color = { 128  202  129}

historical_ideas = {
	merchant_adventures
	superior_seamanship
	smithian_economics
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	italian_condotta
	swiss_landsknechten
	gaelic_mercenary
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	anglofrench_line
	british_square
	napoleonic_square
	french_impulse
	mixed_order_infantry
	western_medieval_knights
	chevauchee
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Ranuccio #0" = 40
	"Alessandro #0" = 20
	"Antonio #0" = 20
	"Carlo #0" = 20
	"Ferdinando #0" = 20
	"Filippo #0" = 20
	"Francesco #0" = 20
	"Odoardo #0" = 20
	"Ottavio #0" = 20
	"Pier Luigi #0" = 20
	"Lodovico #0" = 10
	"Isabella #0" = -5
	"Orazio #0" = 5
	"Pietro #0" = 5
	"Achille #0" = 0
	"Alberto #0" = 0
	"Benedetto #0" = 0
	"Bernardo #0" = 0
	"Ciro #0" = 0
	"Claudio #0" = 0
	"Daniele #0" = 0
	"Demetrio #0" = 0
	"Elmo #0" = 0
	"Enrico #0" = 0
	"Fabio #0" = 0
	"Flavio #0" = 0
	"Gaetano #0" = 0
	"Graziano #0" = 0
	"Luciano #0" = 0
	"Manfredo #0" = 0
	"Mario #0" = 0
	"Modesto #0" = 0
	"Nestore #0" = 0
	"Ottaviano #0" = 0
	"Prudenzio #0" = 0
	"Roberto #0" = 0
	"Samuele #0" = 0
	"Sesto #0" = 0
	"Valerio #0" = 0
	"Vincenzo #0" = 0
}

leader_names = {
        Anguissola Anvidi "d'Appiani d'Aragona" d'Asburgo
	Bajardi Barattieri Bernini Bertorella Bianconese "di Borbone" Borghese
	Cantelli "di Castagnola" Cavagnari Cavalcab� Cella Cerati Conaccia Confallonieri
	Draghi Emanueli "di Farnese" Fumagalli Gioia Landi Lemignano Linati Lombardi
	Manfredi Marazzani Marescalchi "Meli Lupi"
	Paletti Pallavicini Pallieri Piatti Pontremoli Riva Rivalta "dalla Rosa" Rossi
	Salati Sanseverino Sanvitale Schizzati Torelli Torrechiara
	Ventura Verdi "dal Verme"
}

ship_names = { 
	Alberi 
	"Benedetto Antelami" "Bernabo' Visconti"
	Calestani Carignano Casaltone Casagnola "Case Capelli" Castalletto Cavanna Certosa Cusani
	"donna Egidia"
	Eia 
	"Fiumi Po" Fontanini 
	Gaione "Ghiaiata Nuova" 
	Montanara Molinetto
	Panocchia Pedrignano Pilastrello Ponte Porporano 
	Quercioli 
	"Rolando Taverna" "Ronco Pascolo" 
	"San Francesco del Prato" "San Lazzaro" "San Leonardo" "San Martino dei Bocci" "San Pancrazio" "San Pietro Apostolo" "San Prospero" "San Ruffino" "Santa Coce" Sant'Ulderico Sissa 
	Valera Valserena "vescovo C�dalo" "vescovo Guiberto" Viazza Vigolante
}

army_names = {
	"Armata di $PROVINCE$"
}
