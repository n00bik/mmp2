#Kurland

graphical_culture = latingfx

color = { 99  137  153}

historical_ideas = {
	national_trade_policy
	superior_seamanship
	bill_of_rights 
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	swedish_caroline
	prussian_drill
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_dragoon
	french_cuirassier
	swedish_arme_blanche
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Ernst Johann #0" = 20
	"Ferdinand #0" = 20
	"Friedrich #0" = 20
	"Friedrich Casimir #0" = 20
	"Friedrich Wilhelm #0" = 20
	"Gotthard #0" = 20
	"Jakob #0" = 20
	"Karl #0" = 20
	"Peter #0" = 20
	"Johann #0" = 10
	"Karl Ernst #0" = 10
	"Ladislas Friedrich #0" = 10
	"Alexander #0" = 5
	"Karl Jakob #0" = 5
	"Leopold Karl #0" = 5
	"Adolf #0" = 0
	"Alfred #0" = 0
	"Benedikt #0" = 0
	"Denis #0" = 0
	"Diederick #0" = 0
	"Erwin #0" = 0
	"Franz #0" = 0
	"Gebhard #0" = 0
	"Heinrich #0" = 0
	"Joachim #0" = 0
	"Kaspar #0" = 0
	"Kurt #0" = 0
	"Leo #0" = 0
	"Ludwig #0" = 0
	"Manfred #0" = 0
	"Moritz #0" = 0
	"Nikolaus #0" = 0
	"Oskar #0" = 0
	"Otto #0" = 0
	"Philipp #0" = 0
	"Reiner #0" = 0
	"Rupprecht #0" = 0
	"Siegmund #0" = 0
	"Urs #0" = 0
	"Werner #0" = 0
}

leader_names = {
        "von Altenbockum" Ascheberg
        "von Bach" Behr Bilderling Blomberg "von Boetticher" Borch Brincken  Br�ggen Buchholtz
        Derschau Drachenfels D�sterlohe
        Engelhardt
        Fircks F�lkersam Funck
        Grotthuss
        Haaren Hahn Heyking "von Hoerner" Howen
        Kleist Klopmann Komorowski Koskull
        Lambsdorff "von der Launitz" "von Lysander"
        Mirbach
        Nettelhorst Nolcken
        Oelsen Offenberg Osten-Sacken
        Pahlen
        Raczynski Recke Ropp Rosenberg "von Rummel"
        Schlippenbach "von Schroeders" Seefeld Stackelberg Stromberg
        Taube "von Timroth"
}

ship_names = {
                  "Bildtnis der Herzoge" Blumentopf Bruno  
                  Cabljau Cavalier Clementia Concordia Constantia Crocodill
                  Dame "Das Wappen der Herzogin" "Der Jungere Prinz" 
                  "Der Jungste Prinz" "Der rote L�we" "Die drey Heringe" "Diderick Tork" D�na
                  Elent 
                  Fidelitas Fortitudo Fortuna "Franco Kerskorff"
                  Goldingen Gr�nlandfahrer
                  Hasenpoth "Herzoginn von Churlandt" Herzogenstein Hoffnung
                  Innocentia Invidia Isl�nder
                  "Jacobus Major" "Jacobus Minor" Jager "Johannes der Evangelist" "Johannes der T�uffer" "Johannes Ungenade" Justitia
                  "K�nig David"
                  L'Esperance Levitas Lux "Lynn kurl�ndische M�we"
                  Meve "Meve von Churlandt" Mitau Mohr
                  Neptunus 
                  Orpheus
                  Parsimonia Patentia Pax Pietas "Prinz von Churlandt" "Prinzessin von Churlandt" Prudentia 
                  "Reimar Hane" Ringeltaube
                  Schwann Scientia Seeburg
                  Temperantia Tukums
                  "von Bockenvorde" "von Breithausen" "von B�rggeneye" "von der Borch" "von der Recke" "von Dinkelaghe" "von Dreileben" 
                  "von Endorp" "von Eltz" "von Feuchtwangen" "von F�rstenberg" "von Galen" "von Gr�ningen" "von Hercke" "von Herse" "von Hohembach" 
                  "von Hornhausen" "von Jocke" "von Loringhofe" "von Lutterberg" "von Mandern" "von Mengede" "von Monheim" "von Nortecken" "von Overbergen" 
                  "von Plettenberg" "von Rassburg" "von Rogga" "von Rutenberg" "von Sangershausen" "von Spanheim" "von Stierland" "von Vietinghof" "von Vrymersheim"
                  Walfisch "Wappen d. Herzogs Jacobus" "Wappen der Herzoge" "Wappen von Churlandt"
                  "Wappen d. eysernen Mannes" "Wappen v Louise Charlotte" "Wappen von Hessen Homburg" Windau
}

