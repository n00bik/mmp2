#Country Name: Hindustan

graphical_culture = indiangfx

color = { 0  128  0 }

historical_ideas = {
	national_conscripts
	national_trade_policy
	church_attendance_duty
}

historical_units = { #optimized by berto#
	indian_footsoldier
	indian_archers
	indian_arquebusier
	south_indian_musketeer
	reformed_mughal_musketeer
	punjabi_irregulars
	maharathan_guerilla_warfare
	bhonsle_infantry
	indian_rifle
	indian_offensive_platoon_fire
	indian_irregular_skirmisher
	indian_offensive_drill
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_hit_and_run
	bhonsle_cavalry
	indian_arme_blanche
	indian_chasseur
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = { #An amalgamation of several important Hindi states
	"Aladdin #0" = 10
	"Baharmalla #0" =10
	"Bhao Singh #0" =10
	"Bhima #0" =10
	"Bishan Singh #0" =10
	"Bragwan Das #0" =10
	"Chandrasena #0" =10
	"Damajo Rao #0" =10
	"Govind Rao #0" =10
	"Ishwari Singh #0" =10
	"Jagat Singh #0" =10
	"Jaya Singh #0" =10
	"Mahdu Singh #0" =10
	"Man Singh #0" =10
	"Pilaji Rao #0" =10
	"Prithvi #0" =10
	"Rajaram #0" =10
	"Rama Singh #0" =10
	"Ramajara #0" =10
	"Ratan #0" =10
	"Sambahji #0" =10
	"Sayaji Rao #0" =10
	"Shahu #0" =10
	"Shahu #0" =10
	"Shivaji #0" =10
	"Udha Rao #0" =10
	}

leader_names = { #An amalgamation of several important Hindi states
	Babber
	Bargujars
	Bhoja
	Chandelas
	Chauhan
	Desai
	Gandhi
	Jadeja
	Jain
	Kachwaha
	Karmavati
	Kaushal
	Khumba
	Khursh
	Krishnamma
	Lahiri
	Lakhani
	Lolaksi
	Mahipala
	Majmudar
	Maruthi
	Mihirbhoj
	Munshi
	Muthuswami
	Muzzafar
	Nagabhatta
	Nagaraj
	Narsala
	Nirmal
	Nisheeth
	Padmini
	Pandya
	Paramaras
	Parikh
	Patel
	Patidar
	Prabhat
	Prajna
	Pratap
	Purujit
	Rajal
	Rajput
	Ramdas
	Ramiah
	Rathore
	Rushi
	Sahadev
	Saraiya
	Shaw
	Shrivastav
	Singh
	Sitipala
	Sugriva
	Sukarman
	Tilak
	Tjakur
	Tomaras
	Tyagri
	Vaghela
	Vanik
	Vatsraja
	Viraj
	Virmani
	Vish
	Vishwa
	Vyapari
	Vyas
	Yateen
}

ship_names = { #An amalgamation of several important Hindi states
	Agni 
	Bhim 
	Brahma 
	Devi 
	Durga
	Ganesh 
	Gayatri 
	Hanumaun 
	Indra
	Jalsena 
	"Kala Jahazi" 
	Kali 
	Kubera
	Krishna 
	Lakshmi 
	"Lal Jahazi" 
	Nausena
	"Nav ka Yudh" 
	"Nila Jahazi" 
	Parvati
	Radha 
	Ratri 
	Sagar 
	Sarasvati 
	Shiva
	Sita 
	Soma 
	Surya 
	Varuna 
	Vayu 
	Vishnu 
	Yama
}
