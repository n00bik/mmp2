#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 161  210  149 } 

historical_ideas = {
	national_conscripts
	military_drill
	patron_of_art
}

historical_units = { #optimized by berto#
	bashi_bazouk
	mongol_bow
	persian_shamshir
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	durrani_rifled_musketeer
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	muslim_cavalry_archers
	mongol_steppe
	mongolian_bow
	east_mongolian_steppe
	shaybani
	reformed_manchu_rifle
	manchu_banner
	mamluk_musket_charge
	durrani_swivel
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"B�bur #2" = 30
	"M�rz� #1" = 10
	"Ab� Sa'�d #1" = 10
        "Sh�h Rukh #1" = 10
	"Ahmad #1" = 10
	"Mahmud #1" = 10
	"Mas'�d #1" = 10
	"B�y Sunqur #1" = 10
	"'Al� #1" = 10
        "Hussain #0" = 10
        "Uways #0" = 10
        "Nas�r #0" = 10
        "Ulugh #1" = 10
        "T�m�r #1" = 1
}

leader_names = {
        Jahangir
	Mohsin
	Zaman
        Ab� Bakr
        Akbar
        Abb�s
        J�m�
        Jal�l
        Khalil
        "ud-D�n"
	}

ship_names = {
	Ardabil
	Bakhura Baluches Balkh
	Herat
	Ispahan
	Kirman Kish Kharesm Kandahar
	Lahore
	Merv
	Nishupur
	Ormuz
	Rustam
	Sikandar Sebzewar Slurax Samarqand
	Tamerlane Tahriz
	"Shah Rukh"
}
