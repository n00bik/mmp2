#Country Name: Please see filename.

graphical_culture = latingfx

color = { 120  77  119}

historical_ideas = {
	national_conscripts
	national_trade_policy
	smithian_economics
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Friedrich #7" = 40
	"Georg Friedrich #0" = 40
	"Albrecht #1" = 20
	"Albrecht #1 Achilles" = 20
	"Christian Albrecht #0" = 20
	"Christian Friedrich Karl Alexander #0" = 20
	"Georg #0" = 20
	"Joachim Ernst #0" = 20
	"Johann Friedrich #0" = 20
	"Karl Wilhelm Friedrich #0" = 20
	"Wilhelm Friedrich #0" = 20
	"Albrecht Ernst #0" = 10
	"Georg Albrecht #0" = 10
	"Johann Cicero #0" = 10
	"Karl Friedrich August #0" = 10
	"Kasimir #0" = 10
	"Leopold Friedrich #0" = 10
	"Johann #3" = 5
	"Friedrich Albrecht #0" = 5
	"Friedrich August #0" = 5
	"Friedrich Karl #0" = 5
	"Gumprecht #0" = 5
	"Johann Albrecht #0" = 5
	"Johann Georg #0" = 5
	"Siegmund #0" = 5
	"Wilhelm #0" = 5
	"Gottfried #2" = 1
	"Adolf #0" = 0
	"Clemens #0" = 0
	"Diederik #0" = 0
	"Eberhard #0" = 0
	"Helmut #0" = 0
	"Hermann #0" = 0
	"Jakob #0" = 0
	"Luitpold #0" = 0
	"Markus #0" = 0
	"Rainer #0" = 0
	"Siegfried #0" = 0
	"Stefan #0" = 0
	"Viktor #0" = 0
}

leader_names = { 
	Fichtelmann Grimm Schmidt S�ll D�umler
	Dick Dietsch Ernst Fischer F�disch Franck
	Hofmann H�hrer Kn�rnschild Langheinrich Maier Narr
	Ott Ranck Rudl Sack Schwager Seel Sell 
	Teichmann Weinrich
}

ship_names = {
	"Friedrich von Ansbach" "Albrecht Achilles"
	"Friedrich V" Georg "Georg Friedrich" Rezat
	Onoldsbach Hohenzollern Kulmbach Bayreuth
}

army_names = {
	"Armee von $PROVINCE$" 
}
