# W�rzburg

graphical_culture = latingfx

color = { 147  146  106}

historical_ideas = {
	patron_of_art
	national_trade_policy
	smithian_economics
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Konrad #1" = 40
	"Johann Gottfried #0" = 40
	"Johann Philipp #0" = 40
	"Gottfried #3" = 20
	"Johann #2" = 20
	"Rudolf #1" = 20
	"Adam Friedrich #0" = 20
	"Anselm Franz #0" = 20
	"Christoph Franz #0" = 20
	"Franz #0" = 20
	"Franz Ludwig #0" = 20
	"Friedrich #0" = 20
	"Friedrich Karl #0" = 20
	"Georg Karl #0" = 20
	"Johann Hartmann #0" = 20
	"Julius Echter #0" = 20
	"Karl Philipp #0" = 20
	"Konrad Wilhelm #0" = 20
	"Lorenz #0" = 20
	"Melchior #0" = 20
	"Peter Philipp #0" = 20
	"Philipp Adolf #0" = 20
	"Philipp Franz #0" = 20
	"Albrecht #3" = 10
	"Hermann #2" = 10
	"Otto #2" = 10
	"Gerhard #1" = 10
	"Wolfram #1" = 10
	"Heinrich #5" = 5
	"Poppo #3" = 5
	"Berthold #2" = 5
	"Andreas #1" = 5
	"Dietrich #1" = 5
	"Friedrich #1" = 5
	"Alfred #0" = 0
	"Christian #0" = 0
	"Hieronymus #0" = 0
	"J�rgen #0" = 0
	"Pankraz #0" = 0
	"Theodor #0" = 0
}

leader_names = {
	Dick Sell S�ll Schwager Fichtelmann H�hrer Rank
	G�belein B�r Degen F�rber Simon Wirth Sack
	Blochberger F�disch Egelkraut Hager Hofmann
}

ship_names = {
	Marienberg "Julius Echter" Main H�chberg
	Rimpar Biebelried Eigelstadt Uettingen
	Gramschatz Wern Bergtheim Kietzingen Ochsenfurth
}

army_names = {
	"Armee von $PROVINCE$" 
}
