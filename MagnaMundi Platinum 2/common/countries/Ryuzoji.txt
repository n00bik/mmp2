#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 39  97  96 }

historical_ideas = {
	military_drill
	superior_seamanship
	regimental_system
}

historical_units = { #optimized by berto#
	japanese_archer
	japanese_footsoldier
	teppi_ashigaru
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer
	asian_offensive_platoon_fire
	asian_offensive_charge_infantry
	asian_defensive_drill
	asian_columnar_infantry
	asian_impulse_infantry
	asian_breech_loaded_rifleman
	eastern_bow
	japanese_samurai
	asian_charge_cavalry
	reformed_japanese_samurai
	reformed_asian_cavalry
	asian_cuirassier
	asian_arme_blanche
	asian_chasseur
	asian_light_hussar
	asian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Ryuzoji Yasuhide" = 30
	"Ryuzoji Iemasa" = 30
	"Ryuzoji Ietane" = 30
	"Ryuzoji Iechika" = 30
	"Ryuzoji Iemori" = 30
	"Ryuzoji Iesada" = 30
	"Ryuzoji Iehide" = 30
	"Ryuzoji Ieuji" = 30
	"Ryuzoji Yasuie" = 30
	"Ryuzoji Taneie" = 30
	"Ryuzoji Chikakazu" = 30
	"Ryuzoji Tanekazu" = 30
	"Ryuzoji Tanehisa" = 30
	"Ryuzoji Tanemitsu" = 30
	"Ryuzoji Ienari" = 30
	"Ryuzoji Yasufusa" = 30
	"Ryuzoji Ietane" = 30
	"Ryuzoji Tadanobu" = 30
	"Ryuzoji Iemori" = 30
	"Ryuzoji Katsushige" = 30
	"Ryuzoji Iekane" = 30
	"Ryuzoji Ieyoshi" = 30
	"Ryuzoji Ienobu" = 30
	"Ryuzoji Yasuchika" = 30
	"Ryuzoji Nobuchika" = 30
	"Akio" = 0
	"Akira" = 0
	"Daisuke" = 0
	"Genzaemon" = 0
	"Hanzo" = 0
	"Harufusa" = 0
	"Hideaki" = 0
	"Hidetada" = 0
	"Hironaga" = 0
	"Hirotsuna" = 0
	"Ieharu" = 0
	"Kagemochi" = 0
	"Katsuhisa" = 0
	"Katsumasa" = 0
	"Katsuyori" = 0
	"Katsuo" = 0
	"Kazuuji" = 0
	"Kiyomasa" = 0
	"Kiyomori" = 0
	"Kunichika" = 0
	"Masakatsu" = 0
	"Masanari" = 0
	"Masuyo" = 0
	"Mochitoyo" = 0
	"Morishige" = 0
	"Musashi" = 0
	"Nagaharu" = 0
	"Naoie" = 0
	"Naoyori" = 0
	"Nobuharu" = 0
	"Nobunari" = 0
	"Ryo" = 0
	"Ryoukan" = 0
	"Sadayoshi" = 0
	"Sojun" = 0
	"Sho" = 0
	"Sumitada" = 0
	"Susumu" = 0
	"Tadakazu" = 0
	"Tadatsune" = 0
	"Takashi" = 0
	"Takatora" = 0
	"Tanezane" = 0
	"Tatsuoki" = 0
	"Tenkai" = 0
	"Toshikatsu" = 0
	"Tsunenaga" = 0
	"Ujikane" = 0
	"Ujikazu" = 0
	"Yoritada" = 0
	"Yoshiaki" = 0
	"Yoshinobu" = 0
	"Yoshio" = 0
	"Yuki" = 0
	"Yukiyasu" = 0
}

leader_names = {
	Asai Abe Adachi Akamatsu Akechi Akita Akiyama Akizuki Amago
	Ando Anayama Asakura Ashikaga Asahina
	Chosokabe
	Date
	Hara Hatakeyama Hatano Hayashi Honda Hojo Hosokawa
	Idaten Ii Ikeda Imagawa Inoue Ishida Ishikawa Ishimaki Ito
	Kikkawa Kiso Kitabatake
	Maeda Matsuda Matsudaira Miura Mikumo Miyoshi Mogami M�ri
	Nanbu Nitta Niwa
	Oda �tomo Ouchi
	Rokkaku
	Sakai Sakuma Shimazu Shiba Sanada Sogo Suwa
	Takeda Takigawa Toda Toki Tokugawa Toyotomi Tsutsui
	Uesugi Ukita
	Yagyu Yamana
}

ship_names = {
	"Asai Maru" "Abe Maru" "Adachi Maru" "Akamatsu Maru" "Akechi Maru"
	"Akita Maru" "Akiyama Maru" "Akizuki Maru" "Amago Maru" "Ando Maru"
	"Anayama Maru" "Asakura Maru" "Ashikaga Maru" "Asano Maru" "Ashina Maru"
	"Atagi Maru" "Azai Maru"
	"Bito Maru" "Byakko Maru"
	"Chiba Maru" "Chousokabe Maru"
	"Date Maru" "Doi Maru"
	"Fujiwara Maru" "Fuji-san Maru"
	"Genbu maru"
	"Haga Maru" "Hatakeyama Maru" "Hatano Maru" "Honda Maru" "Hojo Maru"
	"Hosokawa Maru" "Hachisuka Maru" "Hayashi Maru" "Hiki Maru"
	"Idaten Maru" "Ikeda Maru" "Imagawa Maru" "Ishida Maru" "Ishikawa Maru"
	"Ishimaki Maru" "Ii Maru" "Inoue Maru" "Ito Maru"
	"Kikkawa Maru" "Kiso Maru" "Kisona Maru" "Kitabatake Maru" "Kyogoku Maru"
	"Maeda Maru" "Matsuda Maru" "Matsudaira Maru" "Miura Maru" "Mikumo Maru"
	"Miyoshi Maru" "Mogami Maru" "Mori Maru"
	"Nitta Maru" "Niwa Maru" "Nihon Maru" "Nanbu Maru"
	"Oda Maru" "Otomo Maru" "Ouchi Maru"
	"Rokkaku Maru"
	"Sakai Maru" "Sakuma Maru" "Satake Maru" "Shimazu Maru" "Shiba Maru"
	"Sanada Maru" "Sogo Maru" "Suwa Maru" "Seiryu Maru" "Suzaku Maru"
	"Takeda Maru" "Tokugawa Maru" "Taira Maru" "Toyotomi Maru" "Tada Maru"
	"Toki Maru" "Tsugaru Maru" "Tsutsui Maru" "Tenno Maru"
	"Uesugi Maru" "Ukita Maru" "Uchia Maru"
	"Yamana Maru" "Yagyu Maru"
}
