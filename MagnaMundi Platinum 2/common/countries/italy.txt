graphical_culture = latingfx

color = { 125  171  84 }

historical_ideas = {
	national_conscripts
	patron_of_art
	shrewd_commerce_practise
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	italian_condotta
	swiss_landsknechten
	gaelic_mercenary
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	anglofrench_line
	british_square
	napoleonic_square
	french_impulse
	mixed_order_infantry
	western_medieval_knights
	chevauchee
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Francesco #0" = 200
	"Carlo #4" = 140
	"Alfonso #0" = 120
	"Ferdinando #0" = 100
	"Vittorio Amedeo #0" = 80
	"Federigo #2" = 60
	"Cosimo #0" = 60
	"Ercole #0" = 60
	"Ludovico #4" = 40
	"Amedeo #0" = 40
	"Alessandro #0" = 40
	"Borso #0" = 40
	"Carlo Emanuele #0" = 40
	"Filiberto #0" = 40
	"Filippo #0" = 40
	"Francesco #0 Maria" = 40
	"Guidobaldo #0" = 40
	"Ranuccio #0" = 40
	"Vincenzo #0" = 40
	"Antonio #0" = 20
	"Carlo Felice #0" = 20
	"Cesare #0" = 20
	"Emanuele Filiberto #0" = 20
	"Fabio #0" = 20
	"Francesco Giacinto #0" = 20
	"Francesco #0 Stefano" = 20
	"Galeazzo Maria #0" = 20
	"Gian Galeazzo #0" = 20
	"Gian Gastone #0" = 20
	"Giovanni Maria #0" = 20
	"Guglielmo #0" = 20
	"Ludovico #4 Maria" = 20
	"Lorenzo #0" = 20
	"Massimiliano #0" = 20
	"Odoardo #0" = 20
	"Ottavio #0" = 20
	"Pier Luigi #0" = 20
	"Pietro Leopoldo #0" = 20
	"Rafaello #0" = 20
	"Rinaldo #0" = 20
	"Bernardo #1" = 10
	"Sigismondo #1" = 10
	"Folco #0" = 10
	"Gaspare #0" = 10
	"Gherardo #0" = 10
	"Girolamo #0" = 10
	"Giulio #0" = 10
	"Obizzo #0" = 10
	"Pietro #0" = 10
	"Vittorio #0" = 10
}

leader_names = {
	"Appiani d'Aragona" "Arborio Mella" 
	Barberini Barzagli Boncompagni Borghese Borromeo Buonarroti "di Busca"
	"di Campofregoso" Carafa "di Card�" "del Carreto" Casanova Castagna
	Castagnaro "di Cavour" Chigi Colonna Contarini Conti Corsini Cybo 
	"Doria di Ciri�" Doria-Landi Doria-Pamphili
	d'Este 	"di Farnese" "di Ferrari"
	"Gaetani dell'Aquila" Galilei Gambacorta Gentileschi Giustiniani Gonzaga Grimaldi
	Ludovisi Malaspina "di Medici" Melzi Montefeltro Orsini
	"di Paesana" "Paleologi Montferrato" Pallavicini Paradisi Petrucci Pico Pignatelli
	"dalla Rosa" "della Rovere"
	"di Saluzzo" "di Savoia" Sfondrati Sforza Spinola
	"della Torre" "della Ubaldini" Vico Visconti
}

ship_names = {
	Medea Vulcano Gloria Minerva Laatea
	Diligenza Vittoria Eolo Concordia Forza 
	Fama Sirena Falcon Ercole Idra "San Andrea"
	"San Zaccaria" "San Spiridon" "San Gaetano"
	"Fortuna Guerriera" Terror Aquileta Constanza
	"Regina del Mar" "San Nicolo" Nettuno Rosa
	Fenice Iride Aurora Tigre Giove "Rizzo d'Oro"
	"Amazzone Guerriera"
}

army_names = {
	"Armata di $PROVINCE$"
}
