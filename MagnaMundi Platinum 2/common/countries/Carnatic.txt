#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 132  70  102 }

historical_ideas = {
	national_conscripts
	grand_army
	national_trade_policy
}

historical_units = { #optimized by berto#
	indian_footsoldier
	indian_archers
	indian_arquebusier
	south_indian_musketeer
	reformed_mughal_musketeer
	punjabi_irregulars
	maharathan_guerilla_warfare
	bhonsle_infantry
	indian_rifle
	indian_offensive_platoon_fire
	indian_irregular_skirmisher
	indian_offensive_drill
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar 
	sikh_hit_and_run
	bhonsle_cavalry
	indian_arme_blanche
	indian_chasseur
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Singhana #0" = 10
	"Ktishna #0" = 10
	"Mandaeva #0" = 10
	"Amana #0" = 10
	"Ramachandra #0" = 10
	"Sankaradeva #0" = 10
	"Hanapaladeva #0" = 10
}

leader_names = {
	Trishna
	Sashi
	Revati
	Patachli
	Omkar
	Nidhi
	Nidra
	Puranjay
	Vishaal
	Yaksha
}

ship_names = {
	Chambal Devi Durga Ganga
	Gayatri Jalsena "Kala Jahazi"
	Lakshmi "Lal Jahazi" Nausena
	"Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Ratri Sagar
	Sarasvati Sita Spiti Yamuna
}
