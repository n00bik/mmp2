#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 153  138  161 }

historical_ideas = {
	national_conscripts
	military_drill
	divine_supremacy
	engineer_corps
	grand_army
	merchant_adventures
	shrewd_commerce_practise
	patron_of_art
	national_trade_policy
	espionage
	battlefield_commisions
	cabinet
}

historical_units = { #optimized by berto#
	mamluk_archer
	mamluk_duel
	taifat_al_rusa
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	persian_rifle
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	muslim_cavalry_archers
	mamluk_cavalry_charge
	ottoman_timariot
	voyniq_heavy_cavalry
	shaybani
	suvarileri_cavalry
	mongol_swarm
	mamluk_musket_charge
	ali_bey_reformed_infantry
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
        "Sulayman #0" = 60
        "'Abdall�h #0" = 40
	"'Al� #0" = 40
	"'Umar #0" = 20
	"D�'�d #0" = 20
	"Hasan #0" = 20
	"Sa'�d #0" = 20
	"'Abbas #0" = 0
        "'Abd al-Karim #0" = 0
	"'Abd al-Rahman #0" = 0
	"Anwar #0" = 0
	"Atuf #0" = 0
	"Bakr #0" = 0
	"Fahd #0" = 0
	"Faruq #0" = 0
	"Fouad #0" = 0
	"Hakim #0" = 0
        "Harun #0" = 0
	"Hikmat #0" = 0
	"Hisham #0" = 0
	"Husayn #0" = 0
	"Isma'il #0" = 0
	"Jafar #0" = 0
        "Jamaal #0" = 0
	"Karim #0" = 0
	"Khalil #0" = 0
        "Mal�k #0" = 0
	"Mirza #0" = 0
	"Muhammad #0" = 0
	"Mustafa #0" = 0
	"Qasim #0" = 0
	"Rash�d #0" = 0
	"Rusul #0" = 0
	"Shimun #0" = 0
	"Tar�q #0" = 0
	"Usama #0" = 0
	"Yasir #0" = 0
	"Yusuf #0" = 0
	"Zaid #0" = 0
	"Z�yad #0" = 0
	"Zulqifar #0" = 0
}

leader_names = {
	Agha
	Ahmad
	Akif
	Husain
	Javid
	Necip
	Nuri
	Rahman
	Resid
	Zaki
}

ship_names = {
	Faydhi
	Fazil
	Kadri
	Jamal
	Midhat
	Nadhif
	Nadhim
	Namiq
	Omer
	Sirri
}
