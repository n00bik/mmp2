#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 206 188 153 } 

historical_ideas = {
	patron_of_art
	national_conscripts
	smithian_economics
}

historical_units = { #optimized by berto#
	south_american_spearmen
	south_american_warfare
	inca_mountain_warfare
	native_indian_mountain_warfare
	peruvian_guerilla_warfare
	south_american_gunpowder_warfare
	american_western_franchise_warfare
	westernized_south_american
	american_late_square_arquebusier
	american_defensive_musketeer
	american_countermarch_musketeer
	american_defensive_platoon_fire
	american_defensive_line
	american_defensive_drill_infantry
	american_columnar_infantry
	american_impulse_infantry
	american_breech_loaded_rifleman
	native_indian_horsemen
	comanche_swarm
	sioux_dragoon
	american_caracolle
	american_galoop
	american_dragoon
	american_cuirassier
	american_arme_blanche
	american_chasseur
	american_light_hussar
	american_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Autachi Duchicela #1" = 20
	"Hualcopo Duchicela #1" = 20 
	"Cacha Duchicela #0" = 20
	"Caran #14" = 50
	"Anco Coyuch #0" = 10
	"Caja Cimcim #0" = 10
	"Chumun Caur #0" = 10
	"Guaman Chumu #0" = 10
	"Michan Caman #0" = 10
	"Guari Cur #1" = 5
	"Nancen Pico #1" = 5
	"Tacaynamo #1" = 1
	"Aiyotechapun #0" = 0
	"Buvaovamu #0" = 0
	"Caja C�pac #0" = 0
	"Cama Bergu #0" = 0
	"Chumtiku #0" = 0
	"Cuyuchi #0" = 0
	"Docoshamu #0" = 0
	"Draytonamu #0" = 0
	"Ehenymacu #0" = 0
	"Felan Tamuc #0" = 0
	"Gusmango #0" = 0
	"Ilsayunamu #0" = 0
	"Jotiantikaka #0" = 0
	"Katitika #0" = 0
	"Litun Orco #0" = 0
	"Melantonan #0" = 0
	"Michan Chimor #0" = 0
	"Muhton Belan #0" = 0
	"Nartamamu #0" = 0
	"Nehuametepun #0" = 0
	"Opolcynamun #0" = 0
	"Pacatmamu #0" = 0
	"Puznaymun #0" = 0
	"Relyen Padnu #0" = 0
	"Seton Apac #0" = 0
	"Seran Tugun #0" = 0
	"Taracun #0" = 0
	"Troynuremo #0" = 0
	"Urtyantemu #0" = 0
	"Vetsalynun #0" = 0
	"Yetuniamu #0" = 0
	"Zergoetanu #0" = 0
}

leader_names = {
	Epiclachima #Brother of Hualcopo Duchicela
	Tomala #Led resistance to the Incas
	Nazacota Puento #General
	Pintag #General
	Guacricaur
	Tacaynamo
	Tiso
	Nancempinco
	Chilche
	Maras
	Luyes
	Maila
	Illa
	Rimac
}

ship_names = {
	Viracocha "Apo Inti" 
	"Chiqui Illapa" Mamquilla Yakumama Mamacocha 
	Pachakama Waca "Chan Chan" Chimor Chicama Moche 
	Viru Sipan Huancaco
}

ai_hard_strategy = { 
	threat = {
		id = INC
		value = 200
		}
	rival = { 
		id = CUZ
		value = 50
		}
	rival = { 
		id = HUA
		value = 50
		}
	rival = { 
		id = AYM
		value = 50
		}
	rival = { 
		id = ICA
		value = 50
		}
	conquer_prov = {
		id = 812 #Chanchan
		value = 25
		}
}
