#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 162  134  164 }

historical_ideas = {
	national_conscripts
	national_trade_policy
	bill_of_rights 	
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	jagir_infantry
	rajput_musketeer
	sikh_hit_and_run
	kalapieda_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Muzaffar #1" = 20
	"Mansur #1" = 20
	"Bahadur #1" = 20
	"Ungku #1" = 20
	"'Alam #1" = 20
}

leader_names = {
	Mahidol
	Nitaya
	Pichit
	Prem
	Puarborn
	Samak
	Sarit
	Sunthorn
	Thawan
	Traivut
}

ship_names = {
	Pattani Yala Narathiwat "Sai Buri"
	"Na Thawi" "Sungai Kolok" 
	Yaring Thepha Tumpat "Bukit Ulu"
}
