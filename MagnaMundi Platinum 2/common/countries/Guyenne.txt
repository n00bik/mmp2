#Country Name: Please see filename.

graphical_culture = latingfx

color = { 32  144  204 }

historical_ideas = {
	patron_of_art
	battlefield_commisions
	church_attendance_duty
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	scottish_highlander
	anglofrench_line
	prussian_drill
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Julien #0" = 40
	"Henri #5" = 20
	"Charles #1" = 20
	"Jean #1" = 20
	"Daniel #0" = 20
	"Gabriel #0" = 20
	"Louis #0" = 20
	"Louis-Jean-Charles #0" = 20
	"Philippe #0" = 20
	"Pierre #0" = 10
	"Andr� #0" = 5		
	"Godefroi #0" = 5
	"Guillaume #10" = 1
	"Edouard #3" = 1
	"Richard #2" = 1
	"Arnaud #0" = 0
	"Aymeric #0" = 0
	"Bartomiu #0" = 0
	"Benedit #0" = 0
	"Bernat #0" = 0
	"Bertran #0" = 0
	"Eneco #0" = 0
	"Enric #0" = 0
	"Est�ve #0" = 0
	"Folquet #0" = 0
	"Frances #0" = 0
	"Gast� #0" = 0
	"Gaufr� #0" = 0
	"Guilhem #0" = 0
	"Jacme #0" = 0
	"Jordan #0" = 0
	"Loys #0" = 0
	"Matheu #0" = 0
	"Miqueu #0" = 0
	"Nicolau #0" = 0
	"Per #0" = 0
	"Pol #0" = 0
	"Thomas #0" = 0
	"Vicentz #0" = 0
	"Xavier #0" = 0
}

leader_names = {
	"d'Al�gre" "d'Altier de Borne" "d'Apchon" "d'Arfeuille" Arnauld "d'Arpajon"
	"de Baffie" "de Balzac" Barbey "de Basredon" "de Beauverger" "de Blanchefort" "Bouill� du Chariol" "Brachet" "de Brossadol" "de Brugi�re"
	"de Canilhac" "Carbonni�res de La Barthe" "de Cardaillac" "de Carlat" "de Casteras" "de Chalen�on" "de Chazeron" "de Clermont" "de Cressac" "du Croc"
	"de Foug�res"
	"de Grandsaigne"
	"d'Humi�res"
	"de Lavie"
	"de Magnac" "de Massebeau" "de Mercoeur" "de Montboissier" "de Montgascon" "de Montpensier" "de Montvert"
	"de N�restang"
	"d'Oliergues"
	"de Pennautier" "de Pascal" "de Polignac"
	"de la Queuille"
	"de Rochefort-d'Ailly" "de Rogier-Beaufort"
	"de Saint Nectaire" "de Scorailles"
	"de Terreveire" "de Thiers"
	"d'Ussel"
}

ship_names = {
        Adour Africain Aigle "Aigle d'Or" "Aimable Nanon"
        "Aimable Marthe" "Aimable Rose" Alexandre "Ste Anne de Bordeaux" Amiti�
        Ang�lique Am�ricain Amphitrite "Arc en Ciel" Bayonnaise 
         "Belle Poule" Bienfaisant Biscayenne Bizarre "Bonnes Amies" 
        Bordelais Brillant Caroline Castor Catiche
        Charente "Charmante Manon" "Charmante Nancy" "Charmante Rachel" "Ch�teau de Bayonne"
        Ch�zine "Commerce de Bordeaux" "Comte de Toulouse" Content D�bonnaire
        D�daigneuse "Deux Fr�res" Diamant Diligente "Duc d'Aquitaine"
        Elisabeth Entreprenante Esp�rance Extravagante Favori
        Fendant Flamand Gaillard Galant Godichon
        "Grand Coureur" "Grand Sirius" "Gros Ventre" Hercule Hermine 
        Imp�rieuse Indien Intr�pide Jolie Joseph
        Labourt Laurence L�g�re Licorne Lion
        Machault Marchand Mars Mascarin Maure
        M�g�re M�lampe Mignon Neptune "Nouvelle Victoire"
        Oiseau Opale P�lican Ph�nix Pollux
        "Quatre Fr�res" Rameau "Reine Marie" Renomm�e Rostan
        "Royal Daim" "Saint Jacques" "Saint Pierre" Sir�ne "Soleil Royal"
        Swinton Tamponne "Toison d'Or" Tourterelle Trinit�
        Triton Utile Valeur V�nus "Ville de Bayonne"
}

army_names = {
         "Arm�e d'Aquitaine"     "Arm�e du Bordelais"  "Arm�e du Blayais"
         "Arm�e du Pays de Buch" "Arm�e du Bazadais"   "Arm�e du Queyran"
         "Arm�e du Fronsadais"   "Arm�e du Libournais" "Arm�e de Castillon" 
         "Arm�e d'Albret"        "Arm�e de la Garonne" "Arm�e de l'Agenais"
	 "Arm�e de $PROVINCE$" 
}
