#Country Name: Please see filename.

graphical_culture = latingfx

color = { 103  62  185}

historical_ideas = {
	merchant_adventures
	military_drill
	smithian_economics
}

historical_units = { #optimized by berto#
	halberd_infantry
	western_medieval_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	french_bluecoat
	dutch_maurician
	swedish_gustavian
	scottish_highlander
	anglofrench_line
	prussian_drill
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Charles #1" = 40
	"Guillaume #6" = 20
	"Jean #2" = 20
	"Marghar�the #2" = -20
	"Arnold #0" = 20
	"Augustin #0" = 20
	"Adolphe #0" = 10
	"Albertine #0" = -10
	"Anne #0" = -10
	"Cornelis #0" = 10
	"El�onore #0" = -10		
	"Humbert #0" = 10
	"Pierre #0" = 10
	"Albert #1" = 5
	"Antoine #0" = 5
	"Baudouin #6" = 1
	"Adam #0" = 0
	"Adrien #0" = 0
	"Christian #0" = 0
	"Edgard #0" = 0
	"�tienne #0" = 0
	"Evrard #0" = 0
	"Fran�ois #0" = 0
	"Gaspard #0" = 0
	"G�rard #0" = 0
	"Hughes #0" = 0
	"Ignace #0" = 0
	"J�r�me #0" = 0
	"Joachim #0" = 0
	"Marc #0" = 0
	"Onesime #0" = 0
	"Orland #0" = 0
	"Raphael #0" = 0
	"Rodolphe #0" = 0
	"Roger #0" = 0
	"Thomas #0" = 0
	"Vincent #0" = 0
	"Xavier #0" = 0
	"Zacharie #0" = 0
	"Roeland #0" = 0
}

leader_names = {
	"de Cuire" "du Trieux" "de Freest"
	Waerseggers Brouckaert "de Avesnes"
	"de Roucy" "de Namur" Galindez Liderie 
	"van Bylandt" "van Dinther" "van Heerdt"
	"van heiden" "van hogendorp" "van Lynden"
	"van Oberndorff" "von Ranzow"
	"von Rechteren" "von Quadt"
}

ship_names = {
	"Reginar I" Sigard "Reginar II" Godfrey
	"Reginar III" Richar Reginald "Godfrey II"
	Herman Amalric Warin Arnulf "Baldvin I"
	"Baldvin II"
}

army_names = {
	"Leger van $PROVINCE$"  }

fleet_names = {  "Vloot van $PROVINCE$" 
	 }
	 
