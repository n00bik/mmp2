#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 161  126  128 }

historical_ideas = {
	national_conscripts
	military_drill
	cabinet
}

historical_units = { #optimized by berto#
	eastern_medieval_infantry
	bardiche_infantry
	germanized_pike
	strel'tsy_infantry
	muscovite_musketeer
	hajduk_irregulars
	saxon_infantry
	eastern_carabinier
	russian_mass
	eastern_european_columnar_infantry
	eastern_european_impulse_infantry
	eastern_european_breech_loaded_rifleman
	eastern_knights
	druzhina_cavalry
	slavic_stradioti
	hungarian_hussar
	pancerni_cavalry
	polish_winged_hussar
	eastern_uhlan
	muscovite_cossack
	russian_cossack
	eastern_skirmisher
	eastern_european_light_hussar
	eastern_european_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Radu #2" = 100
	"Vlad #2" = 100
	"Alexandru #1" = 80
	"Constantin #0" = 60
	"George #0" = 60
	"Grigore #0" = 60
	"Mihnea #0" = 60
	"Nicolae #0" = 60
	"Besarab #1" = 40
	"Mircea #1" = 40
	"Vladislav #1" = 40
	"Mattia #0" = 40
	"Mihail #0" = 40
	"Petru #0" = 40
	"Radu Ilias #0" = 40
	"Stefan #0" = 40
	"Alexandru Ilias #0" = 20
	"Gabriel #0" = 20
	"Leon #0" = 20
	"Moise #0" = 20
	"Neagoie Besarab #0" = 20
	"Petrascu #0" = 20
	"Radu Leon #0" = 20
	"Radu Mihnea #0" = 20
	"Scarlat #0" = 20
	"Serban #0" = 20
	"Simeon #0" = 20
	"Teodosiu #0" = 20
	"Vintila #0" = 20
	"Vladut #0" = 20
	"Milos #0" = 5
	"Dan #2" = 1
	"Corneliu #0" = 0
	"Flaviu #0" = 0
	"Iuliu #0" = 0
	"Octavian #0" = 0
	"Sergiu #0" = 0
	"Teodor #0" = 0
	"Valeriu #0" = 0
	"Virgiliu #0" = 0
}

leader_names = {
	Basarab Brāncoveanu
	Cantacuzino Caragea Craiovescu
	Dracul Duca
	Ghica Golescu
	Ilias Ipsilanti
	Mavrocordat Mircea Moruzi
	Racovita
	Sutu 
	Tepelus Tolmay
	Vacarescu Voda
}

ship_names = {
	Dracul
	Viteazul
	Putera mari
	Sperietoaria
	"Frumuseatsa valurilor"
	Posada
	Calugareni
	Rovine
	Sirmium
	"Atacul de noapte"
	Litovoi
}

army_names = {
	"Oastea de $PROVINCE$" 
}
