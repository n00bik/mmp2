#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 201  28  54 }

ai_hard_strategy = { personality = diplomacy }

historical_ideas = {
	national_trade_policy #a royal monopoly on trade used to dominate nearby states; a focus on controlling trade in the archepelago
	naval_fighting_instruction #sent several naval expeditions and expanded militarily
	patron_of_art #a sophisticated court with refined taste in art and literature
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	jagir_infantry
	rajput_musketeer
	sikh_hit_and_run
	kalapieda_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}


monarch_names = {
	"Girindrawardhana #0" = 20
	"Girishawardhana #0" = 20
	"Kertabumi #0" = 20
	"Kertawijaya #0" = 20
	"Rajasawardhana #0" = 20
	"Suraprabhawa #0" = 20
	"Wikramawardhana #0" = 20
	"Hayam Wuruk #1" = 15
	"Kalagamet #1" = 15
	"Raden Wijaya #1" = 15
	"Suhita #0" = -15
	"Sri Gitarja #1" = -10
	"Kertawardhana #0" = 10
	"Raden Sotor #0" = 10
	"Raden Sumirat #0" = 10
	"Singhawardhana #0" = 10
}

leader_names = {
	Hamengku
	Agung
	Slamet
	Budi
	Harto
	Karno
	Wani
	Mas
	Kencono
	Ratri
}

ship_names = {
	Pendet Legong Baris Topeng Barong
	Kecak Batur Catur Bratan Pohen Lesong
	Sangyang Batukaru Patas Musi Mesehe
	Agong Seraya Merbuk Sanglang Kelatakan
}
