#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 136  157  23 }

historical_ideas = {
	national_conscripts
	military_drill
	bureaucracy
}

historical_units = { #optimized by berto#
	eastern_medieval_infantry
	bardiche_infantry
	germanized_pike
	strel'tsy_infantry
	muscovite_musketeer
	hajduk_irregulars
	saxon_infantry
	eastern_carabinier
	russian_mass
	eastern_european_columnar_infantry
	eastern_european_impulse_infantry
	eastern_european_breech_loaded_rifleman
	eastern_knights
	druzhina_cavalry
	slavic_stradioti
	hungarian_hussar
	pancerni_cavalry
	polish_winged_hussar
	eastern_uhlan
	muscovite_cossack
	russian_cossack
	eastern_skirmisher
	eastern_european_light_hussar
	eastern_european_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Alexandru #1" = 180
	"Stefan #2" = 160
	"Constantin #0" = 120
	"Grigore #0" = 60
	"Ioan #0" = 60
	"Mihail #0" = 60
	"Petru #2" = 40
	"Bogdan #1" = 40
	"Antioh #0" = 40
	"Stefanito #0" = 40
	"George #0" = 40
	"Petru #2 Aron" = 20
	"Ilias #1" = 20
	"Aron #0" = 20
	"Dimitrie #0" = 20
	"Dumitrascu #0" = 20
	"Emanuel #0" = 20
	"Eustratius #0" = 20
	"Gaspar #0" = 20
	"George Stefan #0" = 20
	"Iancu #0" = 20
	"Ilias Alexandru #0" = 20
	"Ioan #0 Iacob" = 20
	"Ioan Teodoru #0" = 20
	"Jeremie #0" = 20
	"Matvei #0" = 20
	"Milos #0" = 20
	"Moise #0" = 20
	"Nicolae #0" = 20
	"Radu #0" = 20
	"Simeon #0" = 20
	"Vasile #0" = 20
	"Ioan #0 Bogdan" = 10
	"Sergei #0" = 5
	"Antiokh #0" = 5
	"Roman #2" = 1
	"Andrei #0" = 0
	"Iulian #0" = 0
	"Mircea #0" = 0
	"Virgilius #0" = 0
}

leader_names = {
	Callimachi Campineanu Cantacuzino Cantemir Caragea Craiovescu
	Duca
	Ghica
	Ilias
	Lupu
	Mavrocordat Moruzi Movila Musatin
	Norocea 
	Racovita Ruset                         
	Sturdza Sutu
	Voda
}

ship_names = {
	"Athleta Christi"
	Baia "Bogdan I"
	Iuga
	Moldova
	"R�ul Prut"
	"Stefan cel Mare" Suceava
	"T�rgul Moldovei"
	Vaslui
}

army_names = {
	"Oastea de $PROVINCE$" 
}

ai_hard_strategy = {
	personality = military
}
