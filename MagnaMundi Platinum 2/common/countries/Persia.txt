#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 157 60 39  } #Mahogany

historical_ideas = {
	national_conscripts
	divine_supremacy
	patron_of_art
}

historical_units = { #optimized by berto#
	persian_footsoldier
	mamluk_duel
	persian_shamshir
	ottoman_janissary
	tofongchis_musketeer
	afsharid_reformed_infantry
	persian_rifle
	near_eastern_offensive_drill
	muslim_mass_infantry
	ottoman_nizami_cedid
	near_eastern_columnar_infantry
	near_eastern_impulse_infantry
	near_eastern_breech_loaded_rifleman
	persian_cavalry_charge
	muslim_cavalry_archers
	ottoman_timariot
	topchis_artillery
	qizilbash_cavalry
	ottoman_toprakli_hit_and_run
	mongol_swarm
	mamluk_musket_charge
	ali_bey_reformed_infantry
	durrani_dragoon
	ottoman_lancer
	near_eastern_light_hussar
	near_eastern_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Esma'il #0" = 30
	"Tam�shp #0" = 20
	"Mohammad Kodab�nda #0" = 10
	"'Abb�s #0" = 30
	"Safi #0" = 15
        "Solaym�n #0" = 20
        "Hosayn #0" = 10
        "N�der #0 Sh�h" = 10
        "'�del #0 Sh�h" = 10
        "Ebr�him #0" = 10
	"Sh�h Rukh #0" = 10
	"N�der Mirza #0" = 5
	"Karim #0 Kh�n" = 10
	"Abu'l Fath #0" = 10
	"Mohammad Ali #0" = 10
        "Mohammad S�diq #0" = 10
        "'Ali Mor�d #0" = 10
        "Ja'far #0" = 10
}

leader_names = {
	Sabiri
	Jalili
	Azizi
	Shahy
	Daei
	Qazai
	Dalwand
	Ahrari
	Fatemi
	Sabet
	Ansari
}

ship_names = {
	Abathur "Adamn Kasia" "Aesma Daeva" Agas
	Ahriman Ahura "Ahura Mazda" Ahurani Airyaman
	"Aka Manah" Aladdin Allatum Amashaspan Ameratat
	"Amesha Spentas" Anahita "Angra Mainyu" Anjuman
	"Apam-natat" Apaosa Aredvi Arishtat Armaiti Arsaces
	"Asha vahista" Asman "Asto Vidatu" "Astvat-Ereta"
	Atar "Azi Dahaka" Baga Bahram Burijas Bushyasta Buyasta
	Camros Daena Daevas Ahaka Dahhak Dena Dev Diwe Drug
	Drvaspa Frashegird Fravashis Gandarewa "Gao-kerena"
	Gayomard Gayomart "Geus-Tasan" "Geus-Urvan" Haoma
	Haurvatat Humay Hvar Hvarekhshaeta Indar Indra Izha
	Jamshid Jeh Karshipta Kavi Khara "Khshathra vairya"
	Kundrav Mah Mahre Mahrianag Manu Manuchihir Mao 
	Mashyane Mashye Menog Mithra Nairyosangha Nanghaithya
	Neriosang Ormazd Peris Peshdadians Rapithwin Rashnu
	Rustam Saurva Simurgh "Spenta Mainyu" Sraosa Srosh
	Tawrich Thunaupa Tistrya Tushnamatay Vanant Vata
	Verethragna "Vohu Manah" Vouruskasha Yam Yasht Yazata
	Yezidi Yima Zal Zam "Zam-Armatay" Zarathustra Zarich
	"Zend-Avesta" Zurvan
}
