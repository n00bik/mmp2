#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 180  137  148 }

historical_ideas = {
	merchant_adventures
	military_drill
	church_attendance_duty
}

historical_units = { #optimized by berto#
	african_spearmen
	mali_tribal_warfare
	african_hill_warfare
	adal_guerilla_warfare
	adal_gunpowder_warfare
	african_western_franchise_warfare
	westernized_adal
	african_soldaty_infantry
	african_offensive_musketeer
	african_countermarch_musketeer
	african_offensive_platoon_fire
	african_irregular_skirmisher
	african_offensive_mass_infantry
	african_columnar_infantry
	african_impulse_infantry
	african_breech_loaded_rifleman
	nubian_knight
	somali_light_cavalry
	ethiopian_cavalry
	african_caracolle
	african_galoop
	african_carabinier
	african_cuirassier
	african_medium_hussar
	african_uhlan
	african_light_hussar
	african_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Mohammed #0" = 100
	"Ibrahim #0" = 60
	"Umar Din #0" = 20
	"Barakat Bin Umar #0" = 20
	"Bat'iah D�l Wanbara #0" = 20
	"Nur #0" = 20
	"'Uthman #0" = 20
	"Talha #0" = 40
	"Nasr #0" = 20
	"Sa'ad al-Din #0" = 20
	"Sabir al-Din #0" = 20
	"Sadiq #0" = 20
	"Malaq Adam #0" = 20
	"'Ali #0" = 40
	"Hashim #0" = 20
	"Abdallah #0" = 20
	"Abu Bakr #0" = 20
	"Khalaf #0" = 20
	"Hamid #0" = 20
	"Yusuf #0" = 20
	"Ahmad #0" = 40
	"Kadhafo #0" = 0 
	"Kadhafo Muhammad #0" = 0 
	"Aydahis #0" = 0 
	"Anfari #0" = 0
	"Aydays #0" = 0
	"Haqq al-Din #0" = 0
	"Jamal al-Din #0" = 0
	"Badlay #0" = 0
	"Shams al-Din #0" = 0
	"Hanfadhe #0" = 0
	"Yahya #0" = 0
	"Abd al-Shakur #0" = 0
	"Abd al-Rahman #0" = 0
	"Abd al-Karim #0" = 0
	"Fakhr al-Din #0" = 0
	"Mansur #0" = 0
	"Husayn #0" = 0
	"Baziwi #0" = 0
	"Fasil #0" = 0
}

leader_names = {
	Tsige
	Barr�
	Bol
	Deng
	Abdelrashid
	Semer
	Yagoub
	Leta
	Lemn
	Teka
}

ship_names = {
	Aura Awash Asayita Berahle Chifra
	Dallol Dubti Dulecha Elidar Ewa Gewane
	Koneba Mille Semera Simurobi Yalo
}
