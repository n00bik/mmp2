#Country Name: Please see filename.

graphical_culture = latingfx

color = { 165  171  115 }

historical_ideas = {
	patron_of_art
	national_trade_policy
	bureaucracy
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	italian_condotta
	swiss_landsknechten
	gaelic_mercenary
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	anglofrench_line
	british_square
	napoleonic_square
	french_impulse
	mixed_order_infantry
	western_medieval_knights
	chevauchee
	french_caracolle
	french_carabinier
	french_cuirassier
	swedish_arme_blanche
	open_order_cavalry
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Cesare #0" = 20
	"Pandolfo #0" = 20
	"Rafaello #0" = 20
	"Francesco #0" = 20
	"Fabio #0" = 20
	"Borghese #0" = 20
	"Gaspare #0" = 10
	"Adriano #0" = 0
	"Alessio #0" = 0
	"Baldovino #0" = 0
	"Beniamino #0" = 0
	"Biagio #0" = 0
	"Bonaventura #0" = 0
	"Celio #0" = 0
	"Constantino #0" = 0
	"Damiano #0" = 0
	"Danilo #0" = 0
	"Eros #0" = 0
	"Eustachio #0" = 0
	"Evangelista #0" = 0
	"Fortunato #0" = 0
	"Gennaro #0" = 0
	"Giancarlo #0" = 0
	"Gianpaolo #0" = 0
	"Giovanni #0" = 0
	"Giustino #0" = 0
	"Innocenzo #0" = 0
	"Leone #0" = 0
	"Marco #0" = 0
	"Mauro #0" = 0
	"Orfeo #0" = 0
	"Pasquale #0" = 0
	"Rinaldo #0" = 0
	"Roberto #0" = 0
	"Sergio #0" = 0
	"Severino #0" = 0
	"Ugo #0" = 0
	"Valerio #0" = 0
	"Vincenzo #0" = 0
	"Vitale #0" = 0
}

leader_names = {
        Accarigi Alighieri Amelia Barone Barzagli Borghese Borgia
        Castro Cecchereni Compagnoni Damiani "Di Stefano"
	Fossombroni Galilei Gallo Gentile "Golia di Siena" Griccioli
	Grosso Guerra Idoni Labriola Nardi Neri Oddo Orlando Palumbo
	Pannocchieschi Petrucci Pianella Piccolomini "del Piero" Pirlo "da Porcari"
	Quercegrossa Riario "de Rossi" Salimbeni "da Sangallo" Segni "da Staggia"
	Testa Torricelli Uccello Vagliagli Visconti Vitali Viviani Zaccardo  Zeti
}

ship_names = {
	"Ambrogio Lorenzetti" Asciano
	"Bartolomeo Guidi" "Bonaguida Lucari"
	"Carlo IV" "Castelnuovo Berardenga" Chianti  Costalpino "Crete Senesi"
	Donatello "Duccio di Buoninsegna" 
	Elsa
	"fiumi Arbia" "Francesco di Rinaldo"
	"Ghibellini" "Giovanni di Balduccio"
	"Isola d'Arbia" 
	Merse "Minuccio di Rinaldo" "Misser santo Micchele arcangelo" Montagnola
	Montaperti Monteriggioni "Monteroni d'Arbia"
	"Orlando Bonsignori"
	"San Bernardino" "San Domenico" "San Francesco" "San Galgano"
	"San Giovanni" "San Miniato" "Sant'Ansano" "Santa Caterina"
	"Santa Maria dei Servi" "Santissima Annunziata" Senius Sovicille 
	"Taverne d'Arbia" 
}

army_names = {
	"Armata di $PROVINCE$"
}
