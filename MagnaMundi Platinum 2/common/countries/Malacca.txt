#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 100  146  197}

historical_ideas = {
	merchant_adventures  
	sea_hawks
	cabinet	
}

historical_units = { #optimized by berto#
	indian_archers
	indian_footsoldier
	indian_arquebusier
	mughal_musketeer
	jagir_infantry
	rajput_musketeer
	sikh_hit_and_run
	kalapieda_infantry
	tipu_sultan_rocket
	indian_defensive_platoon_fire
	indian_defensive_line
	indian_rifled_musketeer
	indian_columnar_infantry
	indian_impulse_infantry
	indian_breech_loaded_rifleman
	indian_elephant
	indian_shock_cavalry
	mughal_mansabdar
	reformed_mughal_mansabdar
	sikh_rifle
	bhonsle_cavalry
	indian_medium_hussar
	indian_uhlan
	indian_light_hussar
	indian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

monarch_names = {
	"Paramesvara #0" = 10
	"Megat Iskander #0" = 10
	"Mumammas #0" = 10
	"Ibrahim #0" = 10
	"Qasim #0" = 10
	"Riayat #0" = 10
	"Mahmid #0" = 10
}

ship_names = {
	Parameswara "Sultan Iskandar" Bendahara Laksamana
	Temenggung "Penghulu bendahari" Shahbandar
	"Hukum Kanun" "Undang-Undang" "Pulau Bakung"
	"Pulau Lingga" Kepulauan "Pulau Singkep" "Kepulauan Riau"
	"Pulau Rempang" "Pulau Kundur" "Pulau Padang"
	"Pulau Bengkaus" "Pulau Tinggi" "Pulau Sibu"
	"Pulau Tioman" "Pulau Aur" "Pulau Rupat"
	"Pulau Rangsang" "Pulau Tebing Tinggi"
}
