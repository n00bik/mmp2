#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 180  20  100 }

historical_ideas = {
	national_conscripts
	excellent_shipwrights
	merchant_adventures
}

historical_units = { #optimized by berto#
	chinese_longspear
	chinese_footsoldier
	chinese_gunner
	asian_arquebusier
	han_banner
	asian_mass_infantry
	asian_defensive_platoon_fire
	asian_defensive_line
	asian_offensive_mass_infantry
	asian_columnar_infantry
	asian_impulse_infantry
	asian_breech_loaded_rifleman
	chinese_lancer
	chinese_steppe
	asian_charge_cavalry
	chinese_dragoon
	reformed_asian_cavalry
	asian_cuirassier
	asian_medium_hussar
	asian_uhlan
	asian_light_hussar
	asian_lancer
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Baoqing #0" = 10
	"Baoyou #0" = 10
	"Baoyuan #0" = 10
	"Chonghe #0" = 10
	"Chenghua #0" = 10
	"Chongzhen #0" = 20
	"Chongning #0" = 10
	"Chunhua #0" = 10
	"Chunxi #0" = 10
	"Chunyou #0" = 10
	"Daguan #0" = 10
	"Deyou #0" = 10
	"Duangong #0" = 10
	"Duanping #0" = 10
	"Hongguang #0" = 20
	"Hongwu #0" = 10
	"Hongxi #0" = 10
	"Hongzhi #0" = 20
	"Huangyou #0" = 10
	"Jiading #0" = 10
	"Jiajing #0" = 20
	"Jiakai #0" = 10
	"Jianlong #0" = 10
	"Jianwen #0" = 10
	"Jianyan #0" = 10
	"Jiaxi #0" = 10
	"Jiayou #0" = 10
	"Jingde #0" = 10
	"Jingding #0" = 10
	"Jingkang #0" = 10
	"Jingtai #0" = 20
	"Jingyan #0" = 10
	"Jingyou #0" = 10
	"Kaibo #0" = 10
	"Kaiqing #0" = 10
	"Kaixi #0" = 10
	"Kangding #0" = 10
	"Longqing #0" = 20
	"Longwu #0" = 20
	"Longxing #0" = 10
	"Mingdao #0" = 10
	"Qiandao #0" = 10
	"Qiande #0" = 10
	"Qianxing #0" = 10
	"Qingli #0" = 10
	"Qingyuan #0" = 10
	"Shaoding #0" = 10
	"Shaowu #0" = 20
	"Shaosheng #0" = 10
	"Shaoxi #0" = 10
	"Shaoxing #0" = 10
	"Taichang #0" = 20
	"Tianqi #0" = 20
	"Tiansheng #0" = 10
	"Tianxi #0" = 10
	"Wanli #0" = 20
	"Xianchun #0" = 10
	"Xianping #0" = 10
	"Xiangxing #0" = 10
	"Xining #0" = 10
	"Xuande #0" = 10
	"Xuanhe #0" = 10
	"Yongle #0" = 10
	"Yongli #0" = 20
	"Yongxi #0" = 10
	"Yuanfeng #0" = 10
	"Yuanfu #0" = 10
	"Yuanyou #0" = 10
	"Zhengtong #0" = 10
	"Zhengde #0" = 20
	"Zhenghe #0" = 10
	"Zhidao #0" = 10
	"Zhihe #0" = 10
	"Zhiping #0" = 10
}

leader_names = {
	Hao
	Song
	Xuan
	Menglin
	Zongyan
	Yan
	Lin
	Ting
	Shixian
	Mingtai
}

ship_names = {
	Gaodi Huidi Wendi Zhaodi Zhangdi "Da Ming Guo"
	Beijing "Zeng He" Ji'nan Nanjing Hangzhou 
	Wuchang Fuzhou Nanchang Guilin Guangzhou
	Guiyang Yunnan Chendo Xi'an Taiyuan "Huang He"
	"Chang Jiang" "Xijiang" "Kung-fu tzu" Mencius
	"Xun Zi" "Yan Hui" "Min Sun" "Ran Geng" "Ran Yong"
	"Zi-you" "Zi-lu" "Zi-wo" "Zi-gong" "Yan Yan"
	"Zi-xia" "Zi-zhang" "Zeng Shen" "Dantai Mieming"
	"Fu Buji" "Yuan Xian" "Gungye Chang" "Nangong Guo"
	"Gongxi Ai" "Zeng Dian" "Yan Wuyao" "Shang Zhu"
	"Gao Chai" "Qidiao Kai" "Gongbo Liao" "Sima Geng"
	"Fan Xu" "You Ruo" "Gongxi Chi" "Wuma Shi" "Liang Zhan"
	"Yan Xing" "Ran Ru" "Cao Xu" "Bo Qian" "Gongsun Long"
}
