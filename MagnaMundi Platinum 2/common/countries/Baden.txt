#Country Name: Please see filename.

graphical_culture = latingfx

color = { 117  161  67 }

historical_ideas = {
	smithian_economics
	national_trade_policy
	cabinet
}

historical_units = { #optimized by berto#
	western_medieval_infantry
	halberd_infantry
	western_men_at_arms
	swiss_landsknechten
	spanish_tercio
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	swedish_gustavian
	british_redcoat
	austrian_grenzer
	austrian_jaeger
	napoleonic_square
	french_impulse
	mixed_order_infantry
	chevauchee
	western_medieval_knights
	swedish_galoop
	french_carabinier
	french_cuirassier
	austrian_hussar
	prussian_uhlan
	british_hussar
	napoleonic_lancers
	large_cast_bronze_mortar
	houfnice
	small_cast_iron_bombard
	pedrero
	large_cast_iron_bombard
	culverin
	chambered_demi_cannon
	leather_cannon
	swivel_cannon
	coehorn_mortar
	royal_mortar
	flying_battery
}

monarch_names = {
	"Bernhard #1" = 50
	"Christoph #0" = 40
	"Jakob #1" = 30
	"Philipp #1" = 30
	"August Georg #0" = 20
	"Eduard Fortunat #0" = 20
	"Ernst Friedrich #0" = 20
	"Georg Friedrich #0" = 20
	"Karl #0" = 20
	"Karl Friedrich #0" = 20
	"Ludwig Georg #0" = 20
	"Ludwig Wilhelm #0" = 20
	"Philibert #0" = 20
	"Wilhelm #0" = 20
	"Ferdinand Maximilian #0" = 10
	"Hermann Fortunat #0" = 10
	"Karl Joseph #0" = 10
	"Karl Ludwig #0" = 10
	"Leopold Wilhelm #0" = 10
	"Wilhelm Georg #0" = 10
	"Rudolf #4" = 5
	"Albrecht #0" = 5
	"Albrecht Karl #0" = 5
	"Christoph Gustav #0" = 5
	"Ernst #0" = 5
	"Georg #0" = 5
	"Johann #0" = 5
	"Johann Karl #0" = 5
	"Markus #0" = 5
	"Philipp Siegmund #0" = 5
	"Wilhelm Ludwig #0" = 5
	"Wolfgang #0" = 5
	"Erich #0" = 0
	"Gregor #0" = 0
	"Isaak #0" = 0
	"Lothar #0" = 0
	"Oskar #0" = 0
	"Wolfram #0" = 0
	"Yvo #0" = 0
}

leader_names = { 
	Albers B�chtold Baehr Beer Benthens Breh Eisenkolb Erb
	Ferstenfeld Fischer G�hringer Grether Heitzmann Helff 
	Kempf Klee Mackensen R�mermann SCh��ler Steinmann 
	Tientjen Tuve Wachsmuth
}

ship_names = {
	"Markgraf Hermann" "Markgraf Bernard I" "Markgraf Karl I"
	Linzgau L�rrach Freiburg Karlsruhe Mannheim Tauber Rhein
	Bruchsal Bretten Constance Eberbach Feldberg Heidelberg
	Kinzig Lahr Offenburg Rastatt Villingen
}

army_names = {
	"Markgr�fische Garde" "Armee von $PROVINCE$" 
}

