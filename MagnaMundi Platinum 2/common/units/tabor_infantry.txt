# Tabor Infantry

type = infantry
unit_type = eastern_europe
unit_type = eastern

maneuver = 1
offensive_morale = 0
defensive_morale = 4
offensive_fire = 1
defensive_fire = 2
offensive_shock = 2
defensive_shock = 3
