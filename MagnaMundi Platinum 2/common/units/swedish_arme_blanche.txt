# Arme-Blanche Tactics

type = cavalry
unit_type = western_europe
unit_type = latin

maneuver = 2
offensive_morale = 8
defensive_morale = 6
offensive_fire = 0
defensive_fire = 1
offensive_shock = 8
defensive_shock = 6
