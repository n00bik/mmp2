# Charge Infantry

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 1
offensive_morale = 2
defensive_morale = 0
offensive_fire = 0
defensive_fire = 1
offensive_shock = 2
defensive_shock = 0
