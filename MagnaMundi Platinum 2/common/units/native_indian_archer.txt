# Native Indian Archer

unit_type = america
unit_type = new_world
type = infantry

maneuver = 1
offensive_morale = 1
defensive_morale = 0
offensive_fire = 1
defensive_fire = 0
offensive_shock = 1
defensive_shock = 0
