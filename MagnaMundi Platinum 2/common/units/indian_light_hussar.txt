# Light Hussar

type = cavalry
unit_type = india
unit_type = indian

maneuver = 3
offensive_morale = 14
defensive_morale = 10
offensive_fire = 5
defensive_fire = 7
offensive_shock = 10
defensive_shock = 7
