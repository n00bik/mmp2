# Ottoman Nizam-i Cedid

type = infantry
unit_type = near_east
unit_type = muslim

maneuver = 1
offensive_shock = 4
defensive_shock = 5
offensive_fire = 10
defensive_fire = 9
offensive_morale = 9
defensive_morale = 10
