# Impulse Infantry

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 3
offensive_morale = 10
defensive_morale = 10
offensive_fire = 12
defensive_fire = 12
offensive_shock = 7
defensive_shock = 6
