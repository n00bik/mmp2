# African Western Franchise Warfare
# Tech level 18
# African Western Franchise Infantry
# African warriors fully adapted to western warfare

unit_type = africa
unit_type = african
type = infantry

maneuver = 1
offensive_morale = 2
defensive_morale = 2
offensive_fire = 3
defensive_fire = 3
offensive_shock = 2
defensive_shock = 1
