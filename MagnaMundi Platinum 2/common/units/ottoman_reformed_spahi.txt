# Ottoman Reformed Spahi

type = cavalry
unit_type = near_east
unit_type = muslim

maneuver = 2
offensive_shock = 6
defensive_shock = 4
offensive_fire = 2
defensive_fire = 4
offensive_morale = 5
defensive_morale = 6
