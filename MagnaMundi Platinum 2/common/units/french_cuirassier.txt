# Cuirassier

type = cavalry
unit_type = western_europe
unit_type = latin

maneuver = 2
offensive_morale = 3
defensive_morale = 6
offensive_fire = 2
defensive_fire = 3
offensive_shock = 6
defensive_shock = 6
