# Medium Hussar

type = cavalry
unit_type = india
unit_type = indian

maneuver = 2
offensive_morale = 7
defensive_morale = 6
offensive_fire = 2
defensive_fire = 4
offensive_shock = 6
defensive_shock = 4
