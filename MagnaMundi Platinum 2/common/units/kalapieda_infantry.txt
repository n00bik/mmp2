# Kala-Pieda Infantry

type = infantry
unit_type = india
unit_type = indian

maneuver = 1
offensive_shock = 3
defensive_shock = 2
offensive_fire = 5
defensive_fire = 4
offensive_morale = 3
defensive_morale = 4
