# Chasseurs de Cheval (Open Order Cavalry)

unit_type = india
unit_type = indian
type = cavalry

maneuver = 3
offensive_morale = 12
defensive_morale = 10
offensive_fire = 3
defensive_fire = 6
offensive_shock = 4
defensive_shock = 2
