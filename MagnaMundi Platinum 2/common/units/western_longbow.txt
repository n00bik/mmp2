# Western Longbow

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 1
offensive_morale = 1
defensive_morale = 0
offensive_fire = 1
defensive_fire = 1
offensive_shock = 1
defensive_shock = 0
