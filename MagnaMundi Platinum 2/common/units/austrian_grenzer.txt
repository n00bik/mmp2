# Irregular Skirmisher

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 3
offensive_morale = 6
defensive_morale = 3
offensive_fire = 4
defensive_fire = 4
offensive_shock = 5
defensive_shock = 3
