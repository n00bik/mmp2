# Western Medieval Infantry

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 1
offensive_morale = 1
defensive_morale = 1
offensive_fire = 0
defensive_fire = 1
offensive_shock = 0
defensive_shock = 0
