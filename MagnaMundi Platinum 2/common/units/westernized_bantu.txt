# Westernized Bantu Tactics
# Tech level 16
# Westernised South African Infantry
# South African warriors influenced by Western military offence tactics and strategies

unit_type = africa
unit_type = african
type = infantry

maneuver = 1
offensive_morale = 3
defensive_morale = 1
offensive_fire = 3
defensive_fire = 1
offensive_shock = 2
defensive_shock = 1
