# Offensive Rifled Musketeer

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 3
offensive_morale = 9
defensive_morale = 5
offensive_fire = 10
defensive_fire = 7
offensive_shock = 4
defensive_shock = 3
