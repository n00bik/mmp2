# Offensive Mass Infantry

type = infantry
unit_type = western_europe
unit_type = latin

maneuver = 1
offensive_morale = 8
defensive_morale = 5
offensive_fire = 9
defensive_fire = 4
offensive_shock = 7
defensive_shock = 5
