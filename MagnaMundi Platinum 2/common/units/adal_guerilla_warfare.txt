# Adal Guerrilla Warfare
# Tech level 10
# East African Guerrillas
# Smaller groups of East African hill warriors employing guerrilla tactics to harass their enemy.

unit_type = africa
unit_type = african
type = infantry

maneuver = 1
offensive_morale = 1
defensive_morale = 2
offensive_fire = 0
defensive_fire = 1
offensive_shock = 1
defensive_shock = 2
