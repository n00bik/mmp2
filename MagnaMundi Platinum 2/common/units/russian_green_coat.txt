# Russian Green Coat

type = infantry
unit_type = eastern_europe
unit_type = eastern

maneuver = 2
offensive_morale = 7
defensive_morale = 6
offensive_fire = 7
defensive_fire = 5
offensive_shock = 5
defensive_shock = 4
