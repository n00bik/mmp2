# Napoleonic Lancers

unit_type = western_europe
unit_type = latin
type = cavalry

maneuver = 2
offensive_morale = 14
defensive_morale = 14
offensive_fire = 3
defensive_fire = 6
offensive_shock = 12
defensive_shock = 10
