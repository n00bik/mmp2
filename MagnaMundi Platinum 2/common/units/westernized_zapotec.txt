# Westernized Zapotec Tactics

unit_type = america
unit_type = new_world
type = infantry

maneuver = 1
offensive_morale = 1
defensive_morale = 1
offensive_fire = 2
defensive_fire = 4
offensive_shock = 3
defensive_shock = 2
