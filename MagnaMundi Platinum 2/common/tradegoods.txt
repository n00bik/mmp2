# Do not change tags in here without changing every other reference to them.
# If adding new resources, make sure they are uniquely named.
# Please add price definitions to prices.txt as well.



########################################################################
#Basic resources (those essencial for a society to exist)
#
#grain, rice, maize, wool, millet, fish
#
#
#
########################################################################


########################################################################
#Common resources (those necessary for a society to achieve a moderate level of well being)
#
#livestock, beer, leather, wax, copper, iron, hemp
#
#
#
##########################################################################

########################################################################
#Advanced resources (those that add something special to the society without being luxuries)
#
#olive, cloth, salt, tea, glassware, palm, fur, opium, slaves, wine
#
#
#
########################################################################

########################################################################
#Luxuries 
#
#ebony(*), indigo, carmine, clove, silk, spices, cinnamon, ivory, chinaware, sandal, brazil, nutmeg
#
#
#
########################################################################


########################################################################
#Colonial resources (directly tied to colonization) 
#
# 
#coffee, tobacco, cotton, sugar, cacao
#
#
########################################################################


########################################################################
#Specialized resources (applied to very particular areas) 
#
# 
#lead, naval supplies
#
#
########################################################################



grain = {
	color = { 0.7 0.7 0 }
}

rice = {

	color = { 0.7 0.7 0 }
}

wine = {
	color = { 0.7 0.0 0 }
}

livestock = {

	color = { 0.0 1.0 0 }
}

olive = {

	color = { 0.1 0.1 0 }
}

cloth = {

	color = { 0.7 1.0 1.0 }
}

silver = {

	color = { 0.0 0.0 0.4 }
}

lead = {
	color = { 1.0 0.4 0.4 }
}

naval_supplies = {
	color = { 1.0 0.4 0.4 }
}

ebony = {
	color = { 0.7 0.0 0 }
}

leather = {
	color = { 1.0 1.0 1 }
}

salt = {
	color = { 1.0 0.0 1 }
}

indigo = {
	color = { 0.0 1.0 0 }
}

carmine = {
	color = { 0.0 1.0 0 }
}

slaves = {
	color = { 0.0 0.0 0.0 }
}

coffee = {
	color = { 1.0 0.0 1.0 }
}

clove = {
	color = { 1.0 1.0 1.0 }
}

tobacco = {
	color = { 0.0 0.7 0.5 }
}



maize = {
	color = { 0.7 0.7 0 }
}



# common, but not necessary
wax = {
	color = { 0.7 0.0 0 }
}

# textiles raw product  (same trajectory as basic need, but more span)
cotton = {
	color = { 1.0 0.0 0.0 }
}

# textiles raw product (same trajectory as basic need, but less span)
wool = {
	color = { 0.7 0.4 0 }
}

# luxury item
silk = {
	color = { 0.7 1.0 1.0 }
}

gold = {
	color = { 0.0 0.0 0.4 }
}

gems = {
	color = { 0.0 0.0 0.4 }
}

# war demand
copper = {
	color = { 0.0 0.4 0.4 }
}

# luxury item
spices = {
	color = { 1.0 1.0 1.0 }
}

# luxury item
cinnamon = {
	color = { 1.0 1.0 1.0 }
}

# 18th-century household item, virtually unavailable earlier
tea = {
	color = { 1.0 1.0 1.0 }
}

# luxury item
ivory = {
	color = { 1.0 1.0 1.0 }
}

# luxury item
chinaware = {
	color = { 1.0 1.0 1.0 }
}

# luxury item
glassware = {
	color = { 1.0 1.0 1.0 }
}

unknown = {
	color = { 0.1 0.1 0.1 }
}

# basic need - demand grows with the wealth of a country, and during war
millet = {
	color = { 0.7 0.7 0 }
}

# common, but not necessary
beer = {
	color = { 0.7 0.0 0 }
}

# basic need - demand grows with the wealth of a country, and during war
fish = {
	color = { 0.0 1.0 0 }
}

# moderate luxury (later take-off for increasing wealth)
palm = {
	color = { 0.0 1.0 0 }
}

# common, but not necessary
hemp = {
	color = { 0.7 1.0 1.0 }
}

# common, but not necessary
iron = {
	color = { 0.0 1.0 0.0 }
}

# luxury item
sandal = {
	color = { 0.7 0.0 0 }
}

# luxury item/dye
brazil = {
	color = { 0.7 0.0 0 }
}


# common, but not necessary
fur = {
	color = { 1.0 1.0 1 }
}

# 18th-century household item, virtually unavailable earlier
sugar = {
	color = { 0.1 0.7 0.5 }
}

# 18th-century household item, virtually unavailable earlier
cacao = {
	color = { 1.0 1.0 1.0 }
}

# luxury item
nutmeg = {
	color = { 1.0 1.0 1.0 }
}

opium = {
	color = { 0.0 0.7 0.5 }
}
