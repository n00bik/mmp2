naval_ideas = {
	sea_hawks = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			OR = {
				NOT = { ai = yes }
				has_country_flag = sea_hawks_enabled
			}
		}
		naval_morale = 0.30
		blockade_efficiency = 0.40
		galley_cost = -0.25
		leader_naval_manuever = 0.5
		range = 0.25
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	superior_seamanship = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			OR = {
				NOT = { ai = yes }
				has_country_flag = superior_seamanship_enabled
			}

		}
		leader_naval_manuever = 1.0 
		naval_morale = 0.60
		range = 0.25
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	excellent_shipwrights = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			OR = {
				NOT = { ai = yes }
				has_country_flag = excellent_shipwrights_enabled
			}

		}
		naval_forcelimit_modifier = 0.25
		naval_attrition = -0.05  
		naval_tech_cost_modifier = -0.10
		range = 0.50
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	naval_fighting_instruction = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			OR = {
				NOT = { ai = yes }
				has_country_flag = naval_fighting_instruction_enabled
			}

		}
		naval_morale = 1.0
		naval_tech_cost_modifier = -0.05
		range = 0.25
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	naval_provisioning = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			OR = {
				NOT = { ai = yes }
				has_country_flag = naval_provisioning_enabled
			}

		}
		naval_attrition = -0.20 
		range = 1.00
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	grand_navy = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			OR = {
				NOT = { ai = yes }
				has_country_flag = grand_navy_enabled
			}
		}
		naval_forcelimit_modifier = 0.50
		prestige_from_naval = 0.25
		range = 0.25
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	press_gangs = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			has_country_flag = idea_grand_navy
			naval_tech = 30
		}
		global_ship_cost = -0.25
		naval_forcelimit_modifier = 0.25
		range = 0.25
		global_trade_income_modifier = 0.05
		global_tariffs = 0.05
	}
	naval_glory = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			num_of_ports = 1
			has_country_flag = high_naval
			OR = {  has_country_flag = medium_power
				has_country_flag = major_power
				has_country_flag = great_power
			}
		}
		naval_forcelimit_modifier = 0.35
		naval_morale = 0.5
		prestige = 0.005	
		navy_tradition = 0.02
		naval_tech_cost_modifier = -0.10
		bigship_cost = -0.05
	}
}
land_ideas = {
	grand_army = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = grand_army_enabled
			}

		}
		land_forcelimit_modifier = 0.5
		land_tech_cost_modifier = -0.03  
		global_manpower_modifier = 0.10  
		infantry_cost = -0.10
		war_exhaustion = -0.01
		max_war_exhaustion = -1
	}
	military_drill = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }

			OR = {
				NOT = { ai = yes }
				has_country_flag = military_drill_enabled
			}
		}
		global_manpower_modifier = 0.10 
		land_morale = 0.80
		discipline = 0.05
		war_exhaustion = -0.01
		max_war_exhaustion = -1
	}
	engineer_corps = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = engineer_corps_enabled
			}
		}
		leader_siege = 1
		leader_fire = 1
		land_morale = 0.45
		build_cost = -0.05 
		war_exhaustion = -0.01
		max_war_exhaustion = -1
	}
	battlefield_commisions = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = battlefield_commisions_enabled
			}
		}
		land_morale = 0.50
		prestige_from_land = 0.05
		leader_shock = 1
		war_exhaustion = -0.01
		max_war_exhaustion = -1
	}
	national_conscripts = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = national_conscripts_enabled
			}
		}
		global_manpower_modifier = 0.35
		land_forcelimit_modifier = 0.25
		war_exhaustion = -0.01
		max_war_exhaustion = -1
	}
	regimental_system = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = regimental_system_enabled
			}
		}
		global_regiment_recruit_speed = -0.25
		land_forcelimit_modifier = 0.25
		war_exhaustion = -0.01
		max_war_exhaustion = -1
		reinforce_speed = 0.20
	}
	napoleonic_warfare = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			has_country_flag = idea_regimental_system
			land_tech = 30
		}
		land_morale = 0.20
		prestige_from_land = 0.05
		discipline = 0.20
		war_exhaustion = -0.01
		max_war_exhaustion = -1
	}
	glorious_arms = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			has_country_flag = high_land
			OR = {  has_country_flag = medium_power
				has_country_flag = major_power
				has_country_flag = great_power
			}
		}
		prestige = 0.005	
		army_tradition = 0.03
		war_exhaustion = -0.01
		global_manpower_modifier = 0.25
		land_forcelimit_modifier = 0.25
		land_tech_cost_modifier = -0.05
		max_war_exhaustion = -1
	}
}
exploration_ideas = {
	merchant_adventures = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = merchant_adventures_enabled
			}
		}
		merchants = 0.5		
		merchant_compete_chance = 0.05
		merchant_placement_chance = 0.05
	}
	colonial_ventures = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = colonial_ventures_enabled
			}
		}
		colonists = 0.5		
		colonist_cost = -0.15
		global_tariffs = 0.05
	}
	shrewd_commerce_practise = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = shrewd_commerce_practise_enabled
			}
		}
		merchant_compete_chance = 0.05 
		merchant_placement_chance = 0.05 
		trade_efficiency = 0.05
  	}
	vice_roys  = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = vice_roys_enabled
			}
		}
		global_tariffs = 0.15
 		colonist_cost = -0.05
		colonist_placement_chance = 0.05
	}
	smithian_economics = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = smithian_economics_enabled
			}
		}
		production_efficiency = 0.05
		trade_efficiency = 0.05
		global_trade_income_modifier = 0.05
	}
	improved_foraging = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = improved_foraging_enabled
			}
		}
		land_attrition = -0.25
		colonist_placement_chance = 0.05
	}
	land_of_opportunity = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			has_country_flag = idea_improved_foraging
			trade_tech = 30
		}
		global_colonial_growth = 0.5	# +20 people per year in colonies.
		colonist_cost = -0.10
		global_tariffs = 0.05
	}
	quest_for_the_new_world	= {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			has_country_flag = high_exploration
			OR = {  has_country_flag = medium_power
				has_country_flag = major_power
				has_country_flag = great_power
			}
		}
		may_explore = yes	
		colonist_placement_chance = 0.10
		range = 0.25
		colonists = 0.5
	}
}
state_business_ideas = {
	vetting = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = vetting_enabled
			}
		}
		global_spy_defence = +0.10
		spies_cost = -0.15
		spies = +0.25
		global_revolt_risk = -2.5
	}
	bureaucracy = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = bureaucracy_enabled
			}

		}
		government_tech_cost_modifier = -0.025
		advisor_cost = -0.20
		global_spy_defence = +0.05
		spy_efficiency = +0.05
		inflation_reduction = 0.02
	}
	espionage  = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = espionage_enabled
			}
		}		
		spies = 0.75
		spy_efficiency = +0.10
		spies_cost = -0.35
	}
	cabinet = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = cabinet_enabled
			}
		}
		prestige = 0.005
		badboy = -0.10
		spy_efficiency = +0.05
	}
	bill_of_rights = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = bill_of_rights_enabled
			}
		}
		global_revolt_risk = -2.5
		stability_investment = 15
		stability_cost_modifier = -0.05
	}
	national_trade_policy = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = national_trade_policy_enabled
			}
		}
		trade_efficiency = 0.10
		merchant_cost = -0.25
		merchants = 0.5
	}
	national_bank = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			has_country_flag = idea_national_trade_policy
			production_tech = 30
		}
		inflation_reduction = 0.05	
		interest = -0.05
		global_tax_modifier = 0.15
	}
	liberty_egalite_fraternity = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			NOT = { has_country_flag = idea_deus_vult }
			NOT = { has_country_flag = idea_church_attendance_duty }
			NOT = { has_country_flag = idea_divine_supremacy }
			has_country_flag = high_state
			OR = {
				has_country_flag = medium_power
				has_country_flag = major_power
				has_country_flag = great_power
			}
		}
		tolerance_heretic = 1
		tolerance_heathen=1
		stability_investment = 15
		global_tax_modifier = +0.05
		production_efficiency = +0.05
	}
}
culture_ideas = {
	deus_vult = {
		trigger = {
			has_country_flag = event_only_DV
			NOT = { has_country_modifier = ni_changed }
			NOT = { has_country_flag = idea_ecumenism }
		}
		cb_on_religious_enemies = yes
		war_exhaustion = -0.01
		missionaries = 0.5
		missionary_cost = -0.2
		prestige = 0.005
	}
	revolution_and_counter = {
		trigger = {
			has_country_flag = event_only_sorry
			NOT = { has_country_modifier = ni_changed }
		}
		cb_on_government_enemies = yes
		war_exhaustion = -0.03
		land_morale = 0.40 
		naval_morale = 0.40
	}
	church_attendance_duty = { 
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			NOT = { has_country_flag = idea_liberty_egalite_fraternity }
			OR = {
				NOT = { ai = yes }
				has_country_flag = church_attendance_duty_enabled
			}
		}
		stability_investment = 10
		stability_cost_modifier = -0.10
		missionary_cost = -0.05
		missionary_placement_chance = 0.01
	}
	divine_supremacy = { 
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			NOT = { has_country_flag = idea_liberty_egalite_fraternity }
			OR = {
				NOT = { ai = yes }
				has_country_flag = divine_supremacy_enabled
			}
		}
		missionaries = 0.5	
		missionary_placement_chance = 0.03
		prestige = 0.005
		stability_cost_modifier = -0.05
		missionary_cost = -0.10
	}
	patron_of_art  = { 
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = patron_of_art_enabled
			}
		}
		prestige = 0.025		
		global_revolt_risk = -1
	}
	humanist_tolerance = { 
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			OR = {
				NOT = { ai = yes }
				has_country_flag = humanist_tolerance_enabled
			}
		}
		tolerance_heathen = 2
		stability_investment = 10
		stability_cost_modifier = -0.05
		global_revolt_risk = -1
	}
	ecumenism = {
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			NOT = { has_country_flag = idea_deus_vult }
			has_country_flag = idea_humanist_tolerance
			government_tech = 30
		}
		global_revolt_risk = -2
		tolerance_heretic = 2
		stability_investment = 15
		stability_cost_modifier = -0.15
	}
	scientific_revolution = { 
		trigger = {
			NOT = { has_country_modifier = ni_changed }
			has_country_flag = high_culture
			OR = {  has_country_flag = medium_power
				has_country_flag = major_power
				has_country_flag = great_power
			}
		}
		technology_cost = -0.2
	}
}
