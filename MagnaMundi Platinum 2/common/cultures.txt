 	# Do not change tags in here without changing every other reference to them.
# If adding new groups or cultures, make sure they are unique.

############
# EUROPEAN #
############

baltic = {
	latvian = { primary = KUR } 
	lithuanian = { primary = LIT } 
	baltic_colonial = { }
	colonial_baltic = { }  #Schuylkillie
}
	
basque = {
	basque = { primary = NAV } 
	basque_colonial = { }
	colonial_basque_na = { }  #Amerikanuak
	colonial_basque = { }  #Bestaldekoa
}
	
british = {
	english = { primary = ENG } 
	scottish = { primary = SCO } 
	british_colonial = { }
	colonial_scottish = { }  #Albannaich
	american = { primary = USA }  #American
	colonial_british_oc = { }  #Australian
	colonial_british = { }  #British Colonial
	colonial_british_sa = { }  #Guyanese
	colonial_british_af = { }  #Rhodesian
}

byzantine = {
	armenian = { primary = ARM } 
	georgian = { primary = GEO } 
	greek = { primary = GRE } 
	byzantine_colonial = { }
	colonial_byzantine = { }  #Hellene
}

finno_ugric = {
	estonian = { primary = RIG } 
	finnish = { primary = FIN } 
	ingrian = { } 
	uralic = { }
	sapmi = { } 
	finnish_colonial = { }
	colonial_finn = { }  #Ulkosuomalaiset
}
	
french = {
	aquitaine = { primary = TOU } 
	burgundian = { primary = BUR } 
	cosmopolitan_french = { primary = FRA } 
	gascon = { primary = AMG } 
	normand = { } 
	occitain = { primary = PRO } 
	wallonian = { primary = LUX } 
	french_colonial = { }
	colonial_french_gp = { }  #Cajun
	colonial_french_na = { }  #Canadien
	colonial_french_sa = { }  #Creole
	colonial_french = { }  #Outremer
	colonial_french_af = { }  #Pied-Noir
}
	
gaelic = {
	breton = { primary = BRI } 
	irish = { primary = IRE } 
	welsh = { } 
	gaelic_colonial = { }
	colonial_irish = { }  #Gael-Mheiriceánach 
	colonial_gaelic = { }  #Hibernian
	}
	
germanic = {
	austrian = { primary = HAB }
	bavarian = { primary = BAV }
	dutch = { primary = NED }
	franconian = { primary = BAV }
	hannoverian = { primary = HAN } # "Lower Saxon"
	hessian = { primary = HES }
	pommeranian =  { primary = POM } # "East Elbian"
	rheinlaender = { primary = WES } # "Rhenanian"
	saxon = { primary = SAX }
	swabian = { primary = WUR }
	westphalian = { primary = HES }	# not in use!
	prussian = { primary = PRU }
	germanic_colonial = { }
	colonial_german_af = { }  #Afrikanisch
	colonial_german_as = { }  #Chinadeutsch
	colonial_german_na = { }  #Nordamerikanisch
	colonial_german_si = { }  #Sibirisch
	colonial_german_sa = { }  #Sudamerikanisch
	colonial_german_fe = { }  #Sudseeddeutsch
	dutch_colonial = { }
	colonial_dutch_af = { }  #Afrikaaner
	colonial_dutch_as = { }  #Batavian
	colonial_dutch_sa = { }  #Boero
	colonial_dutch_na = { }  #Patroon
	colonial_dutch = { }  #Trekboer
}

iberian = {
	andalucian = { primary = GRA } 
	castillian = { primary = CAS } 
	catalan = { primary = ARA } 
	galician = { } 
	portugese = { primary = POR } 
	spanish_colonial = { }
	colonial_spanish_na = { }  #Criollo
	colonial_spanish = { }  #Hispanic
	colonial_spanish_as = { }  #Filipino
	portuguese_colonial = { }
	colonial_portuguese_af = { }  #Angolan
	colonial_portuguese_sa = { }  #Brasileira
	colonial_portuguese = { }  #Crioula
	colonial_portuguese_na = { }  #Lusa
	colonial_portuguese_as = { }  #Kristang
	catalonian_colonial = { }
	colonial_catalonian = { }  #Alou
	colonial_catalonian_na = { }  #Menorquin
	colonial_catalonian_sa = { }  #Minyon
	colonial_catalonian_af = { }  #Patuet
}
	
latin = {
	neapolitan = { primary = NAP } 
	ligurian = { primary = GEN } 
	lombard = { primary = MLO } 
	sardinian = { primary = SAR } 
	sicilian = { primary = SIC } 
	tuscan = { primary = TUS } 
	umbrian = { primary = URB } 
	venetian = { primary = VEN } 
	italian_colonial = { }
	colonial_latin = { } #Latin (Carcamano is derogatory)
	}
	
ruthenian = {
	belarussian = { }  #Belarusian/White Ruthenian
	russian = { primary = RUS } 
	ukrainian = { } 
	ruthenian_colonial = { }
	colonial_ruthenian = { }  #Emigranty
	colonial_ruthenian_na = { }  #Kreoly
	colonial_ruthenian_si = { }  #Sibiryak
}
	
scandinavian = {
	danish = { primary = DAN } 
	icelandic = { primary = NOR } 
	norwegian = { primary = NOR } 
	swedish = { primary = SWE } 
	scandinavian_colonial = { }
	colonial_scandinavian_na = { }  #Norskamerikaner
	colonial_scandinavian = { }  #Nordic
}

south_slavic = { #Balkan
	albanian = { primary = ALB } 
	bulgarian = { primary = BUL } 
	croatian = { primary = CRO } 
	romanian = { }
	serbian = { primary = SER } 
	slovenian = { } 
	south_slavic_colonial = { }
	colonial_south_slav = { }  #Prekomorjani
}

ugric = { 
	hungarian = { primary = HUN } #Magyar
	ugric_colonial = { }
	colonial_ugric = { }
	colonial_ugric_na = { }
	colonial_ugric_oc = { }
}
	
west_slavic = {	
	czech = { primary = BOH } 
	slovak = { } 
	polish = { primary = POL } 
	schlesian = { } 
	west_slavic_colonial = { }
	colonial_polish = { }  #Polonian
	colonial_west_slav = { }  #Wend
}
	
##########################
# MIDDLE EASTERN (ASIAN) #
##########################
	
semitic = {
	al_iraqiya_arabic = {  } 
	al_misr_arabic = {  } 
	al_suryah_arabic = {  } 
	bedouin_arabic = { primary = ARB } 
	berber = { primary = ALG } 
	maltese = { } 
	maghreb_arabic = { primary = MOR } 
	semitic_colonial = { }
	colonial_arab_as = { }  #Chaush
	colonial_arab_fe = { }  #Hadrami
	colonial_arab = { }  #Istimari
}
	
persian = {
	baluchi = { primary = BAL }
	kurdish = { }
	pashtun = { }
	persian = { primary = PER }
	tajihk = { }
	persian_colonial = { }
	colonial_persian = { } #Ajam
}

turkic = {
	azerbadjani = { } #Azeri
	astrakhani = { primary = AST } #Astrakhan Tatar
	chagatai = { primary = CHG }
	circassian = { } #used for the Mamelukes, actually related to Georgians
	khazak = { primary = KZH }
	nogai = { primary = NOG }
	sibir = { primary = SIB } #Siberian Tatar
	turkish = { primary = TUR }
	turkmeni = { primary = KHO } 
	uyghur = { }
	uzbehk = { primary = SHY }
	tartar = { primary = GOL } #Volga Tatar, several competing countries
	crimean = { primary = CRI } #Crimea Tatar
	turkic_colonial = { }
	colonial_turkic = { } #Gocmen
}

crusaders = {
	first_crusade_knight = { } #Outremer
	fourth_crusade_knight = { } #Cruzado, for lack of a better term
	hospitaller_knight = { } #Hospitaller
	}

###################
# INDIAN (ASIAN ) #
###################

eastern_aryan = {
	assamese = { primary = ASS }
	bengali = { } #Several competing countries
	bihari = { primary = AHM }
	nepali = { primary = NPL }
	oriya = { primary = ORI }
	sinhala = { primary = KTH }
	eastern_aryan_colonial = { }
	colonial_eastern_aryan = { } #
}

hindusthani = {
	avadhi = { primary = GDW }
	kanauji = { }
	panjabi = { primary = PUN }
	kashmiri = { primary = KSH }
	hindi_colonial = { }
	colonial_hindusthani_sa = { } #Hindoestanen
	colonial_hindusthani = { } #Desi
}

western_aryan = {
	gujarati = { primary = GUJ }
	marathi = { primary = MAR }
	sindhi = { primary = SND }
	rajput = { primary = RAJ }
	western_aryan_colonial = { }
	colonial_western_aryan = { } #Muhajir (Urdu-speakers who left India for Pakistan and the west)
}

dravidian = {
	kannada = { }
	malayalam = { primary = TRV }
	tamil = { primary = MAD }
	telegu = { primary = KRK }
	dravidian_colonial = { }
	colonial_dravidian_fe = { } #Chitty
	colonial_dravidian = { } #Jaffnese
	colonial_dravidian_af = { } #Malabar
}

##############
# EAST ASIAN #
##############

altaic = {
	jurchen = { primary = MCH }
	mongol = { primary = KHA }
	tangut = { primary = XIA }
	yakut = { }
	altaic_colonial = { }
	colonial_altaic = { } #
}

chinese = {
	chu = { primary = CHU }
	gan = { primary = WUU }
	jilu = { primary = QII }
	jin = { primary = JIN }
	miao = { }
	min = { primary = MIN }
	shu = { primary = SHU }
	wu = { primary = WYU } 
	yue = { primary = NHN }
	zhongyuan = { primary = QIN }
	chinese_colonial = { }
	colonial_chinese = { } #Huayi
	colonial_chinese_fe = { } #Peranakan
}

japanese = {
	azuma = { primary = JTG }
	kyushu = { primary = JTG }
	ryukyuan = { primary = RYU }
	yamato = { primary = JTG }
	japanese_colonial = { }
	colonial_japanese = { }
}

choson = {
	korean = { primary = KOR }
	korean_colonial = { }
	colonial_korean_na = { } #Miguk
	colonial_korean = { } #Han-In
}

###################
# SOUTHEAST ASIAN #
###################

mon_khmer = { 
	khmer = { primary = KHM }
	mon = { primary = PEG }
	vietnamese = { } #Several competing countries
	mon_colonial = { }
	colonial_mon = { } #Viet Kieu
}

malay = { 
	cham = { primary = CHA }
	filipino = { primary = SUL }
	formosan = { }
	javanese = { } #Several competing countries
	madagascan = { }
	malayan = { } #Several competing countries
	polynesian = { } 
	sulawesi = { } #Several competing countries
	malaysian_colonial = { }
	colonial_malay_af = { } #Cape Malay
	colonial_malay = { } #Colonial Malay
}

thai = { 
	bai = { primary = DAL }
	central_thai = { primary = AYU }
	lao = { } #Several competing countries
	northern_thai = { } #Several competing countries
	shan = { primary = SST }
	zhuang = { }
	thai_colonial = { }
	colonial_thai = { } #Colonial Thai
}

burman = { 
	burmese = { primary = TAU }
	chin = { }
	tibetan = { primary = TIB }
	burmese_colonial = { }
	colonial_burman = { } #Colonial Burman
}

############
# FAR EAST #
############

pacific = {
	ainu = { }
	aboriginal = { } 
	melanesian = { }
	moluccan = { }
	papuan = { }
}

###########
# AFRICAN #
###########

african = {
	aka = { primary = ASH }
	bambara = { primary = SEG }
	bantu = { }
	bilala = { primary = YAO }
	dyola = { }
	edo = { primary = BEN }
	ethiopian = { primary = ETH }
	hausa = { primary = HAU }
	kongolese = { primary = KON }
	madagasque = { }
	mali = { } #Several competing states
	mossi = { primary = MSI }
	kanuri = { primary = KBO }
	khoisan = { }
	nubian = { primary = NUB }
	tuareg = { primary = TRP }
	senegambian = { primary = JOL }
	shona = { primary = ZIM }
	somali = { primary = ADA }
	songhai = { primary = SON }
	swahili = { primary = ZAN }
	yorumba = { primary = OYO }
}

#############
# NEW WORLD #
#############

mesoamerican = {
	coahuiltec = { }
	mayan = { primary = MAY }
	mexica = { primary = AZT }
	nahua = { } #Several competing states
	piman = { }
	tepanec = { }
	zacatec = { }
}
	
south_american = {
	amazonian = { }
	aymara = { primary = AYM }
	cara = { primary = QTO }
	guajiro = { }
	guarani = { }
	quechua = { primary = CUZ }
	mataco = { }
	moche = { primary = CHM }
	nazca = { primary = ICA }
	patagonian = { }
	teremembe = { }
	tupinamba = { }
	south_american_mixed = { }
	mestizo = { } #mixed
}

caribbean = {
	arawak = { }
	carib = { }
	calusa = { }
	timucua = { }
}

north_american = {
	abenaki = { }
	aleut = { }
	algonquin = { }
	apache = { }
	assiniboine = { }
	atakapa = { }
	beothuk = { }
	caddo = { }
	catawba = { }
	cherokee = { }
	cheyenne = { }
	chocktaw = { }
	comanche = { }
	cree_montagnais = { }
	delaware = { }
	illinois = { }
	inuit = { } 
	iroquois = { }
	micmac = { }
	mojave = { }
	muskogee = { }
	navajo = { }
	nez_perce = { }
	ojibwa = { }
	osage = { }
	pawnee = { }
	salish = { }
	shawnee = { }
	sioux = { }
	tlingit = { }
	wyandot = { }
	yokut = { }
	north_american_mixed = { }
	metis = { } #Mixed
}

